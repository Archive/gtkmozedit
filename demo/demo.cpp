/*
 * Demo stuff
 * License: Public Domain
 */

#include <gtk/gtk.h>
#include <gtkmozembed.h>
#include <gtkmozembed_internal.h>
#include <gtkmozedit/gme-html-view.h>
#include <gtkmozedit/gme-html-view-private.h>

#include <gtkmozedit/gtkmozedit-config.h>

#include "nsCOMPtr.h"
#include "nsIInterfaceRequestorUtils.h"
#include "nsIDOMWindow.h"
#include "nsIDOMDocument.h"
#include "nsIDOMHTMLDocument.h"
#include "nsIDOMNodeList.h"
#include "nsIDOMElement.h"
#include "nsIDOMHTMLBodyElement.h"
#include "nsIDOMEvent.h"
#include "nsIEditingSession.h"
#include "nsIEditor.h"
#include "nsIHTMLEditor.h"
#include "nsIEditorObserver.h"

class EditorObserver : public nsIEditorObserver 
{
public: 
	EditorObserver (void) 
	{
		mRefCnt = 1;
	}
	~EditorObserver () 
	{
		/* lifecycle is controlled by the wrapping gobject
		g_object_unref (G_OBJECT (observer)); 
		*/
	}
	NS_IMETHOD EditAction(void)
	{
		printf ("observe\n");
	}
	NS_IMETHODIMP QueryInterface(const nsIID &aIID, void **aResult)
	{
		#warning implement
		/*
		if (aResult == NULL) {
			return NS_ERROR_NULL_POINTER;
		}
		*aResult = NULL;
		if (aIID.Equals(kISupportsIID)) {
			*aResult = (void *) this;
		}
		if (*aResult == NULL) {
			return NS_ERROR_NO_INTERFACE;
		}
		AddRef();
		*/
		return NS_OK;
	}
	NS_IMETHODIMP_(nsrefcnt) AddRef ()  
	{
		return ++mRefCnt;
	}
	NS_IMETHODIMP_(nsrefcnt) Release ()
	{
		if (--mRefCnt == 0) {
			delete this;
			return 0;
		}
		return mRefCnt;
	}
private: 
	gint mRefCnt;
};

gint 
dom_mouse_down_cb (GtkMozEmbed *embed, 
		   gpointer dom_event)
{
	nsCOMPtr<nsIDOMEvent> event;
//	nsEmbedString type;
	nsString type;
	const char *foo = g_newa (gchar, 100);

	event = reinterpret_cast<nsIDOMEvent*>(dom_event);
	g_assert (event);

	event->GetType (type);

	printf ("'%s'\n", NS_ConvertUTF16toUTF8(type).get());
/*
	NS_ConvertUTF16toUTF8(your_string).get()

	type.AssignWithConversion (foo);
	printf ("'%s'\n", foo);
*/

/*
	printf ("type: '%x', '%x', '%x', '%c'\n", type.get (), ((const char *)type.get ()) + 1, *(((const char *)type.get ()) + 1), *(((const char *)type.get ()) + 1));
*/

/*
	for (int i = 0; i < 5; i++)
		printf ("%c", *(((const char *)type.get ()) + i));
	printf ("\n");
*/
}

static void
bold_cb (GtkButton *button, 
	    GmeHTMLView *moz)
{
	printf ("clicked\n");

	gme_html_view_do_command (moz, "cmd_bold", NULL);
}

static void
nsieditor_cb (GtkButton *button, 
	    GmeHTMLView *moz)
{
	nsCOMPtr<nsIWebBrowser> browser;
	nsCOMPtr<nsIDOMWindow> dom_window;
	nsCOMPtr<nsIDOMDocument> document;
	nsCOMPtr<nsIDOMNodeList> list;
	nsCOMPtr<nsIDOMElement> element;
	nsEmbedString name;
	nsresult rv;

	gtk_moz_embed_get_nsIWebBrowser (GTK_MOZ_EMBED (moz), getter_AddRefs (browser));
	g_return_if_fail (browser);
	rv = browser->GetContentDOMWindow (getter_AddRefs(dom_window));
	g_assert (dom_window);
	rv = dom_window->GetDocument (getter_AddRefs(document));


	nsCOMPtr<nsIEditingSession> edit_session;
	edit_session = do_GetInterface (moz->wrapped_ptr, &rv);
	g_assert (edit_session);

	nsCOMPtr<nsIEditor> editor;
	edit_session->GetEditorForWindow (dom_window, getter_AddRefs (editor));
	nsIEditor *e = editor;

	nsCOMPtr<nsIHTMLEditor> html_editor = do_QueryInterface (editor, &rv);
	nsIHTMLEditor *he = html_editor;
	printf ("editor: %x, %x\n", e, he);

	EditorObserver *observer = new EditorObserver ();
	editor->AddEditorObserver (observer);
}

static void
foo_cb (GtkButton *button, 
	    GmeHTMLView *moz)
{
	nsCOMPtr<nsIWebBrowser> browser;
	nsCOMPtr<nsIDOMWindow> dom_window;
	nsCOMPtr<nsIDOMDocument> document;
	nsCOMPtr<nsIDOMNodeList> list;
	nsCOMPtr<nsIDOMElement> element;
	nsEmbedString name;
	nsresult rv;

	printf ("foo\n");

	gtk_moz_embed_get_nsIWebBrowser (GTK_MOZ_EMBED (moz), getter_AddRefs (browser));
	g_return_if_fail (browser);
	rv = browser->GetContentDOMWindow (getter_AddRefs(dom_window));
	g_assert (dom_window);
	rv = dom_window->GetDocument (getter_AddRefs(document));

	nsCOMPtr<nsIDOMHTMLElement> bodyel;
	nsCOMPtr<nsIDOMHTMLDocument> doc(do_QueryInterface(document));
	doc->GetBody(getter_AddRefs(bodyel));
	printf("body=%p\n", bodyel.get());

	bodyel->GetTagName (name);
	printf("name=%s\n", NS_ConvertUTF16toUTF8(name).get ());

	nsCOMPtr<nsIDOMHTMLBodyElement> body(do_QueryInterface(bodyel));
	NS_NAMED_LITERAL_STRING(bgcolor, "#ff0000");
	body->SetBgColor (bgcolor);

	NS_NAMED_LITERAL_STRING(text, "hollarei");
	body->SetText (text);

/*
	document->GetDocumentElement (getter_AddRefs(element));

//	nsEmbedString b = nsEmbedString ((PRUnichar*)"body");
	NS_NAMED_LITERAL_STRING(b, "body");
	element->GetElementsByTagName (b, getter_AddRefs(list));

	PRUint32 length;
	list->GetLength (&length);
	printf ("'%s' length: %d\n", b.get (), length);

	
	document->GetElementById (b, getter_AddRefs(element));
	g_return_if_fail (element);
	element->GetTagName (name);
	printf ("'%s'\n", name.get ());
*/
/*
	element->GetTagName (name);
	printf ("'%s'\n", name.get ());
*/

//	document->GetDocumentElement (getter_AddRefs(body));

//	gme_html_view_do_command (moz, "cmd_bold", NULL);
}

static void
save_cb (GtkButton *button, 
	    GmeHTMLView *moz)
{
	gboolean ret;

	printf ("save: ");

	ret = gme_html_view_save (moz, "/home/robert/Desktop/foo.html", TRUE);

	printf ("%d\n", ret);
}

int
main (int argc, char **argv)
{
	GtkWidget *window 	= NULL;
	GtkWidget *vbox		= NULL;
	GtkWidget *moz 		= NULL;
	GtkWidget *hbox		= NULL;
	GtkWidget *button	= NULL;
	const gchar *url	= NULL;
	
	gtk_init (&argc, &argv);

	window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	g_signal_connect (G_OBJECT (window), "delete-event", G_CALLBACK (gtk_main_quit), NULL);

	vbox = gtk_vbox_new (FALSE, 0);
	gtk_container_add (GTK_CONTAINER (window), vbox);
	
	moz = gme_html_view_new ();
//	g_signal_connect (G_OBJECT (moz), "dom_mouse_down", G_CALLBACK (dom_mouse_down_cb), NULL);
//	g_signal_connect (G_OBJECT (moz), "dom_mouse_up", G_CALLBACK (dom_mouse_down_cb), NULL);
	if (argc > 1 && argv[1]) {
		printf ("will load: '%s'\n", argv[1]);
		gme_html_view_load (GME_HTML_VIEW (moz), argv[1]);
	}
	gtk_box_pack_start (GTK_BOX (vbox), moz, TRUE, TRUE, 0);

	hbox = gtk_hbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, FALSE, 0);	

	button = gtk_button_new_with_label ("bold");
	gtk_box_pack_start (GTK_BOX (hbox), button, FALSE, FALSE, 0);	
	g_signal_connect (G_OBJECT (button), "clicked", G_CALLBACK (bold_cb), (gpointer) moz);

	button = gtk_button_new_with_label ("foo");
	gtk_box_pack_start (GTK_BOX (hbox), button, FALSE, FALSE, 0);	
	g_signal_connect (G_OBJECT (button), "clicked", G_CALLBACK (foo_cb), (gpointer) moz);

	button = gtk_button_new_with_label ("nsIEditor");
	gtk_box_pack_start (GTK_BOX (hbox), button, FALSE, FALSE, 0);	
	g_signal_connect (G_OBJECT (button), "clicked", G_CALLBACK (nsieditor_cb), (gpointer) moz);

	button = gtk_button_new_with_label ("save");
	gtk_box_pack_start (GTK_BOX (hbox), button, FALSE, FALSE, 0);	
	g_signal_connect (G_OBJECT (button), "clicked", G_CALLBACK (save_cb), (gpointer) moz);

	gtk_widget_show_all (window);
	gtk_main ();
	return 0;
}
