
import os
import gtk
import bongo

if os.environ.has_key ('BONGO_PREFIX'): 
	_pixmapdir = os.environ['BONGO_PREFIX'] + '/share/pixmaps/bongo'
	_datadir = os.environ['BONGO_PREFIX'] + '/share/bongo'
else:
	_pixmapdir = os.getcwd () + '/data/pixmaps'
	_datadir = os.getcwd () + '/data'

class App: 

	pixmapdir = _pixmapdir
	datadir = _datadir

	controller = None
	mainwin = None
	filename = None

	def __init__ (self):

		self.controller = bongo.Controller (self)
		self.mainwin = bongo.ui.MainWindow (self.controller)


	def load (self, filename, forget_filename = False):

		if (filename[0] == '/'):
			load_filename = 'file://' + filename
		else:
			load_filename = 'file://' + os.getcwd () + '/' + filename
			filename = os.getcwd () + '/' + filename
			
		self.mainwin.load (load_filename)

		if (forget_filename == False):
			self.filename = filename


	def load_blank (self):

		self.load (self.datadir + '/blank.html', True)


	def save (self):

		print 'save()', self.filename
		filename = ''
		if (self.filename == None):

			chooser = gtk.FileChooserDialog ()
			chooser.set_action (gtk.FILE_CHOOSER_ACTION_SAVE)

			dlg = bongo.ui.Dialog (self.mainwin)
			dlg.wrap (chooser)
			dlg.set_default_response (gtk.RESPONSE_CANCEL)
			dlg.add_button (gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL)
			ok_button = dlg.add_button (gtk.STOCK_SAVE, gtk.RESPONSE_OK)
			ok_button.grab_default ()
			response = dlg.run ()
			if (response == gtk.RESPONSE_OK):
				filename = chooser.get_filename ()
			dlg.destroy ()
		else:
			filename = self.filename

		print 'save()', filename
		if (filename != ''): 
			self.filename = filename
			ret = self.mainwin.get_moz ().save (filename, True)
			print ret


	def run (self):

		self.mainwin.show_all ()
		gtk.main ()
