
import gtk
import gtkmozedit
import bongo.ui


ACTION_SAVE			= 'bongo-save'

ACTION_DOC_PROPS 		= 'bongo-doc-props'
ACTION_DOC_CLOSE 		= 'bongo-doc-close'

ACTION_PRINT_PRINT 		= 'bongo-print-print'
ACTION_PRINT_SETUP 		= 'bongo-print-setup'
ACTION_PRINT_PREVIEW 		= 'bongo-print-preview'	

ACTION_STYLE_DIALOG		= 'bongo-style-dialog'
ACTION_STYLE_TITLE 		= 'bongo-style-title'
ACTION_STYLE_H1			= 'bongo-style-h1'
ACTION_STYLE_H2			= 'bongo-style-h2'
ACTION_STYLE_BLOCK		= 'bongo-style-blockquote'
ACTION_STYLE_PRE 		= 'bongo-style-pre' 
ACTION_STYLE_EM			= 'bongo-style-em'
ACTION_STYLE_SAMP 		= 'bongo-style-samp'
ACTION_STYLE_STRONG 		= 'bongo-style-strong'

ACTION_LIST_DIALOG 		= 'bongo-list-dialog'
ACTION_LIST_BULLET 		= 'bongo-list-bullet'
ACTION_LIST_DASH 		= 'bongo-list-dash'
ACTION_LIST_TICK 		= 'bongo-list-tick' 
ACTION_LIST_NUMBERED 		= 'bongo-list-numbered'
ACTION_LIST_LOWER 		= 'bongo-list-lower'
ACTION_LIST_ROMAN 		= 'bongo-list-roman'

ACTION_INSERT_IMAGE 		= 'bongo-insert-image'
ACTION_INSERT_SYMBOL 		= 'bongo-insert-symbol'
ACTION_INSERT_SEPARATOR		= 'bongo-insert-separator'

ACTION_TABLE_INSERT 		= 'bongo-table-insert'
ACTION_TABLE_INSROW 		= 'bongo-table-insrow'
ACTION_TABLE_INSCOL 		= 'bongo-table-inscol'
ACTION_TABLE_DELROW 		= 'bongo-table-delrow'
ACTION_TABLE_DELCOL 		= 'bongo-table-delcol'
ACTION_TABLE_DIALOG 		= 'bongo-table-dialog'


class Controller: 

	app = None
	global_actions = None
	toolbar_actions = None
	command_manager = None
	observer = None
	dom_window = None
	html_document = None

	def __init__ (self, app): 

		self.app = app
		self.create_global_actions ()
		self.create_toolbar_actions ()


	def set_command_manager (self, command_manager, dom_window):

		self.command_manager = command_manager
		self.dom_window = dom_window
		gtype = gtkmozedit.dom.html.document_get_gtype ()
		self.html_document = dom_window.get_document ().query_interface (gtype)

		# TODO this should observe all commands, doesn't work though
		self.observer = gtkmozedit.Observer ()
		self.observer.connect ('observe', self.cmd_state_cb)
		self.command_manager.add_command_observer (self.observer, '')

	
	def save_cb (self, action): 

		self.app.save ()


	def cmd_state_cb (self, subject, topic, data):

		print 'cmd_state_cb() ', subject, topic, data
		# TODO lookup action and enable/disable


	def doc_cb (self, action): 

		print action.props.name


	def print_cb (self, action): 

		print action.props.name


	def custom_style_cb (self, action): 

		print action.props.name
		# TODO do initialization stuff only once
		gtype = gtkmozedit.editing_session_get_gtype ()
		edit_session = self.app.mainwin.moz.query_interface (gtype)
		#print gtype, edit_session
		web_browser = self.app.mainwin.moz.get_web_browser ()
		dom_window = web_browser.get_content_dom_window ()
		#print web_browser, dom_window
		editor = edit_session.get_editor_for_window (dom_window)
		#print editor
		html_editor_gtype = gtkmozedit.html_editor_get_gtype ()
		html_editor = editor.query_interface (html_editor_gtype)
		#print html_editor_gtype, html_editor
		if (action.props.name == ACTION_STYLE_H1): 
			self.command_manager.toggle_state (editor, 'h1')
		elif (action.props.name == ACTION_STYLE_H2):
			self.command_manager.toggle_state (editor, 'h2')
		elif (action.props.name == ACTION_STYLE_BLOCK):
			self.command_manager.toggle_state (editor, 'blockquote')
		elif (action.props.name == ACTION_STYLE_PRE):
			self.command_manager.toggle_state (editor, 'pre')


	def style_cb (self, action): 

		# actions are in the format 'bongo-style-foo'
		# the "foo" part matches mozilla's command
		p = action.props.name.find ('-') + 1
		cmd = action.props.name[p:]
		cmd = cmd.replace ('style', 'cmd')
		cmd = cmd.replace ('-', '_')
		print action.props.name, cmd
		self.command_manager.do_command (cmd, gtkmozedit.CommandParams (), self.dom_window)

		# COMMANDS:
		# cmd_bold, cmd_italics, cmd_underline, cmd_tt, 
		# cmd_strikethru, cmd_superscript, cmd_subscript, cmd_nobreak, 
		# cmd_em, cmd_strong, cmd_cite, cmd_abbr, cmd_acronym, cmd_code, cmd_samp, cmd_var

	def olist_cb (self, action): 

		print action.props.name
		self.command_manager.do_command ("cmd_ol", gtkmozedit.CommandParams (), self.dom_window)


	def ulist_cb (self, action): 

		print action.props.name
		self.command_manager.do_command ("cmd_ul", gtkmozedit.CommandParams (), self.dom_window)


	def list_cb (self, action): 

		print action.props.name


	def insert_cb (self, action): 

		print action.props.name
		params = gtkmozedit.CommandParams ()
		if (action.props.name == ACTION_INSERT_IMAGE): 
			chooser = gtk.FileChooserDialog ()
			chooser.set_action (gtk.FILE_CHOOSER_ACTION_OPEN)
			ok_button = chooser.add_button(gtk.STOCK_OK, gtk.RESPONSE_OK)
			ok_button.grab_default ()			
			response = chooser.run ()
			if (response == gtk.RESPONSE_OK):
				filename = 'file://' + chooser.get_filename ()
				print filename
				params.set_string_value ('state_attribute', filename)
				ret = self.command_manager.do_command ('cmd_insertImageNoUI', params, self.dom_window)
				print ret
			chooser.destroy ()
		elif (action.props.name == ACTION_INSERT_SYMBOL):
			pass
		elif (action.props.name == ACTION_INSERT_SEPARATOR):
			self.command_manager.do_command ("cmd_insertHR", params, self.dom_window)


	def table_cb (self, action): 

		print action.props.name


	def get_global_actions (self):

		return self.global_actions


	def create_global_actions (self): 

		if (self.global_actions):
			return
		self.global_actions = gtk.ActionGroup ("global-actions")
		actions = [(ACTION_SAVE, None, "Properties", None, "Save document", self.save_cb)]
		self.global_actions.add_actions (actions)
		

	def get_toolbar_actions (self):

		return self.toolbar_actions


	def create_toolbar_actions (self): 

		if (self.toolbar_actions):
			return

		self.toolbar_actions = gtk.ActionGroup ("toolbar-actions")

		actions = [(ACTION_DOC_PROPS, gtk.STOCK_EDIT, "Properties", None, "Document properties", self.doc_cb),
			(ACTION_DOC_CLOSE, None, "Close", None, "Close the file", self.doc_cb)]

		self.toolbar_actions.add_actions (actions)

		actions = [(ACTION_PRINT_PRINT, gtk.STOCK_PRINT, "Print", None, "Send to printer", self.print_cb), 
			(ACTION_PRINT_SETUP, None, "Page Setup", None, "Edit margins etc.", self.print_cb), 
			(ACTION_PRINT_PREVIEW, None, "Print Preview", None, "Create a new File", self.print_cb)]
	
		self.toolbar_actions.add_actions (actions)

		actions = [(ACTION_STYLE_DIALOG, gtk.STOCK_SELECT_FONT, "Style", None, "Create a new File", self.style_cb), 
			(ACTION_STYLE_TITLE, None, "Title", None, "Create a new File", self.custom_style_cb), 
			(ACTION_STYLE_H1, None, "Heading 1", None, "Create a new File", self.custom_style_cb), 
			(ACTION_STYLE_H2, None, "Heading 2", None, "Create a new File", self.custom_style_cb), 
			(ACTION_STYLE_BLOCK, None, "Blockquote", None, "Create a new File", self.custom_style_cb), 
			(ACTION_STYLE_PRE, None, "Preformatted", None, "Create a new File", self.custom_style_cb), 
			(ACTION_STYLE_EM, None, "Emphasized", None, "Create a new File", self.style_cb), 
			(ACTION_STYLE_SAMP, None, "Sample", None, "Create a new File", self.style_cb), 
			(ACTION_STYLE_STRONG, None, "Strong", None, "Create a new File", self.style_cb)]

		self.toolbar_actions.add_actions (actions)

		actions = [(ACTION_LIST_DIALOG, bongo.ui.STOCK_LIST, "List", None, "Create a new File", self.list_cb), 
			(ACTION_LIST_BULLET, None, "Bullet List", None, "Create a new File", self.ulist_cb), 
			(ACTION_LIST_DASH, None, "Dash List", None, "Create a new File", self.ulist_cb), 
			(ACTION_LIST_TICK, None, "Tick List", None, "Create a new File", self.ulist_cb), 
			(ACTION_LIST_NUMBERED, None, "Numbered List", None, "Create a new File", self.olist_cb), 
			(ACTION_LIST_LOWER, None, "Lowercase List", None, "Create a new File", self.olist_cb), 
			(ACTION_LIST_ROMAN, None, "Roman List", None, "Create a new File", self.olist_cb)]

		self.toolbar_actions.add_actions (actions)

		actions = [(ACTION_INSERT_IMAGE, bongo.ui.STOCK_INSERT, "Insert", None, "Create a new File", self.insert_cb), 
			(ACTION_INSERT_SYMBOL, None, "Symbol", None, "Create a new File", self.insert_cb),
			(ACTION_INSERT_SEPARATOR, None, 'Separator', None, 'Insert Separator', self.insert_cb)]

#		action = gtk.Action ("bongo-insert-pageno", "Page Number", "Create a new File", None)
#		action = gtk.Action ("bongo-insert-footnote", "Footnote", "Create a new File", None)
#		action = gtk.Action ("bongo-insert-endnote", "Endnote", "Create a new File", None)
#		action = gtk.Action ("bongo-insert-header", "Header", "Create a new File", None)
#		action = gtk.Action ("bongo-insert-footer", "Footer", "Create a new File", None)

		self.toolbar_actions.add_actions (actions)

		actions = [(ACTION_TABLE_INSERT, bongo.ui.STOCK_TABLE, "Table", None, "Create a new File", self.table_cb), 
			(ACTION_TABLE_INSROW, None, "Insert Row", None, "Create a new File", self.table_cb), 
			(ACTION_TABLE_INSCOL, None, "Insert Column", None, "Create a new File", self.table_cb), 
			(ACTION_TABLE_DELROW, None, "Delete Row", None, "Create a new File", self.table_cb), 
			(ACTION_TABLE_DELCOL, None, "Delete Column", None, "Create a new File", self.table_cb), 
			(ACTION_TABLE_DIALOG, None, "Table Properties ...", None, "Create a new File", self.table_cb)]

		self.toolbar_actions.add_actions (actions)
	
#		action = gtk.Action ("bongo-proof-spell", "Proof", "Create a new File", gtk.STOCK_SPELL_CHECK)
#		self.toolbar_actions.add_action (action)
#		action = gtk.Action ("bongo-proof-grammar", "Check Grammar", "Create a new File", None)
#		self.toolbar_actions.add_action (action)
#	
#		action = gtk.Action ("bongo-color-text", "Colour", "Create a new File", gtk.STOCK_SELECT_COLOR)
#		self.toolbar_actions.add_action (action)
#		action = gtk.Action ("bongo-color-background", "Background Colour", "Create a new File", None)
#		self.toolbar_actions.add_action (action)
#	
#		action = gtk.Action ("bongo-font-dialog", "Font", "Create a new File", gtk.STOCK_SELECT_FONT)
#		self.toolbar_actions.add_action (action)
