#
#  Copyright (C) 2005 Robert Staudinger
#
#  This software is free software; you can redistribute it and/or modify
#  it under the terms of the GNU Library General Public License as published by
#  the Free Software Foundation; either version 2, or (at your option)
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Library General Public License for more details.
#
#  You should have received a copy of the GNU Library General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#

import gtk
import bongo

class Toolbar (gtk.HBox): 

	ui_manager = None
	action_group = None
	title_label = None
	doctype_label = None
	popup_in_progress = False

	def __init__ (self, action_group):

		gtk.HBox.__init__ (self)
		self.set_spacing (10)

		self.action_group = action_group
		self.create_stock_icons ()

		self.ui_manager = gtk.UIManager ()
		self.ui_manager.insert_action_group (self.action_group, 0)
		self.ui_manager.add_ui_from_file (bongo.App.datadir + '/bongo-ui.xml')

		# big box for doc icon and toolbar
		hbox = gtk.HBox ()
		self.pack_start (hbox, False, False)

		action = self.action_group.get_action (bongo.ACTION_DOC_PROPS)
		popup = self.ui_manager.get_widget ("/ui/file")
		button = self.create_doc_button (action, popup)
		hbox.pack_start (button, False, False)

		vbox = gtk.VBox ()
		self.pack_start (vbox, True, True)

		# document title
		align = gtk.Alignment (0, 0.5)
		vbox.pack_start (align, True, True)
		self.title_label = gtk.Label ('')
		align.add (self.title_label)
		self.title_label.set_use_markup (True)
		self.title_label.set_markup ('<big><b>Foo Bar Baz</b></big>');

		#toolbar
		toolbar = gtk.HBox ()
		vbox.pack_start (toolbar, False, False)

		label = gtk.Label ("")
		toolbar.pack_start (label, True, True)

		box = gtk.HBox ()
		toolbar.pack_start (box, False)

		action = self.action_group.get_action ("bongo-print-print")
		popup = self.ui_manager.get_widget ("/ui/print")
		button = self.create_button (action, popup)
		box.pack_start (button)

#		label = gtk.Label ("")
#		toolbar.pack_start (label, True, True)

		box = gtk.HBox ()
		toolbar.pack_start (box, False)
		
		action = self.action_group.get_action ("bongo-style-dialog")
		popup = self.ui_manager.get_widget ("/ui/style")
		button = self.create_button (action, popup)
		box.pack_start (button)

		action = self.action_group.get_action ("bongo-list-dialog")
		popup = self.ui_manager.get_widget ("/ui/list")
		button = self.create_button (action, popup)
		box.pack_start (button)

#		label = gtk.Label ("")
#		toolbar.pack_start (label, True, True)

		box = gtk.HBox ()
		toolbar.pack_start (box, False)
		
		action = self.action_group.get_action ("bongo-insert-image")
		popup = self.ui_manager.get_widget ("/ui/insert")
		button = self.create_button (action, popup)
		box.pack_start (button)

		action = self.action_group.get_action ("bongo-table-insert")
		popup = self.ui_manager.get_widget ("/ui/table")
		button = self.create_button (action, popup)
		box.pack_start (button)

		label = gtk.Label ("")
		toolbar.pack_start (label, True, True)

		label = gtk.Label ("")
		toolbar.pack_start (label, False, False)
		label.set_size_request (64, 1)

#
#		box = gtk.HBox ()
#		toolbar.pack_start (box, False)
#		
#		action = self.action_group.get_action ("bongo-proof-spell")
#		popup = self.ui_manager.get_widget ("/ui/proof")
#		button = self.create_button (action, popup)
#		box.pack_start (button)
#
#		action = self.action_group.get_action ("bongo-color-text")
#		popup = self.ui_manager.get_widget ("/ui/color")
#		button = self.create_button (action, popup)
#		box.pack_start (button)
#
#		action = self.action_group.get_action ("bongo-font-dialog")
#		popup = self.ui_manager.get_widget ("/ui/font")
#		button = self.create_button (action, popup)
#		box.pack_start (button)


	def set_title (self, title): 

		if (len (title) == 0):
			title = 'New Document'
		self.title_label.set_markup ('<big><b>' + title + '</b></big>')


	def create_doc_button (self, action, popup): 

		evbox = gtk.EventBox ()
		vbox = gtk.VBox ()
		evbox.add (vbox)

		pixbuf = gtk.gdk.pixbuf_new_from_file_at_size (bongo.App.pixmapdir + '/gtk-edit.svg', 48, 48);
		image = gtk.Image ()
		image.set_from_pixbuf (pixbuf)
		button = gtk.ToolButton (image)
		button.connect ('clicked', self.doc_button_clicked_cb, action)
		vbox.pack_start (button)

		label = gtk.MenuItem ('Document')
		vbox.pack_start (label)
		label.set_submenu (popup)
		label.connect ('enter-notify-event', self.enter_cb)
		label.connect ('leave-notify-event', self.leave_cb)
		label.connect ("button-press-event", self.popup_cb, popup)

		popup.connect ('selection-done', self.selection_done_cb, label)
		popup.set_data ("popup-button", evbox)

		return evbox


	def create_button (self, action, popup): 

		evbox = gtk.EventBox ()
		vbox = gtk.VBox ()
		evbox.add (vbox)
		button = action.create_tool_item ()
		vbox.pack_start (button)

		label = gtk.MenuItem (action.props.label)
		vbox.pack_start (label)
		label.connect ('enter-notify-event', self.enter_cb)
		label.connect ('leave-notify-event', self.leave_cb)
		label.connect ("button-press-event", self.popup_cb, popup)

		popup.connect ('selection-done', self.selection_done_cb, label)
		popup.set_data ("popup-button", evbox)

		return evbox


	def create_stock_icons (self): 

		factory = gtk.IconFactory ();

		pixbuf = gtk.gdk.pixbuf_new_from_file (bongo.App.pixmapdir + '/style.png')
		set = gtk.IconSet (pixbuf)
		factory.add ("bongo-style", set)

		pixbuf = gtk.gdk.pixbuf_new_from_file (bongo.App.pixmapdir + '/list.png')
		set = gtk.IconSet (pixbuf)
		factory.add ("bongo-list", set)

		pixbuf = gtk.gdk.pixbuf_new_from_file (bongo.App.pixmapdir + '/insert.png')
		set = gtk.IconSet (pixbuf)
		factory.add ("bongo-insert", set)

		pixbuf = gtk.gdk.pixbuf_new_from_file (bongo.App.pixmapdir + '/table.png')
		set = gtk.IconSet (pixbuf)
		factory.add ("bongo-table", set)

		factory.add_default ()


	# custom icon for the big toolbutton, so we need to activate manually
	def doc_button_clicked_cb (self, button, action): 

		action.activate ()


	def enter_cb (self, item, event): 

		print "enter"
		item.select ()
		# propagate
		return False


	def leave_cb (self, item, event): 

		if (self.popup_in_progress == False):
			item.deselect ()
		else:
			self.popup_in_progress = False
		# propagate
		return False


	def popup_cb (self, item, event, popup): 

		self.popup_in_progress = True
		popup.popup (None, None, self.popup_func, 1, gtk.get_current_event_time ())
		# propagate
		return False


	def popup_func (self, popup): 

		popup_evbox = popup.get_data ("popup-button")
		origin = popup_evbox.window.get_origin ()
		alloc = popup_evbox.get_allocation ()
		return (origin[0], origin[1] + alloc.height, True)

	
	def activate_cb (self, action): 

		print action.props.name

	
	def selection_done_cb (self, menushell, item): 
		
		item.deselect ()
