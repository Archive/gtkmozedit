#
#  Copyright (C) 2005 Robert Staudinger
#
#  This software is free software; you can redistribute it and/or modify
#  it under the terms of the GNU Library General Public License as published by
#  the Free Software Foundation; either version 2, or (at your option)
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Library General Public License for more details.
#
#  You should have received a copy of the GNU Library General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#


import gtk
import pygtk
import gtkmozedit
import sys

from toolbar import *


class MainWindow (gtk.Window):
	
	vbox = None
	toolbar = None
	splitter = None
	moz = None
	status = None
	save_action = None

	def __init__ (self, controller): 
		
		gtk.Window.__init__ (self)
		self.set_title ('HTML Document')
		self.resize (640, 480)
		self.connect ("delete-event", self.delete_cb)
		self.save_action = controller.get_global_actions ().get_action (bongo.ACTION_SAVE) 
		print self.save_action

		self.vbox = gtk.VBox ()
		self.add (self.vbox)

		self.toolbar = Toolbar (controller.get_toolbar_actions ())
		self.vbox.pack_start (self.toolbar, False, False)
		
		frame = gtk.Frame ()
		frame.set_shadow_type (gtk.SHADOW_IN)
		self.vbox.pack_start (frame, True, True)

		self.moz = gtkmozedit.HTMLView ()
		frame.add (self.moz)
		self.moz.connect ('realize', self.moz_realize_cb, controller)

		self.status = gtk.Statusbar ()
		self.vbox.pack_start (self.status, False, False)


	def get_moz (self): 

		return self.moz


	def moz_realize_cb (self, moz, controller): 	

		cmd_mgr_gtype = gtkmozedit.command_manager_get_gtype ()	
		command_manager = self.moz.query_interface (cmd_mgr_gtype)
		dom_window = moz.get_web_browser ().get_content_dom_window ()
		controller.set_command_manager (command_manager, dom_window)

		gtype = gtkmozedit.dom.html.document_get_gtype ()
		html_document = dom_window.get_document ().query_interface (gtype)
		self.toolbar.set_title (html_document.get_title ())


	def delete_cb (self, widget, event):

		self.save_action.activate ()
		gtk.main_quit ()


	def load (self, filename): 

		self.moz.load (filename)

