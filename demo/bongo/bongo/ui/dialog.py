
import gobject
import gtk


class Dialog (gtk.Window): 

	appwin = None
	loop = None
	configure_cb_id = None
	response = None
	wrapped_dialog = None

	def __init__ (self, appwin): 

		gtk.Window.__init__ (self, gtk.WINDOW_POPUP)
		self.appwin = appwin
		self.set_border_width (2)
		self.set_transient_for (appwin)

		self.connect_after ('expose-event', self.expose_cb)
		self.connect ('delete-event', self.delete_cb)
		self.configure_cb_id = appwin.connect ('configure_event', self.configure_cb)
		

	def wrap (self, dialog): 

		if (self.wrapped_dialog):
			print "*** Error: can only wrap one dialog"
			return False

		dialog.realize ()
		dialog.get_child ().reparent (self)
		self.wrapped_dialog = dialog
		return True

	
	def add_button (self, button_text, response_id): 

		button = gtk.Button (stock=button_text)
		button.connect ('clicked', self.clicked_cb, response_id)
		self.wrapped_dialog.add_action_widget (button, response_id)
		return button


	def set_default_response (self, response_id): 

		self.response = response_id


	def run (self): 

		# position
		self.realize ()
		self.position ()
	
		# run		
		self.appwin.set_sensitive (False)
		self.set_modal (True)
		self.show_all ()
		self.loop = gobject.MainLoop ()
		self.loop.run ()

		#cleanup
		self.appwin.disconnect (self.configure_cb_id)
		self.appwin.set_sensitive (True)
		return self.response
	

	def configure_cb (self, appwin, event):

		self.position ()


	def position (self):

		origin = self.appwin.get_child ().window.get_origin ()
		parent_alloc = self.appwin.get_allocation ()
		child_alloc = self.get_allocation ()
		self.move (origin[0] + ((parent_alloc.width - child_alloc.width) / 2), origin[1])


	def expose_cb (self, widget, event): 

		window = self.get_child ().window
		style = gtk.Style ()
		style = style.attach (window)
		alloc = self.get_allocation ()

		style.paint_hline (window, gtk.STATE_ACTIVE, None, self, '', 0, alloc.width, 0)
		style.paint_hline (window, gtk.STATE_ACTIVE, None, self, '', 0, alloc.width, alloc.height - 1)
		style.paint_vline (window, gtk.STATE_ACTIVE, None, self, '', 0, alloc.height, 0)
		style.paint_vline (window, gtk.STATE_ACTIVE, None, self, '', 0, alloc.height, alloc.width - 1)


	def clicked_cb (self, button, response_id): 

		self.response = response_id
		self.loop.quit ()


	def delete_cb (self, window): 

		self.loop.quit ()
