/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __BG_FOO_H__
#define __BG_FOO_H__

#include <glib.h>
#include <glib-object.h>

G_BEGIN_DECLS

#define BG_TYPE_FOO                  (bg_foo_get_gtype ())
#define BG_FOO(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), BG_TYPE_FOO, BgFoo))
#define BG_FOO_CLASS(klass)          (G_TYPE_CHECK_CLASS_CAST ((klass), BG_TYPE_FOO, BgFooClass))
#define BG_IS_FOO(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), BG_TYPE_FOO))
#define BG_IS_FOO_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), BG_TYPE_FOO))
#define BG_FOO_GET_CLASS(obj)        (G_TYPE_INSTANCE_GET_CLASS ((obj), BG_TYPE_FOO, BgFooClass))

typedef struct _BgFoo BgFoo;
typedef struct _BgFooClass BgFooClass;

struct _BgFoo {
	GObject parent;
	gboolean is_disposed;
};

struct _BgFooClass {
	GObjectClass parent;
	void (* dispose) (GObject *instance);
};

GType bg_foo_get_gtype (void);
BgFoo* bg_foo_new (void);
void bg_foo_bar (BgFoo *self, const gchar *foo);
/*
gboolean bg_foo_toggle_state (BgFoo *self, GmeEditor *editor, const gchar *tag_name);

void bg_foo_position_dialog (GtkWidget *dialog, GtkWidget *parent);
*/

G_END_DECLS

#endif /* __BG_FOO_H__ */
