/* -- THIS FILE IS GENERATED - DO NOT EDIT *//* -*- Mode: C; c-basic-offset: 4 -*- */

#include <Python.h>



#line 3 "./cbongo.override"
#include <Python.h>
#include <pygobject.h>
#include "bg-foo.h"
#line 12 "cbongo.c"


/* ---------- types from other modules ---------- */
static PyTypeObject *_PyGObject_Type;
#define PyGObject_Type (*_PyGObject_Type)


/* ---------- forward type declarations ---------- */
PyTypeObject PyBgFoo_Type;

#line 23 "cbongo.c"



/* ----------- BgFoo ----------- */

static int
_wrap_bg_foo_new(PyGObject *self, PyObject *args, PyObject *kwargs)
{
    static char* kwlist[] = { NULL };

    if (!PyArg_ParseTupleAndKeywords(args, kwargs, ":cbongo.Foo.__init__", kwlist))
        return -1;

    pygobject_constructv(self, 0, NULL);

    if (!self->obj) {
        PyErr_SetString(PyExc_RuntimeError, "could not create %(typename)s object");
        return -1;
    }

    return 0;
}


static PyObject *
_wrap_bg_foo_bar(PyGObject *self, PyObject *args, PyObject *kwargs)
{
    static char *kwlist[] = { "foo", NULL };
    char *foo;

    if (!PyArg_ParseTupleAndKeywords(args, kwargs, "s:BgFoo.bar", kwlist, &foo))
        return NULL;
    bg_foo_bar(BG_FOO(self->obj), foo);
    Py_INCREF(Py_None);
    return Py_None;
}

static PyMethodDef _PyBgFoo_methods[] = {
    { "bar", (PyCFunction)_wrap_bg_foo_bar, METH_VARARGS|METH_KEYWORDS },
    { NULL, NULL, 0 }
};

PyTypeObject PyBgFoo_Type = {
    PyObject_HEAD_INIT(NULL)
    0,					/* ob_size */
    "cbongo.Foo",			/* tp_name */
    sizeof(PyGObject),	        /* tp_basicsize */
    0,					/* tp_itemsize */
    /* methods */
    (destructor)0,	/* tp_dealloc */
    (printfunc)0,			/* tp_print */
    (getattrfunc)0,	/* tp_getattr */
    (setattrfunc)0,	/* tp_setattr */
    (cmpfunc)0,		/* tp_compare */
    (reprfunc)0,		/* tp_repr */
    (PyNumberMethods*)0,     /* tp_as_number */
    (PySequenceMethods*)0, /* tp_as_sequence */
    (PyMappingMethods*)0,   /* tp_as_mapping */
    (hashfunc)0,		/* tp_hash */
    (ternaryfunc)0,		/* tp_call */
    (reprfunc)0,		/* tp_str */
    (getattrofunc)0,	/* tp_getattro */
    (setattrofunc)0,	/* tp_setattro */
    (PyBufferProcs*)0,	/* tp_as_buffer */
    Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE,                      /* tp_flags */
    NULL, 				/* Documentation string */
    (traverseproc)0,	/* tp_traverse */
    (inquiry)0,		/* tp_clear */
    (richcmpfunc)0,	/* tp_richcompare */
    offsetof(PyGObject, weakreflist),             /* tp_weaklistoffset */
    (getiterfunc)0,		/* tp_iter */
    (iternextfunc)0,	/* tp_iternext */
    _PyBgFoo_methods,			/* tp_methods */
    0,					/* tp_members */
    0,		       	/* tp_getset */
    NULL,				/* tp_base */
    NULL,				/* tp_dict */
    (descrgetfunc)0,	/* tp_descr_get */
    (descrsetfunc)0,	/* tp_descr_set */
    offsetof(PyGObject, inst_dict),                 /* tp_dictoffset */
    (initproc)_wrap_bg_foo_new,		/* tp_init */
    (allocfunc)0,           /* tp_alloc */
    (newfunc)0,               /* tp_new */
    (freefunc)0,             /* tp_free */
    (inquiry)0              /* tp_is_gc */
};



/* ----------- functions ----------- */

static PyObject *
_wrap_bg_foo_get_gtype(PyObject *self)
{
    GType ret;

    ret = bg_foo_get_gtype();
    return pyg_type_wrapper_new(ret);
}

PyMethodDef cbongo_functions[] = {
    { "bg_foo_get_gtype", (PyCFunction)_wrap_bg_foo_get_gtype, METH_NOARGS },
    { NULL, NULL, 0 }
};

/* initialise stuff extension classes */
void
cbongo_register_classes(PyObject *d)
{
    PyObject *module;

    if ((module = PyImport_ImportModule("gobject")) != NULL) {
        PyObject *moddict = PyModule_GetDict(module);

        _PyGObject_Type = (PyTypeObject *)PyDict_GetItemString(moddict, "GObject");
        if (_PyGObject_Type == NULL) {
            PyErr_SetString(PyExc_ImportError,
                "cannot import name GObject from gobject");
            return;
        }
    } else {
        PyErr_SetString(PyExc_ImportError,
            "could not import gobject");
        return;
    }


#line 151 "cbongo.c"
    pygobject_register_class(d, "BgFoo", BG_TYPE_FOO, &PyBgFoo_Type, Py_BuildValue("(O)", &PyGObject_Type));
    pyg_set_object_has_new_constructor(BG_TYPE_FOO);
}
