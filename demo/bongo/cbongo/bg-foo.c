/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "bg-foo.h"

/*
#include <nsCOMPtr.h>
#include <nsIEditor.h>
*/

enum {
	PROP_0,
	PROP_WRAPPED_PTR,
	_NUM_PROPS
};

static GObjectClass *bg_foo_parent_class = NULL;

static void
instance_init (BgFoo *self)
{
	self->is_disposed = FALSE;
}

static void
instance_dispose (GObject *instance)
{
	BgFoo *self = BG_FOO (instance);

	if (self->is_disposed)
		return;

	self->is_disposed = TRUE;

	bg_foo_parent_class->dispose (G_OBJECT (self));
}

static void
set_property (GObject      *object,
	      guint         prop_id,
	      GValue const *value,
	      GParamSpec   *pspec)
{
	BgFoo *self = BG_FOO (object);

	switch (prop_id) {
	case PROP_WRAPPED_PTR:
		/**/
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
get_property (GObject    *object,
	      guint       prop_id,
	      GValue     *value,
	      GParamSpec *pspec)
{
	BgFoo *self = BG_FOO (object);

	switch (prop_id) {
	case PROP_WRAPPED_PTR:
		/**/
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
class_init (BgFooClass *klass)
{
	GObjectClass *gobject_class = G_OBJECT_CLASS (klass);

	/* vfuncs */
	klass->dispose = instance_dispose;

	/* hook gobject vfuncs */
	gobject_class->dispose = instance_dispose;

	bg_foo_parent_class = (GObjectClass*) g_type_class_peek_parent (klass);

	gobject_class->set_property = set_property;
	gobject_class->get_property = get_property;

	/*
	g_object_class_install_property (gobject_class,
		PROP_WRAPPED_PTR,
		g_param_spec_pointer ("wrapped-ptr", _("Wrapped Pointer"),
			_("Pointer to the wrapped c++ object"),
			(GParamFlags)(G_PARAM_READABLE)));
	*/
}

GType
bg_foo_get_gtype (void)
{
        static GType type = 0;
        if (!type) {
                static const GTypeInfo info = {
                        sizeof (BgFooClass),
                        NULL,           /* base_init */
                        NULL,           /* base_finalize */
                        (GClassInitFunc) class_init,
                        NULL,           /* class_finalize */
                        NULL,           /* class_data */
                        sizeof (BgFoo),
                        0,              /* n_preallocs */
                        (GInstanceInitFunc) instance_init,
                };
                type = g_type_register_static (G_TYPE_OBJECT, "BgFoo", &info, (GTypeFlags)0);
        }
        return type;
}

void 
bg_foo_bar (BgFoo *self, const gchar *foo)
{
	printf ("bg_foo_foo ('%s')\n", foo);
}

/* from nsComposerCommands.cpp */
/*
gboolean 
bg_foo_toggle_state (BgFoo *self, GmeEditor *editor, const gchar *tag_name)
{
  nsIEditor *aEditor = editor->wrapped_ptr;

  nsCOMPtr<nsIHTMLEditor> htmlEditor = do_QueryInterface(aEditor);
  if (!htmlEditor)
    return NS_ERROR_NO_INTERFACE;

  //create some params now...
  nsresult rv;
  nsCOMPtr<nsICommandParams> params =
      do_CreateInstance(NS_COMMAND_PARAMS_CONTRACTID,&rv);
  if (NS_FAILED(rv) || !params)
    return rv;

  // tags "href" and "name" are special cases in the core editor 
  // they are used to remove named anchor/link and shouldn't be used for insertion
  nsAutoString tagName; tagName.AssignWithConversion(tag_name);
  PRBool doTagRemoval;
  if (tagName.EqualsLiteral("href") ||
      tagName.EqualsLiteral("name"))
    doTagRemoval = PR_TRUE;
  else
  {
    // check current selection; set doTagRemoval if formatting should be removed
    rv = GetCurrentState(aEditor, tag_name, params);
    if (NS_FAILED(rv)) 
      return rv;
    rv = params->GetBooleanValue(STATE_ALL, &doTagRemoval);
    if (NS_FAILED(rv)) 
      return rv;
  }

  if (doTagRemoval)
    rv = RemoveTextProperty(aEditor, tagName.get(), nsnull);
  else
  {
    // Superscript and Subscript styles are mutually exclusive
    nsAutoString removeName; 
    aEditor->BeginTransaction();

    if (tagName.EqualsLiteral("sub"))
    {
      removeName.AssignLiteral("sup");
      rv = RemoveTextProperty(aEditor,tagName.get(), nsnull);
    } 
    else if (tagName.EqualsLiteral("sup"))
    {
      removeName.AssignLiteral("sub");
      rv = RemoveTextProperty(aEditor, tagName.get(), nsnull);
    }
    if (NS_SUCCEEDED(rv))
      rv = SetTextProperty(aEditor,tagName.get(), nsnull, nsnull);

    aEditor->EndTransaction();
  }

  return rv;
}
*/
