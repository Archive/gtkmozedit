/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/* include this first, before NO_IMPORT_PYGOBJECT is defined */
#include <pygobject.h>
#include <glib/gmacros.h>

G_BEGIN_DECLS

void cbongo_register_classes (PyObject *d);
extern PyMethodDef cbongo_functions[];

DL_EXPORT(void)
initcbongo(void)
{
	PyObject *cbongo_module = NULL;
	PyObject *dict = NULL;

	init_pygobject ();

	cbongo_module = Py_InitModule3 ("cbongo", 
					 cbongo_functions, 
					 "cbongo module.");

	dict = PyModule_GetDict (cbongo_module);
	cbongo_register_classes (dict);
	if (PyErr_Occurred ()) {
		PyErr_Print ();
		Py_FatalError ("can't initialise module cbongo");
	}
}

G_END_DECLS
