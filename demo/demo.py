# 
# Demo stuff
# License: Public Domain
# 

import gtk
import pygtk
import gtkmozedit
import sys


def foo_clicked_cb (widget, moz):

	browser = moz.get_web_browser ()
	win = browser.get_content_dom_window ()
	doc = win.get_document ()
	elem = doc.get_element_by_id ("body")
	elem.set_attribute ("bgcolor", "#ff0000")

	# query_interface / downcasting
	body = elem.query_interface (gtkmozedit.dom.html.body_element_get_gtype ())
	body.set_background ("file:///home/robert/Desktop/cart_19x13.gif")

	node_list = elem.query_interface (gtkmozedit.dom.node_list_get_gtype ())
	if node_list == None:
		print "query_interface() failed (as it should)"


def save_clicked_cb (widget, moz):
	print "save_clicked_cb()"
	moz.save ("/home/robert/Desktop/foo.html", True)

def observe_cb (subject, topic, data): 
	print "observe_cb", subject, topic, data

def observe_clicked_cb (widget, moz):
	print "observe_clicked_cb()"
	manager = moz.query_interface (gtkmozedit.html_view_get_gtype ())
	if manager != None: 
		observer = gtkmozedit.Observer ()
		observer.connect ("observe", observe_cb)
		manager.add_command_observer (observer, "cmd_bold")

def bold_clicked_cb (widget, moz):
	moz.do_command ("cmd_bold", "")

def query_interface_cb (widget, moz):
	print "query_interface_cb()"
	cmd_mgr = moz.query_interface (gtkmozedit.command_manager_get_gtype ())
	print cmd_mgr


win = gtk.Window ()
win.set_default_size (480, 320)
win.connect ("delete-event", gtk.main_quit, None) 

vbox = gtk.VBox ()
win.add (vbox)

moz = gtkmozedit.HTMLView ()
vbox.pack_start (moz)

hbox = gtk.HBox ()
vbox.pack_start (hbox, False)

button = gtk.Button ("Foo")
button.connect ("clicked", foo_clicked_cb, moz);
hbox.pack_start (button, False)

button = gtk.Button ("Save")
button.connect ("clicked", save_clicked_cb, moz);
hbox.pack_start (button, False)

button = gtk.Button ("Observe")
button.connect ("clicked", observe_clicked_cb, moz);
hbox.pack_start (button, False)

button = gtk.Button ("Bold")
button.connect ("clicked", bold_clicked_cb, moz);
hbox.pack_start (button, False)

button = gtk.Button ("query_interface")
button.connect ("clicked", query_interface_cb, moz);
hbox.pack_start (button, False)

if len (sys.argv) > 1:
	moz.load (sys.argv[1])

win.show_all ()
gtk.main ()
