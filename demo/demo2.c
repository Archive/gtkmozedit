/*
 * Demo stuff
 * License: Public Domain
 */

#include <gtk/gtk.h>
#include <gtkmozedit/gtkmozedit.h>

static void
bold_cb (GtkButton *button, 
	    GmeHTMLView *moz)
{
	printf ("clicked\n");

	gme_html_view_do_command (moz, "cmd_bold", NULL);
}

static void
foo_cb (GtkButton *button, 
	    GmeHTMLView *moz)
{
	GmeWebBrowser *browser = NULL;
	GmeDOMWindow *win = NULL;
	GmeDOMDocument *doc = NULL;
	GmeDOMElement *elem = NULL;
	GmeDOMHTMLBodyElement *body = NULL;
	GmeDOMHTMLBodyElement *body2 = NULL;
	GmeDOMNodeList *list = NULL;
	gboolean ret;

	browser = gme_html_view_get_web_browser (moz);
	g_return_if_fail (browser);
		
	win = gme_web_browser_get_content_dom_window (browser);
	g_return_if_fail (win);		

	doc = gme_dom_window_get_document (win);
	g_return_if_fail (doc);		

	elem = gme_dom_document_get_element_by_id (doc, "body");
	g_return_if_fail (elem);		
	gme_dom_element_set_attribute (elem, "bgcolor", "#ff0000");

	/* query_interface / downcasting */
	body = gme_supports_query_interface (GME_SUPPORTS (elem), GME_TYPE_DOM_HTML_BODY_ELEMENT);
	g_assert (body);
	gme_dom_html_body_element_set_background (body, "file:///home/robert/Desktop/cart_19x13.gif");

	/* more query_interface */
	body2 = gme_isupports_query_interface (GME_ISUPPORTS (elem), GME_TYPE_DOM_HTML_BODY_ELEMENT);
	g_printf ("body2: %x\n", body2);

	/* this must fail */
	list = gme_supports_query_interface (GME_SUPPORTS (body), GME_TYPE_DOM_NODE_LIST);
	g_printf ("GmeDOMNodeList: %x\n", list);
}

static void
query_interface_cb (GtkButton *button, 
		    GmeHTMLView *moz)
{
	GmeCommandManager *cmd_mgr = NULL;
	GmeWebBrowser* browser = NULL;
	GmeDOMWindow *win = NULL;
	GmeCommandParams *params = NULL;
	gboolean ret;

	cmd_mgr = GME_COMMAND_MANAGER (gme_html_view_query_interface (GME_ISUPPORTS (moz), GME_TYPE_COMMAND_MANAGER));
	printf ("supports: %x\n", cmd_mgr);
	
	browser = gme_html_view_get_web_browser (moz);
	win = gme_web_browser_get_content_dom_window (browser);
	params = gme_command_params_new ();
	ret = gme_command_manager_do_command  (cmd_mgr, "cmd_bold", params, win);
	printf ("cmd_bold: %d\n", ret);
}

static void
save_cb (GtkButton *button, 
	    GmeHTMLView *moz)
{
	gboolean ret;

	printf ("save: ");

	ret = gme_html_view_save (moz, "/home/robert/Desktop/foo.html", TRUE);

	printf ("%d\n", ret);
}

int
main (int argc, char **argv)
{
	GtkWidget *window 	= NULL;
	GtkWidget *vbox		= NULL;
	GtkWidget *moz1, *moz2	= NULL;
	GtkWidget *hbox		= NULL;
	GtkWidget *button	= NULL;
	const gchar *url	= NULL;
	int i;
	
	gtk_init (&argc, &argv);

	window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	g_signal_connect (G_OBJECT (window), "delete-event", G_CALLBACK (gtk_main_quit), NULL);

	vbox = gtk_vbox_new (FALSE, 0);
	gtk_container_add (GTK_CONTAINER (window), vbox);
	
	hbox = gtk_hbox_new (FALSE, 10);
	gtk_box_pack_start_defaults (GTK_BOX (vbox), hbox);

	for (i = 0; i < argc; i++)
		printf ("%s\n", argv[i]);

	moz1 = gme_html_view_new ();
	if (argc > 1 && argv[1]) {
		printf ("will load: '%s'\n", argv[1]);
		gme_html_view_load (GME_HTML_VIEW (moz1), argv[1]);
	}
	gtk_box_pack_start (GTK_BOX (hbox), moz1, TRUE, TRUE, 0);

	moz2 = gme_html_view_new ();
	if (argc > 2 && argv[2]) {
		printf ("will load: '%s'\n", argv[2]);
		gme_html_view_load (GME_HTML_VIEW (moz2), argv[2]);
	}
	gtk_box_pack_start (GTK_BOX (hbox), moz2, TRUE, TRUE, 0);

	hbox = gtk_hbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, FALSE, 0);	

	button = gtk_button_new_with_label ("bold");
	gtk_box_pack_start (GTK_BOX (hbox), button, FALSE, FALSE, 0);	
	g_signal_connect (G_OBJECT (button), "clicked", G_CALLBACK (bold_cb), (gpointer) moz1);

	button = gtk_button_new_with_label ("foo");
	gtk_box_pack_start (GTK_BOX (hbox), button, FALSE, FALSE, 0);	
	g_signal_connect (G_OBJECT (button), "clicked", G_CALLBACK (foo_cb), (gpointer) moz1);

	button = gtk_button_new_with_label ("save");
	gtk_box_pack_start (GTK_BOX (hbox), button, FALSE, FALSE, 0);	
	g_signal_connect (G_OBJECT (button), "clicked", G_CALLBACK (save_cb), (gpointer) moz1);

	button = gtk_button_new_with_label ("query interface");
	gtk_box_pack_start (GTK_BOX (hbox), button, FALSE, FALSE, 0);	
	g_signal_connect (G_OBJECT (button), "clicked", G_CALLBACK (query_interface_cb), (gpointer) moz1);

	gtk_widget_show_all (window);
	gtk_main ();
	return 0;
}
