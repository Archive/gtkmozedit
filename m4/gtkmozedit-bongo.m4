# 
# start: m4/gtkmozedit-bongo.m4
# 
AC_DEFUN([GTKMOZEDIT_BONGO], [

AC_ARG_ENABLE(bongo,
			[AC_HELP_STRING([--enable-bongo], [Compile demo application])],enable_bongo="$enableval",enable_bongo=no)

AM_CONDITIONAL(WITH_BONGO, test x$enable_bongo = xyes)

])
# 
# end: m4/gtkmozedit-bongo.m4
# 
