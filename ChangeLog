
2005-11-55 Robert Staudinger <robsta@stereolyzer.net>

	* Fix build system so bongo build works from gtkmozedit and standalone.

2005-11-05 Robert Staudinger <robsta@stereolyzer.net>

	* Enable building against seamonkey.

2005-11-03 Robert Staudinger <robsta@stereolyzer.net>

	* Buildsystem for bongo, so make dist works.
	* Crude implementation of styles, except "Title"
	* Move bongo.Dialog to bongo.ui.Dialog

2005-11-02 Robert Staudinger <robsta@stereolyzer.net>

	* Implement Bongo dialogs.
	* Create GmeEditor and GmeHTMLEditor skeletons.
	* gtkmozedit/gme-command-manager.cpp: gme_command_manager_toggle_state
	  for custom commands

2005-10-31 Robert Staudinger <robsta@stereolyzer.net>

	* gtkmozedit/gme-observer.cpp: fix refcounting
	* demo/bongo/*.py: implement ideas from the tango project
	* Bongo: implement load/save

2005-10-27 Robert Staudinger <robsta@stereolyzer.net>

	* Restructure bongo.
	* Make bongo installable.
	* Compile python classes before installing.
	* Add demo/bongo/cbongo for native bongo classes.

2005-10-20 Robert Staudinger <robsta@stereolyzer.net>

	* Wrap nsIDOMCSS* interfaces.
	* Create new python package gtkmozedit.dom.css and wrap CSS interfaces
	  into.
	* Wrap auxiliary css classes and provide python bindings.
	* Fix out of srctree build for python binding.
	* Fix bongo demo after big renaming.
	* Bump version.
	* Update ROADMAP

2005-10-19 Robert Staudinger <robsta@stereolyzer.net>

	* Rename GtkMozEdit widget to GmeHTMLView.
	* Rename DOM table methods from _t_$foo to _t$foo

2005-10-18 Robert Staudinger <robsta@stereolyzer.net>

	* Implement GmeISupports and implement it in GmeSupports.
	  Facilitates query_interface via interface, rather that subclass.
	* Move python bindings creation scripts to bindings/.
	* Create toplevel header gtkmozedit/gtkmozedit.h.
	* Split python bindings in packages.

2005-10-16 Robert Staudinger <robsta@stereolyzer.net>

	* Implement command manager and observer.

2005-10-13 Philip Van Hoof <pvanhoof@gnome.org>

	* Fixed little typo in the gtkmozedit-1.pc.in

2005-10-12 Robert Staudinger <robsta@stereolyzer.net>

	* Add NEWS.
	* Bump version, release 0.2.
	* gtkmozedit/gtk-moz-edit-private.h: add to CVS.
	* Release fixed 0.2.1 tarball.
	* Remove TODO, stuff is in ROADMAP.

2005-10-11 Robert Staudinger <robsta@stereolyzer.net>

	* $foo_get_type () -> $foo_get_gtype () in all wrapped objects to
	  avoid collision with DOM attributes.
	* Wrap nsIDOMHTML*, support for all methods and attributes in place. 

2005-10-05 Robert Staudinger <robsta@stereolyzer.net>

	* Correctly catch failed query_interface() invocations.

2005-10-04 Robert Staudinger <robsta@stereolyzer.net>

	* Implement GmeDOMHTMLBodyElement, GmeDOMHTMLCollection, 
	  GmeDOMHTMLDocument.
	* Implement root baseclass GmeSupports with query_interface method.
	* Get rid of nsCOMPtr.
	* Implement demo/demo2.c, show and document query_interface() in the
	  demos.

2005-10-03 Robert Staudinger <robsta@stereolyzer.net>

	* gtkmozedit/gtk-moz-edit.*: get rid of private struct, move
	definition to private header.
	* Implement GmeDOMHTMLElement.
	* demo/demo.py: add some API invocations.

2005-10-02 Robert Staudinger <robsta@stereolyzer.net>

	* Implement GmeDOMCDATASection, GmeDOMComment, GmeDOMEntityReference,
	GmeDOMProcessingInstruction, GmeDOMText.
	* Partial implementation of GmeDOMWindow, GmeWebBrowser.
	* gtkmozedit/dom/gme-dom-document.{cpp h}: finish preliminary
	implementation (no *_ns methods).

2005-10-01 Robert Staudinger <robsta@stereolyzer.net>

	* gtkmozedit/dom/*.{cpp h}: remove finalize, it's not needed, 
	  no more private struct, move class definition to private header.
	* gtkmozedit/dom/gme-dom-element*: finish preliminary implementation
	  (no *_ns methods).
	* Implement GmeDOMCharacterData, GmeDOMDocumentFragment.
	* gtkmozedit/dom/*.h: remove traces of priv struct.
	* Also build python bindings for DOM classes.

2005-09-30 Robert Staudinger <robsta@stereolyzer.net>

	* gtkmozedit/dom/*.cpp: fix use of nsCOMPtr.
	* demo/Makefile.am: build demo1.
