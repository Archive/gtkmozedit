#!/bin/sh
sed -e "s/_DOMDOM_/_DOM_DOM_/g" 			\
    -e "s/_DOMCDATA_/_DOM_CDATA_/g" 			\
    -e "s/_DOMHTML_/_DOM_HTML_/g" 			\
    -e "s/_DOMHTMLD_LIST_/_DOM_HTML_DLIST_/g"		\
    -e "s/_DOMHTMLBR_/_DOM_HTML_BR_/g"			\
    -e "s/_DOMHTMLHR_/_DOM_HTML_HR_/g"			\
    -e "s/_DOMHTMLI_FRAME_/_DOM_HTML_IFRAME_/g"		\
    -e "s/_DOMHTMLLI_/_DOM_HTML_LI_/g"			\
    -e "s/_DOMHTMLO_LIST_/_DOM_HTML_OLIST_/g"		\
    -e "s/_DOM_HTML_OPT_GROUP_/_DOM_HTML_OPTGROUP_/g"	\
    -e "s/_DOMHTMLU_LIST_/_DOM_HTML_ULIST_/g" 		\
    -e "s/_DOMCSSNS/_DOM_CSS_NS/g" 			\
    -e "s/_DOMCSS/_DOM_CSS/g" $1
