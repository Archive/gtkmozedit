#!/bin/sh

#pubhdr=`find ../../gtkmozedit -name $1`
pubhdr=$1
privhdr=`dirname $pubhdr`/`basename $pubhdr .h`"-private.h"
mergehdr=`basename $pubhdr`

if [ ! -e "$privhdr" ] 
then 
	echo "no private header for $pubhdr, using copy"
	cat $pubhdr > $mergehdr
	exit 0;
fi

# stuff until class forward decl
grep -B100 "^typedef" $pubhdr > $mergehdr

# add object forward decl based on class fwd decl
grep "^typedef" $mergehdr | sed -e "s/Class//g" >> $mergehdr

# merge stuff from private hdr
grep -A4  "^struct" $privhdr | tr -d '\--' >> $mergehdr

# append remainder of public hdr
grep -A500 "^GType" $pubhdr >> $mergehdr
