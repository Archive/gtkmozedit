/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/* include this first, before NO_IMPORT_PYGOBJECT is defined */
#include <pygobject.h>
#include <glib/gmacros.h>

G_BEGIN_DECLS

void gtkmozedit_register_classes (PyObject *d);
void gtkmozedit_base_register_classes (PyObject *d);
void gtkmozedit_dom_register_classes (PyObject *d);
void gtkmozedit_dom_html_register_classes (PyObject *d);
void gtkmozedit_dom_css_register_classes (PyObject *d);

extern PyMethodDef gtkmozedit_functions[];
extern PyMethodDef gtkmozedit_base_functions[];
extern PyMethodDef gtkmozedit_dom_functions[];
extern PyMethodDef gtkmozedit_dom_html_functions[];
extern PyMethodDef gtkmozedit_dom_css_functions[];

DL_EXPORT(void)
initgtkmozedit(void)
{
	PyObject *mozedit_module = NULL;
	PyObject *mozedit_base_module = NULL;
	PyObject *mozedit_dom_module = NULL;
	PyObject *mozedit_dom_html_module = NULL;
	PyObject *mozedit_dom_css_module = NULL;
	PyObject *dict = NULL;

	init_pygobject ();

	/* create empty root package so we can attach stuff */
	mozedit_module = Py_InitModule3 ("gtkmozedit", 
					 gtkmozedit_functions, 
					 "gtkmozedit module.");

	/* base package "gtkmozedit.base" */
	mozedit_base_module = Py_InitModule3 ("gtkmozedit.base", 
					      gtkmozedit_base_functions, 
					      "gtkmozedit.base module.");
	dict = PyModule_GetDict (mozedit_base_module);
	gtkmozedit_base_register_classes (dict);
	if (PyErr_Occurred ()) {
		PyErr_Print ();
		Py_FatalError ("can't initialise module gtkmozedit.base");
	}
	/* attach sub-package base */
	PyModule_AddObject (mozedit_module, "base", mozedit_base_module);
	if (PyErr_Occurred ()) {
		PyErr_Print ();
		Py_FatalError ("can't add object base");
	}

	/* sub-package "gtkmozedit.dom" */
	mozedit_dom_module = Py_InitModule3 ("gtkmozedit.dom", 
					     gtkmozedit_dom_functions, 
					     "gtkmozedit.dom module.");
	dict = PyModule_GetDict (mozedit_dom_module);
	gtkmozedit_dom_register_classes (dict);
	if (PyErr_Occurred ()) {
		PyErr_Print ();
		Py_FatalError ("can't initialise module gtkmozedit.dom");
	}
	/* attach sub-package dom */
	PyModule_AddObject (mozedit_module, "dom", mozedit_dom_module);
	if (PyErr_Occurred ()) {
		PyErr_Print ();
		Py_FatalError ("can't add object gtkmozedit.dom");
	}

	/* sub-package "gtkmozedit.dom.html" */
	mozedit_dom_html_module = Py_InitModule3 ("gtkmozedit.dom.html", 
						  gtkmozedit_dom_html_functions, 
						  "gtkmozedit.dom.html module.");
	dict = PyModule_GetDict (mozedit_dom_html_module);
	gtkmozedit_dom_html_register_classes (dict);
	if (PyErr_Occurred ()) {
		PyErr_Print ();
		Py_FatalError ("can't initialise module gtkmozedit.dom.html");
	}
	/* attach sub-package html */
	PyModule_AddObject (mozedit_dom_module, "html", mozedit_dom_html_module);
	if (PyErr_Occurred ()) {
		PyErr_Print ();
		Py_FatalError ("can't add object gtkmozedit.dom.html");
	}

	/* sub-package "gtkmozedit.dom.css" */
	mozedit_dom_css_module = Py_InitModule3 ("gtkmozedit.dom.css", 
						  gtkmozedit_dom_css_functions, 
						  "gtkmozedit.dom.css module.");
	dict = PyModule_GetDict (mozedit_dom_css_module);
	gtkmozedit_dom_css_register_classes (dict);
	if (PyErr_Occurred ()) {
		PyErr_Print ();
		Py_FatalError ("can't initialise module gtkmozedit.dom.css");
	}
	/* attach sub-package css */
	PyModule_AddObject (mozedit_dom_module, "css", mozedit_dom_css_module);
	if (PyErr_Occurred ()) {
		PyErr_Print ();
		Py_FatalError ("can't add object gtkmozedit.dom.css");
	}

	/* finish root package "gtkmozedit" */
	dict = PyModule_GetDict (mozedit_module);
	gtkmozedit_register_classes (dict);
	if (PyErr_Occurred ()) {
		PyErr_Print ();
		Py_FatalError ("can't initialise module gtkmozedit");
	}
}

G_END_DECLS
