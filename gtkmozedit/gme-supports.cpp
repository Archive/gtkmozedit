/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <gtkmozedit/gtkmozedit-config.h>
#include "nsISupports.h"

#include "gme-supports-private.h"
#include "gme-isupports.h"

enum {
	PROP_0,
	PROP_WRAPPED_PTR,
	_NUM_PROPS
};

static GObjectClass *gme_supports_parent_class = NULL;

static void
instance_init (GmeSupports *self)
{
	self->wrapped_ptr = NULL;
	self->is_disposed = FALSE;
}

static void
interface_init (gpointer iface,
		gpointer iface_data)
{
	GmeISupportsClass *klass = (GmeISupportsClass *) iface;
	klass->query_interface = (GmeISupports* (*)(GmeISupports*, GType)) gme_supports_query_interface;
	klass->get_wrapped_ptr = (void* (*)(GmeISupports*)) gme_supports_get_wrapped_ptr; 
}

static void
instance_dispose (GObject *instance)
{
	GmeSupports *self = GME_SUPPORTS (instance);

	if (self->is_disposed)
		return;

	if (self->wrapped_ptr) NS_RELEASE (self->wrapped_ptr);
	self->wrapped_ptr = NULL;
	self->is_disposed = TRUE;

	gme_supports_parent_class->dispose (G_OBJECT (self));
}

static void
set_property (GObject      *object,
	      guint         prop_id,
	      GValue const *value,
	      GParamSpec   *pspec)
{
	GmeSupports *self = GME_SUPPORTS (object);

	switch (prop_id) {
	case PROP_WRAPPED_PTR:
		self->wrapped_ptr = reinterpret_cast<nsISupports*>(g_value_get_pointer (value));
		NS_ADDREF (self->wrapped_ptr);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
get_property (GObject    *object,
	      guint       prop_id,
	      GValue     *value,
	      GParamSpec *pspec)
{
	GmeSupports *self = GME_SUPPORTS (object);

	switch (prop_id) {
	case PROP_WRAPPED_PTR:
		g_value_set_pointer (value, (gpointer) self->wrapped_ptr);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

gulong 
connect_signal (gpointer     instance, 
		const gchar *detailed_signal, 
		GCallback    handler, 
		gpointer     data)
{
	return g_signal_connect (instance, detailed_signal, handler, data);
}

static void
class_init (GmeSupportsClass *klass)
{
	GObjectClass *gobject_class = G_OBJECT_CLASS (klass);

	/* vfuncs */
	klass->dispose = instance_dispose;
	klass->connect_signal = connect_signal;

	/* hook gobject vfuncs */
	gobject_class->dispose = instance_dispose;

	gme_supports_parent_class = (GObjectClass*) g_type_class_peek_parent (klass);

	gobject_class->set_property = set_property;
	gobject_class->get_property = get_property;

	g_object_class_install_property (gobject_class,
		PROP_WRAPPED_PTR,
		g_param_spec_pointer ("wrapped-ptr", _("Wrapped Pointer"),
			_("Pointer to the wrapped c++ object"),
			(GParamFlags)(G_PARAM_READABLE)));
}

GType
gme_supports_get_gtype (void)
{
        static GType type = 0;
        if (!type) {
                static const GTypeInfo info = {
                        sizeof (GmeSupportsClass),
                        NULL,           /* base_init */
                        NULL,           /* base_finalize */
                        (GClassInitFunc) class_init,
                        NULL,           /* class_finalize */
                        NULL,           /* class_data */
                        sizeof (GmeSupports),
                        0,              /* n_preallocs */
                        (GInstanceInitFunc) instance_init,
                };
		static const GInterfaceInfo isupports_info = {
			(GInterfaceInitFunc) interface_init,	/* interface_init */
			NULL,					/* interface_finalize */
			NULL					/* interface_data */
		};
                type = g_type_register_static (G_TYPE_OBJECT, "GmeSupports", &info, (GTypeFlags)0);
		g_type_add_interface_static (type, GME_TYPE_ISUPPORTS, &isupports_info);
        }
        return type;
}

void 
gme_supports_private_set_wrapped_ptr (GmeSupports *self, 
				      nsISupports *wrapped_ptr)
{
	g_assert (self && wrapped_ptr);
	self->wrapped_ptr = wrapped_ptr;
	NS_ADDREF (self->wrapped_ptr);	
}

GmeISupports* 
gme_supports_query_interface (GmeSupports *self, 
			      GType type)
{
	/* TODO this could be a macro, so we are consistent in every implementation */
	GmeISupportsClass *klass = GME_ISUPPORTS_GET_CLASS (self);
	gpointer wrapped_ptr = klass->get_wrapped_ptr (GME_ISUPPORTS (self));
	GmeISupports *obj = NULL;
	g_assert (wrapped_ptr);

	obj = GME_ISUPPORTS (g_object_new (type, "wrapped-ptr", wrapped_ptr, NULL));
	
	/* check if the new object could handle the wrapped ptr internally */
	wrapped_ptr = NULL;
	g_object_get (G_OBJECT (obj), "wrapped-ptr", &wrapped_ptr, NULL);
	if (!wrapped_ptr) {
		/* downcasting failed */
		g_object_unref (obj);
		obj = NULL;
	}
	return obj;
}

gpointer      
gme_supports_get_wrapped_ptr (GmeSupports *self)
{
	return self->wrapped_ptr;
}

gulong	     
gme_supports_connect_signal (GmeSupports *self, 
			     const gchar *detailed_signal, 
			     GCallback handler, 
			     gpointer data)
{
	g_assert (self);

	GmeSupportsClass *klass = GME_SUPPORTS_GET_CLASS (self);
	return klass->connect_signal (self, detailed_signal, handler, data);
}
