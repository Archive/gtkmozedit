/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <gtkmozedit/gtkmozedit-config.h>

#include "nsISupports.h"
#include "nsCOMPtr.h"
#include "nsICommandManager.h"
#include "nsIEditor.h"
#include "nsIHTMLEditor.h"
#include "nsIInterfaceRequestor.h"
#include "nsIInterfaceRequestorUtils.h"
/* 
#include "nsReadableUtils.h" 
#include "nsUnicharUtils.h"
*/
#include "nsComponentManagerUtils.h"

/* DIRTY HACK required for building against seamonkey */
#ifdef GTKMOZEMBED_VERSION_1_1
/* HACK */
#define nsAString_h___
#define MOZILLA_INTERNAL_API
#endif
#include "nsIAtom.h"

#include <gtkmozedit/gme-editor-private.h>
#include <gtkmozedit/gme-command-manager-private.h>
#include <gtkmozedit/gme-command-params-private.h>
#include <gtkmozedit/gme-observer-private.h>
#include <gtkmozedit/dom/gme-dom-window-private.h>

enum {
	PROP_0,
	PROP_WRAPPED_PTR,
	_NUM_PROPS
};

static GmeSupportsClass *gme_command_manager_parent_class = NULL;

static void
instance_init (GmeCommandManager *self)
{
	self->wrapped_ptr = NULL;
	self->is_disposed = FALSE;
}

static void
instance_dispose (GObject *instance)
{
	GmeCommandManager *self = GME_COMMAND_MANAGER (instance);

	if (self->is_disposed)
		return;

	if (self->wrapped_ptr) NS_RELEASE (self->wrapped_ptr);
	self->wrapped_ptr = NULL;
	self->is_disposed = TRUE;

	gme_command_manager_parent_class->dispose (G_OBJECT (self));
}

static void
set_property (GObject      *object,
	      guint         prop_id,
	      GValue const *value,
	      GParamSpec   *pspec)
{
	GmeCommandManager *self = GME_COMMAND_MANAGER (object);
	nsISupports *ptr = NULL;
	nsCOMPtr<nsICommandManager> cmd_mgr;
	nsresult rv;

	switch (prop_id) {
	case PROP_WRAPPED_PTR:
		ptr = reinterpret_cast<nsISupports*>(g_value_get_pointer (value));
		/* strange, CallQueryInterface seems to fail while the below solution 
		rv = CallQueryInterface (ptr, &self->wrapped_ptr);
		*/
		cmd_mgr = do_GetInterface (ptr, &rv);
		if (NS_SUCCEEDED (rv)) {
			self->wrapped_ptr = cmd_mgr;
			NS_ADDREF (self->wrapped_ptr);
			/* constuction param, init parent */
			gme_supports_private_set_wrapped_ptr (GME_SUPPORTS (self), self->wrapped_ptr);
		}
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
get_property (GObject    *object,
	      guint       prop_id,
	      GValue     *value,
	      GParamSpec *pspec)
{
	GmeCommandManager *self = GME_COMMAND_MANAGER (object);

	switch (prop_id) {
	case PROP_WRAPPED_PTR:
		g_value_set_pointer (value, (gpointer) self->wrapped_ptr);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
class_init (GmeCommandManagerClass *klass)
{
	GObjectClass *gobject_class = G_OBJECT_CLASS (klass);

	/* vfuncs */
	klass->dispose = instance_dispose;

	/* hook gobject vfuncs */
	gobject_class->dispose = instance_dispose;

	gme_command_manager_parent_class = (GmeSupportsClass*) g_type_class_peek_parent (klass);

	gobject_class->set_property = set_property;
	gobject_class->get_property = get_property;

	g_object_class_install_property (gobject_class,
		PROP_WRAPPED_PTR,
		g_param_spec_pointer ("wrapped-ptr", _("Wrapped Pointer"),
			_("Pointer to the wrapped c++ object"),
			(GParamFlags)(G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY)));
}

GType
gme_command_manager_get_gtype (void)
{
        static GType type = 0;
        if (!type) {
                static const GTypeInfo info = {
                        sizeof (GmeCommandManagerClass),
                        NULL,           /* base_init */
                        NULL,           /* base_finalize */
                        (GClassInitFunc) class_init,
                        NULL,           /* class_finalize */
                        NULL,           /* class_data */
                        sizeof (GmeCommandManager),
                        0,              /* n_preallocs */
                        (GInstanceInitFunc) instance_init,
                };
                type = g_type_register_static (GME_TYPE_SUPPORTS, "GmeCommandManager", &info, (GTypeFlags)0);
        }
        return type;
}

void 
gme_command_manager_private_set_wrapped_ptr (GmeCommandManager *self, 
				      nsICommandManager *wrapped_ptr)
{
	g_assert (self && wrapped_ptr);
	self->wrapped_ptr = wrapped_ptr;
	NS_ADDREF (self->wrapped_ptr);
	/* constuction param, init parent */
	gme_supports_private_set_wrapped_ptr (GME_SUPPORTS (self), self->wrapped_ptr);
}

GmeCommandManager* 
gme_command_manager_new (nsICommandManager *wrapped_ptr)
{
	return GME_COMMAND_MANAGER (g_object_new (GME_TYPE_COMMAND_MANAGER, "wrapped-ptr", wrapped_ptr, NULL));
}

gboolean 
gme_command_manager_add_command_observer (GmeCommandManager *self, 
					  GmeObserver *observer, 
					  const gchar *cmd)
{
	nsresult rv;
	g_assert (self && observer);

	/* TODO 
	rv = self->wrapped_ptr->AddCommandObserver (observer->wrapped_ptr, cmd); 
	*/
	return NS_SUCCEEDED (rv);
}

gboolean 
gme_command_manager_remove_command_observer (GmeCommandManager *self, 
					     GmeObserver *observer, 
					     const gchar *cmd)
{
	nsresult rv;
	g_assert (self && observer);

	rv = self->wrapped_ptr->RemoveCommandObserver (observer->wrapped_ptr, cmd);
	return NS_SUCCEEDED (rv);
}

gboolean 
gme_command_manager_do_command  (GmeCommandManager *self, 
				 const gchar *cmd, 
				 GmeCommandParams *params, 
				 GmeDOMWindow *win)
{
	nsresult rv;
	g_assert (self && params && win);

	rv = self->wrapped_ptr->DoCommand (cmd, params->wrapped_ptr, win->wrapped_ptr);
	return NS_SUCCEEDED (rv);
}

gboolean   
gme_command_manager_get_command_state (GmeCommandManager *self, 
				       const gchar *cmd, 
				       GmeDOMWindow *win, 
				       GmeCommandParams *params)
{
	nsresult rv;
	g_assert (self && params && win);

	rv = self->wrapped_ptr->GetCommandState (cmd, win->wrapped_ptr, params->wrapped_ptr);
	return NS_SUCCEEDED (rv);	
}

gboolean 
gme_command_manager_is_command_enabled (GmeCommandManager *self, 
					const gchar *cmd, 
					GmeDOMWindow *win)
{
	gboolean is_enabled;
	nsresult rv;
	g_assert (self && win);

	rv = self->wrapped_ptr->IsCommandEnabled (cmd, win->wrapped_ptr, &is_enabled);
	if (NS_SUCCEEDED (rv)) {
		return is_enabled;
	}
	return FALSE;
}

gboolean 
gme_command_manager_is_command_supported (GmeCommandManager *self, 
					  const gchar *cmd, 
					  GmeDOMWindow *win)
{
	gboolean is_supported;
	nsresult rv;
	g_assert (self && win);

	rv = self->wrapped_ptr->IsCommandSupported (cmd, win->wrapped_ptr, &is_supported);
	if (NS_SUCCEEDED (rv)) {
		return is_supported;
	}
	return FALSE;
}

/* TODO maybe these should go in a header */
#define STATE_ENABLED  "state_enabled"
#define STATE_ALL "state_all"
#define STATE_ANY "state_any"
#define STATE_MIXED "state_mixed"
#define STATE_BEGIN "state_begin"
#define STATE_END "state_end"
#define STATE_ATTRIBUTE "state_attribute"
#define STATE_DATA "state_data"

static nsresult
remove_one_property (nsIHTMLEditor *html_editor,
		     const gchar *prop, 
		     const gchar *attr)
{
	nsCOMPtr<nsIAtom> style_atom = do_GetAtom(prop);
	NS_ConvertUTF8toUTF16 aAttr (attr);

	if (!html_editor) {
		g_warning ("Editor not initialized");
		return NS_ERROR_NOT_INITIALIZED;
	}

	/// XXX Hack alert! Look in nsIEditProperty.h for this
	if (! style_atom) {
		g_warning ("Out of memory");
		return NS_ERROR_OUT_OF_MEMORY;
	}

	return html_editor->RemoveInlineProperty (style_atom, aAttr);
}

/*
static nsresult
remove_one_property (nsIHTMLEditor *aEditor,
		     const nsString &aProp, 
		     const nsString &aAttr)
{
  if (!aEditor) 
    return NS_ERROR_NOT_INITIALIZED;

  /// XXX Hack alert! Look in nsIEditProperty.h for this
  nsCOMPtr<nsIAtom> styleAtom = do_GetAtom(aProp);
  if (! styleAtom) 
    return NS_ERROR_OUT_OF_MEMORY;

  return aEditor->RemoveInlineProperty(styleAtom, aAttr);
}
*/

/*
   the name of the attribute here should be the contents of the appropriate
   tag, e.g. 'b' for bold, 'i' for italics.
*/
static nsresult
remove_text_property (nsIEditor *editor, 
		      const gchar *prop, 
		      const gchar *attr)
{
	nsCOMPtr<nsIHTMLEditor> html_editor = NULL;
	NS_ConvertUTF8toUTF16 allStr (prop);
	NS_ConvertUTF8toUTF16 all ("all");
	PRBool doingAll;
	nsresult err;

	if (!editor) {
		g_warning ("Editor not initialized");
		return NS_ERROR_NOT_INITIALIZED;
	}

	html_editor = do_QueryInterface (editor);
	if (!editor) {
		g_warning ("Invalid argument `editor'");
		return NS_ERROR_INVALID_ARG;
	}

	/* 
	   OK, I'm really hacking now. This is just so that 
	   we can accept 'all' as input.  
	*/
	/* TODO bails linking; we can only use lowercase 
	ToLowerCase (allStr);
	*/
	doingAll = allStr.Equals (all);
	err = NS_OK;

	if (doingAll) {
		err = html_editor->RemoveAllInlineProperties ();
	}
	else {
		err = remove_one_property (html_editor, prop, attr);
	}

	return err;
}

/*
   the name of the attribute here should be the contents of the appropriate
   tag, e.g. 'b' for bold, 'i' for italics.
*/
static nsresult
set_text_property (nsIEditor *editor, 
		   const gchar *prop, 
                   const gchar *attr, 
		   const gchar *value)
{
	static const gchar sEmptyStr = '\0';
	nsCOMPtr<nsIAtom> style_atom;
	nsCOMPtr<nsIHTMLEditor> html_editor;
	nsresult err;

	if (!editor) {
		g_warning ("Invalid argument `editor'");		
		return NS_ERROR_NOT_INITIALIZED;
	}

	/* XXX Hack alert! Look in nsIEditProperty.h for this */
	style_atom = do_GetAtom (prop);
	if (!style_atom) {
		g_warning ("Out of memory");		
		return NS_ERROR_OUT_OF_MEMORY;
	}

	err = NS_NOINTERFACE;

	html_editor = do_QueryInterface (editor, &err);
	if (html_editor) {
		err = html_editor->SetInlineProperty (style_atom,
			NS_ConvertUTF8toUTF16 (attr?attr:&sEmptyStr),
			NS_ConvertUTF8toUTF16 (value?value:&sEmptyStr));
	}

	return err;
}

static nsresult
get_current_state (nsIEditor *editor, 
		   const gchar* tag_name,
		   nsICommandParams *aParams)
{
	NS_ASSERTION (editor, "Need editor here");
	nsCOMPtr<nsIHTMLEditor> html_editor = do_QueryInterface(editor);
	nsCOMPtr<nsIAtom> style_atom;
	PRBool firstOfSelectionHasProp = PR_FALSE;
	PRBool anyOfSelectionHasProp = PR_FALSE;
	PRBool allOfSelectionHasProp = PR_FALSE;
	nsresult rv;

	if (!html_editor) {
		g_warning ("Editor not initialized");
		return NS_ERROR_NOT_INITIALIZED;
	}

	rv = NS_OK;

	style_atom = do_GetAtom (tag_name);
	rv = html_editor->GetInlineProperty (style_atom, EmptyString (), 
					EmptyString (), 
					&firstOfSelectionHasProp, 
					&anyOfSelectionHasProp, 
					&allOfSelectionHasProp);

	aParams->SetBooleanValue (STATE_ENABLED, NS_SUCCEEDED (rv));
	aParams->SetBooleanValue (STATE_ALL, allOfSelectionHasProp);
	aParams->SetBooleanValue (STATE_ANY, anyOfSelectionHasProp);
	aParams->SetBooleanValue (STATE_MIXED, anyOfSelectionHasProp && !allOfSelectionHasProp);
	aParams->SetBooleanValue (STATE_BEGIN, firstOfSelectionHasProp);
	aParams->SetBooleanValue (STATE_END, allOfSelectionHasProp); /* not completely accurate */
	return NS_OK;
}

gboolean 
gme_command_manager_apply_state (GmeCommandManager *self, 
				  GmeEditor *editor, 
				  const gchar* tag_name)
{
	/* TODO */
}

gboolean
gme_command_manager_toggle_state (GmeCommandManager *self, 
				  GmeEditor *editor, 
				  const gchar *tag_name)
{
	nsCOMPtr<nsIHTMLEditor> html_editor = do_QueryInterface(editor->wrapped_ptr);
	if (!html_editor) {
		g_warning ("No interface");
		return NS_ERROR_NO_INTERFACE;
	}

	//create some params now...
	nsresult rv;
	nsCOMPtr<nsICommandParams> params =
	do_CreateInstance(NS_COMMAND_PARAMS_CONTRACTID,&rv);
	if (NS_FAILED(rv) || !params) {
		return rv;
	}

	// tags "href" and "name" are special cases in the core editor 
	// they are used to remove named anchor/link and shouldn't be used for insertion
	NS_ConvertUTF8toUTF16 tagName (tag_name);
	NS_ConvertUTF8toUTF16 href ("href");
	NS_ConvertUTF8toUTF16 name ("name");
	PRBool doTagRemoval;
	if (tagName.Equals (href) || tagName.Equals (name)) {
		doTagRemoval = PR_TRUE;
	}
	else {
		// check current selection; set doTagRemoval if formatting should be removed
		rv = get_current_state (editor->wrapped_ptr, tag_name, params);
		if (NS_FAILED(rv)) 
		return rv;
		rv = params->GetBooleanValue(STATE_ALL, &doTagRemoval);
		if (NS_FAILED(rv)) 
		return rv;
	}

	if (doTagRemoval) {
		rv = remove_text_property (editor->wrapped_ptr, tag_name, nsnull);
	}
	else {
		// Superscript and Subscript styles are mutually exclusive
		NS_ConvertUTF8toUTF16 sub ("sub");
		NS_ConvertUTF8toUTF16 sup ("sup");
		nsEmbedString removeName; 
		editor->wrapped_ptr->BeginTransaction();

		if (tagName.Equals (sub)) {
			removeName.Assign (sup);
			rv = remove_text_property (editor->wrapped_ptr, tag_name, nsnull);
		} 
		else if (tagName.Equals (sup)) {
			removeName.Assign (sub);
			rv = remove_text_property (editor->wrapped_ptr, tag_name, nsnull);
		}
		if (NS_SUCCEEDED(rv)) {
			rv = set_text_property (editor->wrapped_ptr, tag_name, nsnull, nsnull);
		}
		editor->wrapped_ptr->EndTransaction();
	}

	return NS_SUCCEEDED (rv);
}
