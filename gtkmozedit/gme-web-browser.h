/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __GME_WEB_BROWSER_H__
#define __GME_WEB_BROWSER_H__

#include <glib.h>
#include <glib-object.h>
#include <glib/gmacros.h>
#include <gtkmozedit/gme-forward.h>
#include <gtkmozedit/dom/gme-dom-forward.h>
#include <gtkmozedit/dom/gme-dom-window.h>

G_BEGIN_DECLS

#define GME_TYPE_WEB_BROWSER                  (gme_web_browser_get_gtype ())
#define GME_WEB_BROWSER(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), GME_TYPE_WEB_BROWSER, GmeWebBrowser))
#define GME_WEB_BROWSER_CLASS(klass)          (G_TYPE_CHECK_CLASS_CAST ((klass), GME_TYPE_WEB_BROWSER, GmeWebBrowserClass))
#define GME_IS_WEB_BROWSER(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GME_TYPE_WEB_BROWSER))
#define GME_IS_WEB_BROWSER_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), GME_TYPE_WEB_BROWSER))
#define GME_WEB_BROWSER_GET_CLASS(obj)        (G_TYPE_INSTANCE_GET_CLASS ((obj), GME_TYPE_WEB_BROWSER, GmeWebBrowserClass))

typedef struct _GmeWebBrowserClass GmeWebBrowserClass;

GType gme_web_browser_get_gtype (void);

/**
 * The nsIWebBrowser interface is implemented by web browser objects.
 * Embedders use this interface during initialisation to associate
 * the new web browser instance with the embedders chrome and
 * to register any listeners. The interface may also be used at runtime
 * to obtain the content DOM window and from that the rest of the DOM.
 *
 * @status FROZEN
 */

/*
    void addWebBrowserListener(in nsIWeakReference aListener, in nsIIDRef aIID);
    void removeWebBrowserListener(in nsIWeakReference aListener, in nsIIDRef aIID);
    attribute nsIWebBrowserChrome containerWindow;
    attribute nsIURIContentListener parentURIContentListener;
*/

GmeDOMWindow* gme_web_browser_get_content_dom_window (GmeWebBrowser *self);

G_END_DECLS

#endif /* __GME_WEB_BROWSER_H__ */
