/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <gtkmozedit/gtkmozedit-config.h>

#include "nsIObserver.h"
#include "nsISupportsUtils.h"

#include <gtkmozedit/gme-observer-private.h>
#include <gtkmozedit/gme-supports-private.h>
#include <gtkmozedit/utils/gme-marshalers.h>

static void observe_cb (GmeObserver *self, GmeSupports *subject, const gchar *topic, const gchar *data);

enum {
	PROP_0,
	PROP_WRAPPED_PTR,
	_NUM_PROPS
};

enum {
	OBSERVE,
	_LAST_SIGNAL
};

static guint gme_observer_signals[_LAST_SIGNAL] = { 0 };
static GmeSupportsClass *gme_observer_parent_class = NULL;

/* TODO this crashes, so it's use in gme-command-manager.cpp is commented out */
class ObserverImpl : public nsIObserver {
public: 
	ObserverImpl (GmeObserver *obs) 
	: observer (obs)
	{
		mRefCnt = 1;
	}
	~ObserverImpl () 
	{
		/* lifecycle is controlled by the wrapping gobject
		g_object_unref (G_OBJECT (observer)); 
		*/
	}
	nsresult Observe (nsISupports *aSubject, const char *aTopic, const PRUnichar *aData) 
	{
		GmeSupports *subject = GME_SUPPORTS (g_object_new (GME_TYPE_SUPPORTS, "wrapped_ptr", aSubject, NULL));
		nsEmbedString t;
		nsEmbedString d (aData);
		gchar *topic = NULL; 
		gchar *data = NULL;

		t = NS_ConvertUTF8toUTF16 (aTopic);
		topic = g_strdup (NS_ConvertUTF16toUTF8 (t).get ());
		data = g_strdup (NS_ConvertUTF16toUTF8 (d).get ());
		observe_cb (observer, subject, topic, data);
		g_free (topic);
		g_free (data);
		g_object_unref (G_OBJECT (subject));
	}
	NS_IMETHODIMP QueryInterface(const nsIID &aIID, void **aResult)
	{
		#warning implement
		/*
		if (aResult == NULL) {
			return NS_ERROR_NULL_POINTER;
		}
		*aResult = NULL;
		if (aIID.Equals(kISupportsIID)) {
			*aResult = (void *) this;
		}
		if (*aResult == NULL) {
			return NS_ERROR_NO_INTERFACE;
		}
		AddRef();
		*/
		return NS_OK;
	}
	NS_IMETHODIMP_(nsrefcnt) AddRef ()  
	{
		return ++mRefCnt;
	}
	NS_IMETHODIMP_(nsrefcnt) Release ()
	{
		if (--mRefCnt == 0) {
			delete this;
			return 0;
		}
		return mRefCnt;
	}
private: 
	GmeObserver *observer;
	gint mRefCnt;
};

static void
instance_init (GmeObserver *self)
{
	self->wrapped_ptr = new ObserverImpl (self);
	self->is_disposed = FALSE;

	/* for completeness init parent */
	gme_supports_private_set_wrapped_ptr (GME_SUPPORTS (self), self->wrapped_ptr);
}

static void
instance_dispose (GObject *instance)
{
	GmeObserver *self = GME_OBSERVER (instance);

	if (self->is_disposed)
		return;

	if (self->wrapped_ptr) NS_RELEASE (self->wrapped_ptr);
	self->wrapped_ptr = NULL;
	self->is_disposed = TRUE;

	gme_observer_parent_class->dispose (G_OBJECT (self));
}

static void
set_property (GObject      *object,
	      guint         prop_id,
	      GValue const *value,
	      GParamSpec   *pspec)
{
	GmeObserver *self = GME_OBSERVER (object);
	nsISupports *ptr = NULL;
	nsresult rv;

	switch (prop_id) {
	/* the object's wrapped_ptr is a private subclass, it cannot be set from outside
	case PROP_WRAPPED_PTR:
		ptr = reinterpret_cast<nsISupports*>(g_value_get_pointer (value));
		rv = CallQueryInterface (ptr, &self->wrapped_ptr);
		if (NS_SUCCEEDED (rv) && self->wrapped_ptr) {
			NS_ADDREF (self->wrapped_ptr);
		}
		break;
	*/
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
get_property (GObject    *object,
	      guint       prop_id,
	      GValue     *value,
	      GParamSpec *pspec)
{
	GmeObserver *self = GME_OBSERVER (object);

	switch (prop_id) {
	case PROP_WRAPPED_PTR:
		g_value_set_pointer (value, (gpointer) self->wrapped_ptr);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
class_init (GmeObserverClass *klass)
{
	GObjectClass *gobject_class = G_OBJECT_CLASS (klass);

	/* vfuncs */
	klass->dispose = instance_dispose;

	/* hook gobject vfuncs */
	gobject_class->dispose = instance_dispose;

	gme_observer_parent_class = (GmeSupportsClass*) g_type_class_peek_parent (klass);

	gobject_class->set_property = set_property;
	gobject_class->get_property = get_property;

	g_object_class_install_property (gobject_class,
		PROP_WRAPPED_PTR,
		g_param_spec_pointer ("wrapped-ptr", _("Wrapped Pointer"),
			_("Pointer to the wrapped c++ object"),
			(GParamFlags)(G_PARAM_READABLE)));

	gme_observer_signals [OBSERVE] =
		g_signal_new (
			"observe",
			G_OBJECT_CLASS_TYPE (klass),
			G_SIGNAL_RUN_LAST,
			G_STRUCT_OFFSET (GmeObserverClass, observe),
			NULL, NULL,
			gme_marshal_VOID__POINTER_STRING_STRING,
			G_TYPE_NONE, 3, G_TYPE_POINTER, G_TYPE_STRING, G_TYPE_STRING);
}

GType
gme_observer_get_gtype (void)
{
        static GType type = 0;
        if (!type) {
                static const GTypeInfo info = {
                        sizeof (GmeObserverClass),
                        NULL,           /* base_init */
                        NULL,           /* base_finalize */
                        (GClassInitFunc) class_init,
                        NULL,           /* class_finalize */
                        NULL,           /* class_data */
                        sizeof (GmeObserver),
                        0,              /* n_preallocs */
                        (GInstanceInitFunc) instance_init,
                };
                type = g_type_register_static (GME_TYPE_SUPPORTS, "GmeObserver", &info, (GTypeFlags)0);
        }
        return type;
}

void 
gme_observer_private_set_wrapped_ptr (GmeObserver *self, 
				      nsIObserver *wrapped_ptr)
{
	g_assert (self && wrapped_ptr);
	self->wrapped_ptr = wrapped_ptr;
	NS_ADDREF (self->wrapped_ptr);
	/* constuction param, init parent */
	gme_supports_private_set_wrapped_ptr (GME_SUPPORTS (self), self->wrapped_ptr);
}

GmeObserver* 
gme_observer_new (void)
{
	return GME_OBSERVER (g_object_new (GME_TYPE_OBSERVER, NULL));
}

static void 
observe_cb (GmeObserver *self, GmeSupports *subject, const gchar *topic, const gchar *data)
{
	g_signal_emit (G_OBJECT (self), gme_observer_signals [OBSERVE], 0, subject, topic, data);
}
