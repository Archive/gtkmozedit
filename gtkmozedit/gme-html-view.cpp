/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <gtkmozedit/gtkmozedit-config.h>

#include <gtkmozembed.h>
#include <gtkmozembed_internal.h>
#include <gtkmozedit/gme-html-view-private.h>
#include <gtkmozedit/gme-web-browser-private.h>
#include <gtkmozedit/dom/gme-dom-window-private.h>
#include <gtkmozedit/gme-supports.h>

#include "nsCOMPtr.h"
#include "nsComponentManagerUtils.h"
#include "nsISupports.h"
#include "nsIInterfaceRequestor.h"
#include "nsIInterfaceRequestorUtils.h"
#include "nsIDOMWindow.h"
#include "nsIEditingSession.h"
#include "nsICommandManager.h"
#include "nsIWebBrowser.h"
#include "nsIWebBrowserPersist.h"
#include "nsILocalFile.h"

enum {
	PROP_0,
	PROP_WRAPPED_PTR,
	_NUM_PROPS
};

static GtkMozEmbedClass *gme_html_view_parent_class = NULL;

void
realize_cb (GmeHTMLView *self,
	    gpointer data)
{
	nsCOMPtr<nsIDOMWindow> dom_window;
	nsCOMPtr<nsIEditingSession> edit_session;
	nsCOMPtr<nsICommandManager> cmd_mgr; 
	nsresult rv;

	gtk_moz_embed_get_nsIWebBrowser (GTK_MOZ_EMBED (self), getter_AddRefs (self->wrapped_ptr));
	self->wrapped_ptr->GetContentDOMWindow (getter_AddRefs(dom_window));
	g_assert (dom_window);

	edit_session = do_GetInterface (self->wrapped_ptr, &rv);
	g_assert (edit_session);

	cmd_mgr = do_GetInterface (self->wrapped_ptr, &rv);
	/* NOTE: For Moz 1.1 and below, the middle parameter was not used */
	edit_session->MakeWindowEditable (dom_window, "html", PR_TRUE);

	gtk_moz_embed_load_url (GTK_MOZ_EMBED (self), self->pending_url);
}

static void
instance_init (GmeHTMLView *self)
{
	self->pending_url = g_strdup ("about:blank");

	g_signal_connect (G_OBJECT (self), "realize", G_CALLBACK (realize_cb), NULL);
}

static void
interface_init (gpointer iface,
		gpointer iface_data)
{
	GmeISupportsClass *klass = (GmeISupportsClass *) iface;
	klass->query_interface = (GmeISupports* (*)(GmeISupports*, GType)) gme_html_view_query_interface;
	klass->get_wrapped_ptr = (void* (*)(GmeISupports*)) gme_html_view_get_wrapped_ptr;
}

static void
instance_destroy (GtkObject *instance)
{
	GmeHTMLView *self = GME_HTML_VIEW (instance);

	if (self->pending_url) {
		g_free (self->pending_url);
		self->pending_url = NULL;
	}

	if (self->wrapped_ptr) {		
		self->wrapped_ptr = NULL;
	}
}

static void
get_property (GObject    *object,
	      guint       prop_id,
	      GValue     *value,
	      GParamSpec *pspec)
{
	GmeHTMLView *self = GME_HTML_VIEW (object);

	switch (prop_id) {
	case PROP_WRAPPED_PTR:
		g_value_set_pointer (value, (gpointer) self->wrapped_ptr);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
class_init (GmeHTMLViewClass *klass)
{
	GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
	GtkObjectClass *object_class = GTK_OBJECT_CLASS (klass);

	gme_html_view_parent_class = (GtkMozEmbedClass*) gtk_type_class (GTK_TYPE_MOZ_EMBED);

	object_class->destroy = instance_destroy;
	gobject_class->get_property = get_property;

	g_object_class_install_property (gobject_class,
		PROP_WRAPPED_PTR,
		g_param_spec_pointer ("wrapped-ptr", _("Wrapped Pointer"),
			_("Pointer to the wrapped c++ object"),
			(GParamFlags)(G_PARAM_READABLE)));
}

GType
gme_html_view_get_gtype (void)
{
        static GType type = 0;
        if (!type) {
                static const GTypeInfo info = {
                        sizeof (GmeHTMLViewClass),
                        NULL,           /* base_init */
                        NULL,           /* base_finalize */
                        (GClassInitFunc) class_init,
                        NULL,           /* class_finalize */
                        NULL,           /* class_data */
                        sizeof (GmeHTMLView),
                        0,              /* n_preallocs */
                        (GInstanceInitFunc) instance_init,
                };
		static const GInterfaceInfo isupports_info = {
			(GInterfaceInitFunc) interface_init,	/* interface_init */
			NULL,					/* interface_finalize */
			NULL					/* interface_data */
		};
                type = g_type_register_static (GTK_TYPE_MOZ_EMBED, "GmeHTMLView", &info, (GTypeFlags)0);
		g_type_add_interface_static (type, GME_TYPE_ISUPPORTS, &isupports_info);
        }
        return type;
}

GtkWidget *
gme_html_view_new ()
{
	return GTK_WIDGET (g_object_new (GME_TYPE_HTML_VIEW, NULL));
}

gboolean 
gme_html_view_do_command (GmeHTMLView *self,
		      const gchar *cmd, 
		      const gchar *val)
{
	nsCOMPtr<nsICommandParams> params;
	nsCOMPtr<nsIDOMWindow> dom_window;
	nsCOMPtr<nsICommandManager> cmd_mgr;
	nsresult rv;
	g_assert (self);

	g_return_val_if_fail (self->wrapped_ptr, FALSE);

	params = do_CreateInstance (NS_COMMAND_PARAMS_CONTRACTID, &rv);
	g_return_val_if_fail (params, FALSE);

	//if the attribute is blank, or not necessary, this doesn't cause any harm
	if (strcmp (cmd, "cmd_insertHTML") == 0) {
		params->SetCStringValue ("state_data", val);
	}
	params->SetCStringValue ("state_attribute", val);

	self->wrapped_ptr->GetContentDOMWindow (getter_AddRefs (dom_window));
	g_return_val_if_fail (dom_window, FALSE);

	cmd_mgr = do_QueryInterface (self->wrapped_ptr, &rv);
	g_return_val_if_fail (cmd_mgr, FALSE);

	rv = cmd_mgr->DoCommand (cmd, params, dom_window);
	if (NS_FAILED (rv)) {
		g_error ("Unable to execute editing command.\n");
	}

	params = nsnull;
}

gboolean   
gme_html_view_is_command_enabled (GmeHTMLView *self, 
				 const gchar *cmd)
{
	nsCOMPtr<nsIDOMWindow> dom_window;
	nsCOMPtr<nsICommandManager> cmd_mgr;
	gboolean is_enabled;
	nsresult rv;

	rv = self->wrapped_ptr->GetContentDOMWindow (getter_AddRefs (dom_window));
	g_assert (NS_SUCCEEDED (rv));

	cmd_mgr = do_GetInterface (self->wrapped_ptr, &rv);
	g_assert (NS_SUCCEEDED (rv));

	rv = cmd_mgr->IsCommandEnabled (cmd, dom_window, &is_enabled);
	if (NS_SUCCEEDED (rv)) {
		return is_enabled;
	}
	return FALSE;
}

gboolean   
gme_html_view_is_command_supported (GmeHTMLView *self, const gchar *cmd)
{
	nsCOMPtr<nsIDOMWindow> dom_window;
	nsCOMPtr<nsICommandManager> cmd_mgr;
	gboolean is_supported;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetContentDOMWindow (getter_AddRefs (dom_window));
	g_assert (NS_SUCCEEDED (rv));

	cmd_mgr = do_GetInterface (self->wrapped_ptr, &rv);
	g_assert (NS_SUCCEEDED (rv));

	rv = cmd_mgr->IsCommandSupported (cmd, dom_window, &is_supported);
	if (NS_SUCCEEDED (rv)) {
		return is_supported;
	}
	return FALSE;	
}

void
gme_html_view_load (GmeHTMLView *self,
		   const gchar *url)
{
	g_return_if_fail (self && url);

	if (GTK_WIDGET_REALIZED (self)) {
		gtk_moz_embed_load_url (GTK_MOZ_EMBED (self), url);
	}
	else {
		self->pending_url = g_strdup (url);
	}
}

gboolean 
gme_html_view_save (GmeHTMLView *self,
		   const gchar *filename, 
		   gboolean save_files)
{
	//Works, but has troubles with frames pages

	gchar *folder = g_strdup_printf ("%s-files", filename);
	nsCOMPtr<nsILocalFile> file;
	nsCOMPtr<nsILocalFile> data;
	PRUint32 flags;

	// Save the file
	nsCOMPtr<nsIWebBrowserPersist> persist (do_QueryInterface (self->wrapped_ptr));
	g_return_val_if_fail (persist, FALSE);

	PRUint32 currentState;
	persist->GetCurrentState (&currentState);
	if (currentState == nsIWebBrowserPersist::PERSIST_STATE_SAVING) {
		//still performing a previous save - can't save again
		return FALSE; 
	}
	NS_NewNativeLocalFile (nsDependentCString (filename), PR_TRUE, getter_AddRefs (file));

	NS_NewNativeLocalFile(nsDependentCString (folder), PR_TRUE, getter_AddRefs (data));
	persist->GetPersistFlags (&flags);
	if (!(flags & nsIWebBrowserPersist::PERSIST_FLAGS_REPLACE_EXISTING_FILES)) {
		persist->SetPersistFlags (nsIWebBrowserPersist::PERSIST_FLAGS_REPLACE_EXISTING_FILES);
	}
	if (save_files) {
		persist->SaveDocument (nsnull, file, data, nsnull, 0, 0);
	}
	else {
		//Hack for a weird Mozilla bug - if you pass nsnull for the data
		//property, (meaning just save the HTML) it will finish saving properly 
		//but internally the save operation is never marked as finished. We need 
		//to explicitly tell it to cancel the (finished) save before we can save again.
		//Make sense? Didn't think so. But it works! =)
		if (currentState == nsIWebBrowserPersist::PERSIST_STATE_READY) {
			persist->CancelSave ();
		}	
		persist->SaveDocument (nsnull, file, nsnull, nsnull, 0, 0);
	}
	//persist->SaveCurrentURI(file);

	return TRUE;
}

GmeWebBrowser* 
gme_html_view_get_web_browser (GmeHTMLView *self)
{
	GmeWebBrowser *browser = NULL;
	g_assert (self);

	browser = gme_web_browser_new (self->wrapped_ptr);
	return browser;
}

GmeDOMWindow* 
gme_html_view_get_content_dom_window (GmeHTMLView *self)
{
	nsIDOMWindow *w = NULL;
	GmeDOMWindow *window = NULL;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetContentDOMWindow (&w);
	if (NS_SUCCEEDED (rv)) {
		window = gme_dom_window_new (w);
	}
	return window;
}

GmeISupports* 
gme_html_view_query_interface (GmeHTMLView *self, 
			      GType type)
{
	/* TODO this could be a macro, so we are consistent in every implementation */
	GmeISupportsClass *klass = GME_ISUPPORTS_GET_CLASS (self);
	gpointer wrapped_ptr = klass->get_wrapped_ptr (GME_ISUPPORTS (self));
	GmeISupports *obj = NULL;
	g_assert (wrapped_ptr);

	obj = GME_ISUPPORTS (g_object_new (type, "wrapped-ptr", wrapped_ptr, NULL));
	
	/* check if the new object could handle the wrapped ptr internally */
	wrapped_ptr = NULL;
	g_object_get (G_OBJECT (obj), "wrapped-ptr", &wrapped_ptr, NULL);
	if (!wrapped_ptr) {
		/* downcasting failed */
		g_object_unref (obj);
		obj = NULL;
	}
	return obj;
}

gpointer      
gme_html_view_get_wrapped_ptr (GmeHTMLView *self)
{
	return self->wrapped_ptr;
}
