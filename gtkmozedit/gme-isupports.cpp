/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <gtkmozedit/gtkmozedit-config.h>
#include "nsISupports.h"

#include "gme-isupports.h"

static void
interface_init (gpointer iface,
		gpointer iface_data)
{
	GmeISupportsClass *klass = (GmeISupportsClass *) iface;
	klass->query_interface = gme_isupports_query_interface;
	klass->get_wrapped_ptr = gme_isupports_get_wrapped_ptr;
}

static void
base_init (gpointer iface)
{
	static gboolean initialized = FALSE;

	if (!initialized) {
		/* create interface signals here. */
		initialized = TRUE;
	}
}

GType
gme_isupports_get_gtype (void)
{
	static GType type = 0;
	if (!type) {
		static const GTypeInfo info = {
			sizeof (GmeISupportsClass),
			base_init,   /* base_init */
			NULL,   /* base_finalize */
			NULL,   /* class_init */
			NULL,   /* class_finalize */
			NULL,   /* class_data */
			0,
			0,      /* n_preallocs */
			NULL    /* instance_init */
		};
		static const GInterfaceInfo isupports_info = {
			(GInterfaceInitFunc) interface_init,    /* interface_init */
			NULL,					/* interface_finalize */
			NULL					/* interface_data */
		};
		type = g_type_register_static (G_TYPE_INTERFACE, "GmeISupports", &info, (GTypeFlags)0);
		/*g_type_add_interface_static (type, GME_TYPE_ISUPPORTS, &isupports_info);*/
	}
	return type;
}

GmeISupports* 
gme_isupports_query_interface (GmeISupports *self, GType type)
{
	return GME_ISUPPORTS_GET_CLASS (self)->query_interface (self, type);
}

gpointer      
gme_isupports_get_wrapped_ptr (GmeISupports *self)
{
	return GME_ISUPPORTS_GET_CLASS (self)->get_wrapped_ptr (self);
}
