/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __GME_COMMAND_MANAGER_H__
#define __GME_COMMAND_MANAGER_H__

#include <glib.h>
#include <glib-object.h>
#include <glib/gmacros.h>
#include <gtkmozedit/gme-forward.h>
#include <gtkmozedit/gme-command-manager.h>
#include <gtkmozedit/gme-command-params.h>
#include <gtkmozedit/gme-observer.h>
#include <gtkmozedit/dom/gme-dom-forward.h>

G_BEGIN_DECLS

#define GME_TYPE_COMMAND_MANAGER                  (gme_command_manager_get_gtype ())
#define GME_COMMAND_MANAGER(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), GME_TYPE_COMMAND_MANAGER, GmeCommandManager))
#define GME_COMMAND_MANAGER_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST ((klass), GME_TYPE_COMMAND_MANAGER, GmeCommandManagerClass))
#define GME_IS_COMMAND_MANAGER(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GME_TYPE_COMMAND_MANAGER))
#define GME_IS_COMMAND_MANAGER_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), GME_TYPE_COMMAND_MANAGER))
#define GME_COMMAND_MANAGER_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS ((obj), GME_TYPE_COMMAND_MANAGER, GmeCommandManagerClass))

typedef struct _GmeCommandManagerClass GmeCommandManagerClass;

GType gme_command_manager_get_gtype (void);

gboolean gme_command_manager_add_command_observer (GmeCommandManager *self, GmeObserver *observer, const gchar *cmd);
gboolean gme_command_manager_remove_command_observer (GmeCommandManager *self, GmeObserver *observer, const gchar *cmd);
gboolean gme_command_manager_do_command  (GmeCommandManager *self, const gchar *cmd, GmeCommandParams *params, GmeDOMWindow *win);
gboolean gme_command_manager_get_command_state (GmeCommandManager *self, const gchar *cmd, GmeDOMWindow *win, GmeCommandParams *params);
gboolean gme_command_manager_is_command_enabled (GmeCommandManager *self, const gchar *cmd, GmeDOMWindow *win);
gboolean gme_command_manager_is_command_supported (GmeCommandManager *self, const gchar *cmd, GmeDOMWindow *win);
gboolean gme_command_manager_toggle_state (GmeCommandManager *self, GmeEditor *editor, const gchar* tag_name);

G_END_DECLS

#endif /* __GME_COMMAND_MANAGER_H__ */
