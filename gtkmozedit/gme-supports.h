/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __GME_SUPPORTS_H__
#define __GME_SUPPORTS_H__

#include <glib.h>
#include <glib-object.h>
#include <glib/gmacros.h>
#include <gtkmozedit/gme-forward.h>
#include <gtkmozedit/gme-isupports.h>

G_BEGIN_DECLS

#define GME_TYPE_SUPPORTS                  (gme_supports_get_gtype ())
#define GME_SUPPORTS(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), GME_TYPE_SUPPORTS, GmeSupports))
#define GME_SUPPORTS_CLASS(klass)          (G_TYPE_CHECK_CLASS_CAST ((klass), GME_TYPE_SUPPORTS, GmeSupportsClass))
#define GME_IS_SUPPORTS(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GME_TYPE_SUPPORTS))
#define GME_IS_SUPPORTS_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), GME_TYPE_SUPPORTS))
#define GME_SUPPORTS_GET_CLASS(obj)        (G_TYPE_INSTANCE_GET_CLASS ((obj), GME_TYPE_SUPPORTS, GmeSupportsClass))

typedef struct _GmeSupportsClass GmeSupportsClass;

GType gme_supports_get_gtype (void);

/* interface GmeISupports */
GmeISupports* gme_supports_query_interface (GmeSupports *instance, GType interface);
gpointer     gme_supports_get_wrapped_ptr (GmeSupports *instance);
gulong	     gme_supports_connect_signal (GmeSupports *instance, const gchar *detailed_signal, GCallback handler, gpointer data);

G_END_DECLS

#endif /* __GME_SUPPORTS_H__ */
