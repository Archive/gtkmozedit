/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __GME_EDITING_SESSION_H__
#define __GME_EDITING_SESSION_H__

#include <glib.h>
#include <glib-object.h>
#include <glib/gmacros.h>
#include <gtkmozedit/gme-forward.h>
#include <gtkmozedit/dom/gme-dom-forward.h>

G_BEGIN_DECLS

#define GME_TYPE_EDITING_SESSION                  (gme_editing_session_get_gtype ())
#define GME_EDITING_SESSION(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), GME_TYPE_EDITING_SESSION, GmeEditingSession))
#define GME_EDITING_SESSION_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST ((klass), GME_TYPE_EDITING_SESSION, GmeEditingSessionClass))
#define GME_IS_EDITING_SESSION(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GME_TYPE_EDITING_SESSION))
#define GME_IS_EDITING_SESSION_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), GME_TYPE_EDITING_SESSION))
#define GME_EDITING_SESSION_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS ((obj), GME_TYPE_EDITING_SESSION, GmeEditingSessionClass))

typedef struct _GmeEditingSessionClass GmeEditingSessionClass;

GType gme_editing_session_get_gtype (void);

#define GME_EDITING_SESSION_EDITOR_OK 				0
#define GME_EDITING_SESSION_EDITOR_CreationInProgress 		1
#define GME_EDITING_SESSION_EDITOR_ErrorCantEditMimeType 	2
#define GME_EDITING_SESSION_EDITOR_ErrorFileNotFound 		3
#define GME_EDITING_SESSION_EDITOR_ErrorCantEditFramesets 	8
#define GME_EDITING_SESSION_EDITOR_ErrorUnknown 		9

gint64 gme_editing_session_get_editor_status (GmeEditingSession *self);
gboolean gme_editing_session_make_window_editable (GmeEditingSession *self, GmeDOMWindow *window, const gchar *editor_type, gboolean do_after_uri_load);
gboolean gme_editing_session_window_is_editable (GmeEditingSession *self, GmeDOMWindow *window);
GmeEditor* gme_editing_session_get_editor_for_window (GmeEditingSession *self, GmeDOMWindow *window);	
gboolean gme_editing_session_setup_editor_on_window (GmeEditingSession *self, GmeDOMWindow *window);
gboolean gme_editing_session_tear_down_editor_on_window (GmeEditingSession *self, GmeDOMWindow *window);
gboolean gme_editing_session_set_editor_on_controllers (GmeEditingSession *self, GmeDOMWindow *window, GmeEditor *editor);

#ifdef GTKMOZEMBED_VERSION_1_0
gboolean gme_editing_session_init (GmeEditingSession *self, GmeDOMWindow *window);
#endif

G_END_DECLS

#endif /* __GME_EDITING_SESSION_H__ */
