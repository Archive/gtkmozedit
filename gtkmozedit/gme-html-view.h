/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __GME_HTML_VIEW_H__
#define __GME_HTML_VIEW_H__

#include <glib.h>
#include <glib-object.h>
#include <glib/gmacros.h>
#include <gtk/gtk.h>
#include <gtkmozedit/gme-forward.h>
#include <gtkmozedit/gme-web-browser.h>
#include <gtkmozedit/gme-isupports.h>

G_BEGIN_DECLS

#define GME_TYPE_HTML_VIEW                  (gme_html_view_get_gtype ())
#define GME_HTML_VIEW(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), GME_TYPE_HTML_VIEW, GmeHTMLView))
#define GME_HTML_VIEW_CLASS(klass)          (G_TYPE_CHECK_CLASS_CAST ((klass), GME_TYPE_HTML_VIEW, GmeHTMLViewClass))
#define GME_IS_HTML_VIEW(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GME_TYPE_HTML_VIEW))
#define GME_IS_HTML_VIEW_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), GME_TYPE_HTML_VIEW))
#define GME_HTML_VIEW_GET_CLASS(obj)        (G_TYPE_INSTANCE_GET_CLASS ((obj), GME_TYPE_HTML_VIEW, GmeHTMLViewClass))

typedef struct _GmeHTMLViewClass GmeHTMLViewClass;

GType	   gme_html_view_get_gtype (void);

GtkWidget *gme_html_view_new 	 (void);
void 	   gme_html_view_load (GmeHTMLView *self, const gchar *url);
gboolean   gme_html_view_save (GmeHTMLView *self, const gchar *filename, gboolean save_files);
GmeWebBrowser* gme_html_view_get_web_browser (GmeHTMLView *self);
/* this has to be done via the GmeWebBrowser
   GmeDOMWindow* gme_html_view_get_content_dom_window (GmeHTMLView *self);
*/

/* simplified command interface, for full binding see gme-command-* */
gboolean   gme_html_view_do_command  (GmeHTMLView *self, const gchar *cmd, const gchar *val);
gboolean   gme_html_view_is_command_enabled (GmeHTMLView *self, const gchar *cmd);
gboolean   gme_html_view_is_command_supported (GmeHTMLView *self, const gchar *cmd);

/* interface GmeISupports */
GmeISupports* gme_html_view_query_interface (GmeHTMLView *self, GType type);
gpointer      gme_html_view_get_wrapped_ptr (GmeHTMLView *self);

G_END_DECLS

#endif /* __GME_HTML_VIEW_H__ */
