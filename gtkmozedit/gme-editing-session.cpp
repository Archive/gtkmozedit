/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <gtkmozedit/gtkmozedit-config.h>
#include "nsCOMPtr.h"
#include "nsIEditingSession.h"
#include "nsIInterfaceRequestorUtils.h"

#include <gtkmozedit/gme-editing-session-private.h>
#include <gtkmozedit/gme-editor-private.h>
#include <gtkmozedit/dom/gme-dom-window-private.h>

enum {
	PROP_0,
	PROP_WRAPPED_PTR,
	_NUM_PROPS
};

static GmeSupportsClass *gme_editing_session_parent_class = NULL;

static void
instance_init (GmeEditingSession *self)
{
	self->wrapped_ptr = NULL;
	self->is_disposed = FALSE;
}

static void
instance_dispose (GObject *instance)
{
	GmeEditingSession *self = GME_EDITING_SESSION (instance);

	if (self->is_disposed)
		return;

	if (self->wrapped_ptr) NS_RELEASE (self->wrapped_ptr);
	self->wrapped_ptr = NULL;
	self->is_disposed = TRUE;

	gme_editing_session_parent_class->dispose (G_OBJECT (self));
}

static void
set_property (GObject      *object,
	      guint         prop_id,
	      GValue const *value,
	      GParamSpec   *pspec)
{
	GmeEditingSession *self = GME_EDITING_SESSION (object);
	nsISupports *ptr = NULL;
	nsCOMPtr<nsIEditingSession> cmd_mgr;
	nsresult rv;

	switch (prop_id) {
	case PROP_WRAPPED_PTR:
		ptr = reinterpret_cast<nsISupports*>(g_value_get_pointer (value));
		/* strange, CallQueryInterface seems to fail while the below solution 
		rv = CallQueryInterface (ptr, &self->wrapped_ptr);
		*/
		cmd_mgr = do_GetInterface (ptr, &rv);
		if (NS_SUCCEEDED (rv)) {
			self->wrapped_ptr = cmd_mgr;
			NS_ADDREF (self->wrapped_ptr);
			/* constuction param, init parent */
			gme_supports_private_set_wrapped_ptr (GME_SUPPORTS (self), self->wrapped_ptr);
		}
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
get_property (GObject    *object,
	      guint       prop_id,
	      GValue     *value,
	      GParamSpec *pspec)
{
	GmeEditingSession *self = GME_EDITING_SESSION (object);

	switch (prop_id) {
	case PROP_WRAPPED_PTR:
		g_value_set_pointer (value, (gpointer) self->wrapped_ptr);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
class_init (GmeEditingSessionClass *klass)
{
	GObjectClass *gobject_class = G_OBJECT_CLASS (klass);

	/* vfuncs */
	klass->dispose = instance_dispose;

	/* hook gobject vfuncs */
	gobject_class->dispose = instance_dispose;

	gme_editing_session_parent_class = (GmeSupportsClass*) g_type_class_peek_parent (klass);

	gobject_class->set_property = set_property;
	gobject_class->get_property = get_property;

	g_object_class_install_property (gobject_class,
		PROP_WRAPPED_PTR,
		g_param_spec_pointer ("wrapped-ptr", _("Wrapped Pointer"),
			_("Pointer to the wrapped c++ object"),
			(GParamFlags)(G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY)));
}

GType
gme_editing_session_get_gtype (void)
{
        static GType type = 0;
        if (!type) {
                static const GTypeInfo info = {
                        sizeof (GmeEditingSessionClass),
                        NULL,           /* base_init */
                        NULL,           /* base_finalize */
                        (GClassInitFunc) class_init,
                        NULL,           /* class_finalize */
                        NULL,           /* class_data */
                        sizeof (GmeEditingSession),
                        0,              /* n_preallocs */
                        (GInstanceInitFunc) instance_init,
                };
                type = g_type_register_static (GME_TYPE_SUPPORTS, "GmeEditingSession", &info, (GTypeFlags)0);
        }
        return type;
}

void 
gme_editing_session_private_set_wrapped_ptr (GmeEditingSession *self, 
				      nsIEditingSession *wrapped_ptr)
{
	g_assert (self && wrapped_ptr);
	self->wrapped_ptr = wrapped_ptr;
	NS_ADDREF (self->wrapped_ptr);
	/* constuction param, init parent */
	gme_supports_private_set_wrapped_ptr (GME_SUPPORTS (self), self->wrapped_ptr);
}

GmeEditingSession* 
gme_editing_session_new (nsIEditingSession *wrapped_ptr)
{
	return GME_EDITING_SESSION (g_object_new (GME_TYPE_EDITING_SESSION, "wrapped-ptr", wrapped_ptr, NULL));
}

gint64 
gme_editing_session_get_editor_status (GmeEditingSession *self)
{
	guint32 status;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetEditorStatus (&status);	
	if (NS_SUCCEEDED (rv)) {
		return status;
	}
	return -1;
}


gboolean 
gme_editing_session_make_window_editable (GmeEditingSession *self, 
					  GmeDOMWindow *window, 
					  const gchar *editor_type, 
					  gboolean do_after_uri_load)
{
	nsresult rv;
	g_assert (self && window);

	rv = self->wrapped_ptr->MakeWindowEditable (window->wrapped_ptr, editor_type, do_after_uri_load);
	return NS_SUCCEEDED (rv);
}
  
gboolean 
gme_editing_session_window_is_editable (GmeEditingSession *self, 
					GmeDOMWindow *window)
{
	gboolean is_editable;
	nsresult rv;
	g_assert (self && window);

	rv = self->wrapped_ptr->WindowIsEditable (window->wrapped_ptr, &is_editable);
	if (NS_SUCCEEDED (rv)) {
		return is_editable;
	}
	return FALSE;
}
  
GmeEditor* 
gme_editing_session_get_editor_for_window (GmeEditingSession *self, 
					   GmeDOMWindow *window)
{
	nsIEditor *e = NULL;
	GmeEditor *editor = NULL;
	nsresult rv;
	g_assert (self && window);

	rv = self->wrapped_ptr->GetEditorForWindow (window->wrapped_ptr, &e);
	if (NS_SUCCEEDED (rv)) {
		editor = gme_editor_new (e);
	}
	return editor;	
}

gboolean 
gme_editing_session_setup_editor_on_window (GmeEditingSession *self, 
					    GmeDOMWindow *window)
{
	nsresult rv;
	g_assert (self && window);

	rv = self->wrapped_ptr->SetupEditorOnWindow (window->wrapped_ptr);
	return NS_SUCCEEDED (rv);
}

gboolean 
gme_editing_session_tear_down_editor_on_window (GmeEditingSession *self, 
						GmeDOMWindow *window)
{
	nsresult rv;
	g_assert (self && window);

	rv = self->wrapped_ptr->TearDownEditorOnWindow (window->wrapped_ptr);
	return NS_SUCCEEDED (rv);
}

gboolean 
gme_editing_session_set_editor_on_controllers (GmeEditingSession *self, 
					       GmeDOMWindow *window, 
					       GmeEditor *editor)
{
	nsresult rv;
	g_assert (self && window && editor);

	rv = self->wrapped_ptr->SetEditorOnControllers (window->wrapped_ptr, editor->wrapped_ptr);
	return NS_SUCCEEDED (rv);
}


#ifdef GTKMOZEMBED_VERSION_1_0

gboolean 
gme_editing_session_init (GmeEditingSession *self, 
			  GmeDOMWindow *window)
{
	nsresult rv;
	g_assert (self && window);

	rv = self->wrapped_ptr->Init (window->wrapped_ptr);
	return NS_SUCCEEDED (rv);
}

#endif
