/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __GME_DOM_DOCUMENT_TYPE_H__
#define __GME_DOM_DOCUMENT_TYPE_H__

#include <glib.h>
#include <glib-object.h>
#include <glib/gmacros.h>

#include <gtkmozedit/dom/gme-dom-forward.h>
#include <gtkmozedit/dom/gme-dom-named-node-map.h>

G_BEGIN_DECLS

#define GME_TYPE_DOM_DOCUMENT_TYPE                  (gme_dom_document_type_get_gtype ())
#define GME_DOM_DOCUMENT_TYPE(obj)          (G_TYPE_CHECK_INSTANCE_CAST ((obj), GME_TYPE_DOM_DOCUMENT_TYPE, GmeDOMDocumentType))
#define GME_DOM_DOCUMENT_TYPE_CLASS(klass) 	    (G_TYPE_CHECK_CLASS_CAST ((klass), GME_TYPE_DOM_DOCUMENT_TYPE, GmeDOMDocumentTypeClass))
#define GME_IS_DOM_DOCUMENT_TYPE(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GME_TYPE_DOM_DOCUMENT_TYPE))
#define GME_IS_DOM_DOCUMENT_TYPE_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), GME_TYPE_DOM_DOCUMENT_TYPE))
#define GME_DOM_DOCUMENT_TYPE_GET_CLASS(obj) 	    (G_TYPE_INSTANCE_GET_CLASS ((obj), GME_TYPE_DOM_DOCUMENT_TYPE, GmeDOMDocumentTypeClass))

typedef struct _GmeDOMDocumentTypeClass GmeDOMDocumentTypeClass;

GType gme_dom_document_type_get_gtype (void);

/**
 * Each Document has a doctype attribute whose value is either null 
 * or a DocumentType object. 
 * The GmeDOMDocumentType interface in the DOM Core provides an 
 * interface to the list of entities that are defined for the document.
 *
 * For more information on this interface please see 
 * http://www.w3.org/TR/DOM-Level-2-Core/
 *
 * @status FROZEN
 */

gchar* gme_dom_document_type_get_name (GmeDOMDocumentType *self);
GmeDOMNamedNodeMap* gme_dom_document_type_get_entities (GmeDOMDocumentType *self);
GmeDOMNamedNodeMap* gme_dom_document_type_get_notations (GmeDOMDocumentType *self);
gchar* gme_dom_document_type_get_public_id (GmeDOMDocumentType *self);
gchar* gme_dom_document_type_get_system_id (GmeDOMDocumentType *self);
gchar* gme_dom_document_type_get_internal_subset (GmeDOMDocumentType *self);

G_END_DECLS

#endif /* __GME_DOM_DOCUMENT_TYPE_H__ */
