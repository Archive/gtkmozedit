/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __GME_DOM_HTML_COLLECTION_PRIVATE_H__
#define __GME_DOM_HTML_COLLECTION_PRIVATE_H__

#include <glib/gmacros.h>
#include <nsIDOMHTMLCollection.h>

#include "gme-dom-html-collection.h"
#include <gtkmozedit/gme-supports-private.h>


G_BEGIN_DECLS

struct _GmeDOMHTMLCollection {
	GmeSupports parent;
	nsIDOMHTMLCollection *coll;
	gboolean is_disposed;
};

struct _GmeDOMHTMLCollectionClass {
	GmeSupportsClass parent;
	void (* dispose) (GObject *instance);
};

GmeDOMHTMLCollection* gme_dom_html_collection_new (nsIDOMHTMLCollection *coll);
void gme_dom_html_collection_private_set_wrapped_ptr (GmeDOMHTMLCollection *self, nsIDOMHTMLCollection *coll);

G_END_DECLS

#endif /* __GME_DOM_HTML_COLLECTION_PRIVATE_H__ */
