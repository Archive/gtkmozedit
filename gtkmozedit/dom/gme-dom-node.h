/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __GME_DOM_NODE_H__
#define __GME_DOM_NODE_H__

#include <glib.h>
#include <glib-object.h>
#include <glib/gmacros.h>
#include <gtkmozedit/dom/gme-dom-forward.h>
#include <gtkmozedit/dom/gme-dom-document.h>
#include <gtkmozedit/dom/gme-dom-node-list.h>
#include <gtkmozedit/dom/gme-dom-named-node-map.h>

G_BEGIN_DECLS

#define GME_TYPE_DOM_NODE                  (gme_dom_node_get_gtype ())
#define GME_DOM_NODE(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), GME_TYPE_DOM_NODE, GmeDOMNode))
#define GME_DOM_NODE_CLASS(klass)          (G_TYPE_CHECK_CLASS_CAST ((klass), GME_TYPE_DOM_NODE, GmeDOMNodeClass))
#define GME_IS_DOM_NODE(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GME_TYPE_DOM_NODE))
#define GME_IS_DOM_NODE_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), GME_TYPE_DOM_NODE))
#define GME_DOM_NODE_GET_CLASS(obj)        (G_TYPE_INSTANCE_GET_CLASS ((obj), GME_TYPE_DOM_NODE, GmeDOMNodeClass))

#define GME_DOM_ELEMENT_NODE			1
#define GME_DOM_ATTRIBUTE_NODE			2
#define GME_DOM_TEXT_NODE			3
#define GME_DOM_CDATA_SECTION_NODE		4
#define GME_DOM_ENTITY_REFERENCE_NODE		5
#define GME_DOM_ENTITY_NODE			6
#define GME_DOM_PROCESSING_INSTRUCTION_NODE	7
#define GME_DOM_COMMENT_NODE			8
#define GME_DOM_DOCUMENT_NODE			9
#define GME_DOM_DOCUMENT_TYPE_NODE		10
#define GME_DOM_DOCUMENT_FRAGMENT_NODE		11
#define GME_DOM_NOTATION_NODE			12

typedef struct _GmeDOMNodeClass GmeDOMNodeClass;

GType gme_dom_node_get_gtype (void);

/**
 * The GmeDOMNode interface is the primary datatype for the entire 
 * Document Object Model.
 * It represents a single node in the document tree.
 *
 * For more information on this interface please see 
 * http://www.w3.org/TR/DOM-Level-2-Core/
 *
 * @status FROZEN
 */

gchar* gme_dom_node_get_node_name (GmeDOMNode *self);
gchar* gme_dom_node_get_node_value (GmeDOMNode *self);
gboolean gme_dom_node_set_node_value (GmeDOMNode *self, const gchar *value);
gint32 gme_dom_node_get_node_type (GmeDOMNode *self);
GmeDOMNode* gme_dom_node_get_parent_node (GmeDOMNode *self);
GmeDOMNodeList* gme_dom_node_get_child_nodes (GmeDOMNode *self);
GmeDOMNode* gme_dom_node_get_first_child (GmeDOMNode *self);
GmeDOMNode* gme_dom_node_get_last_child (GmeDOMNode *self);
GmeDOMNode* gme_dom_node_get_previous_sibling (GmeDOMNode *self);
GmeDOMNode* gme_dom_node_get_next_sibling (GmeDOMNode *self);
GmeDOMNamedNodeMap *gme_dom_node_get_attributes (GmeDOMNode *self);
/* Modified in DOM Level 2 */
GmeDOMDocument *gme_dom_node_get_owner_document (GmeDOMNode *self);
GmeDOMNode* gme_dom_node_insert_before (GmeDOMNode *self, GmeDOMNode *new_child, GmeDOMNode *ref_child);
GmeDOMNode* gme_dom_node_replace_child (GmeDOMNode *self, GmeDOMNode *new_child, GmeDOMNode *old_child);
GmeDOMNode* gme_dom_node_remove_child (GmeDOMNode *self, GmeDOMNode *old_child);
GmeDOMNode* gme_dom_node_append_child (GmeDOMNode *self, GmeDOMNode *new_child);
gboolean gme_dom_node_has_child_nodes (GmeDOMNode *self);
GmeDOMNode* gme_dom_node_clone_node (GmeDOMNode *self, gboolean deep);
/* Modified in DOM Level 2 */
gboolean gme_dom_node_normalize (GmeDOMNode *self);
/* Introduced in DOM Level 2 */
gboolean gme_dom_node_is_supported (GmeDOMNode *self, const gchar *feature, const gchar *version);
/* Introduced in DOM Level 2*/
gchar* gme_dom_node_get_namespace_uri (GmeDOMNode *self);
/* Introduced in DOM Level 2 */
gchar* gme_dom_node_get_prefix (GmeDOMNode *self);
/* Introduced in DOM Level 2 */
gchar* gme_dom_node_get_local_name (GmeDOMNode *self);
/* Introduced in DOM Level 2 */
gboolean gme_dom_node_has_attributes (GmeDOMNode *self);

G_END_DECLS

#endif /* __GME_DOM_NODE_H__ */
