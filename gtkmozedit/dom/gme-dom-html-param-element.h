/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __GME_DOM_HTML_PARAM_ELEMENT_H__
#define __GME_DOM_HTML_PARAM_ELEMENT_H__

#include <glib.h>
#include <glib-object.h>
#include <glib/gmacros.h>
#include <gtkmozedit/dom/gme-dom-forward.h>


G_BEGIN_DECLS

#define GME_TYPE_DOM_HTML_PARAM_ELEMENT                  (gme_dom_html_param_element_get_gtype ())
#define GME_DOM_HTML_PARAM_ELEMENT(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), GME_TYPE_DOM_HTML_PARAM_ELEMENT, GmeDOMHTMLParamElement))
#define GME_DOM_HTML_PARAM_ELEMENT_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST ((klass), GME_TYPE_DOM_HTML_PARAM_ELEMENT, GmeDOMHTMLParamElementClass))
#define GME_IS_DOM_HTML_PARAM_ELEMENT(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GME_TYPE_DOM_HTML_PARAM_ELEMENT))
#define GME_IS_DOM_HTML_PARAM_ELEMENT_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), GME_TYPE_DOM_HTML_PARAM_ELEMENT))
#define GME_DOM_HTML_PARAM_ELEMENT_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS ((obj), GME_TYPE_DOM_HTML_PARAM_ELEMENT, GmeDOMHTMLParamElementClass))

typedef struct _GmeDOMHTMLParamElementClass GmeDOMHTMLParamElementClass;

GType gme_dom_html_param_element_get_gtype (void);

gchar* gme_dom_html_param_element_get_name (GmeDOMHTMLParamElement *self);
gboolean gme_dom_html_param_element_set_name (GmeDOMHTMLParamElement *self, const gchar *param);
gchar* gme_dom_html_param_element_get_type (GmeDOMHTMLParamElement *self);
gboolean gme_dom_html_param_element_set_type (GmeDOMHTMLParamElement *self, const gchar *param);
gchar* gme_dom_html_param_element_get_value (GmeDOMHTMLParamElement *self);
gboolean gme_dom_html_param_element_set_value (GmeDOMHTMLParamElement *self, const gchar *param);
gchar* gme_dom_html_param_element_get_value_type (GmeDOMHTMLParamElement *self);
gboolean gme_dom_html_param_element_set_value_type (GmeDOMHTMLParamElement *self, const gchar *param);


G_END_DECLS

#endif /* __GME_DOM_HTML_PARAM_ELEMENT_H__ */
