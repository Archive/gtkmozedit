/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __GME_DOM_HTML_TABLE_ELEMENT_H__
#define __GME_DOM_HTML_TABLE_ELEMENT_H__

#include <glib.h>
#include <glib-object.h>
#include <glib/gmacros.h>
#include <gtkmozedit/dom/gme-dom-forward.h>
#include <gtkmozedit/dom/gme-dom-html-table-caption-element.h>
#include <gtkmozedit/dom/gme-dom-html-table-section-element.h>
#include <gtkmozedit/dom/gme-dom-html-collection.h>
#include <gtkmozedit/dom/gme-dom-html-element.h>


G_BEGIN_DECLS

#define GME_TYPE_DOM_HTML_TABLE_ELEMENT                  (gme_dom_html_table_element_get_gtype ())
#define GME_DOM_HTML_TABLE_ELEMENT(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), GME_TYPE_DOM_HTML_TABLE_ELEMENT, GmeDOMHTMLTableElement))
#define GME_DOM_HTML_TABLE_ELEMENT_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST ((klass), GME_TYPE_DOM_HTML_TABLE_ELEMENT, GmeDOMHTMLTableElementClass))
#define GME_IS_DOM_HTML_TABLE_ELEMENT(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GME_TYPE_DOM_HTML_TABLE_ELEMENT))
#define GME_IS_DOM_HTML_TABLE_ELEMENT_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), GME_TYPE_DOM_HTML_TABLE_ELEMENT))
#define GME_DOM_HTML_TABLE_ELEMENT_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS ((obj), GME_TYPE_DOM_HTML_TABLE_ELEMENT, GmeDOMHTMLTableElementClass))

typedef struct _GmeDOMHTMLTableElementClass GmeDOMHTMLTableElementClass;

GType gme_dom_html_table_element_get_gtype (void);

GmeDOMHTMLTableCaptionElement* gme_dom_html_table_element_get_caption (GmeDOMHTMLTableElement *self);
gboolean gme_dom_html_table_element_set_caption (GmeDOMHTMLTableElement *self, GmeDOMHTMLTableCaptionElement *param);
GmeDOMHTMLTableSectionElement* gme_dom_html_table_element_get_thead (GmeDOMHTMLTableElement *self);
gboolean gme_dom_html_table_element_set_thead (GmeDOMHTMLTableElement *self, GmeDOMHTMLTableSectionElement *param);
GmeDOMHTMLTableSectionElement* gme_dom_html_table_element_get_tfoot (GmeDOMHTMLTableElement *self);
gboolean gme_dom_html_table_element_set_tfoot (GmeDOMHTMLTableElement *self, GmeDOMHTMLTableSectionElement *param);
GmeDOMHTMLCollection* gme_dom_html_table_element_get_rows (GmeDOMHTMLTableElement *self);
GmeDOMHTMLCollection* gme_dom_html_table_element_get_tbodies (GmeDOMHTMLTableElement *self);
gchar* gme_dom_html_table_element_get_align (GmeDOMHTMLTableElement *self);
gboolean gme_dom_html_table_element_set_align (GmeDOMHTMLTableElement *self, const gchar *param);
gchar* gme_dom_html_table_element_get_bg_color (GmeDOMHTMLTableElement *self);
gboolean gme_dom_html_table_element_set_bg_color (GmeDOMHTMLTableElement *self, const gchar *param);
gchar* gme_dom_html_table_element_get_border (GmeDOMHTMLTableElement *self);
gboolean gme_dom_html_table_element_set_border (GmeDOMHTMLTableElement *self, const gchar *param);
gchar* gme_dom_html_table_element_get_cell_padding (GmeDOMHTMLTableElement *self);
gboolean gme_dom_html_table_element_set_cell_padding (GmeDOMHTMLTableElement *self, const gchar *param);
gchar* gme_dom_html_table_element_get_cell_spacing (GmeDOMHTMLTableElement *self);
gboolean gme_dom_html_table_element_set_cell_spacing (GmeDOMHTMLTableElement *self, const gchar *param);
gchar* gme_dom_html_table_element_get_frame (GmeDOMHTMLTableElement *self);
gboolean gme_dom_html_table_element_set_frame (GmeDOMHTMLTableElement *self, const gchar *param);
gchar* gme_dom_html_table_element_get_rules (GmeDOMHTMLTableElement *self);
gboolean gme_dom_html_table_element_set_rules (GmeDOMHTMLTableElement *self, const gchar *param);
gchar* gme_dom_html_table_element_get_summary (GmeDOMHTMLTableElement *self);
gboolean gme_dom_html_table_element_set_summary (GmeDOMHTMLTableElement *self, const gchar *param);
gchar* gme_dom_html_table_element_get_width (GmeDOMHTMLTableElement *self);
gboolean gme_dom_html_table_element_set_width (GmeDOMHTMLTableElement *self, const gchar *param);

GmeDOMHTMLElement* gme_dom_html_table_element_create_thead (GmeDOMHTMLTableElement *self);
gboolean gme_dom_html_table_element_delete_thead (GmeDOMHTMLTableElement *self);
GmeDOMHTMLElement* gme_dom_html_table_element_create_tfoot (GmeDOMHTMLTableElement *self);
gboolean gme_dom_html_table_element_delete_tfoot (GmeDOMHTMLTableElement *self);
GmeDOMHTMLElement* gme_dom_html_table_element_create_caption (GmeDOMHTMLTableElement *self);
gboolean gme_dom_html_table_element_delete_caption (GmeDOMHTMLTableElement *self);


G_END_DECLS

#endif /* __GME_DOM_HTML_TABLE_ELEMENT_H__ */
