/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __GME_DOM_CSS_NS_PROPERTIES_H__
#define __GME_DOM_CSS_NS_PROPERTIES_H__

#include <glib.h>
#include <glib-object.h>
#include <glib/gmacros.h>
#include <gtkmozedit/dom/gme-dom-forward.h>

G_BEGIN_DECLS

#define GME_TYPE_DOM_CSS_NS_PROPERTIES                  (gme_dom_css_ns_properties_get_gtype ())
#define GME_DOM_CSS_NS_PROPERTIES(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), GME_TYPE_DOM_CSS_NS_PROPERTIES, GmeDOMCSSNSProperties))
#define GME_DOM_CSS_NS_PROPERTIES_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST ((klass), GME_TYPE_DOM_CSS_NS_PROPERTIES, GmeDOMCSSNSPropertiesClass))
#define GME_IS_DOM_CSS_NS_PROPERTIES(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GME_TYPE_DOM_CSS_NS_PROPERTIES))
#define GME_IS_DOM_CSS_NS_PROPERTIES_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), GME_TYPE_DOM_CSS_NS_PROPERTIES))
#define GME_DOM_CSS_NS_PROPERTIES_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS ((obj), GME_TYPE_DOM_CSS_NS_PROPERTIES, GmeDOMCSSNSPropertiesClass))

typedef struct _GmeDOMCSSNSPropertiesClass GmeDOMCSSNSPropertiesClass;

GType gme_dom_css_ns_properties_get_gtype (void);

gchar* gme_dom_css_ns_properties_get_appearance (GmeDOMCSSNSProperties *self);
gboolean gme_dom_css_ns_properties_set_appearance (GmeDOMCSSNSProperties *self, const gchar *param);
gchar* gme_dom_css_ns_properties_get_background_clip (GmeDOMCSSNSProperties *self);
gboolean gme_dom_css_ns_properties_set_background_clip (GmeDOMCSSNSProperties *self, const gchar *param);
gchar* gme_dom_css_ns_properties_get_background_inline_policy (GmeDOMCSSNSProperties *self);
gboolean gme_dom_css_ns_properties_set_background_inline_policy (GmeDOMCSSNSProperties *self, const gchar *param);
gchar* gme_dom_css_ns_properties_get_background_origin (GmeDOMCSSNSProperties *self);
gboolean gme_dom_css_ns_properties_set_background_origin (GmeDOMCSSNSProperties *self, const gchar *param);
gchar* gme_dom_css_ns_properties_get_binding (GmeDOMCSSNSProperties *self);
gboolean gme_dom_css_ns_properties_set_binding (GmeDOMCSSNSProperties *self, const gchar *param);
gchar* gme_dom_css_ns_properties_get_border_bottom_colors (GmeDOMCSSNSProperties *self);
gboolean gme_dom_css_ns_properties_set_border_bottom_colors (GmeDOMCSSNSProperties *self, const gchar *param);
gchar* gme_dom_css_ns_properties_get_border_left_colors (GmeDOMCSSNSProperties *self);
gboolean gme_dom_css_ns_properties_set_border_left_colors (GmeDOMCSSNSProperties *self, const gchar *param);
gchar* gme_dom_css_ns_properties_get_border_right_colors (GmeDOMCSSNSProperties *self);
gboolean gme_dom_css_ns_properties_set_border_right_colors (GmeDOMCSSNSProperties *self, const gchar *param);
gchar* gme_dom_css_ns_properties_get_border_top_colors (GmeDOMCSSNSProperties *self);
gboolean gme_dom_css_ns_properties_set_border_top_colors (GmeDOMCSSNSProperties *self, const gchar *param);
gchar* gme_dom_css_ns_properties_get_border_radius (GmeDOMCSSNSProperties *self);
gboolean gme_dom_css_ns_properties_set_border_radius (GmeDOMCSSNSProperties *self, const gchar *param);
gchar* gme_dom_css_ns_properties_get_border_radius_topleft (GmeDOMCSSNSProperties *self);
gboolean gme_dom_css_ns_properties_set_border_radius_topleft (GmeDOMCSSNSProperties *self, const gchar *param);
gchar* gme_dom_css_ns_properties_get_border_radius_topright (GmeDOMCSSNSProperties *self);
gboolean gme_dom_css_ns_properties_set_border_radius_topright (GmeDOMCSSNSProperties *self, const gchar *param);
gchar* gme_dom_css_ns_properties_get_border_radius_bottomleft (GmeDOMCSSNSProperties *self);
gboolean gme_dom_css_ns_properties_set_border_radius_bottomleft (GmeDOMCSSNSProperties *self, const gchar *param);
gchar* gme_dom_css_ns_properties_get_border_radius_bottomright (GmeDOMCSSNSProperties *self);
gboolean gme_dom_css_ns_properties_set_border_radius_bottomright (GmeDOMCSSNSProperties *self, const gchar *param);
gchar* gme_dom_css_ns_properties_get_box_align (GmeDOMCSSNSProperties *self);
gboolean gme_dom_css_ns_properties_set_box_align (GmeDOMCSSNSProperties *self, const gchar *param);
gchar* gme_dom_css_ns_properties_get_box_direction (GmeDOMCSSNSProperties *self);
gboolean gme_dom_css_ns_properties_set_box_direction (GmeDOMCSSNSProperties *self, const gchar *param);
gchar* gme_dom_css_ns_properties_get_box_flex (GmeDOMCSSNSProperties *self);
gboolean gme_dom_css_ns_properties_set_box_flex (GmeDOMCSSNSProperties *self, const gchar *param);
gchar* gme_dom_css_ns_properties_get_box_orient (GmeDOMCSSNSProperties *self);
gboolean gme_dom_css_ns_properties_set_box_orient (GmeDOMCSSNSProperties *self, const gchar *param);
gchar* gme_dom_css_ns_properties_get_box_ordinal_group (GmeDOMCSSNSProperties *self);
gboolean gme_dom_css_ns_properties_set_box_ordinal_group (GmeDOMCSSNSProperties *self, const gchar *param);
gchar* gme_dom_css_ns_properties_get_box_pack (GmeDOMCSSNSProperties *self);
gboolean gme_dom_css_ns_properties_set_box_pack (GmeDOMCSSNSProperties *self, const gchar *param);
gchar* gme_dom_css_ns_properties_get_box_sizing (GmeDOMCSSNSProperties *self);
gboolean gme_dom_css_ns_properties_set_box_sizing (GmeDOMCSSNSProperties *self, const gchar *param);
gchar* gme_dom_css_ns_properties_get_float_edge (GmeDOMCSSNSProperties *self);
gboolean gme_dom_css_ns_properties_set_float_edge (GmeDOMCSSNSProperties *self, const gchar *param);
gchar* gme_dom_css_ns_properties_get_force_broken_image_icon (GmeDOMCSSNSProperties *self);
gboolean gme_dom_css_ns_properties_set_force_broken_image_icon (GmeDOMCSSNSProperties *self, const gchar *param);
gchar* gme_dom_css_ns_properties_get_image_region (GmeDOMCSSNSProperties *self);
gboolean gme_dom_css_ns_properties_set_image_region (GmeDOMCSSNSProperties *self, const gchar *param);
gchar* gme_dom_css_ns_properties_get_margin_end (GmeDOMCSSNSProperties *self);
gboolean gme_dom_css_ns_properties_set_margin_end (GmeDOMCSSNSProperties *self, const gchar *param);
gchar* gme_dom_css_ns_properties_get_margin_start (GmeDOMCSSNSProperties *self);
gboolean gme_dom_css_ns_properties_set_margin_start (GmeDOMCSSNSProperties *self, const gchar *param);
gchar* gme_dom_css_ns_properties_get_outline (GmeDOMCSSNSProperties *self);
gboolean gme_dom_css_ns_properties_set_outline (GmeDOMCSSNSProperties *self, const gchar *param);
gchar* gme_dom_css_ns_properties_get_outline_color (GmeDOMCSSNSProperties *self);
gboolean gme_dom_css_ns_properties_set_outline_color (GmeDOMCSSNSProperties *self, const gchar *param);
gchar* gme_dom_css_ns_properties_get_outline_radius (GmeDOMCSSNSProperties *self);
gboolean gme_dom_css_ns_properties_set_outline_radius (GmeDOMCSSNSProperties *self, const gchar *param);
gchar* gme_dom_css_ns_properties_get_outline_radius_topleft (GmeDOMCSSNSProperties *self);
gboolean gme_dom_css_ns_properties_set_outline_radius_topleft (GmeDOMCSSNSProperties *self, const gchar *param);
gchar* gme_dom_css_ns_properties_get_outline_radius_topright (GmeDOMCSSNSProperties *self);
gboolean gme_dom_css_ns_properties_set_outline_radius_topright (GmeDOMCSSNSProperties *self, const gchar *param);
gchar* gme_dom_css_ns_properties_get_outline_radius_bottomleft (GmeDOMCSSNSProperties *self);
gboolean gme_dom_css_ns_properties_set_outline_radius_bottomleft (GmeDOMCSSNSProperties *self, const gchar *param);
gchar* gme_dom_css_ns_properties_get_outline_radius_bottomright (GmeDOMCSSNSProperties *self);
gboolean gme_dom_css_ns_properties_set_outline_radius_bottomright (GmeDOMCSSNSProperties *self, const gchar *param);
gchar* gme_dom_css_ns_properties_get_outline_style (GmeDOMCSSNSProperties *self);
gboolean gme_dom_css_ns_properties_set_outline_style (GmeDOMCSSNSProperties *self, const gchar *param);
gchar* gme_dom_css_ns_properties_get_outline_width (GmeDOMCSSNSProperties *self);
gboolean gme_dom_css_ns_properties_set_outline_width (GmeDOMCSSNSProperties *self, const gchar *param);
gchar* gme_dom_css_ns_properties_get_padding_end (GmeDOMCSSNSProperties *self);
gboolean gme_dom_css_ns_properties_set_padding_end (GmeDOMCSSNSProperties *self, const gchar *param);
gchar* gme_dom_css_ns_properties_get_padding_start (GmeDOMCSSNSProperties *self);
gboolean gme_dom_css_ns_properties_set_padding_start (GmeDOMCSSNSProperties *self, const gchar *param);
gchar* gme_dom_css_ns_properties_get_user_focus (GmeDOMCSSNSProperties *self);
gboolean gme_dom_css_ns_properties_set_user_focus (GmeDOMCSSNSProperties *self, const gchar *param);
gchar* gme_dom_css_ns_properties_get_user_input (GmeDOMCSSNSProperties *self);
gboolean gme_dom_css_ns_properties_set_user_input (GmeDOMCSSNSProperties *self, const gchar *param);
gchar* gme_dom_css_ns_properties_get_user_modify (GmeDOMCSSNSProperties *self);
gboolean gme_dom_css_ns_properties_set_user_modify (GmeDOMCSSNSProperties *self, const gchar *param);
gchar* gme_dom_css_ns_properties_get_user_select (GmeDOMCSSNSProperties *self);
gboolean gme_dom_css_ns_properties_set_user_select (GmeDOMCSSNSProperties *self, const gchar *param);

/* TODO for seamonkey these must go to gme-dom-css-properties.h */
#ifdef GTKMOZEMBED_VERSION_1_0 
gchar* gme_dom_css_ns_properties_get_counter_increment (GmeDOMCSSNSProperties *self);
gboolean gme_dom_css_ns_properties_set_counter_increment (GmeDOMCSSNSProperties *self, const gchar *param);
gchar* gme_dom_css_ns_properties_get_counter_reset (GmeDOMCSSNSProperties *self);
gboolean gme_dom_css_ns_properties_set_counter_reset (GmeDOMCSSNSProperties *self, const gchar *param);
gchar* gme_dom_css_ns_properties_get_resizer (GmeDOMCSSNSProperties *self);
gboolean gme_dom_css_ns_properties_set_resizer (GmeDOMCSSNSProperties *self, const gchar *param);
#endif

G_END_DECLS

#endif /* __GME_DOM_CSS_NS_PROPERTIES_H__ */
