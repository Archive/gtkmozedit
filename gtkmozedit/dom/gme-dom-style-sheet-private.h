/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __GME_DOM_STYLE_SHEET_PRIVATE_H__
#define __GME_DOM_STYLE_SHEET_PRIVATE_H__

#include <glib/gmacros.h>
#include <nsIDOMStyleSheet.h>

#include <gtkmozedit/dom/gme-dom-style-sheet.h>
#include <gtkmozedit/gme-supports-private.h>

G_BEGIN_DECLS

struct _GmeDOMStyleSheet {
	GmeSupports parent;
	nsIDOMStyleSheet *wrapped_ptr;
	gboolean is_disposed;
};

struct _GmeDOMStyleSheetClass {
	GmeSupportsClass parent;
	void (* dispose) (GObject *instance);
};

GmeDOMStyleSheet* gme_dom_style_sheet_new (nsIDOMStyleSheet *wrapped_ptr);
void gme_dom_style_sheet_private_set_wrapped_ptr (GmeDOMStyleSheet *self, nsIDOMStyleSheet *wrapped_ptr);

G_END_DECLS

#endif /* __GME_DOM_STYLE_SHEET_PRIVATE_H__ */
