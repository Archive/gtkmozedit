/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <gtkmozedit/gtkmozedit-config.h>

#include "nsIDOMCharacterData.h"

#include "gme-dom-character-data-private.h"
#include "gme-dom-node-private.h"

enum {
	PROP_0,
	PROP_WRAPPED_PTR,
	_NUM_PROPS
};

static GmeDOMNodeClass *gme_dom_character_data_parent_class = NULL;

static void
instance_init (GmeDOMCharacterData *self)
{
	self->cdata = NULL;
	self->is_disposed = FALSE;
}

static void
instance_dispose (GObject *instance)
{
	GmeDOMCharacterData *self = GME_DOM_CHARACTER_DATA (instance);

	if (self->is_disposed)
		return;

	if (self->cdata) NS_RELEASE (self->cdata);
	self->cdata = NULL;
	self->is_disposed = TRUE;

	gme_dom_character_data_parent_class->dispose (G_OBJECT (self));
}

static void
set_property (GObject      *object,
	      guint         prop_id,
	      GValue const *value,
	      GParamSpec   *pspec)
{
	GmeDOMCharacterData *self = GME_DOM_CHARACTER_DATA (object);
	nsISupports *ptr = NULL;
	nsresult rv;

	switch (prop_id) {
	case PROP_WRAPPED_PTR:
		ptr = reinterpret_cast<nsISupports*>(g_value_get_pointer (value));
		rv = CallQueryInterface (ptr, &self->cdata);
		if (NS_SUCCEEDED (rv) && self->cdata) {
			NS_ADDREF (self->cdata);
			/* constuction param, init parent */
			gme_dom_node_private_set_wrapped_ptr (GME_DOM_NODE (self), self->cdata);
		}
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
get_property (GObject    *object,
	      guint       prop_id,
	      GValue     *value,
	      GParamSpec *pspec)
{
	GmeDOMCharacterData *self = GME_DOM_CHARACTER_DATA (object);

	switch (prop_id) {
	case PROP_WRAPPED_PTR:
		g_value_set_pointer (value, (gpointer) self->cdata);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
class_init (GmeDOMCharacterDataClass *klass)
{
	GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
	GmeDOMNodeClass *node_class = GME_DOM_NODE_CLASS (klass);

	/* vfuncs */
	klass->dispose = instance_dispose;

	/* hook gobject vfuncs */
	gobject_class->dispose = instance_dispose;

	gme_dom_character_data_parent_class = (GmeDOMNodeClass*) g_type_class_peek_parent (klass);

	gobject_class->set_property = set_property;
	gobject_class->get_property = get_property;

	g_object_class_install_property (gobject_class,
		PROP_WRAPPED_PTR,
		g_param_spec_pointer ("wrapped-ptr", _("Wrapped Pointer"),
			_("Pointer to the wrapped c++ object"),
			(GParamFlags)(G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY)));
}

GType
gme_dom_character_data_get_gtype (void)
{
        static GType type = 0;
        if (!type) {
                static const GTypeInfo info = {
                        sizeof (GmeDOMCharacterDataClass),
                        NULL,           /* base_init */
                        NULL,           /* base_finalize */
                        (GClassInitFunc) class_init,
                        NULL,           /* class_finalize */
                        NULL,           /* class_data */
                        sizeof (GmeDOMCharacterData),
                        0,              /* n_preallocs */
                        (GInstanceInitFunc) instance_init,
                };
                type = g_type_register_static (GME_TYPE_DOM_NODE, "GmeDOMCharacterData", &info, (GTypeFlags)0);
        }
        return type;
}

void 
gme_dom_character_data_private_set_wrapped_ptr (GmeDOMCharacterData *self, 
				      nsIDOMCharacterData *cdata)
{
	g_assert (self && cdata);
	self->cdata = cdata;
	/* constuction param, init parent */
	gme_dom_node_private_set_wrapped_ptr (GME_DOM_NODE (self), self->cdata);
}

GmeDOMCharacterData* 
gme_dom_character_data_new (nsIDOMCharacterData *cdata)
{
	return GME_DOM_CHARACTER_DATA (g_object_new (GME_TYPE_DOM_CHARACTER_DATA, "wrapped-ptr", cdata, NULL));
}

gchar* 
gme_dom_character_data_get_data (GmeDOMCharacterData *self)
{
	nsEmbedString data;
	nsresult rv;
	g_assert (self);

	rv = self->cdata->GetData (data);
	if (NS_SUCCEEDED (rv)) {
		return g_strdup (NS_ConvertUTF16toUTF8 (data).get ());
	}
	return NULL;
}

gboolean 
gme_dom_character_data_set_data (GmeDOMCharacterData *self, const gchar *data)
{
	nsEmbedString d;
	nsresult rv;
	g_assert (self);

	d = NS_ConvertUTF8toUTF16 (data);
	rv = self->cdata->SetData (d);
	return NS_SUCCEEDED (rv);
}

gint64 
gme_dom_character_data_get_length (GmeDOMCharacterData *self)
{
	guint32 length;
	nsresult rv;
	g_assert (self);

	rv = self->cdata->GetLength (&length);
	if (NS_SUCCEEDED (rv)) {
		return length;
	}
	return -1;
}

gchar* 
gme_dom_character_data_substring_data (GmeDOMCharacterData *self, guint32 offset, guint32 count)
{
	nsEmbedString data;
	nsresult rv;
	g_assert (self);

	rv = self->cdata->SubstringData (offset, count, data);
	if (NS_SUCCEEDED (rv)) {
		return g_strdup (NS_ConvertUTF16toUTF8 (data).get ());
	}
	return NULL;
}

gboolean 
gme_dom_character_data_append_data (GmeDOMCharacterData *self, const gchar *data)
{
	nsEmbedString d;
	nsresult rv;
	g_assert (self);

	d = NS_ConvertUTF8toUTF16 (data);
	rv = self->cdata->AppendData (d);
	return NS_SUCCEEDED (rv);
}

gboolean 
gme_dom_character_data_insert_data (GmeDOMCharacterData *self, guint32 offset, const gchar *data)
{
	nsEmbedString d;
	nsresult rv;
	g_assert (self);

	d = NS_ConvertUTF8toUTF16 (data);
	rv = self->cdata->InsertData (offset, d);
	return NS_SUCCEEDED (rv);
}

gboolean 
gme_dom_character_data_delete_data (GmeDOMCharacterData *self, guint32 offset, guint32 count)
{
	nsresult rv;
	g_assert (self);

	rv = self->cdata->DeleteData (offset, count);
	return NS_SUCCEEDED (rv);
}

gboolean 
gme_dom_character_data_replace_data (GmeDOMCharacterData *self, guint32 offset, guint32 count, const gchar *data)
{
	nsEmbedString d;
	nsresult rv;
	g_assert (self);

	d = NS_ConvertUTF8toUTF16 (data);
	rv = self->cdata->ReplaceData (offset, count, d);
	return NS_SUCCEEDED (rv);
}
