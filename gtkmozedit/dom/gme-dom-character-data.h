/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __GME_DOM_CHARACTER_DATA_H__
#define __GME_DOM_CHARACTER_DATA_H__

#include <glib.h>
#include <glib-object.h>
#include <glib/gmacros.h>
#include <gtkmozedit/dom/gme-dom-forward.h>
#include <gtkmozedit/dom/gme-dom-node.h>

G_BEGIN_DECLS

#define GME_TYPE_DOM_CHARACTER_DATA                  (gme_dom_character_data_get_gtype ())
#define GME_DOM_CHARACTER_DATA(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), GME_TYPE_DOM_CHARACTER_DATA, GmeDOMCharacterData))
#define GME_DOM_CHARACTER_DATA_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST ((klass), GME_TYPE_DOM_CHARACTER_DATA, GmeDOMCharacterDataClass))
#define GME_IS_DOM_CHARACTER_DATA(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GME_TYPE_DOM_CHARACTER_DATA))
#define GME_IS_DOM_CHARACTER_DATA_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), GME_TYPE_DOM_CHARACTER_DATA))
#define GME_DOM_CHARACTER_DATA_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS ((obj), GME_TYPE_DOM_CHARACTER_DATA, GmeDOMCharacterDataClass))

typedef struct _GmeDOMCharacterDataClass GmeDOMCharacterDataClass;

GType gme_dom_character_data_get_gtype (void);

/**
 * The GmeDOMCharacterData interface extends GmeDOMNode with a set of 
 * attributes and methods for accessing character data in the DOM.
 * 
 * For more information on this interface please see 
 * http://www.w3.org/TR/DOM-Level-2-Core/
 *
 * @status FROZEN
 */

gchar* gme_dom_character_data_get_data (GmeDOMCharacterData *self);
gboolean gme_dom_character_data_set_data (GmeDOMCharacterData *self, const gchar *data);
gint64 gme_dom_character_data_get_length (GmeDOMCharacterData *self);
gchar* gme_dom_character_data_substring_data (GmeDOMCharacterData *self, guint32 offset, guint32 count);
gboolean gme_dom_character_data_append_data (GmeDOMCharacterData *self, const gchar *data);
gboolean gme_dom_character_data_insert_data (GmeDOMCharacterData *self, guint32 offset, const gchar *data);
gboolean gme_dom_character_data_delete_data (GmeDOMCharacterData *self, guint32 offset, guint32 count);
gboolean gme_dom_character_data_replace_data (GmeDOMCharacterData *self, guint32 offset, guint32 count, const gchar *data);

G_END_DECLS

#endif /* __GME_DOM_CHARACTER_DATA_H__ */
