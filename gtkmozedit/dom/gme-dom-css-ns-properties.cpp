/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <gtkmozedit/gtkmozedit-config.h>

#include "nsIDOMCSS2Properties.h"

#include "gme-dom-css-ns-properties-private.h"

enum {
	PROP_0,
	PROP_WRAPPED_PTR,
	_NUM_PROPS
};

static GmeDOMCSSPropertiesClass *gme_dom_css_ns_properties_parent_class = NULL;

static void
instance_init (GmeDOMCSSNSProperties *self)
{
	self->wrapped_ptr = NULL;
	self->is_disposed = FALSE;
}

static void
instance_dispose (GObject *instance)
{
	GmeDOMCSSNSProperties *self = GME_DOM_CSS_NS_PROPERTIES (instance);

	if (self->is_disposed)
		return;

	if (self->wrapped_ptr) NS_RELEASE (self->wrapped_ptr);
	self->wrapped_ptr = NULL;
	self->is_disposed = TRUE;

	gme_dom_css_ns_properties_parent_class->dispose (G_OBJECT (self));
}

static void
set_property (GObject      *object,
	      guint         prop_id,
	      GValue const *value,
	      GParamSpec   *pspec)
{
	GmeDOMCSSNSProperties *self = GME_DOM_CSS_NS_PROPERTIES (object);
	nsISupports *ptr = NULL;
	nsresult rv;

	switch (prop_id) {
	case PROP_WRAPPED_PTR:
		ptr = reinterpret_cast<nsISupports*>(g_value_get_pointer (value));
		rv = CallQueryInterface (ptr, &self->wrapped_ptr);
		if (NS_SUCCEEDED (rv) && self->wrapped_ptr) {
			NS_ADDREF (self->wrapped_ptr);
			/* constuction param, init parent */
			gme_dom_css_properties_private_set_wrapped_ptr (GME_DOM_CSS_PROPERTIES (self), self->wrapped_ptr);
		}
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
get_property (GObject    *object,
	      guint       prop_id,
	      GValue     *value,
	      GParamSpec *pspec)
{
	GmeDOMCSSNSProperties *self = GME_DOM_CSS_NS_PROPERTIES (object);

	switch (prop_id) {
	case PROP_WRAPPED_PTR:
		g_value_set_pointer (value, (gpointer) self->wrapped_ptr);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
class_init (GmeDOMCSSNSPropertiesClass *klass)
{
	GObjectClass *gobject_class = G_OBJECT_CLASS (klass);

	/* vfuncs */
	klass->dispose = instance_dispose;

	/* hook gobject vfuncs */
	gobject_class->dispose = instance_dispose;

	gme_dom_css_ns_properties_parent_class = (GmeDOMCSSPropertiesClass*) g_type_class_peek_parent (klass);

	gobject_class->set_property = set_property;
	gobject_class->get_property = get_property;

	g_object_class_install_property (gobject_class,
		PROP_WRAPPED_PTR,
		g_param_spec_pointer ("wrapped-ptr", _("Wrapped Pointer"),
			_("Pointer to the wrapped c++ object"),
			(GParamFlags)(G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY)));
}

GType
gme_dom_css_ns_properties_get_gtype (void)
{
        static GType type = 0;
        if (!type) {
                static const GTypeInfo info = {
                        sizeof (GmeDOMCSSNSPropertiesClass),
                        NULL,           /* base_init */
                        NULL,           /* base_finalize */
                        (GClassInitFunc) class_init,
                        NULL,           /* class_finalize */
                        NULL,           /* class_data */
                        sizeof (GmeDOMCSSNSProperties),
                        0,              /* n_preallocs */
                        (GInstanceInitFunc) instance_init,
                };
                type = g_type_register_static (GME_TYPE_DOM_CSS_PROPERTIES, "GmeDOMCSSNSProperties", &info, (GTypeFlags)0);
        }
        return type;
}

void 
gme_dom_css_ns_properties_private_set_wrapped_ptr (GmeDOMCSSNSProperties *self, 
				      nsIDOMNSCSS2Properties *wrapped_ptr)
{
	g_assert (self && wrapped_ptr);
	self->wrapped_ptr = wrapped_ptr;
	NS_ADDREF (self->wrapped_ptr);
	/* constuction param, init parent */
	gme_dom_css_properties_private_set_wrapped_ptr (GME_DOM_CSS_PROPERTIES (self), self->wrapped_ptr);
}

GmeDOMCSSNSProperties* 
gme_dom_css_ns_properties_new (nsIDOMNSCSS2Properties *wrapped_ptr)
{
	return GME_DOM_CSS_NS_PROPERTIES (g_object_new (GME_TYPE_DOM_CSS_NS_PROPERTIES, "wrapped-ptr", wrapped_ptr, NULL));
}

gchar* 
gme_dom_css_ns_properties_get_appearance (GmeDOMCSSNSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetMozAppearance (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_ns_properties_set_appearance (GmeDOMCSSNSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetMozAppearance (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_ns_properties_get_background_clip (GmeDOMCSSNSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetMozBackgroundClip (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_ns_properties_set_background_clip (GmeDOMCSSNSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetMozBackgroundClip (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_ns_properties_get_background_inline_policy (GmeDOMCSSNSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetMozBackgroundInlinePolicy (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_ns_properties_set_background_inline_policy (GmeDOMCSSNSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetMozBackgroundInlinePolicy (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_ns_properties_get_background_origin (GmeDOMCSSNSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetMozBackgroundOrigin (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_ns_properties_set_background_origin (GmeDOMCSSNSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetMozBackgroundOrigin (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_ns_properties_get_binding (GmeDOMCSSNSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetMozBinding (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_ns_properties_set_binding (GmeDOMCSSNSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetMozBinding (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_ns_properties_get_border_bottom_colors (GmeDOMCSSNSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetMozBorderBottomColors (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_ns_properties_set_border_bottom_colors (GmeDOMCSSNSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetMozBorderBottomColors (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_ns_properties_get_border_left_colors (GmeDOMCSSNSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetMozBorderLeftColors (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_ns_properties_set_border_left_colors (GmeDOMCSSNSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetMozBorderLeftColors (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_ns_properties_get_border_right_colors (GmeDOMCSSNSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetMozBorderRightColors (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_ns_properties_set_border_right_colors (GmeDOMCSSNSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetMozBorderRightColors (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_ns_properties_get_border_top_colors (GmeDOMCSSNSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetMozBorderTopColors (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_ns_properties_set_border_top_colors (GmeDOMCSSNSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetMozBorderTopColors (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_ns_properties_get_border_radius (GmeDOMCSSNSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetMozBorderRadius (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_ns_properties_set_border_radius (GmeDOMCSSNSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetMozBorderRadius (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_ns_properties_get_border_radius_topleft (GmeDOMCSSNSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetMozBorderRadiusTopleft (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_ns_properties_set_border_radius_topleft (GmeDOMCSSNSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetMozBorderRadiusTopleft (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_ns_properties_get_border_radius_topright (GmeDOMCSSNSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetMozBorderRadiusTopright (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_ns_properties_set_border_radius_topright (GmeDOMCSSNSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetMozBorderRadiusTopright (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_ns_properties_get_border_radius_bottomleft (GmeDOMCSSNSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetMozBorderRadiusBottomleft (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_ns_properties_set_border_radius_bottomleft (GmeDOMCSSNSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetMozBorderRadiusBottomleft (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_ns_properties_get_border_radius_bottomright (GmeDOMCSSNSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetMozBorderRadiusBottomright (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_ns_properties_set_border_radius_bottomright (GmeDOMCSSNSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetMozBorderRadiusBottomright (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_ns_properties_get_box_align (GmeDOMCSSNSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetMozBoxAlign (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_ns_properties_set_box_align (GmeDOMCSSNSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetMozBoxAlign (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_ns_properties_get_box_direction (GmeDOMCSSNSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetMozBoxDirection (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_ns_properties_set_box_direction (GmeDOMCSSNSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetMozBoxDirection (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_ns_properties_get_box_flex (GmeDOMCSSNSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetMozBoxFlex (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_ns_properties_set_box_flex (GmeDOMCSSNSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetMozBoxFlex (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_ns_properties_get_box_orient (GmeDOMCSSNSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetMozBoxOrient (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_ns_properties_set_box_orient (GmeDOMCSSNSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetMozBoxOrient (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_ns_properties_get_box_ordinal_group (GmeDOMCSSNSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetMozBoxOrdinalGroup (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_ns_properties_set_box_ordinal_group (GmeDOMCSSNSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetMozBoxOrdinalGroup (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_ns_properties_get_box_pack (GmeDOMCSSNSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetMozBoxPack (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_ns_properties_set_box_pack (GmeDOMCSSNSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetMozBoxPack (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_ns_properties_get_box_sizing (GmeDOMCSSNSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetMozBoxSizing (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_ns_properties_set_box_sizing (GmeDOMCSSNSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetMozBoxSizing (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_ns_properties_get_float_edge (GmeDOMCSSNSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetMozFloatEdge (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_ns_properties_set_float_edge (GmeDOMCSSNSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetMozFloatEdge (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_ns_properties_get_force_broken_image_icon (GmeDOMCSSNSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetMozForceBrokenImageIcon (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_ns_properties_set_force_broken_image_icon (GmeDOMCSSNSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetMozForceBrokenImageIcon (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_ns_properties_get_image_region (GmeDOMCSSNSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetMozImageRegion (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_ns_properties_set_image_region (GmeDOMCSSNSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetMozImageRegion (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_ns_properties_get_margin_end (GmeDOMCSSNSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetMozMarginEnd (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_ns_properties_set_margin_end (GmeDOMCSSNSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetMozMarginEnd (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_ns_properties_get_margin_start (GmeDOMCSSNSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetMozMarginStart (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_ns_properties_set_margin_start (GmeDOMCSSNSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetMozMarginStart (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_ns_properties_get_outline (GmeDOMCSSNSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetMozOutline (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_ns_properties_set_outline (GmeDOMCSSNSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetMozOutline (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_ns_properties_get_outline_color (GmeDOMCSSNSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetMozOutlineColor (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_ns_properties_set_outline_color (GmeDOMCSSNSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetMozOutlineColor (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_ns_properties_get_outline_radius (GmeDOMCSSNSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetMozOutlineRadius (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_ns_properties_set_outline_radius (GmeDOMCSSNSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetMozOutlineRadius (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_ns_properties_get_outline_radius_topleft (GmeDOMCSSNSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetMozOutlineRadiusTopleft (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_ns_properties_set_outline_radius_topleft (GmeDOMCSSNSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetMozOutlineRadiusTopleft (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_ns_properties_get_outline_radius_topright (GmeDOMCSSNSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetMozOutlineRadiusTopright (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_ns_properties_set_outline_radius_topright (GmeDOMCSSNSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetMozOutlineRadiusTopright (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_ns_properties_get_outline_radius_bottomleft (GmeDOMCSSNSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetMozOutlineRadiusBottomleft (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_ns_properties_set_outline_radius_bottomleft (GmeDOMCSSNSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetMozOutlineRadiusBottomleft (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_ns_properties_get_outline_radius_bottomright (GmeDOMCSSNSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetMozOutlineRadiusBottomright (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_ns_properties_set_outline_radius_bottomright (GmeDOMCSSNSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetMozOutlineRadiusBottomright (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_ns_properties_get_outline_style (GmeDOMCSSNSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetMozOutlineStyle (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_ns_properties_set_outline_style (GmeDOMCSSNSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetMozOutlineStyle (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_ns_properties_get_outline_width (GmeDOMCSSNSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetMozOutlineWidth (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_ns_properties_set_outline_width (GmeDOMCSSNSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetMozOutlineWidth (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_ns_properties_get_padding_end (GmeDOMCSSNSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetMozPaddingEnd (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_ns_properties_set_padding_end (GmeDOMCSSNSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetMozPaddingEnd (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_ns_properties_get_padding_start (GmeDOMCSSNSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetMozPaddingStart (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_ns_properties_set_padding_start (GmeDOMCSSNSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetMozPaddingStart (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_ns_properties_get_user_focus (GmeDOMCSSNSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetMozUserFocus (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_ns_properties_set_user_focus (GmeDOMCSSNSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetMozUserFocus (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_ns_properties_get_user_input (GmeDOMCSSNSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetMozUserInput (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_ns_properties_set_user_input (GmeDOMCSSNSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetMozUserInput (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_ns_properties_get_user_modify (GmeDOMCSSNSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetMozUserModify (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_ns_properties_set_user_modify (GmeDOMCSSNSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetMozUserModify (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_ns_properties_get_user_select (GmeDOMCSSNSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetMozUserSelect (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_ns_properties_set_user_select (GmeDOMCSSNSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetMozUserSelect (p);
	return NS_SUCCEEDED (rv);
}


#ifdef GTKMOZEMBED_VERSION_1_0 

gchar* 
gme_dom_css_ns_properties_get_counter_increment (GmeDOMCSSNSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetMozCounterIncrement (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_ns_properties_set_counter_increment (GmeDOMCSSNSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetMozCounterIncrement (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_ns_properties_get_counter_reset (GmeDOMCSSNSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetMozCounterReset (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_ns_properties_set_counter_reset (GmeDOMCSSNSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetMozCounterReset (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_ns_properties_get_resizer (GmeDOMCSSNSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetMozResizer (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_ns_properties_set_resizer (GmeDOMCSSNSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetMozResizer (p);
	return NS_SUCCEEDED (rv);
}
#endif
