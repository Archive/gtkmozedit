/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <gtkmozedit/gtkmozedit-config.h>

#include "nsIDOMStyleSheet.h"

#include "gme-dom-style-sheet-private.h"
#include "gme-dom-node-private.h"
#include "gme-dom-media-list-private.h"

enum {
	PROP_0,
	PROP_WRAPPED_PTR,
	_NUM_PROPS
};

static GmeSupportsClass *gme_dom_style_sheet_parent_class = NULL;

static void
instance_init (GmeDOMStyleSheet *self)
{
	self->wrapped_ptr = NULL;
	self->is_disposed = FALSE;
}

static void
instance_dispose (GObject *instance)
{
	GmeDOMStyleSheet *self = GME_DOM_STYLE_SHEET (instance);

	if (self->is_disposed)
		return;

	if (self->wrapped_ptr) NS_RELEASE (self->wrapped_ptr);
	self->wrapped_ptr = NULL;
	self->is_disposed = TRUE;

	gme_dom_style_sheet_parent_class->dispose (G_OBJECT (self));
}

static void
set_property (GObject      *object,
	      guint         prop_id,
	      GValue const *value,
	      GParamSpec   *pspec)
{
	GmeDOMStyleSheet *self = GME_DOM_STYLE_SHEET (object);
	nsISupports *ptr = NULL;
	nsresult rv;

	switch (prop_id) {
	case PROP_WRAPPED_PTR:
		ptr = reinterpret_cast<nsISupports*>(g_value_get_pointer (value));
		rv = CallQueryInterface (ptr, &self->wrapped_ptr);
		if (NS_SUCCEEDED (rv) && self->wrapped_ptr) {
			NS_ADDREF (self->wrapped_ptr);
			/* constuction param, init parent */
			gme_supports_private_set_wrapped_ptr (GME_SUPPORTS (self), self->wrapped_ptr);
		}
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
get_property (GObject    *object,
	      guint       prop_id,
	      GValue     *value,
	      GParamSpec *pspec)
{
	GmeDOMStyleSheet *self = GME_DOM_STYLE_SHEET (object);

	switch (prop_id) {
	case PROP_WRAPPED_PTR:
		g_value_set_pointer (value, (gpointer) self->wrapped_ptr);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
class_init (GmeDOMStyleSheetClass *klass)
{
	GObjectClass *gobject_class = G_OBJECT_CLASS (klass);

	/* vfuncs */
	klass->dispose = instance_dispose;

	/* hook gobject vfuncs */
	gobject_class->dispose = instance_dispose;

	gme_dom_style_sheet_parent_class = (GmeSupportsClass*) g_type_class_peek_parent (klass);

	gobject_class->set_property = set_property;
	gobject_class->get_property = get_property;

	g_object_class_install_property (gobject_class,
		PROP_WRAPPED_PTR,
		g_param_spec_pointer ("wrapped-ptr", _("Wrapped Pointer"),
			_("Pointer to the wrapped c++ object"),
			(GParamFlags)(G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY)));
}

GType
gme_dom_style_sheet_get_gtype (void)
{
        static GType type = 0;
        if (!type) {
                static const GTypeInfo info = {
                        sizeof (GmeDOMStyleSheetClass),
                        NULL,           /* base_init */
                        NULL,           /* base_finalize */
                        (GClassInitFunc) class_init,
                        NULL,           /* class_finalize */
                        NULL,           /* class_data */
                        sizeof (GmeDOMStyleSheet),
                        0,              /* n_preallocs */
                        (GInstanceInitFunc) instance_init,
                };
                type = g_type_register_static (GME_TYPE_SUPPORTS, "GmeDOMStyleSheet", &info, (GTypeFlags)0);
        }
        return type;
}

void 
gme_dom_style_sheet_private_set_wrapped_ptr (GmeDOMStyleSheet *self, 
				      nsIDOMStyleSheet *wrapped_ptr)
{
	g_assert (self && wrapped_ptr);
	self->wrapped_ptr = wrapped_ptr;
	NS_ADDREF (self->wrapped_ptr);
	/* constuction param, init parent */
	gme_supports_private_set_wrapped_ptr (GME_SUPPORTS (self), self->wrapped_ptr);
}

GmeDOMStyleSheet* 
gme_dom_style_sheet_new (nsIDOMStyleSheet *wrapped_ptr)
{
	return GME_DOM_STYLE_SHEET (g_object_new (GME_TYPE_DOM_STYLE_SHEET, "wrapped-ptr", wrapped_ptr, NULL));
}

gchar* 
gme_dom_style_sheet_get_type (GmeDOMStyleSheet *self)
{
	nsEmbedString type;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetType (type);
	if (NS_SUCCEEDED (rv)) {
		return g_strdup (NS_ConvertUTF16toUTF8 (type).get ());
	}
	return NULL;
}

gboolean 
gme_dom_style_sheet_get_disabled (GmeDOMStyleSheet *self)
{
	gboolean disabled;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetDisabled (&disabled);
	if (NS_SUCCEEDED (rv)) {
		return disabled;
	}
	return FALSE;
}

gboolean 
gme_dom_style_sheet_set_disabled (GmeDOMStyleSheet *self, gboolean param)
{
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->SetDisabled (param);
	return NS_SUCCEEDED (rv);
}

GmeDOMNode* 
gme_dom_style_sheet_get_owner_node (GmeDOMStyleSheet *self)
{
	nsIDOMNode *n = NULL;
	GmeDOMNode *node = NULL;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetOwnerNode (&n);
	if (NS_SUCCEEDED (rv)) {
		node = gme_dom_node_new (n);
	}
	return node;
}

GmeDOMStyleSheet* 
gme_dom_style_sheet_get_parent_style_sheet (GmeDOMStyleSheet *self)
{
	nsIDOMStyleSheet *ss = NULL;
	GmeDOMStyleSheet *stylesheet = NULL;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetParentStyleSheet (&ss);
	if (NS_SUCCEEDED (rv)) {
		stylesheet = gme_dom_style_sheet_new (ss);
	}
	return stylesheet;
}

gchar* 
gme_dom_style_sheet_get_href (GmeDOMStyleSheet *self)
{
	nsEmbedString href;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetHref (href);
	if (NS_SUCCEEDED (rv)) {
		return g_strdup (NS_ConvertUTF16toUTF8 (href).get ());
	}
	return NULL;
}

gchar* 
gme_dom_style_sheet_get_title (GmeDOMStyleSheet *self)
{
	nsEmbedString title;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetHref (title);
	if (NS_SUCCEEDED (rv)) {
		return g_strdup (NS_ConvertUTF16toUTF8 (title).get ());
	}
	return NULL;
}

GmeDOMMediaList* 
gme_dom_style_sheet_get_media (GmeDOMStyleSheet *self)
{
	nsIDOMMediaList *ml = NULL;
	GmeDOMMediaList *medialist = NULL;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetMedia (&ml);
	if (NS_SUCCEEDED (rv)) {
		medialist = gme_dom_media_list_new (ml);
	}
	return medialist;
}
