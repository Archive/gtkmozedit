/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <gtkmozedit/gtkmozedit-config.h>

#include "nsIDOMHTMLObjectElement.h"

#include "gme-dom-html-object-element-private.h"
#include "gme-dom-html-form-element-private.h"
#include "gme-dom-document-private.h"


enum {
	PROP_0,
	PROP_WRAPPED_PTR,
	_NUM_PROPS
};

static GmeDOMHTMLElementClass *gme_dom_html_object_element_parent_class = NULL;

static void
instance_init (GmeDOMHTMLObjectElement *self)
{
	self->wrapped_ptr = NULL;
	self->is_disposed = FALSE;
}

static void
instance_dispose (GObject *instance)
{
	GmeDOMHTMLObjectElement *self = GME_DOM_HTML_OBJECT_ELEMENT (instance);

	if (self->is_disposed)
		return;

	if (self->wrapped_ptr) NS_RELEASE (self->wrapped_ptr);
	self->wrapped_ptr = NULL;
	self->is_disposed = TRUE;

	gme_dom_html_object_element_parent_class->dispose (G_OBJECT (self));
}

static void
set_property (GObject      *object,
	      guint         prop_id,
	      GValue const *value,
	      GParamSpec   *pspec)
{
	GmeDOMHTMLObjectElement *self = GME_DOM_HTML_OBJECT_ELEMENT (object);
	nsISupports *ptr = NULL;
	nsresult rv;

	switch (prop_id) {
	case PROP_WRAPPED_PTR:
		ptr = reinterpret_cast<nsISupports*>(g_value_get_pointer (value));
		rv = CallQueryInterface (ptr, &self->wrapped_ptr);
		if (NS_SUCCEEDED (rv) && self->wrapped_ptr) {
			NS_ADDREF (self->wrapped_ptr);
			/* constuction param, init parent */
			gme_dom_html_element_private_set_wrapped_ptr (GME_DOM_HTML_ELEMENT (self), self->wrapped_ptr);
		}
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
get_property (GObject    *object,
	      guint       prop_id,
	      GValue     *value,
	      GParamSpec *pspec)
{
	GmeDOMHTMLObjectElement *self = GME_DOM_HTML_OBJECT_ELEMENT (object);

	switch (prop_id) {
	case PROP_WRAPPED_PTR:
		g_value_set_pointer (value, (gpointer) self->wrapped_ptr);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
class_init (GmeDOMHTMLObjectElementClass *klass)
{
	GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
	GmeDOMHTMLElementClass *node_class = GME_DOM_HTML_ELEMENT_CLASS (klass);

	/* vfuncs */
	klass->dispose = instance_dispose;

	/* hook gobject vfuncs */
	gobject_class->dispose = instance_dispose;

	gme_dom_html_object_element_parent_class = (GmeDOMHTMLElementClass*) g_type_class_peek_parent (klass);

	gobject_class->set_property = set_property;
	gobject_class->get_property = get_property;

	g_object_class_install_property (gobject_class,
		PROP_WRAPPED_PTR,
		g_param_spec_pointer ("wrapped-ptr", _("Wrapped Pointer"),
			_("Pointer to the wrapped c++ object"),
			(GParamFlags)(G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY)));
}

GType
gme_dom_html_object_element_get_gtype (void)
{
        static GType type = 0;
        if (!type) {
                static const GTypeInfo info = {
                        sizeof (GmeDOMHTMLObjectElementClass),
                        NULL,           /* base_init */
                        NULL,           /* base_finalize */
                        (GClassInitFunc) class_init,
                        NULL,           /* class_finalize */
                        NULL,           /* class_data */
                        sizeof (GmeDOMHTMLObjectElement),
                        0,              /* n_preallocs */
                        (GInstanceInitFunc) instance_init,
                };
                type = g_type_register_static (GME_TYPE_DOM_HTML_ELEMENT, "GmeDOMHTMLObjectElement", &info, (GTypeFlags)0);
        }
        return type;
}

void 
gme_dom_html_object_element_private_set_wrapped_ptr (GmeDOMHTMLObjectElement *self, 
				      nsIDOMHTMLObjectElement *wrapped_ptr)
{
	g_assert (self && wrapped_ptr);
	self->wrapped_ptr = wrapped_ptr;
	NS_ADDREF (self->wrapped_ptr);
	/* constuction param, init parent */
	gme_dom_html_element_private_set_wrapped_ptr (GME_DOM_HTML_ELEMENT (self), self->wrapped_ptr);
}

GmeDOMHTMLObjectElement* 
gme_dom_html_object_element_new (nsIDOMHTMLObjectElement *wrapped_ptr)
{
	return GME_DOM_HTML_OBJECT_ELEMENT (g_object_new (GME_TYPE_DOM_HTML_OBJECT_ELEMENT, "wrapped-ptr", wrapped_ptr, NULL));
}

GmeDOMHTMLFormElement* 
gme_dom_html_object_element_get_form (GmeDOMHTMLObjectElement *self)
{
	nsIDOMHTMLFormElement *param = NULL;
	GmeDOMHTMLFormElement *object = NULL;
	nsresult rv;
	g_assert (self);
	
	rv = self->wrapped_ptr->GetForm (&param);
	if (NS_SUCCEEDED (rv)) {
		object = gme_dom_html_form_element_new (param);
	}
	return object;
}

gchar* 
gme_dom_html_object_element_get_code (GmeDOMHTMLObjectElement *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetCode (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_html_object_element_set_code (GmeDOMHTMLObjectElement *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetCode (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_html_object_element_get_align (GmeDOMHTMLObjectElement *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetAlign (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_html_object_element_set_align (GmeDOMHTMLObjectElement *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetAlign (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_html_object_element_get_archive (GmeDOMHTMLObjectElement *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetArchive (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_html_object_element_set_archive (GmeDOMHTMLObjectElement *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetArchive (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_html_object_element_get_border (GmeDOMHTMLObjectElement *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetBorder (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_html_object_element_set_border (GmeDOMHTMLObjectElement *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetBorder (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_html_object_element_get_code_base (GmeDOMHTMLObjectElement *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetCodeBase (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_html_object_element_set_code_base (GmeDOMHTMLObjectElement *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetCodeBase (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_html_object_element_get_code_type (GmeDOMHTMLObjectElement *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetCodeType (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_html_object_element_set_code_type (GmeDOMHTMLObjectElement *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetCodeType (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_html_object_element_get_data (GmeDOMHTMLObjectElement *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetData (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_html_object_element_set_data (GmeDOMHTMLObjectElement *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetData (p);
	return NS_SUCCEEDED (rv);
}

gboolean 
gme_dom_html_object_element_get_declare (GmeDOMHTMLObjectElement *self)
{
	gboolean param;
	nsresult rv;
	g_assert (self);
	
	rv = self->wrapped_ptr->GetDeclare (&param);
	return param;
}

gboolean 
gme_dom_html_object_element_set_declare (GmeDOMHTMLObjectElement *self, gboolean param)
{
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->SetDeclare (param);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_html_object_element_get_height (GmeDOMHTMLObjectElement *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetHeight (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_html_object_element_set_height (GmeDOMHTMLObjectElement *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetHeight (p);
	return NS_SUCCEEDED (rv);
}

gint64 
gme_dom_html_object_element_get_hspace (GmeDOMHTMLObjectElement *self)
{
	gint32 param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetHspace (&param);
	if (NS_SUCCEEDED (rv)) {
		return param;
	}
	return -1;
}

gboolean 
gme_dom_html_object_element_set_hspace (GmeDOMHTMLObjectElement *self, gint32 param)
{
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->SetHspace (param);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_html_object_element_get_name (GmeDOMHTMLObjectElement *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetName (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_html_object_element_set_name (GmeDOMHTMLObjectElement *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetName (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_html_object_element_get_standby (GmeDOMHTMLObjectElement *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetStandby (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_html_object_element_set_standby (GmeDOMHTMLObjectElement *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetStandby (p);
	return NS_SUCCEEDED (rv);
}

gint64 
gme_dom_html_object_element_get_tab_index (GmeDOMHTMLObjectElement *self)
{
	gint32 param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetTabIndex (&param);
	if (NS_SUCCEEDED (rv)) {
		return param;
	}
	return -1;
}

gboolean 
gme_dom_html_object_element_set_tab_index (GmeDOMHTMLObjectElement *self, gint32 param)
{
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->SetTabIndex (param);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_html_object_element_get_type (GmeDOMHTMLObjectElement *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetType (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_html_object_element_set_type (GmeDOMHTMLObjectElement *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetType (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_html_object_element_get_use_map (GmeDOMHTMLObjectElement *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetUseMap (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_html_object_element_set_use_map (GmeDOMHTMLObjectElement *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetUseMap (p);
	return NS_SUCCEEDED (rv);
}

gint64 
gme_dom_html_object_element_get_vspace (GmeDOMHTMLObjectElement *self)
{
	gint32 param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetVspace (&param);
	if (NS_SUCCEEDED (rv)) {
		return param;
	}
	return -1;
}

gboolean 
gme_dom_html_object_element_set_vspace (GmeDOMHTMLObjectElement *self, gint32 param)
{
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->SetVspace (param);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_html_object_element_get_width (GmeDOMHTMLObjectElement *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetWidth (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_html_object_element_set_width (GmeDOMHTMLObjectElement *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetWidth (p);
	return NS_SUCCEEDED (rv);
}

GmeDOMDocument* 
gme_dom_html_object_element_get_content_document (GmeDOMHTMLObjectElement *self)
{
	nsIDOMDocument *param = NULL;
	GmeDOMDocument *object = NULL;
	nsresult rv;
	g_assert (self);
	
	rv = self->wrapped_ptr->GetContentDocument (&param);
	if (NS_SUCCEEDED (rv)) {
		object = gme_dom_document_new (param);
	}
	return object;
}


