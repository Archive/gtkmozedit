/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <gtkmozedit/gtkmozedit-config.h>

#include "nsIDOMHTMLBodyElement.h"

#include "gme-dom-html-body-element-private.h"


enum {
	PROP_0,
	PROP_WRAPPED_PTR,
	_NUM_PROPS
};

static GmeDOMHTMLElementClass *gme_dom_html_body_element_parent_class = NULL;

static void
instance_init (GmeDOMHTMLBodyElement *self)
{
	self->wrapped_ptr = NULL;
	self->is_disposed = FALSE;
}

static void
instance_dispose (GObject *instance)
{
	GmeDOMHTMLBodyElement *self = GME_DOM_HTML_BODY_ELEMENT (instance);

	if (self->is_disposed)
		return;

	if (self->wrapped_ptr) NS_RELEASE (self->wrapped_ptr);
	self->wrapped_ptr = NULL;
	self->is_disposed = TRUE;

	gme_dom_html_body_element_parent_class->dispose (G_OBJECT (self));
}

static void
set_property (GObject      *object,
	      guint         prop_id,
	      GValue const *value,
	      GParamSpec   *pspec)
{
	GmeDOMHTMLBodyElement *self = GME_DOM_HTML_BODY_ELEMENT (object);
	nsISupports *ptr = NULL;
	nsresult rv;

	switch (prop_id) {
	case PROP_WRAPPED_PTR:
		ptr = reinterpret_cast<nsISupports*>(g_value_get_pointer (value));
		rv = CallQueryInterface (ptr, &self->wrapped_ptr);
		if (NS_SUCCEEDED (rv) && self->wrapped_ptr) {
			NS_ADDREF (self->wrapped_ptr);
			/* constuction param, init parent */
			gme_dom_html_element_private_set_wrapped_ptr (GME_DOM_HTML_ELEMENT (self), self->wrapped_ptr);
		}
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
get_property (GObject    *object,
	      guint       prop_id,
	      GValue     *value,
	      GParamSpec *pspec)
{
	GmeDOMHTMLBodyElement *self = GME_DOM_HTML_BODY_ELEMENT (object);

	switch (prop_id) {
	case PROP_WRAPPED_PTR:
		g_value_set_pointer (value, (gpointer) self->wrapped_ptr);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
class_init (GmeDOMHTMLBodyElementClass *klass)
{
	GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
	GmeDOMHTMLElementClass *node_class = GME_DOM_HTML_ELEMENT_CLASS (klass);

	/* vfuncs */
	klass->dispose = instance_dispose;

	/* hook gobject vfuncs */
	gobject_class->dispose = instance_dispose;

	gme_dom_html_body_element_parent_class = (GmeDOMHTMLElementClass*) g_type_class_peek_parent (klass);

	gobject_class->set_property = set_property;
	gobject_class->get_property = get_property;

	g_object_class_install_property (gobject_class,
		PROP_WRAPPED_PTR,
		g_param_spec_pointer ("wrapped-ptr", _("Wrapped Pointer"),
			_("Pointer to the wrapped c++ object"),
			(GParamFlags)(G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY)));
}

GType
gme_dom_html_body_element_get_gtype (void)
{
        static GType type = 0;
        if (!type) {
                static const GTypeInfo info = {
                        sizeof (GmeDOMHTMLBodyElementClass),
                        NULL,           /* base_init */
                        NULL,           /* base_finalize */
                        (GClassInitFunc) class_init,
                        NULL,           /* class_finalize */
                        NULL,           /* class_data */
                        sizeof (GmeDOMHTMLBodyElement),
                        0,              /* n_preallocs */
                        (GInstanceInitFunc) instance_init,
                };
                type = g_type_register_static (GME_TYPE_DOM_HTML_ELEMENT, "GmeDOMHTMLBodyElement", &info, (GTypeFlags)0);
        }
        return type;
}

void 
gme_dom_html_body_element_private_set_wrapped_ptr (GmeDOMHTMLBodyElement *self, 
				      nsIDOMHTMLBodyElement *wrapped_ptr)
{
	g_assert (self && wrapped_ptr);
	self->wrapped_ptr = wrapped_ptr;
	NS_ADDREF (self->wrapped_ptr);
	/* constuction param, init parent */
	gme_dom_html_element_private_set_wrapped_ptr (GME_DOM_HTML_ELEMENT (self), self->wrapped_ptr);
}

GmeDOMHTMLBodyElement* 
gme_dom_html_body_element_new (nsIDOMHTMLBodyElement *wrapped_ptr)
{
	return GME_DOM_HTML_BODY_ELEMENT (g_object_new (GME_TYPE_DOM_HTML_BODY_ELEMENT, "wrapped-ptr", wrapped_ptr, NULL));
}

gchar* 
gme_dom_html_body_element_get_a_link (GmeDOMHTMLBodyElement *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetALink (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_html_body_element_set_a_link (GmeDOMHTMLBodyElement *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetALink (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_html_body_element_get_background (GmeDOMHTMLBodyElement *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetBackground (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_html_body_element_set_background (GmeDOMHTMLBodyElement *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetBackground (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_html_body_element_get_bg_color (GmeDOMHTMLBodyElement *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetBgColor (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_html_body_element_set_bg_color (GmeDOMHTMLBodyElement *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetBgColor (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_html_body_element_get_link (GmeDOMHTMLBodyElement *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetLink (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_html_body_element_set_link (GmeDOMHTMLBodyElement *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetLink (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_html_body_element_get_text (GmeDOMHTMLBodyElement *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetText (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_html_body_element_set_text (GmeDOMHTMLBodyElement *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetText (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_html_body_element_get_v_link (GmeDOMHTMLBodyElement *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetVLink (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_html_body_element_set_v_link (GmeDOMHTMLBodyElement *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetVLink (p);
	return NS_SUCCEEDED (rv);
}


