/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __GME_DOM_HTML_APPLET_ELEMENT_H__
#define __GME_DOM_HTML_APPLET_ELEMENT_H__

#include <glib.h>
#include <glib-object.h>
#include <glib/gmacros.h>
#include <gtkmozedit/dom/gme-dom-forward.h>


G_BEGIN_DECLS

#define GME_TYPE_DOM_HTML_APPLET_ELEMENT                  (gme_dom_html_applet_element_get_gtype ())
#define GME_DOM_HTML_APPLET_ELEMENT(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), GME_TYPE_DOM_HTML_APPLET_ELEMENT, GmeDOMHTMLAppletElement))
#define GME_DOM_HTML_APPLET_ELEMENT_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST ((klass), GME_TYPE_DOM_HTML_APPLET_ELEMENT, GmeDOMHTMLAppletElementClass))
#define GME_IS_DOM_HTML_APPLET_ELEMENT(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GME_TYPE_DOM_HTML_APPLET_ELEMENT))
#define GME_IS_DOM_HTML_APPLET_ELEMENT_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), GME_TYPE_DOM_HTML_APPLET_ELEMENT))
#define GME_DOM_HTML_APPLET_ELEMENT_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS ((obj), GME_TYPE_DOM_HTML_APPLET_ELEMENT, GmeDOMHTMLAppletElementClass))

typedef struct _GmeDOMHTMLAppletElementClass GmeDOMHTMLAppletElementClass;

GType gme_dom_html_applet_element_get_gtype (void);

gchar* gme_dom_html_applet_element_get_align (GmeDOMHTMLAppletElement *self);
gboolean gme_dom_html_applet_element_set_align (GmeDOMHTMLAppletElement *self, const gchar *param);
gchar* gme_dom_html_applet_element_get_alt (GmeDOMHTMLAppletElement *self);
gboolean gme_dom_html_applet_element_set_alt (GmeDOMHTMLAppletElement *self, const gchar *param);
gchar* gme_dom_html_applet_element_get_archive (GmeDOMHTMLAppletElement *self);
gboolean gme_dom_html_applet_element_set_archive (GmeDOMHTMLAppletElement *self, const gchar *param);
gchar* gme_dom_html_applet_element_get_code (GmeDOMHTMLAppletElement *self);
gboolean gme_dom_html_applet_element_set_code (GmeDOMHTMLAppletElement *self, const gchar *param);
gchar* gme_dom_html_applet_element_get_code_base (GmeDOMHTMLAppletElement *self);
gboolean gme_dom_html_applet_element_set_code_base (GmeDOMHTMLAppletElement *self, const gchar *param);
gchar* gme_dom_html_applet_element_get_height (GmeDOMHTMLAppletElement *self);
gboolean gme_dom_html_applet_element_set_height (GmeDOMHTMLAppletElement *self, const gchar *param);
gint64 gme_dom_html_applet_element_get_hspace (GmeDOMHTMLAppletElement *self);
gboolean gme_dom_html_applet_element_set_hspace (GmeDOMHTMLAppletElement *self, gint32 param);
gchar* gme_dom_html_applet_element_get_name (GmeDOMHTMLAppletElement *self);
gboolean gme_dom_html_applet_element_set_name (GmeDOMHTMLAppletElement *self, const gchar *param);
gchar* gme_dom_html_applet_element_get_object (GmeDOMHTMLAppletElement *self);
gboolean gme_dom_html_applet_element_set_object (GmeDOMHTMLAppletElement *self, const gchar *param);
gint64 gme_dom_html_applet_element_get_vspace (GmeDOMHTMLAppletElement *self);
gboolean gme_dom_html_applet_element_set_vspace (GmeDOMHTMLAppletElement *self, gint32 param);
gchar* gme_dom_html_applet_element_get_width (GmeDOMHTMLAppletElement *self);
gboolean gme_dom_html_applet_element_set_width (GmeDOMHTMLAppletElement *self, const gchar *param);


G_END_DECLS

#endif /* __GME_DOM_HTML_APPLET_ELEMENT_H__ */
