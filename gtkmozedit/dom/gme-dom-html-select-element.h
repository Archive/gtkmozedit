/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __GME_DOM_HTML_SELECT_ELEMENT_H__
#define __GME_DOM_HTML_SELECT_ELEMENT_H__

#include <glib.h>
#include <glib-object.h>
#include <glib/gmacros.h>
#include <gtkmozedit/dom/gme-dom-forward.h>
#include <gtkmozedit/dom/gme-dom-html-form-element.h>
#include <gtkmozedit/dom/gme-dom-html-options-collection.h>


G_BEGIN_DECLS

#define GME_TYPE_DOM_HTML_SELECT_ELEMENT                  (gme_dom_html_select_element_get_gtype ())
#define GME_DOM_HTML_SELECT_ELEMENT(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), GME_TYPE_DOM_HTML_SELECT_ELEMENT, GmeDOMHTMLSelectElement))
#define GME_DOM_HTML_SELECT_ELEMENT_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST ((klass), GME_TYPE_DOM_HTML_SELECT_ELEMENT, GmeDOMHTMLSelectElementClass))
#define GME_IS_DOM_HTML_SELECT_ELEMENT(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GME_TYPE_DOM_HTML_SELECT_ELEMENT))
#define GME_IS_DOM_HTML_SELECT_ELEMENT_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), GME_TYPE_DOM_HTML_SELECT_ELEMENT))
#define GME_DOM_HTML_SELECT_ELEMENT_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS ((obj), GME_TYPE_DOM_HTML_SELECT_ELEMENT, GmeDOMHTMLSelectElementClass))

typedef struct _GmeDOMHTMLSelectElementClass GmeDOMHTMLSelectElementClass;

GType gme_dom_html_select_element_get_gtype (void);

gchar* gme_dom_html_select_element_get_type (GmeDOMHTMLSelectElement *self);
gint64 gme_dom_html_select_element_get_selected_index (GmeDOMHTMLSelectElement *self);
gboolean gme_dom_html_select_element_set_selected_index (GmeDOMHTMLSelectElement *self, gint32 param);
gchar* gme_dom_html_select_element_get_value (GmeDOMHTMLSelectElement *self);
gboolean gme_dom_html_select_element_set_value (GmeDOMHTMLSelectElement *self, const gchar *param);
gint64 gme_dom_html_select_element_get_length (GmeDOMHTMLSelectElement *self);
gboolean gme_dom_html_select_element_set_length (GmeDOMHTMLSelectElement *self, gint32 param);
GmeDOMHTMLFormElement* gme_dom_html_select_element_get_form (GmeDOMHTMLSelectElement *self);
GmeDOMHTMLOptionsCollection* gme_dom_html_select_element_get_options (GmeDOMHTMLSelectElement *self);
gboolean gme_dom_html_select_element_get_disabled (GmeDOMHTMLSelectElement *self);
gboolean gme_dom_html_select_element_set_disabled (GmeDOMHTMLSelectElement *self, gboolean param);
gboolean gme_dom_html_select_element_get_multiple (GmeDOMHTMLSelectElement *self);
gboolean gme_dom_html_select_element_set_multiple (GmeDOMHTMLSelectElement *self, gboolean param);
gchar* gme_dom_html_select_element_get_name (GmeDOMHTMLSelectElement *self);
gboolean gme_dom_html_select_element_set_name (GmeDOMHTMLSelectElement *self, const gchar *param);
gint64 gme_dom_html_select_element_get_size (GmeDOMHTMLSelectElement *self);
gboolean gme_dom_html_select_element_set_size (GmeDOMHTMLSelectElement *self, gint32 param);
gint64 gme_dom_html_select_element_get_tab_index (GmeDOMHTMLSelectElement *self);
gboolean gme_dom_html_select_element_set_tab_index (GmeDOMHTMLSelectElement *self, gint32 param);
gboolean gme_dom_html_select_element_remove (GmeDOMHTMLSelectElement *self, gint32 param);
gboolean gme_dom_html_select_element_blur (GmeDOMHTMLSelectElement *self);
gboolean gme_dom_html_select_element_focus (GmeDOMHTMLSelectElement *self);


G_END_DECLS

#endif /* __GME_DOM_HTML_SELECT_ELEMENT_H__ */
