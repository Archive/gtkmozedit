/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __GME_DOM_ELEMENT_H__
#define __GME_DOM_ELEMENT_H__

#include <glib.h>
#include <glib-object.h>
#include <glib/gmacros.h>
#include <gtkmozedit/dom/gme-dom-forward.h>
#include <gtkmozedit/dom/gme-dom-node.h>
#include <gtkmozedit/dom/gme-dom-attr.h>
#include <gtkmozedit/dom/gme-dom-node-list.h>

G_BEGIN_DECLS

#define GME_TYPE_DOM_ELEMENT                  (gme_dom_element_get_gtype ())
#define GME_DOM_ELEMENT(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), GME_TYPE_DOM_ELEMENT, GmeDOMElement))
#define GME_DOM_ELEMENT_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST ((klass), GME_TYPE_DOM_ELEMENT, GmeDOMElementClass))
#define GME_IS_DOM_ELEMENT(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GME_TYPE_DOM_ELEMENT))
#define GME_IS_DOM_ELEMENT_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), GME_TYPE_DOM_ELEMENT))
#define GME_DOM_ELEMENT_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS ((obj), GME_TYPE_DOM_ELEMENT, GmeDOMElementClass))

typedef struct _GmeDOMElementClass GmeDOMElementClass;

GType gme_dom_element_get_gtype (void);

/**
 * The nsIDOMElement interface represents an element in an HTML or 
 * XML document. 
 *
 * For more information on this interface please see 
 * http://www.w3.org/TR/DOM-Level-2-Core/
 *
 * @status FROZEN
 */

gchar* gme_dom_element_get_tag_name (GmeDOMElement *self);
gchar* gme_dom_element_get_attribute (GmeDOMElement *self, const gchar *name);
gboolean gme_dom_element_set_attribute (GmeDOMElement *self, const gchar *name, const gchar *value);
gboolean gme_dom_element_remove_attribute (GmeDOMElement *self, const gchar *name);
GmeDOMAttr* gme_dom_element_get_attribute_node (GmeDOMElement *self, const gchar *name);
GmeDOMAttr* gme_dom_element_set_attribute_node (GmeDOMElement *self, GmeDOMAttr *new_attr);
GmeDOMAttr* gme_dom_element_remove_attribute_node (GmeDOMElement *self, GmeDOMAttr *old_attr);
GmeDOMNodeList* gme_dom_element_get_elements_by_tag_name (GmeDOMElement *self, const gchar *name);
/*
// Introduced in DOM Level 2:
// gchar* gme_dom_element_get_attribute_ns (GmeDOMElement *self, const gchar *namespace_uri, const gchar *local_name);
// Introduced in DOM Level 2:
// gboolean gme_dom_element_set_attribute_ns (GmeDOMElement *self, 
// 					const gchar *namespace_uri, const gchar *qualified_name, const gchar *value);
// Introduced in DOM Level 2:
// gboolean gme_dom_element_remove_attribute_ns (GmeDOMElement *self, 
// 					const gchar *namespace_uri, const gchar *local_name);
// Introduced in DOM Level 2:
// GmeDOMAttr* gme_dom_element_get_attribute_node_ns (GmeDOMElement *self, 
// 					const gchar *namespace_uri, const gchar *local_name);
// Introduced in DOM Level 2:
// GmeDOMAttr* gme_dom_element_set_attribute_node_ns (GmeDOMElement *self, GmeDOMAttr *new_attr);
// Introduced in DOM Level 2:
// GmeDOMNodeList* gme_dom_element_get_elements_by_tag_name_ns (GmeDOMElement *self, 
// 						const gchar *namespace_uri, const gchar *local_name);
*/
/* Introduced in DOM Level 2 */
gboolean gme_dom_element_has_attribute (GmeDOMElement *self, const gchar *name);
/*
// Introduced in DOM Level 2:
// boolean gme_dom_element_has_attribute_ns (GmeDOMElement *self, 
// 					const gchar *namespace_uri, const gchar *local_name);
*/
G_END_DECLS

#endif /* __GME_DOM_ELEMENT_H__ */
