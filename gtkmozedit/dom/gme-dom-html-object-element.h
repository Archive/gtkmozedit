/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __GME_DOM_HTML_OBJECT_ELEMENT_H__
#define __GME_DOM_HTML_OBJECT_ELEMENT_H__

#include <glib.h>
#include <glib-object.h>
#include <glib/gmacros.h>
#include <gtkmozedit/dom/gme-dom-forward.h>
#include <gtkmozedit/dom/gme-dom-html-form-element.h>
#include <gtkmozedit/dom/gme-dom-document.h>


G_BEGIN_DECLS

#define GME_TYPE_DOM_HTML_OBJECT_ELEMENT                  (gme_dom_html_object_element_get_gtype ())
#define GME_DOM_HTML_OBJECT_ELEMENT(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), GME_TYPE_DOM_HTML_OBJECT_ELEMENT, GmeDOMHTMLObjectElement))
#define GME_DOM_HTML_OBJECT_ELEMENT_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST ((klass), GME_TYPE_DOM_HTML_OBJECT_ELEMENT, GmeDOMHTMLObjectElementClass))
#define GME_IS_DOM_HTML_OBJECT_ELEMENT(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GME_TYPE_DOM_HTML_OBJECT_ELEMENT))
#define GME_IS_DOM_HTML_OBJECT_ELEMENT_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), GME_TYPE_DOM_HTML_OBJECT_ELEMENT))
#define GME_DOM_HTML_OBJECT_ELEMENT_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS ((obj), GME_TYPE_DOM_HTML_OBJECT_ELEMENT, GmeDOMHTMLObjectElementClass))

typedef struct _GmeDOMHTMLObjectElementClass GmeDOMHTMLObjectElementClass;

GType gme_dom_html_object_element_get_gtype (void);

GmeDOMHTMLFormElement* gme_dom_html_object_element_get_form (GmeDOMHTMLObjectElement *self);
gchar* gme_dom_html_object_element_get_code (GmeDOMHTMLObjectElement *self);
gboolean gme_dom_html_object_element_set_code (GmeDOMHTMLObjectElement *self, const gchar *param);
gchar* gme_dom_html_object_element_get_align (GmeDOMHTMLObjectElement *self);
gboolean gme_dom_html_object_element_set_align (GmeDOMHTMLObjectElement *self, const gchar *param);
gchar* gme_dom_html_object_element_get_archive (GmeDOMHTMLObjectElement *self);
gboolean gme_dom_html_object_element_set_archive (GmeDOMHTMLObjectElement *self, const gchar *param);
gchar* gme_dom_html_object_element_get_border (GmeDOMHTMLObjectElement *self);
gboolean gme_dom_html_object_element_set_border (GmeDOMHTMLObjectElement *self, const gchar *param);
gchar* gme_dom_html_object_element_get_code_base (GmeDOMHTMLObjectElement *self);
gboolean gme_dom_html_object_element_set_code_base (GmeDOMHTMLObjectElement *self, const gchar *param);
gchar* gme_dom_html_object_element_get_code_type (GmeDOMHTMLObjectElement *self);
gboolean gme_dom_html_object_element_set_code_type (GmeDOMHTMLObjectElement *self, const gchar *param);
gchar* gme_dom_html_object_element_get_data (GmeDOMHTMLObjectElement *self);
gboolean gme_dom_html_object_element_set_data (GmeDOMHTMLObjectElement *self, const gchar *param);
gboolean gme_dom_html_object_element_get_declare (GmeDOMHTMLObjectElement *self);
gboolean gme_dom_html_object_element_set_declare (GmeDOMHTMLObjectElement *self, gboolean param);
gchar* gme_dom_html_object_element_get_height (GmeDOMHTMLObjectElement *self);
gboolean gme_dom_html_object_element_set_height (GmeDOMHTMLObjectElement *self, const gchar *param);
gint64 gme_dom_html_object_element_get_hspace (GmeDOMHTMLObjectElement *self);
gboolean gme_dom_html_object_element_set_hspace (GmeDOMHTMLObjectElement *self, gint32 param);
gchar* gme_dom_html_object_element_get_name (GmeDOMHTMLObjectElement *self);
gboolean gme_dom_html_object_element_set_name (GmeDOMHTMLObjectElement *self, const gchar *param);
gchar* gme_dom_html_object_element_get_standby (GmeDOMHTMLObjectElement *self);
gboolean gme_dom_html_object_element_set_standby (GmeDOMHTMLObjectElement *self, const gchar *param);
gint64 gme_dom_html_object_element_get_tab_index (GmeDOMHTMLObjectElement *self);
gboolean gme_dom_html_object_element_set_tab_index (GmeDOMHTMLObjectElement *self, gint32 param);
gchar* gme_dom_html_object_element_get_type (GmeDOMHTMLObjectElement *self);
gboolean gme_dom_html_object_element_set_type (GmeDOMHTMLObjectElement *self, const gchar *param);
gchar* gme_dom_html_object_element_get_use_map (GmeDOMHTMLObjectElement *self);
gboolean gme_dom_html_object_element_set_use_map (GmeDOMHTMLObjectElement *self, const gchar *param);
gint64 gme_dom_html_object_element_get_vspace (GmeDOMHTMLObjectElement *self);
gboolean gme_dom_html_object_element_set_vspace (GmeDOMHTMLObjectElement *self, gint32 param);
gchar* gme_dom_html_object_element_get_width (GmeDOMHTMLObjectElement *self);
gboolean gme_dom_html_object_element_set_width (GmeDOMHTMLObjectElement *self, const gchar *param);
GmeDOMDocument* gme_dom_html_object_element_get_content_document (GmeDOMHTMLObjectElement *self);


G_END_DECLS

#endif /* __GME_DOM_HTML_OBJECT_ELEMENT_H__ */
