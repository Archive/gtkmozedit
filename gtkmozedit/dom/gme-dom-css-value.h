/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __GME_DOM_CSS_VALUE_H__
#define __GME_DOM_CSS_VALUE_H__

#include <glib.h>
#include <glib-object.h>
#include <glib/gmacros.h>
#include <gtkmozedit/dom/gme-dom-forward.h>
#include <gtkmozedit/gme-supports.h>

G_BEGIN_DECLS

#define GME_TYPE_DOM_CSS_VALUE                  (gme_dom_css_value_get_gtype ())
#define GME_DOM_CSS_VALUE(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), GME_TYPE_DOM_CSS_VALUE, GmeDOMCSSValue))
#define GME_DOM_CSS_VALUE_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST ((klass), GME_TYPE_DOM_CSS_VALUE, GmeDOMCSSValueClass))
#define GME_IS_DOM_CSS_VALUE(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GME_TYPE_DOM_CSS_VALUE))
#define GME_IS_DOM_CSS_VALUE_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), GME_TYPE_DOM_CSS_VALUE))
#define GME_DOM_CSS_VALUE_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS ((obj), GME_TYPE_DOM_CSS_VALUE, GmeDOMCSSValueClass))

#define CSS_VALUE_TYPE_INHERIT		0
#define CSS_VALUE_TYPE_PRIMITIVE_VALUE 	1
#define CSS_VALUE_TYPE_VALUE_LIST	2
#define CSS_VALUE_TYPE_CUSTOM		3

typedef struct _GmeDOMCSSValueClass GmeDOMCSSValueClass;

GType gme_dom_css_value_get_gtype (void);

gchar* gme_dom_css_value_get_css_text (GmeDOMCSSValue *self);
gboolean gme_dom_css_value_set_css_text (GmeDOMCSSValue *self, const gchar *param);
gint32 gme_dom_css_value_get_css_value_type (GmeDOMCSSValue *self);

G_END_DECLS

#endif /* __GME_DOM_CSS_VALUE_H__ */
