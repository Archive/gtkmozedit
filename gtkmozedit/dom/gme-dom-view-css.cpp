/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <gtkmozedit/gtkmozedit-config.h>

#include "nsIDOMViewCSS.h"

#include "gme-dom-view-css-private.h"
#include "gme-dom-abstract-view-private.h"
#include "gme-dom-css-style-declaration-private.h"
#include "gme-dom-element-private.h"

enum {
	PROP_0,
	PROP_WRAPPED_PTR,
	_NUM_PROPS
};

static GmeDOMAbstractViewClass *gme_dom_view_css_parent_class = NULL;

static void
instance_init (GmeDOMViewCSS *self)
{
	self->wrapped_ptr = NULL;
	self->is_disposed = FALSE;
}

static void
instance_dispose (GObject *instance)
{
	GmeDOMViewCSS *self = GME_DOM_VIEW_CSS (instance);

	if (self->is_disposed)
		return;

	if (self->wrapped_ptr) NS_RELEASE (self->wrapped_ptr);
	self->wrapped_ptr = NULL;
	self->is_disposed = TRUE;

	gme_dom_view_css_parent_class->dispose (G_OBJECT (self));
}

static void
set_property (GObject      *object,
	      guint         prop_id,
	      GValue const *value,
	      GParamSpec   *pspec)
{
	GmeDOMViewCSS *self = GME_DOM_VIEW_CSS (object);
	nsIDOMAbstractView *ptr = NULL;
	nsresult rv;

	switch (prop_id) {
	case PROP_WRAPPED_PTR:
		ptr = reinterpret_cast<nsIDOMAbstractView*>(g_value_get_pointer (value));
		rv = CallQueryInterface (ptr, &self->wrapped_ptr);
		if (NS_SUCCEEDED (rv) && self->wrapped_ptr) {
			NS_ADDREF (self->wrapped_ptr);
			/* constuction param, init parent */
			gme_dom_abstract_view_private_set_wrapped_ptr (GME_DOM_ABSTRACT_VIEW (self), self->wrapped_ptr);
		}
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
get_property (GObject    *object,
	      guint       prop_id,
	      GValue     *value,
	      GParamSpec *pspec)
{
	GmeDOMViewCSS *self = GME_DOM_VIEW_CSS (object);

	switch (prop_id) {
	case PROP_WRAPPED_PTR:
		g_value_set_pointer (value, (gpointer) self->wrapped_ptr);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
class_init (GmeDOMViewCSSClass *klass)
{
	GObjectClass *gobject_class = G_OBJECT_CLASS (klass);

	/* vfuncs */
	klass->dispose = instance_dispose;

	/* hook gobject vfuncs */
	gobject_class->dispose = instance_dispose;

	gme_dom_view_css_parent_class = (GmeDOMAbstractViewClass*) g_type_class_peek_parent (klass);

	gobject_class->set_property = set_property;
	gobject_class->get_property = get_property;

	g_object_class_install_property (gobject_class,
		PROP_WRAPPED_PTR,
		g_param_spec_pointer ("wrapped-ptr", _("Wrapped Pointer"),
			_("Pointer to the wrapped c++ object"),
			(GParamFlags)(G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY)));
}

GType
gme_dom_view_css_get_gtype (void)
{
        static GType type = 0;
        if (!type) {
                static const GTypeInfo info = {
                        sizeof (GmeDOMViewCSSClass),
                        NULL,           /* base_init */
                        NULL,           /* base_finalize */
                        (GClassInitFunc) class_init,
                        NULL,           /* class_finalize */
                        NULL,           /* class_data */
                        sizeof (GmeDOMViewCSS),
                        0,              /* n_preallocs */
                        (GInstanceInitFunc) instance_init,
                };
                type = g_type_register_static (GME_TYPE_DOM_ABSTRACT_VIEW, "GmeDOMViewCSS", &info, (GTypeFlags)0);
        }
        return type;
}

void 
gme_dom_view_css_private_set_wrapped_ptr (GmeDOMViewCSS *self, 
				      nsIDOMViewCSS *wrapped_ptr)
{
	g_assert (self && wrapped_ptr);
	self->wrapped_ptr = wrapped_ptr;
	NS_ADDREF (self->wrapped_ptr);
	/* constuction param, init parent */
	gme_dom_abstract_view_private_set_wrapped_ptr (GME_DOM_ABSTRACT_VIEW (self), self->wrapped_ptr);
}

GmeDOMViewCSS* 
gme_dom_view_css_new (nsIDOMViewCSS *wrapped_ptr)
{
	return GME_DOM_VIEW_CSS (g_object_new (GME_TYPE_DOM_VIEW_CSS, "wrapped-ptr", wrapped_ptr, NULL));
}

GmeDOMCSSStyleDeclaration* 
gme_dom_view_css_get_computed_style (GmeDOMViewCSS *self, GmeDOMElement *element, const gchar *pseudo_element)
{
	nsIDOMCSSStyleDeclaration *d = NULL;
	GmeDOMCSSStyleDeclaration *decl = NULL;
	nsEmbedString ps;
	nsresult rv;
	g_assert (self);

	ps = NS_ConvertUTF8toUTF16 (pseudo_element);
	rv = self->wrapped_ptr->GetComputedStyle (element->wrapped_ptr, ps, &d);
	if (NS_SUCCEEDED (rv)) {
		decl = gme_dom_css_style_declaration_new (d);
	}
	return decl;

}
