/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __GME_DOM_HTML_ANCHOR_ELEMENT_H__
#define __GME_DOM_HTML_ANCHOR_ELEMENT_H__

#include <glib.h>
#include <glib-object.h>
#include <glib/gmacros.h>
#include <gtkmozedit/dom/gme-dom-forward.h>


G_BEGIN_DECLS

#define GME_TYPE_DOM_HTML_ANCHOR_ELEMENT                  (gme_dom_html_anchor_element_get_gtype ())
#define GME_DOM_HTML_ANCHOR_ELEMENT(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), GME_TYPE_DOM_HTML_ANCHOR_ELEMENT, GmeDOMHTMLAnchorElement))
#define GME_DOM_HTML_ANCHOR_ELEMENT_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST ((klass), GME_TYPE_DOM_HTML_ANCHOR_ELEMENT, GmeDOMHTMLAnchorElementClass))
#define GME_IS_DOM_HTML_ANCHOR_ELEMENT(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GME_TYPE_DOM_HTML_ANCHOR_ELEMENT))
#define GME_IS_DOM_HTML_ANCHOR_ELEMENT_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), GME_TYPE_DOM_HTML_ANCHOR_ELEMENT))
#define GME_DOM_HTML_ANCHOR_ELEMENT_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS ((obj), GME_TYPE_DOM_HTML_ANCHOR_ELEMENT, GmeDOMHTMLAnchorElementClass))

typedef struct _GmeDOMHTMLAnchorElementClass GmeDOMHTMLAnchorElementClass;

GType gme_dom_html_anchor_element_get_gtype (void);

gchar* gme_dom_html_anchor_element_get_access_key (GmeDOMHTMLAnchorElement *self);
gboolean gme_dom_html_anchor_element_set_access_key (GmeDOMHTMLAnchorElement *self, const gchar *param);
gchar* gme_dom_html_anchor_element_get_charset (GmeDOMHTMLAnchorElement *self);
gboolean gme_dom_html_anchor_element_set_charset (GmeDOMHTMLAnchorElement *self, const gchar *param);
gchar* gme_dom_html_anchor_element_get_coords (GmeDOMHTMLAnchorElement *self);
gboolean gme_dom_html_anchor_element_set_coords (GmeDOMHTMLAnchorElement *self, const gchar *param);
gchar* gme_dom_html_anchor_element_get_href (GmeDOMHTMLAnchorElement *self);
gboolean gme_dom_html_anchor_element_set_href (GmeDOMHTMLAnchorElement *self, const gchar *param);
gchar* gme_dom_html_anchor_element_get_hreflang (GmeDOMHTMLAnchorElement *self);
gboolean gme_dom_html_anchor_element_set_hreflang (GmeDOMHTMLAnchorElement *self, const gchar *param);
gchar* gme_dom_html_anchor_element_get_name (GmeDOMHTMLAnchorElement *self);
gboolean gme_dom_html_anchor_element_set_name (GmeDOMHTMLAnchorElement *self, const gchar *param);
gchar* gme_dom_html_anchor_element_get_rel (GmeDOMHTMLAnchorElement *self);
gboolean gme_dom_html_anchor_element_set_rel (GmeDOMHTMLAnchorElement *self, const gchar *param);
gchar* gme_dom_html_anchor_element_get_rev (GmeDOMHTMLAnchorElement *self);
gboolean gme_dom_html_anchor_element_set_rev (GmeDOMHTMLAnchorElement *self, const gchar *param);
gchar* gme_dom_html_anchor_element_get_shape (GmeDOMHTMLAnchorElement *self);
gboolean gme_dom_html_anchor_element_set_shape (GmeDOMHTMLAnchorElement *self, const gchar *param);
gint64 gme_dom_html_anchor_element_get_tab_index (GmeDOMHTMLAnchorElement *self);
gboolean gme_dom_html_anchor_element_set_tab_index (GmeDOMHTMLAnchorElement *self, gint32 param);
gchar* gme_dom_html_anchor_element_get_target (GmeDOMHTMLAnchorElement *self);
gboolean gme_dom_html_anchor_element_set_target (GmeDOMHTMLAnchorElement *self, const gchar *param);
gchar* gme_dom_html_anchor_element_get_type (GmeDOMHTMLAnchorElement *self);
gboolean gme_dom_html_anchor_element_set_type (GmeDOMHTMLAnchorElement *self, const gchar *param);
gboolean gme_dom_html_anchor_element_blur (GmeDOMHTMLAnchorElement *self);
gboolean gme_dom_html_anchor_element_focus (GmeDOMHTMLAnchorElement *self);


G_END_DECLS

#endif /* __GME_DOM_HTML_ANCHOR_ELEMENT_H__ */
