/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __GME_DOM_PROCESSING_INSTRUCTION_H__
#define __GME_DOM_PROCESSING_INSTRUCTION_H__

#include <glib.h>
#include <glib-object.h>
#include <glib/gmacros.h>
#include <gtkmozedit/dom/gme-dom-forward.h>

G_BEGIN_DECLS

#define GME_TYPE_DOM_PROCESSING_INSTRUCTION                  (gme_dom_processing_instruction_get_gtype ())
#define GME_DOM_PROCESSING_INSTRUCTION(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), GME_TYPE_DOM_PROCESSING_INSTRUCTION, GmeDOMProcessingInstruction))
#define GME_DOM_PROCESSING_INSTRUCTION_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST ((klass), GME_TYPE_DOM_PROCESSING_INSTRUCTION, GmeDOMProcessingInstructionClass))
#define GME_IS_DOM_PROCESSING_INSTRUCTION(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GME_TYPE_DOM_PROCESSING_INSTRUCTION))
#define GME_IS_DOM_PROCESSING_INSTRUCTION_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), GME_TYPE_DOM_PROCESSING_INSTRUCTION))
#define GME_DOM_PROCESSING_INSTRUCTION_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS ((obj), GME_TYPE_DOM_PROCESSING_INSTRUCTION, GmeDOMProcessingInstructionClass))

typedef struct _GmeDOMProcessingInstructionClass GmeDOMProcessingInstructionClass;

GType gme_dom_processing_instruction_get_gtype (void);

/**
 * The nsIDOMProcessingInstruction interface represents a 
 * "processing instruction", used in XML as a way to keep processor-specific 
 * information in the text of the document.
 *
 * For more information on this interface please see 
 * http://www.w3.org/TR/DOM-Level-2-Core/
 *
 * @status FROZEN
 */

gchar* gme_dom_processing_instruction_get_target (GmeDOMProcessingInstruction *self);
gchar* gme_dom_processing_instruction_get_data (GmeDOMProcessingInstruction *self);
gboolean gme_dom_processing_instruction_set_data (GmeDOMProcessingInstruction *self, const gchar *data);

G_END_DECLS

#endif /* __GME_DOM_PROCESSING_INSTRUCTION_H__ */
