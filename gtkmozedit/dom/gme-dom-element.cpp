/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <gtkmozedit/gtkmozedit-config.h>

#include "nsIDOMNode.h"
#include "nsIDOMElement.h"

#include "gme-dom-element-private.h"
#include "gme-dom-attr-private.h"
#include "gme-dom-node-private.h"
#include "gme-dom-node-list-private.h"

enum {
	PROP_0,
	PROP_WRAPPED_PTR,
	_NUM_PROPS
};

static GmeDOMNodeClass *gme_dom_element_parent_class = NULL;

static void
instance_init (GmeDOMElement *self)
{
	self->wrapped_ptr = NULL;
	self->is_disposed = FALSE;
}

static void
instance_dispose (GObject *instance)
{
	GmeDOMElement *self = GME_DOM_ELEMENT (instance);

	if (self->is_disposed)
		return;

	if (self->wrapped_ptr) NS_RELEASE (self->wrapped_ptr);
	self->wrapped_ptr = NULL;
	self->is_disposed = TRUE;

	gme_dom_element_parent_class->dispose (G_OBJECT (self));
}

static void
set_property (GObject      *object,
	      guint         prop_id,
	      GValue const *value,
	      GParamSpec   *pspec)
{
	GmeDOMElement *self = GME_DOM_ELEMENT (object);
	nsISupports *ptr = NULL;
	nsresult rv;

	switch (prop_id) {
	case PROP_WRAPPED_PTR:
		ptr = reinterpret_cast<nsISupports*>(g_value_get_pointer (value));
		rv = CallQueryInterface (ptr, &self->wrapped_ptr);
		if (NS_SUCCEEDED (rv) && self->wrapped_ptr) {
			NS_ADDREF (self->wrapped_ptr);
			/* constuction param, init parent */
			gme_dom_node_private_set_wrapped_ptr (GME_DOM_NODE (self), self->wrapped_ptr);
		}
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
get_property (GObject    *object,
	      guint       prop_id,
	      GValue     *value,
	      GParamSpec *pspec)
{
	GmeDOMElement *self = GME_DOM_ELEMENT (object);

	switch (prop_id) {
	case PROP_WRAPPED_PTR:
		g_value_set_pointer (value, (gpointer) self->wrapped_ptr);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
class_init (GmeDOMElementClass *klass)
{
	GObjectClass *gobject_class = G_OBJECT_CLASS (klass);

	/* vfuncs */
	klass->dispose = instance_dispose;

	/* hook gobject vfuncs */
	gobject_class->dispose = instance_dispose;

	gme_dom_element_parent_class = (GmeDOMNodeClass*) g_type_class_peek_parent (klass);

	gobject_class->set_property = set_property;
	gobject_class->get_property = get_property;

	g_object_class_install_property (gobject_class,
		PROP_WRAPPED_PTR,
		g_param_spec_pointer ("wrapped-ptr", _("Wrapped Pointer"),
			_("Pointer to the wrapped c++ object"),
			(GParamFlags)(G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY)));
}

GType
gme_dom_element_get_gtype (void)
{
        static GType type = 0;
        if (!type) {
                static const GTypeInfo info = {
                        sizeof (GmeDOMElementClass),
                        NULL,           /* base_init */
                        NULL,           /* base_finalize */
                        (GClassInitFunc) class_init,
                        NULL,           /* class_finalize */
                        NULL,           /* class_data */
                        sizeof (GmeDOMElement),
                        0,              /* n_preallocs */
                        (GInstanceInitFunc) instance_init,
                };
                type = g_type_register_static (GME_TYPE_DOM_NODE, "GmeDOMElement", &info, (GTypeFlags)0);
        }
        return type;
}

void 
gme_dom_element_private_set_wrapped_ptr (GmeDOMElement *self, 
					 nsIDOMElement *element)
{
	g_assert (self && element);
	self->wrapped_ptr = element;
	NS_ADDREF (self->wrapped_ptr);
	/* constuction param, init parent */
	gme_supports_private_set_wrapped_ptr (GME_SUPPORTS (self), self->wrapped_ptr);
}

GmeDOMElement* 
gme_dom_element_new (nsIDOMElement *element)
{
	return GME_DOM_ELEMENT (g_object_new (GME_TYPE_DOM_ELEMENT, "wrapped-ptr", element, NULL));
}

gchar* 
gme_dom_element_get_tag_name (GmeDOMElement *self)
{
	nsEmbedString name;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetTagName (name);
	if (NS_SUCCEEDED (rv)) {
		return g_strdup (NS_ConvertUTF16toUTF8 (name).get ());
	}
	return NULL;
}

gchar* 
gme_dom_element_get_attribute (GmeDOMElement *self, const gchar *name)
{
	nsEmbedString n, a;
	nsresult rv;
	g_assert (self);

	n = NS_ConvertUTF8toUTF16 (name);
	rv = self->wrapped_ptr->GetAttribute (n, a);
	if (NS_SUCCEEDED (rv)) {
		return g_strdup (NS_ConvertUTF16toUTF8 (a).get ());
	}
	return NULL;
}

gboolean 
gme_dom_element_set_attribute (GmeDOMElement *self, const gchar *name, const gchar *value)
{
	nsEmbedString n, v;
	nsresult rv;
	g_assert (self);

	n = NS_ConvertUTF8toUTF16 (name);
	v = NS_ConvertUTF8toUTF16 (value);
	rv = self->wrapped_ptr->SetAttribute (n, v);
	return NS_SUCCEEDED (rv);	
}

gboolean 
gme_dom_element_remove_attribute (GmeDOMElement *self, const gchar *name)
{
	nsEmbedString n;
	nsresult rv;
	g_assert (self);

	n = NS_ConvertUTF8toUTF16 (name);
	rv = self->wrapped_ptr->RemoveAttribute (n);
	return NS_SUCCEEDED (rv); 
}

GmeDOMAttr* 
gme_dom_element_get_attribute_node (GmeDOMElement *self, const gchar *name)
{
	nsIDOMAttr *a = NULL;
	GmeDOMAttr *attr = NULL;
	nsEmbedString n;
	nsresult rv;
	g_assert (self);

	n = NS_ConvertUTF8toUTF16 (name);
	rv = self->wrapped_ptr->GetAttributeNode (n, &(a));
	if (NS_SUCCEEDED (rv)) {
		attr = gme_dom_attr_new (a);
	}
	return attr;
}

GmeDOMAttr* 
gme_dom_element_set_attribute_node (GmeDOMElement *self, GmeDOMAttr *new_attr)
{
	nsIDOMAttr *a = NULL;
	GmeDOMAttr *attr = NULL;
	nsresult rv;
	g_assert (self && new_attr);

	rv = self->wrapped_ptr->SetAttributeNode (new_attr->attr, &(a));
	if (NS_SUCCEEDED (rv)) {
		attr = gme_dom_attr_new (a);
	}
	return attr;
}

GmeDOMAttr* 
gme_dom_element_remove_attribute_node (GmeDOMElement *self, GmeDOMAttr *old_attr)
{
	nsIDOMAttr *a = NULL;
	GmeDOMAttr *attr = NULL;
	nsresult rv;
	g_assert (self && old_attr);

	rv = self->wrapped_ptr->RemoveAttributeNode (old_attr->attr, &(a));
	if (NS_SUCCEEDED (rv)) {
		attr = gme_dom_attr_new (a);
	}
	return attr;
}

GmeDOMNodeList* 
gme_dom_element_get_elements_by_tag_name (GmeDOMElement *self, const gchar *name)
{
	nsIDOMNodeList *l = NULL;
	GmeDOMNodeList *list = NULL;
	nsEmbedString n;
	nsresult rv;
	g_assert (self);

	n = NS_ConvertUTF8toUTF16 (name);
	rv = self->wrapped_ptr->GetElementsByTagName (n, &(l));
	if (NS_SUCCEEDED (rv)) {
		list = gme_dom_node_list_new (l);
	}
	return list;	
}

gboolean 
gme_dom_element_has_attribute (GmeDOMElement *self, const gchar *name)
{
	nsEmbedString n;
	gboolean has_attribute;
	nsresult rv;
	g_assert (self);

	n = NS_ConvertUTF8toUTF16 (name);
	rv = self->wrapped_ptr->HasAttribute (n, &has_attribute);
	return has_attribute;
}
