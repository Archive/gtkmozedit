/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __GME_DOM_HTML_OPTION_ELEMENT_H__
#define __GME_DOM_HTML_OPTION_ELEMENT_H__

#include <glib.h>
#include <glib-object.h>
#include <glib/gmacros.h>
#include <gtkmozedit/dom/gme-dom-forward.h>
#include <gtkmozedit/dom/gme-dom-html-form-element.h>


G_BEGIN_DECLS

#define GME_TYPE_DOM_HTML_OPTION_ELEMENT                  (gme_dom_html_option_element_get_gtype ())
#define GME_DOM_HTML_OPTION_ELEMENT(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), GME_TYPE_DOM_HTML_OPTION_ELEMENT, GmeDOMHTMLOptionElement))
#define GME_DOM_HTML_OPTION_ELEMENT_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST ((klass), GME_TYPE_DOM_HTML_OPTION_ELEMENT, GmeDOMHTMLOptionElementClass))
#define GME_IS_DOM_HTML_OPTION_ELEMENT(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GME_TYPE_DOM_HTML_OPTION_ELEMENT))
#define GME_IS_DOM_HTML_OPTION_ELEMENT_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), GME_TYPE_DOM_HTML_OPTION_ELEMENT))
#define GME_DOM_HTML_OPTION_ELEMENT_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS ((obj), GME_TYPE_DOM_HTML_OPTION_ELEMENT, GmeDOMHTMLOptionElementClass))

typedef struct _GmeDOMHTMLOptionElementClass GmeDOMHTMLOptionElementClass;

GType gme_dom_html_option_element_get_gtype (void);

GmeDOMHTMLFormElement* gme_dom_html_option_element_get_form (GmeDOMHTMLOptionElement *self);
gboolean gme_dom_html_option_element_get_default_selected (GmeDOMHTMLOptionElement *self);
gboolean gme_dom_html_option_element_set_default_selected (GmeDOMHTMLOptionElement *self, gboolean param);
gchar* gme_dom_html_option_element_get_text (GmeDOMHTMLOptionElement *self);
gint64 gme_dom_html_option_element_get_index (GmeDOMHTMLOptionElement *self);
gboolean gme_dom_html_option_element_get_disabled (GmeDOMHTMLOptionElement *self);
gboolean gme_dom_html_option_element_set_disabled (GmeDOMHTMLOptionElement *self, gboolean param);
gchar* gme_dom_html_option_element_get_label (GmeDOMHTMLOptionElement *self);
gboolean gme_dom_html_option_element_set_label (GmeDOMHTMLOptionElement *self, const gchar *param);
gboolean gme_dom_html_option_element_get_selected (GmeDOMHTMLOptionElement *self);
gboolean gme_dom_html_option_element_set_selected (GmeDOMHTMLOptionElement *self, gboolean param);
gchar* gme_dom_html_option_element_get_value (GmeDOMHTMLOptionElement *self);
gboolean gme_dom_html_option_element_set_value (GmeDOMHTMLOptionElement *self, const gchar *param);


G_END_DECLS

#endif /* __GME_DOM_HTML_OPTION_ELEMENT_H__ */
