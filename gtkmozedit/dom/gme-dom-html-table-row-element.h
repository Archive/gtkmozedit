/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __GME_DOM_HTML_TABLE_ROW_ELEMENT_H__
#define __GME_DOM_HTML_TABLE_ROW_ELEMENT_H__

#include <glib.h>
#include <glib-object.h>
#include <glib/gmacros.h>
#include <gtkmozedit/dom/gme-dom-forward.h>
#include <gtkmozedit/dom/gme-dom-html-collection.h>


G_BEGIN_DECLS

#define GME_TYPE_DOM_HTML_TABLE_ROW_ELEMENT                  (gme_dom_html_table_row_element_get_gtype ())
#define GME_DOM_HTML_TABLE_ROW_ELEMENT(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), GME_TYPE_DOM_HTML_TABLE_ROW_ELEMENT, GmeDOMHTMLTableRowElement))
#define GME_DOM_HTML_TABLE_ROW_ELEMENT_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST ((klass), GME_TYPE_DOM_HTML_TABLE_ROW_ELEMENT, GmeDOMHTMLTableRowElementClass))
#define GME_IS_DOM_HTML_TABLE_ROW_ELEMENT(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GME_TYPE_DOM_HTML_TABLE_ROW_ELEMENT))
#define GME_IS_DOM_HTML_TABLE_ROW_ELEMENT_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), GME_TYPE_DOM_HTML_TABLE_ROW_ELEMENT))
#define GME_DOM_HTML_TABLE_ROW_ELEMENT_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS ((obj), GME_TYPE_DOM_HTML_TABLE_ROW_ELEMENT, GmeDOMHTMLTableRowElementClass))

typedef struct _GmeDOMHTMLTableRowElementClass GmeDOMHTMLTableRowElementClass;

GType gme_dom_html_table_row_element_get_gtype (void);

gint64 gme_dom_html_table_row_element_get_row_index (GmeDOMHTMLTableRowElement *self);
gint64 gme_dom_html_table_row_element_get_section_row_index (GmeDOMHTMLTableRowElement *self);
GmeDOMHTMLCollection* gme_dom_html_table_row_element_get_cells (GmeDOMHTMLTableRowElement *self);
gchar* gme_dom_html_table_row_element_get_align (GmeDOMHTMLTableRowElement *self);
gboolean gme_dom_html_table_row_element_set_align (GmeDOMHTMLTableRowElement *self, const gchar *param);
gchar* gme_dom_html_table_row_element_get_bg_color (GmeDOMHTMLTableRowElement *self);
gboolean gme_dom_html_table_row_element_set_bg_color (GmeDOMHTMLTableRowElement *self, const gchar *param);
gchar* gme_dom_html_table_row_element_get_ch (GmeDOMHTMLTableRowElement *self);
gboolean gme_dom_html_table_row_element_set_ch (GmeDOMHTMLTableRowElement *self, const gchar *param);
gchar* gme_dom_html_table_row_element_get_ch_off (GmeDOMHTMLTableRowElement *self);
gboolean gme_dom_html_table_row_element_set_ch_off (GmeDOMHTMLTableRowElement *self, const gchar *param);
gchar* gme_dom_html_table_row_element_get_v_align (GmeDOMHTMLTableRowElement *self);
gboolean gme_dom_html_table_row_element_set_v_align (GmeDOMHTMLTableRowElement *self, const gchar *param);


G_END_DECLS

#endif /* __GME_DOM_HTML_TABLE_ROW_ELEMENT_H__ */
