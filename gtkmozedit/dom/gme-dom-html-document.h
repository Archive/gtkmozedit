/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __GME_DOM_HTML_DOCUMENT_H__
#define __GME_DOM_HTML_DOCUMENT_H__

#include <glib.h>
#include <glib-object.h>
#include <glib/gmacros.h>
#include <gtkmozedit/dom/gme-dom-forward.h>
#include <gtkmozedit/dom/gme-dom-html-collection.h>
#include <gtkmozedit/dom/gme-dom-html-element.h>
#include <gtkmozedit/dom/gme-dom-node-list.h>

G_BEGIN_DECLS

#define GME_TYPE_DOM_HTML_DOCUMENT                  (gme_dom_html_document_get_gtype ())
#define GME_DOM_HTML_DOCUMENT(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), GME_TYPE_DOM_HTML_DOCUMENT, GmeDOMHTMLDocument))
#define GME_DOM_HTML_DOCUMENT_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST ((klass), GME_TYPE_DOM_HTML_DOCUMENT, GmeDOMHTMLDocumentClass))
#define GME_IS_DOM_HTML_DOCUMENT(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GME_TYPE_DOM_HTML_DOCUMENT))
#define GME_IS_DOM_HTML_DOCUMENT_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), GME_TYPE_DOM_HTML_DOCUMENT))
#define GME_DOM_HTML_DOCUMENT_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS ((obj), GME_TYPE_DOM_HTML_DOCUMENT, GmeDOMHTMLDocumentClass))

typedef struct _GmeDOMHTMLDocumentClass GmeDOMHTMLDocumentClass;

GType gme_dom_html_document_get_gtype (void);

/**
 * The nsIDOMHTMLDocument interface is the interface to a [X]HTML
 * document object.
 *
 * For more information on this interface please see
 * http://www.w3.org/TR/DOM-Level-2-HTML/
 *
 * @status FROZEN
 */

gchar* gme_dom_html_document_get_title (GmeDOMHTMLDocument *self);
gboolean gme_dom_html_document_set_title (GmeDOMHTMLDocument *self, const gchar *title);

gchar* gme_dom_html_document_get_referrer (GmeDOMHTMLDocument *self);

gchar* gme_dom_html_document_get_domain (GmeDOMHTMLDocument *self);
gchar* gme_dom_html_document_get_url (GmeDOMHTMLDocument *self);

GmeDOMHTMLElement* gme_dom_html_document_get_body (GmeDOMHTMLDocument *self);
gboolean gme_dom_html_document_set_body (GmeDOMHTMLDocument *self, GmeDOMHTMLElement *body);

GmeDOMHTMLCollection* gme_dom_html_document_get_images (GmeDOMHTMLDocument *self);
GmeDOMHTMLCollection* gme_dom_html_document_get_applets (GmeDOMHTMLDocument *self);
GmeDOMHTMLCollection* gme_dom_html_document_get_links (GmeDOMHTMLDocument *self);
GmeDOMHTMLCollection* gme_dom_html_document_get_forms (GmeDOMHTMLDocument *self);
GmeDOMHTMLCollection* gme_dom_html_document_get_anchors (GmeDOMHTMLDocument *self);

gchar* gme_dom_html_document_get_cookie (GmeDOMHTMLDocument *self);
gboolean gme_dom_html_document_set_cookie (GmeDOMHTMLDocument *self, const gchar *cookie);

/* TODO implement
  // open() is noscript here since the DOM Level 0 version of open()
  // returned nsIDOMDocument, and not void. The script version of
  // open() is defined in nsIDOMNSHTMLDocument.
  [noscript] void           open();
  void                      close();

  // The methods write() and writeln() must be callable from JS with
  // no arguments for backwards compatibility, thus they are marked
  // [noscript] here. The JS versions of these methods are defined in
  // nsIDOMNSHTMLDocument.
  [noscript] void           write(in DOMString text);
  [noscript] void           writeln(in DOMString text);
*/

GmeDOMNodeList* gme_dom_html_document_get_elements_by_name (GmeDOMHTMLDocument *self, const gchar *name);

G_END_DECLS

#endif /* __GME_DOM_HTML_DOCUMENT_H__ */
