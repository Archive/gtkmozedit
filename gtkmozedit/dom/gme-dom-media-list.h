/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __GME_DOM_MEDIA_LIST_H__
#define __GME_DOM_MEDIA_LIST_H__

#include <glib.h>
#include <glib-object.h>
#include <glib/gmacros.h>
#include <gtkmozedit/dom/gme-dom-forward.h>
#include <gtkmozedit/gme-supports.h>

G_BEGIN_DECLS

#define GME_TYPE_DOM_MEDIA_LIST                  (gme_dom_media_list_get_gtype ())
#define GME_DOM_MEDIA_LIST(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), GME_TYPE_DOM_MEDIA_LIST, GmeDOMMediaList))
#define GME_DOM_MEDIA_LIST_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST ((klass), GME_TYPE_DOM_MEDIA_LIST, GmeDOMMediaListClass))
#define GME_IS_DOM_MEDIA_LIST(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GME_TYPE_DOM_MEDIA_LIST))
#define GME_IS_DOM_MEDIA_LIST_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), GME_TYPE_DOM_MEDIA_LIST))
#define GME_DOM_MEDIA_LIST_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS ((obj), GME_TYPE_DOM_MEDIA_LIST, GmeDOMMediaListClass))

typedef struct _GmeDOMMediaListClass GmeDOMMediaListClass;

GType gme_dom_media_list_get_gtype (void);

gchar* gme_dom_media_list_get_media_text (GmeDOMMediaList *self);
gboolean gme_dom_media_list_set_media_text (GmeDOMMediaList *self, const gchar *param);
gint64 gme_dom_media_list_get_length (GmeDOMMediaList *self);
gchar* gme_dom_media_list_item (GmeDOMMediaList *self, guint32 index);
gboolean gme_dom_media_list_delete_medium (GmeDOMMediaList *self, const gchar *old_medium);
gboolean gme_dom_media_list_append_medium (GmeDOMMediaList *self, const gchar *new_medium);

G_END_DECLS

#endif /* __GME_DOM_MEDIA_LIST_H__ */
