/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __GME_DOM_CSS_FONT_FACE_RULE_H__
#define __GME_DOM_CSS_FONT_FACE_RULE_H__

#include <glib.h>
#include <glib-object.h>
#include <glib/gmacros.h>
#include <gtkmozedit/dom/gme-dom-forward.h>
#include <gtkmozedit/dom/gme-dom-css-style-declaration.h>


G_BEGIN_DECLS

#define GME_TYPE_DOM_CSS_FONT_FACE_RULE                  (gme_dom_css_font_face_rule_get_gtype ())
#define GME_DOM_CSS_FONT_FACE_RULE(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), GME_TYPE_DOM_CSS_FONT_FACE_RULE, GmeDOMCSSFontFaceRule))
#define GME_DOM_CSS_FONT_FACE_RULE_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST ((klass), GME_TYPE_DOM_CSS_FONT_FACE_RULE, GmeDOMCSSFontFaceRuleClass))
#define GME_IS_DOM_CSS_FONT_FACE_RULE(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GME_TYPE_DOM_CSS_FONT_FACE_RULE))
#define GME_IS_DOM_CSS_FONT_FACE_RULE_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), GME_TYPE_DOM_CSS_FONT_FACE_RULE))
#define GME_DOM_CSS_FONT_FACE_RULE_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS ((obj), GME_TYPE_DOM_CSS_FONT_FACE_RULE, GmeDOMCSSFontFaceRuleClass))

typedef struct _GmeDOMCSSFontFaceRuleClass GmeDOMCSSFontFaceRuleClass;

GType gme_dom_css_font_face_rule_get_gtype (void);

GmeDOMCSSStyleDeclaration* gme_dom_css_font_face_rule_get_style (GmeDOMCSSFontFaceRule *self);


G_END_DECLS

#endif /* __GME_DOM_CSS_FONT_FACE_RULE_H__ */
