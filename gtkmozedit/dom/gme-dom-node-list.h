/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __GME_DOM_NODE_LIST_H__
#define __GME_DOM_NODE_LIST_H__

#include <glib.h>
#include <glib-object.h>
#include <glib/gmacros.h>
#include <gtkmozedit/dom/gme-dom-forward.h>
#include <gtkmozedit/dom/gme-dom-node.h>

G_BEGIN_DECLS

#define GME_TYPE_DOM_NODE_LIST                  (gme_dom_node_list_get_gtype ())
#define GME_DOM_NODE_LIST(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), GME_TYPE_DOM_NODE_LIST, GmeDOMNodeList))
#define GME_DOM_NODE_LIST_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST ((klass), GME_TYPE_DOM_NODE_LIST, GmeDOMNodeListClass))
#define GME_IS_DOM_NODE_LIST(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GME_TYPE_DOM_NODE_LIST))
#define GME_IS_DOM_NODE_LIST_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), GME_TYPE_DOM_NODE_LIST))
#define GME_DOM_NODE_LIST_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS ((obj), GME_TYPE_DOM_NODE_LIST, GmeDOMNodeListClass))

typedef struct _GmeDOMNodeListClass GmeDOMNodeListClass;

GType gme_dom_node_list_get_gtype (void);

/**
 * The GmeDOMNodeList interface provides the abstraction of an ordered 
 * collection of nodes, without defining or constraining how this collection 
 * is implemented.
 * The items in the list are accessible via an integral index, starting from 0.
 *
 * For more information on this interface please see 
 * http://www.w3.org/TR/DOM-Level-2-Core/
 *
 * @status FROZEN
 */

GmeDOMNode* gme_dom_node_list_get_item (GmeDOMNodeList *self, gulong index);
gint64 gme_dom_node_list_get_length (GmeDOMNodeList *self);

G_END_DECLS

#endif /* __GME_DOM_NODE_LIST_H__ */
