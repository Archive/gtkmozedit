/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __GME_DOM_CSS_STYLE_DECLARATION_H__
#define __GME_DOM_CSS_STYLE_DECLARATION_H__

#include <glib.h>
#include <glib-object.h>
#include <glib/gmacros.h>
#include <gtkmozedit/dom/gme-dom-forward.h>
#include <gtkmozedit/gme-supports.h>

G_BEGIN_DECLS

#define GME_TYPE_DOM_CSS_STYLE_DECLARATION                  (gme_dom_css_style_declaration_get_gtype ())
#define GME_DOM_CSS_STYLE_DECLARATION(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), GME_TYPE_DOM_CSS_STYLE_DECLARATION, GmeDOMCSSStyleDeclaration))
#define GME_DOM_CSS_STYLE_DECLARATION_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST ((klass), GME_TYPE_DOM_CSS_STYLE_DECLARATION, GmeDOMCSSStyleDeclarationClass))
#define GME_IS_DOM_CSS_STYLE_DECLARATION(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GME_TYPE_DOM_CSS_STYLE_DECLARATION))
#define GME_IS_DOM_CSS_STYLE_DECLARATION_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), GME_TYPE_DOM_CSS_STYLE_DECLARATION))
#define GME_DOM_CSS_STYLE_DECLARATION_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS ((obj), GME_TYPE_DOM_CSS_STYLE_DECLARATION, GmeDOMCSSStyleDeclarationClass))

#define CSS_STYLE_DECLARATION_TYPE_INHERIT		0
#define CSS_STYLE_DECLARATION_TYPE_PRIMITIVE_STYLE_DECLARATION 	1
#define CSS_STYLE_DECLARATION_TYPE_STYLE_DECLARATION_LIST	2
#define CSS_STYLE_DECLARATION_TYPE_CUSTOM		3

typedef struct _GmeDOMCSSStyleDeclarationClass GmeDOMCSSStyleDeclarationClass;

GType gme_dom_css_style_declaration_get_gtype (void);

gchar* gme_dom_css_style_declaration_get_css_text (GmeDOMCSSStyleDeclaration *self);
gboolean gme_dom_css_style_declaration_set_css_text (GmeDOMCSSStyleDeclaration *self, const gchar *param);
gchar* gme_dom_css_style_declaration_get_property_value (GmeDOMCSSStyleDeclaration *self, const gchar *name);
GmeDOMCSSValue* gme_dom_css_style_declaration_get_property_css_value (GmeDOMCSSStyleDeclaration *self, const gchar *name);
gchar* gme_dom_css_style_declaration_remove_property (GmeDOMCSSStyleDeclaration *self, const gchar *name);
gchar* gme_dom_css_style_declaration_get_property_priority (GmeDOMCSSStyleDeclaration *self, const gchar *name);
gboolean gme_dom_css_style_declaration_set_property (GmeDOMCSSStyleDeclaration *self, const gchar *name, const gchar *value, const gchar *priority);
gint64 gme_dom_css_style_declaration_get_length (GmeDOMCSSStyleDeclaration *self);
gchar* gme_dom_css_style_declaration_item (GmeDOMCSSStyleDeclaration *self, guint32 index);
GmeDOMCSSRule* gme_dom_css_style_declaration_get_parent_rule (GmeDOMCSSStyleDeclaration *self);

G_END_DECLS

#endif /* __GME_DOM_CSS_STYLE_DECLARATION_H__ */
