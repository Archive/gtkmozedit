/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <gtkmozedit/gtkmozedit-config.h>

#include "nsIDOMCSSMediaRule.h"

#include "gme-dom-css-media-rule-private.h"
#include "gme-dom-css-rule-list-private.h"
#include "gme-dom-media-list-private.h"


enum {
	PROP_0,
	PROP_WRAPPED_PTR,
	_NUM_PROPS
};

static GmeDOMCSSRuleClass *gme_dom_css_media_rule_parent_class = NULL;

static void
instance_init (GmeDOMCSSMediaRule *self)
{
	self->wrapped_ptr = NULL;
	self->is_disposed = FALSE;
}

static void
instance_dispose (GObject *instance)
{
	GmeDOMCSSMediaRule *self = GME_DOM_CSS_MEDIA_RULE (instance);

	if (self->is_disposed)
		return;

	if (self->wrapped_ptr) NS_RELEASE (self->wrapped_ptr);
	self->wrapped_ptr = NULL;
	self->is_disposed = TRUE;

	gme_dom_css_media_rule_parent_class->dispose (G_OBJECT (self));
}

static void
set_property (GObject      *object,
	      guint         prop_id,
	      GValue const *value,
	      GParamSpec   *pspec)
{
	GmeDOMCSSMediaRule *self = GME_DOM_CSS_MEDIA_RULE (object);
	nsISupports *ptr = NULL;
	nsresult rv;

	switch (prop_id) {
	case PROP_WRAPPED_PTR:
		ptr = reinterpret_cast<nsISupports*>(g_value_get_pointer (value));
		rv = CallQueryInterface (ptr, &self->wrapped_ptr);
		if (NS_SUCCEEDED (rv) && self->wrapped_ptr) {
			NS_ADDREF (self->wrapped_ptr);
			/* constuction param, init parent */
			gme_dom_css_rule_private_set_wrapped_ptr (GME_DOM_CSS_RULE (self), self->wrapped_ptr);
		}
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
get_property (GObject    *object,
	      guint       prop_id,
	      GValue     *value,
	      GParamSpec *pspec)
{
	GmeDOMCSSMediaRule *self = GME_DOM_CSS_MEDIA_RULE (object);

	switch (prop_id) {
	case PROP_WRAPPED_PTR:
		g_value_set_pointer (value, (gpointer) self->wrapped_ptr);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
class_init (GmeDOMCSSMediaRuleClass *klass)
{
	GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
	GmeDOMCSSRuleClass *node_class = GME_DOM_CSS_RULE_CLASS (klass);

	/* vfuncs */
	klass->dispose = instance_dispose;

	/* hook gobject vfuncs */
	gobject_class->dispose = instance_dispose;

	gme_dom_css_media_rule_parent_class = (GmeDOMCSSRuleClass*) g_type_class_peek_parent (klass);

	gobject_class->set_property = set_property;
	gobject_class->get_property = get_property;

	g_object_class_install_property (gobject_class,
		PROP_WRAPPED_PTR,
		g_param_spec_pointer ("wrapped-ptr", _("Wrapped Pointer"),
			_("Pointer to the wrapped c++ object"),
			(GParamFlags)(G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY)));
}

GType
gme_dom_css_media_rule_get_gtype (void)
{
        static GType type = 0;
        if (!type) {
                static const GTypeInfo info = {
                        sizeof (GmeDOMCSSMediaRuleClass),
                        NULL,           /* base_init */
                        NULL,           /* base_finalize */
                        (GClassInitFunc) class_init,
                        NULL,           /* class_finalize */
                        NULL,           /* class_data */
                        sizeof (GmeDOMCSSMediaRule),
                        0,              /* n_preallocs */
                        (GInstanceInitFunc) instance_init,
                };
                type = g_type_register_static (GME_TYPE_DOM_CSS_RULE, "GmeDOMCSSMediaRule", &info, (GTypeFlags)0);
        }
        return type;
}

void 
gme_dom_css_media_rule_private_set_wrapped_ptr (GmeDOMCSSMediaRule *self, 
				      nsIDOMCSSMediaRule *wrapped_ptr)
{
	g_assert (self && wrapped_ptr);
	self->wrapped_ptr = wrapped_ptr;
	NS_ADDREF (self->wrapped_ptr);
	/* constuction param, init parent */
	gme_dom_css_rule_private_set_wrapped_ptr (GME_DOM_CSS_RULE (self), self->wrapped_ptr);
}

GmeDOMCSSMediaRule* 
gme_dom_css_media_rule_new (nsIDOMCSSMediaRule *wrapped_ptr)
{
	return GME_DOM_CSS_MEDIA_RULE (g_object_new (GME_TYPE_DOM_CSS_MEDIA_RULE, "wrapped-ptr", wrapped_ptr, NULL));
}

GmeDOMMediaList* 
gme_dom_css_media_rule_get_media (GmeDOMCSSMediaRule *self)
{
	nsIDOMMediaList *param = NULL;
	GmeDOMMediaList *object = NULL;
	nsresult rv;
	g_assert (self);
	
	rv = self->wrapped_ptr->GetMedia (&param);
	if (NS_SUCCEEDED (rv)) {
		object = gme_dom_media_list_new (param);
	}
	return object;
}

GmeDOMCSSRuleList* 
gme_dom_css_media_rule_get_css_rules (GmeDOMCSSMediaRule *self)
{
	nsIDOMCSSRuleList *param = NULL;
	GmeDOMCSSRuleList *object = NULL;
	nsresult rv;
	g_assert (self);
	
	rv = self->wrapped_ptr->GetCssRules (&param);
	if (NS_SUCCEEDED (rv)) {
		object = gme_dom_css_rule_list_new (param);
	}
	return object;
}

gint64 
gme_dom_css_media_rule_insert_rule (GmeDOMCSSMediaRule *self, const gchar *rule, guint32 index)
{
	nsEmbedString s;
	guint32 ret;
	nsresult rv;
	g_assert (self);

	s = NS_ConvertUTF8toUTF16 (rule);
	rv = self->wrapped_ptr->InsertRule (s, index, &ret);
	if (NS_SUCCEEDED (rv)) {
		return ret;
	}
	return -1;
}

gboolean 
gme_dom_css_media_rule_delete_rule (GmeDOMCSSMediaRule *self, guint32 index)
{
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->DeleteRule (index);
	return NS_SUCCEEDED (rv);
}
