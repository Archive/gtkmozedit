/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __GME_DOM_DOCUMENT_CSS_PRIVATE_H__
#define __GME_DOM_DOCUMENT_CSS_PRIVATE_H__

#include <glib/gmacros.h>
#include <nsIDOMDocumentCSS.h>

#include <gtkmozedit/dom/gme-dom-document-css.h>
#include <gtkmozedit/dom/gme-dom-document-style-private.h>
#include <gtkmozedit/dom/gme-dom-css-style-declaration-private.h>

G_BEGIN_DECLS

struct _GmeDOMDocumentCSS {
	GmeDOMDocumentStyle parent;
	nsIDOMDocumentCSS *wrapped_ptr;
	gboolean is_disposed;
};

struct _GmeDOMDocumentCSSClass {
	GmeDOMDocumentStyleClass parent;
	void (* dispose) (GObject *instance);
};

GmeDOMDocumentCSS* gme_dom_document_css_new (nsIDOMDocumentCSS *wrapped_ptr);
void gme_dom_document_css_private_set_wrapped_ptr (GmeDOMDocumentCSS *self, nsIDOMDocumentCSS *wrapped_ptr);

G_END_DECLS

#endif /* __GME_DOM_DOCUMENT_CSS_PRIVATE_H__ */
