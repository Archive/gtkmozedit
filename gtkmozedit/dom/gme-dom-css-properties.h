/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __GME_DOM_CSS_PROPERTIES_H__
#define __GME_DOM_CSS_PROPERTIES_H__

#include <glib.h>
#include <glib-object.h>
#include <glib/gmacros.h>
#include <gtkmozedit/dom/gme-dom-forward.h>
#include <gtkmozedit/gme-supports.h>

G_BEGIN_DECLS

#define GME_TYPE_DOM_CSS_PROPERTIES                  (gme_dom_css_properties_get_gtype ())
#define GME_DOM_CSS_PROPERTIES(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), GME_TYPE_DOM_CSS_PROPERTIES, GmeDOMCSSProperties))
#define GME_DOM_CSS_PROPERTIES_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST ((klass), GME_TYPE_DOM_CSS_PROPERTIES, GmeDOMCSSPropertiesClass))
#define GME_IS_DOM_CSS_PROPERTIES(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GME_TYPE_DOM_CSS_PROPERTIES))
#define GME_IS_DOM_CSS_PROPERTIES_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), GME_TYPE_DOM_CSS_PROPERTIES))
#define GME_DOM_CSS_PROPERTIES_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS ((obj), GME_TYPE_DOM_CSS_PROPERTIES, GmeDOMCSSPropertiesClass))

typedef struct _GmeDOMCSSPropertiesClass GmeDOMCSSPropertiesClass;

GType gme_dom_css_properties_get_gtype (void);

gchar* gme_dom_css_properties_get_azimuth (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_azimuth (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_background (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_background (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_background_attachment (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_background_attachment (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_background_color (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_background_color (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_background_image (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_background_image (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_background_position (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_background_position (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_background_repeat (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_background_repeat (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_border (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_border (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_border_collapse (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_border_collapse (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_border_color (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_border_color (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_border_spacing (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_border_spacing (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_border_style (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_border_style (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_border_top (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_border_top (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_border_right (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_border_right (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_border_bottom (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_border_bottom (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_border_left (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_border_left (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_border_top_color (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_border_top_color (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_border_right_color (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_border_right_color (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_border_bottom_color (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_border_bottom_color (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_border_left_color (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_border_left_color (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_border_top_style (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_border_top_style (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_border_right_style (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_border_right_style (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_border_bottom_style (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_border_bottom_style (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_border_left_style (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_border_left_style (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_border_top_width (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_border_top_width (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_border_right_width (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_border_right_width (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_border_bottom_width (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_border_bottom_width (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_border_left_width (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_border_left_width (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_border_width (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_border_width (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_bottom (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_bottom (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_caption_side (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_caption_side (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_clear (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_clear (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_clip (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_clip (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_color (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_color (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_content (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_content (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_counter_increment (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_counter_increment (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_counter_reset (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_counter_reset (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_cue (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_cue (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_cue_after (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_cue_after (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_cue_before (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_cue_before (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_cursor (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_cursor (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_direction (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_direction (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_display (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_display (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_elevation (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_elevation (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_empty_cells (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_empty_cells (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_css_float (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_css_float (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_font (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_font (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_font_family (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_font_family (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_font_size (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_font_size (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_font_size_adjust (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_font_size_adjust (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_font_stretch (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_font_stretch (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_font_style (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_font_style (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_font_variant (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_font_variant (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_font_weight (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_font_weight (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_height (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_height (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_left (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_left (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_letter_spacing (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_letter_spacing (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_line_height (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_line_height (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_list_style (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_list_style (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_list_style_image (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_list_style_image (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_list_style_position (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_list_style_position (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_list_style_type (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_list_style_type (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_margin (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_margin (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_margin_top (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_margin_top (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_margin_right (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_margin_right (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_margin_bottom (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_margin_bottom (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_margin_left (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_margin_left (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_marker_offset (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_marker_offset (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_marks (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_marks (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_max_height (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_max_height (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_max_width (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_max_width (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_min_height (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_min_height (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_min_width (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_min_width (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_orphans (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_orphans (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_outline (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_outline (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_outline_color (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_outline_color (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_outline_style (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_outline_style (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_outline_width (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_outline_width (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_overflow (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_overflow (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_padding (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_padding (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_padding_top (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_padding_top (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_padding_right (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_padding_right (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_padding_bottom (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_padding_bottom (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_padding_left (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_padding_left (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_page (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_page (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_page_break_after (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_page_break_after (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_page_break_before (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_page_break_before (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_page_break_inside (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_page_break_inside (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_pause (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_pause (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_pause_after (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_pause_after (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_pause_before (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_pause_before (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_pitch (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_pitch (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_pitch_range (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_pitch_range (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_position (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_position (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_quotes (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_quotes (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_richness (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_richness (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_right (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_right (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_size (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_size (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_speak (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_speak (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_speak_header (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_speak_header (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_speak_numeral (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_speak_numeral (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_speak_punctuation (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_speak_punctuation (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_speech_rate (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_speech_rate (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_stress (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_stress (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_table_layout (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_table_layout (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_text_align (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_text_align (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_text_decoration (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_text_decoration (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_text_indent (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_text_indent (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_text_shadow (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_text_shadow (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_text_transform (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_text_transform (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_top (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_top (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_unicode_bidi (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_unicode_bidi (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_vertical_align (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_vertical_align (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_visibility (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_visibility (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_voice_family (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_voice_family (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_volume (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_volume (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_white_space (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_white_space (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_widows (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_widows (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_width (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_width (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_word_spacing (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_word_spacing (GmeDOMCSSProperties *self, const gchar *param);
gchar* gme_dom_css_properties_get_zindex (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_zindex (GmeDOMCSSProperties *self, const gchar *param);
/* CSS3 */
gchar* gme_dom_css_properties_get_opacity (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_opacity (GmeDOMCSSProperties *self, const gchar *param);

#ifdef GTKMOZEMBED_VERSION_1_0 
gchar* gme_dom_css_properties_get_play_during (GmeDOMCSSProperties *self);
gboolean gme_dom_css_properties_set_play_during (GmeDOMCSSProperties *self, const gchar *param);
#endif

G_END_DECLS

#endif /* __GME_DOM_CSS_PROPERTIES_H__ */
