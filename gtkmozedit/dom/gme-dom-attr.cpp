/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <gtkmozedit/gtkmozedit-config.h>
#include "nsIDOMNode.h"

#include "gme-dom-attr-private.h"
#include "gme-dom-element-private.h"
#include "gme-dom-node-private.h"

enum {
	PROP_0,
	PROP_WRAPPED_PTR,
	_NUM_PROPS
};

static GmeDOMNodeClass *gme_dom_attr_parent_class = NULL;

static void
instance_init (GmeDOMAttr *self)
{
	self->attr = NULL;
	self->is_disposed = FALSE;
}

static void
instance_dispose (GObject *instance)
{
	GmeDOMAttr *self = GME_DOM_ATTR (instance);

	if (self->is_disposed)
		return;

	if (self->attr) NS_RELEASE (self->attr);
	self->attr = NULL;
	self->is_disposed = TRUE;

	gme_dom_attr_parent_class->dispose (G_OBJECT (self));
}

static void
set_property (GObject      *object,
	      guint         prop_id,
	      GValue const *value,
	      GParamSpec   *pspec)
{
	GmeDOMAttr *self = GME_DOM_ATTR (object);
	nsISupports *ptr = NULL;
	nsresult rv;

	switch (prop_id) {
	case PROP_WRAPPED_PTR:
		ptr = reinterpret_cast<nsISupports*>(g_value_get_pointer (value));
		rv = CallQueryInterface (ptr, &self->attr);
		if (NS_SUCCEEDED (rv) && self->attr) {
			NS_ADDREF (self->attr);
			/* constuction param, init parent */
			gme_dom_node_private_set_wrapped_ptr (GME_DOM_NODE (self), self->attr);
		}
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
get_property (GObject    *object,
	      guint       prop_id,
	      GValue     *value,
	      GParamSpec *pspec)
{
	GmeDOMAttr *self = GME_DOM_ATTR (object);

	switch (prop_id) {
	case PROP_WRAPPED_PTR:
		g_value_set_pointer (value, (gpointer) self->attr);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
class_init (GmeDOMAttrClass *klass)
{
	GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
	GmeDOMNodeClass *node_class = GME_DOM_NODE_CLASS (klass);

	/* vfuncs */
	klass->dispose = instance_dispose;

	/* hook gobject vfuncs */
	gobject_class->dispose = instance_dispose;

	gme_dom_attr_parent_class = (GmeDOMNodeClass*) g_type_class_peek_parent (klass);

	gobject_class->set_property = set_property;
	gobject_class->get_property = get_property;

	g_object_class_install_property (gobject_class,
		PROP_WRAPPED_PTR,
		g_param_spec_pointer ("wrapped-ptr", _("Wrapped Pointer"),
			_("Pointer to the wrapped c++ object"),
			(GParamFlags)(G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY)));
}

GType
gme_dom_attr_get_gtype (void)
{
        static GType type = 0;
        if (!type) {
                static const GTypeInfo info = {
                        sizeof (GmeDOMAttrClass),
                        NULL,           /* base_init */
                        NULL,           /* base_finalize */
                        (GClassInitFunc) class_init,
                        NULL,           /* class_finalize */
                        NULL,           /* class_data */
                        sizeof (GmeDOMAttr),
                        0,              /* n_preallocs */
                        (GInstanceInitFunc) instance_init,
                };
                type = g_type_register_static (GME_TYPE_DOM_NODE, "GmeDOMAttr", &info, (GTypeFlags)0);
        }
        return type;
}

void 
gme_dom_attr_private_set_wrapped_ptr (GmeDOMAttr *self, 
				      nsIDOMAttr *attr)
{
	g_assert (self && attr);
	self->attr = attr;
	NS_ADDREF (self->attr);
	/* constuction param, init parent */
	gme_dom_node_private_set_wrapped_ptr (GME_DOM_NODE (self), self->attr);
}

GmeDOMAttr* 
gme_dom_attr_new (nsIDOMAttr *attr)
{
	return GME_DOM_ATTR (g_object_new (GME_TYPE_DOM_ATTR, "wrapped-ptr", attr, NULL));
}

gchar* 
gme_dom_attr_get_name (GmeDOMAttr *self)
{
	nsEmbedString name;
	nsresult rv;
	g_assert (self);

	rv = self->attr->GetName (name);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (name).get ());
	}
	return NULL;
}

gboolean 
gme_dom_attr_get_specified (GmeDOMAttr *self)
{
	gboolean is_specified;
	nsresult rv;
	g_assert (self);
	
	rv = self->attr->GetSpecified (&is_specified);
	return is_specified;
}

gchar* 
gme_dom_attr_get_value (GmeDOMAttr *self)
{
	nsEmbedString value;
	nsresult rv;
	g_assert (self);

	rv = self->attr->GetValue (value);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (value).get ());
	}
	return NULL;
}

gboolean 
gme_dom_attr_set_value (GmeDOMAttr *self, 
			const gchar *value)
{
	nsEmbedString v;
	nsresult rv;
	g_assert (self);

	v = NS_ConvertUTF8toUTF16 (value);
	rv = self->attr->SetValue (v);
	return NS_SUCCEEDED (rv);
}

GmeDOMElement* 
gme_dom_attr_get_owner_element (GmeDOMAttr *self)
{
	nsIDOMElement *e = NULL;
	GmeDOMElement *element = NULL;
	nsresult rv;
	g_assert (self);

	rv = self->attr->GetOwnerElement (&(e));
	if (NS_SUCCEEDED (rv)) {
		element = gme_dom_element_new (e);
	}
	return element;
}
