/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <gtkmozedit/gtkmozedit-config.h>

#include "nsCOMPtr.h"
#include "nsIDOMCSS2Properties.h"
#include "nsIInterfaceRequestorUtils.h"

#include "gme-dom-css-properties-private.h"

enum {
	PROP_0,
	PROP_WRAPPED_PTR,
	_NUM_PROPS
};

static GmeSupportsClass *gme_dom_css_properties_parent_class = NULL;

static void
instance_init (GmeDOMCSSProperties *self)
{
	self->wrapped_ptr = NULL;
	self->is_disposed = FALSE;
}

static void
instance_dispose (GObject *instance)
{
	GmeDOMCSSProperties *self = GME_DOM_CSS_PROPERTIES (instance);

	if (self->is_disposed)
		return;

	if (self->wrapped_ptr) NS_RELEASE (self->wrapped_ptr);
	self->wrapped_ptr = NULL;
	self->is_disposed = TRUE;

	gme_dom_css_properties_parent_class->dispose (G_OBJECT (self));
}

static void
set_property (GObject      *object,
	      guint         prop_id,
	      GValue const *value,
	      GParamSpec   *pspec)
{
	GmeDOMCSSProperties *self = GME_DOM_CSS_PROPERTIES (object);
	nsISupports *ptr = NULL;
	nsresult rv;

	switch (prop_id) {
	case PROP_WRAPPED_PTR:
		ptr = reinterpret_cast<nsISupports*>(g_value_get_pointer (value));
		rv = CallQueryInterface (ptr, &self->wrapped_ptr);
		if (NS_SUCCEEDED (rv) && self->wrapped_ptr) {
			NS_ADDREF (self->wrapped_ptr);
			/* constuction param, init parent */
			gme_supports_private_set_wrapped_ptr (GME_SUPPORTS (self), self->wrapped_ptr);
		}
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
get_property (GObject    *object,
	      guint       prop_id,
	      GValue     *value,
	      GParamSpec *pspec)
{
	GmeDOMCSSProperties *self = GME_DOM_CSS_PROPERTIES (object);

	switch (prop_id) {
	case PROP_WRAPPED_PTR:
		g_value_set_pointer (value, (gpointer) self->wrapped_ptr);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
class_init (GmeDOMCSSPropertiesClass *klass)
{
	GObjectClass *gobject_class = G_OBJECT_CLASS (klass);

	/* vfuncs */
	klass->dispose = instance_dispose;

	/* hook gobject vfuncs */
	gobject_class->dispose = instance_dispose;

	gme_dom_css_properties_parent_class = (GmeSupportsClass*) g_type_class_peek_parent (klass);

	gobject_class->set_property = set_property;
	gobject_class->get_property = get_property;

	g_object_class_install_property (gobject_class,
		PROP_WRAPPED_PTR,
		g_param_spec_pointer ("wrapped-ptr", _("Wrapped Pointer"),
			_("Pointer to the wrapped c++ object"),
			(GParamFlags)(G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY)));
}

GType
gme_dom_css_properties_get_gtype (void)
{
        static GType type = 0;
        if (!type) {
                static const GTypeInfo info = {
                        sizeof (GmeDOMCSSPropertiesClass),
                        NULL,           /* base_init */
                        NULL,           /* base_finalize */
                        (GClassInitFunc) class_init,
                        NULL,           /* class_finalize */
                        NULL,           /* class_data */
                        sizeof (GmeDOMCSSProperties),
                        0,              /* n_preallocs */
                        (GInstanceInitFunc) instance_init,
                };
                type = g_type_register_static (GME_TYPE_SUPPORTS, "GmeDOMCSSProperties", &info, (GTypeFlags)0);
        }
        return type;
}

void 
gme_dom_css_properties_private_set_wrapped_ptr (GmeDOMCSSProperties *self, 
				      nsIDOMCSS2Properties *wrapped_ptr)
{
	g_assert (self && wrapped_ptr);
	self->wrapped_ptr = wrapped_ptr;
	NS_ADDREF (self->wrapped_ptr);
	/* constuction param, init parent */
	gme_supports_private_set_wrapped_ptr (GME_SUPPORTS (self), self->wrapped_ptr);
}

GmeDOMCSSProperties* 
gme_dom_css_properties_new (nsIDOMCSS2Properties *wrapped_ptr)
{
	return GME_DOM_CSS_PROPERTIES (g_object_new (GME_TYPE_DOM_CSS_PROPERTIES, "wrapped-ptr", wrapped_ptr, NULL));
}

gchar* 
gme_dom_css_properties_get_azimuth (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetAzimuth (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_azimuth (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetAzimuth (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_background (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetBackground (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_background (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetBackground (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_background_attachment (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetBackgroundAttachment (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_background_attachment (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetBackgroundAttachment (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_background_color (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetBackgroundColor (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_background_color (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetBackgroundColor (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_background_image (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetBackgroundImage (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_background_image (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetBackgroundImage (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_background_position (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetBackgroundPosition (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_background_position (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetBackgroundPosition (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_background_repeat (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetBackgroundRepeat (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_background_repeat (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetBackgroundRepeat (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_border (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetBorder (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_border (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetBorder (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_border_collapse (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetBorderCollapse (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_border_collapse (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetBorderCollapse (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_border_color (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetBorderColor (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_border_color (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetBorderColor (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_border_spacing (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetBorderSpacing (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_border_spacing (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetBorderSpacing (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_border_style (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetBorderStyle (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_border_style (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetBorderStyle (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_border_top (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetBorderTop (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_border_top (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetBorderTop (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_border_right (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetBorderRight (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_border_right (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetBorderRight (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_border_bottom (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetBorderBottom (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_border_bottom (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetBorderBottom (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_border_left (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetBorderLeft (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_border_left (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetBorderLeft (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_border_top_color (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetBorderTopColor (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_border_top_color (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetBorderTopColor (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_border_right_color (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetBorderRightColor (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_border_right_color (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetBorderRightColor (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_border_bottom_color (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetBorderBottomColor (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_border_bottom_color (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetBorderBottomColor (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_border_left_color (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetBorderLeftColor (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_border_left_color (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetBorderLeftColor (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_border_top_style (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetBorderTopStyle (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_border_top_style (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetBorderTopStyle (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_border_right_style (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetBorderRightStyle (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_border_right_style (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetBorderRightStyle (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_border_bottom_style (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetBorderBottomStyle (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_border_bottom_style (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetBorderBottomStyle (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_border_left_style (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetBorderLeftStyle (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_border_left_style (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetBorderLeftStyle (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_border_top_width (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetBorderTopWidth (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_border_top_width (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetBorderTopWidth (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_border_right_width (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetBorderRightWidth (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_border_right_width (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetBorderRightWidth (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_border_bottom_width (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetBorderBottomWidth (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_border_bottom_width (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetBorderBottomWidth (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_border_left_width (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetBorderLeftWidth (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_border_left_width (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetBorderLeftWidth (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_border_width (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetBorderWidth (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_border_width (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetBorderWidth (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_bottom (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetBottom (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_bottom (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetBottom (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_caption_side (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetCaptionSide (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_caption_side (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetCaptionSide (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_clear (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetClear (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_clear (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetClear (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_clip (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetClip (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_clip (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetClip (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_color (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetColor (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_color (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetColor (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_content (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetContent (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_content (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetContent (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_counter_increment (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetCounterIncrement (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_counter_increment (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetCounterIncrement (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_counter_reset (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetCounterReset (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_counter_reset (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetCounterReset (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_cue (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetCue (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_cue (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetCue (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_cue_after (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetCueAfter (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_cue_after (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetCueAfter (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_cue_before (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetCueBefore (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_cue_before (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetCueBefore (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_cursor (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetCursor (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_cursor (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetCursor (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_direction (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetDirection (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_direction (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetDirection (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_display (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetDisplay (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_display (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetDisplay (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_elevation (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetElevation (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_elevation (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetElevation (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_empty_cells (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetEmptyCells (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_empty_cells (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetEmptyCells (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_css_float (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetCssFloat (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_css_float (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetCssFloat (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_font (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetFont (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_font (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetFont (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_font_family (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetFontFamily (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_font_family (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetFontFamily (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_font_size (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetFontSize (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_font_size (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetFontSize (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_font_size_adjust (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetFontSizeAdjust (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_font_size_adjust (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetFontSizeAdjust (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_font_stretch (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetFontStretch (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_font_stretch (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetFontStretch (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_font_style (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetFontStyle (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_font_style (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetFontStyle (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_font_variant (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetFontVariant (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_font_variant (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetFontVariant (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_font_weight (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetFontWeight (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_font_weight (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetFontWeight (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_height (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetHeight (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_height (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetHeight (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_left (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetLeft (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_left (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetLeft (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_letter_spacing (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetLetterSpacing (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_letter_spacing (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetLetterSpacing (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_line_height (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetLineHeight (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_line_height (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetLineHeight (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_list_style (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetListStyle (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_list_style (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetListStyle (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_list_style_image (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetListStyleImage (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_list_style_image (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetListStyleImage (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_list_style_position (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetListStylePosition (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_list_style_position (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetListStylePosition (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_list_style_type (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetListStyleType (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_list_style_type (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetListStyleType (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_margin (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetMargin (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_margin (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetMargin (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_margin_top (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetMarginTop (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_margin_top (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetMarginTop (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_margin_right (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetMarginRight (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_margin_right (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetMarginRight (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_margin_bottom (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetMarginBottom (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_margin_bottom (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetMarginBottom (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_margin_left (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetMarginLeft (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_margin_left (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetMarginLeft (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_marker_offset (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetMarkerOffset (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_marker_offset (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetMarkerOffset (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_marks (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetMarks (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_marks (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetMarks (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_max_height (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetMaxHeight (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_max_height (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetMaxHeight (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_max_width (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetMaxWidth (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_max_width (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetMaxWidth (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_min_height (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetMinHeight (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_min_height (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetMinHeight (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_min_width (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetMinWidth (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_min_width (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetMinWidth (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_orphans (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetOrphans (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_orphans (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetOrphans (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_outline (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetOutline (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_outline (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetOutline (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_outline_color (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetOutlineColor (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_outline_color (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetOutlineColor (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_outline_style (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetOutlineStyle (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_outline_style (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetOutlineStyle (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_outline_width (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetOutlineWidth (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_outline_width (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetOutlineWidth (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_overflow (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetOverflow (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_overflow (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetOverflow (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_padding (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetPadding (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_padding (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetPadding (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_padding_top (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetPaddingTop (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_padding_top (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetPaddingTop (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_padding_right (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetPaddingRight (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_padding_right (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetPaddingRight (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_padding_bottom (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetPaddingBottom (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_padding_bottom (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetPaddingBottom (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_padding_left (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetPaddingLeft (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_padding_left (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetPaddingLeft (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_page (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetPage (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_page (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetPage (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_page_break_after (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetPageBreakAfter (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_page_break_after (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetPageBreakAfter (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_page_break_before (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetPageBreakBefore (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_page_break_before (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetPageBreakBefore (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_page_break_inside (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetPageBreakInside (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_page_break_inside (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetPageBreakInside (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_pause (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetPause (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_pause (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetPause (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_pause_after (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetPauseAfter (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_pause_after (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetPauseAfter (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_pause_before (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetPauseBefore (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_pause_before (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetPauseBefore (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_pitch (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetPitch (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_pitch (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetPitch (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_pitch_range (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetPitchRange (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_pitch_range (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetPitchRange (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_position (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetPosition (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_position (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetPosition (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_quotes (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetQuotes (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_quotes (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetQuotes (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_richness (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetRichness (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_richness (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetRichness (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_right (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetRight (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_right (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetRight (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_size (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetSize (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_size (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetSize (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_speak (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetSpeak (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_speak (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetSpeak (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_speak_header (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetSpeakHeader (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_speak_header (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetSpeakHeader (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_speak_numeral (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetSpeakNumeral (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_speak_numeral (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetSpeakNumeral (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_speak_punctuation (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetSpeakPunctuation (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_speak_punctuation (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetSpeakPunctuation (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_speech_rate (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetSpeechRate (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_speech_rate (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetSpeechRate (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_stress (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetStress (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_stress (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetStress (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_table_layout (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetTableLayout (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_table_layout (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetTableLayout (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_text_align (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetTextAlign (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_text_align (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetTextAlign (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_text_decoration (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetTextDecoration (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_text_decoration (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetTextDecoration (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_text_indent (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetTextIndent (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_text_indent (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetTextIndent (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_text_shadow (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetTextShadow (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_text_shadow (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetTextShadow (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_text_transform (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetTextTransform (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_text_transform (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetTextTransform (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_top (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetTop (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_top (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetTop (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_unicode_bidi (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetUnicodeBidi (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_unicode_bidi (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetUnicodeBidi (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_vertical_align (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetVerticalAlign (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_vertical_align (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetVerticalAlign (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_visibility (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetVisibility (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_visibility (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetVisibility (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_voice_family (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetVoiceFamily (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_voice_family (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetVoiceFamily (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_volume (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetVolume (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_volume (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetVolume (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_white_space (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetWhiteSpace (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_white_space (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetWhiteSpace (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_widows (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetWidows (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_widows (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetWidows (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_width (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetWidth (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_width (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetWidth (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_word_spacing (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetWordSpacing (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_word_spacing (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetWordSpacing (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_zindex (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetZIndex (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_zindex (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetZIndex (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_css_properties_get_opacity (GmeDOMCSSProperties *self)
{
	nsCOMPtr<nsIDOMNSCSS2Properties> nsprops = NULL;
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	nsprops = do_GetInterface (self->wrapped_ptr, &rv);
	g_assert (NS_SUCCEEDED (rv));
	rv = nsprops->GetOpacity (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_opacity (GmeDOMCSSProperties *self, const gchar *param)
{
	nsCOMPtr<nsIDOMNSCSS2Properties> nsprops = NULL;
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	nsprops = do_GetInterface (self->wrapped_ptr, &rv);
	g_assert (NS_SUCCEEDED (rv));
	p = NS_ConvertUTF8toUTF16 (param);
	rv = nsprops->SetOpacity (p);
	return NS_SUCCEEDED (rv);
}


#ifdef GTKMOZEMBED_VERSION_1_0 

gchar* 
gme_dom_css_properties_get_play_during (GmeDOMCSSProperties *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetPlayDuring (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_css_properties_set_play_during (GmeDOMCSSProperties *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetPlayDuring (p);
	return NS_SUCCEEDED (rv);
}

#endif
