/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __GME_DOM_HTML_FRAME_ELEMENT_H__
#define __GME_DOM_HTML_FRAME_ELEMENT_H__

#include <glib.h>
#include <glib-object.h>
#include <glib/gmacros.h>
#include <gtkmozedit/dom/gme-dom-forward.h>
#include <gtkmozedit/dom/gme-dom-document.h>


G_BEGIN_DECLS

#define GME_TYPE_DOM_HTML_FRAME_ELEMENT                  (gme_dom_html_frame_element_get_gtype ())
#define GME_DOM_HTML_FRAME_ELEMENT(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), GME_TYPE_DOM_HTML_FRAME_ELEMENT, GmeDOMHTMLFrameElement))
#define GME_DOM_HTML_FRAME_ELEMENT_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST ((klass), GME_TYPE_DOM_HTML_FRAME_ELEMENT, GmeDOMHTMLFrameElementClass))
#define GME_IS_DOM_HTML_FRAME_ELEMENT(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GME_TYPE_DOM_HTML_FRAME_ELEMENT))
#define GME_IS_DOM_HTML_FRAME_ELEMENT_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), GME_TYPE_DOM_HTML_FRAME_ELEMENT))
#define GME_DOM_HTML_FRAME_ELEMENT_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS ((obj), GME_TYPE_DOM_HTML_FRAME_ELEMENT, GmeDOMHTMLFrameElementClass))

typedef struct _GmeDOMHTMLFrameElementClass GmeDOMHTMLFrameElementClass;

GType gme_dom_html_frame_element_get_gtype (void);

gchar* gme_dom_html_frame_element_get_frame_border (GmeDOMHTMLFrameElement *self);
gboolean gme_dom_html_frame_element_set_frame_border (GmeDOMHTMLFrameElement *self, const gchar *param);
gchar* gme_dom_html_frame_element_get_long_desc (GmeDOMHTMLFrameElement *self);
gboolean gme_dom_html_frame_element_set_long_desc (GmeDOMHTMLFrameElement *self, const gchar *param);
gchar* gme_dom_html_frame_element_get_margin_height (GmeDOMHTMLFrameElement *self);
gboolean gme_dom_html_frame_element_set_margin_height (GmeDOMHTMLFrameElement *self, const gchar *param);
gchar* gme_dom_html_frame_element_get_margin_width (GmeDOMHTMLFrameElement *self);
gboolean gme_dom_html_frame_element_set_margin_width (GmeDOMHTMLFrameElement *self, const gchar *param);
gchar* gme_dom_html_frame_element_get_name (GmeDOMHTMLFrameElement *self);
gboolean gme_dom_html_frame_element_set_name (GmeDOMHTMLFrameElement *self, const gchar *param);
gboolean gme_dom_html_frame_element_get_no_resize (GmeDOMHTMLFrameElement *self);
gboolean gme_dom_html_frame_element_set_no_resize (GmeDOMHTMLFrameElement *self, gboolean param);
gchar* gme_dom_html_frame_element_get_scrolling (GmeDOMHTMLFrameElement *self);
gboolean gme_dom_html_frame_element_set_scrolling (GmeDOMHTMLFrameElement *self, const gchar *param);
gchar* gme_dom_html_frame_element_get_src (GmeDOMHTMLFrameElement *self);
gboolean gme_dom_html_frame_element_set_src (GmeDOMHTMLFrameElement *self, const gchar *param);
GmeDOMDocument* gme_dom_html_frame_element_get_content_document (GmeDOMHTMLFrameElement *self);


G_END_DECLS

#endif /* __GME_DOM_HTML_FRAME_ELEMENT_H__ */
