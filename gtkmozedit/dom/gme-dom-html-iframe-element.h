/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __GME_DOM_HTML_IFRAME_ELEMENT_H__
#define __GME_DOM_HTML_IFRAME_ELEMENT_H__

#include <glib.h>
#include <glib-object.h>
#include <glib/gmacros.h>
#include <gtkmozedit/dom/gme-dom-forward.h>
#include <gtkmozedit/dom/gme-dom-document.h>


G_BEGIN_DECLS

#define GME_TYPE_DOM_HTML_IFRAME_ELEMENT                  (gme_dom_html_iframe_element_get_gtype ())
#define GME_DOM_HTML_IFRAME_ELEMENT(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), GME_TYPE_DOM_HTML_IFRAME_ELEMENT, GmeDOMHTMLIFrameElement))
#define GME_DOM_HTML_IFRAME_ELEMENT_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST ((klass), GME_TYPE_DOM_HTML_IFRAME_ELEMENT, GmeDOMHTMLIFrameElementClass))
#define GME_IS_DOM_HTML_IFRAME_ELEMENT(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GME_TYPE_DOM_HTML_IFRAME_ELEMENT))
#define GME_IS_DOM_HTML_IFRAME_ELEMENT_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), GME_TYPE_DOM_HTML_IFRAME_ELEMENT))
#define GME_DOM_HTML_IFRAME_ELEMENT_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS ((obj), GME_TYPE_DOM_HTML_IFRAME_ELEMENT, GmeDOMHTMLIFrameElementClass))

typedef struct _GmeDOMHTMLIFrameElementClass GmeDOMHTMLIFrameElementClass;

GType gme_dom_html_iframe_element_get_gtype (void);

gchar* gme_dom_html_iframe_element_get_align (GmeDOMHTMLIFrameElement *self);
gboolean gme_dom_html_iframe_element_set_align (GmeDOMHTMLIFrameElement *self, const gchar *param);
gchar* gme_dom_html_iframe_element_get_frame_border (GmeDOMHTMLIFrameElement *self);
gboolean gme_dom_html_iframe_element_set_frame_border (GmeDOMHTMLIFrameElement *self, const gchar *param);
gchar* gme_dom_html_iframe_element_get_height (GmeDOMHTMLIFrameElement *self);
gboolean gme_dom_html_iframe_element_set_height (GmeDOMHTMLIFrameElement *self, const gchar *param);
gchar* gme_dom_html_iframe_element_get_long_desc (GmeDOMHTMLIFrameElement *self);
gboolean gme_dom_html_iframe_element_set_long_desc (GmeDOMHTMLIFrameElement *self, const gchar *param);
gchar* gme_dom_html_iframe_element_get_margin_height (GmeDOMHTMLIFrameElement *self);
gboolean gme_dom_html_iframe_element_set_margin_height (GmeDOMHTMLIFrameElement *self, const gchar *param);
gchar* gme_dom_html_iframe_element_get_margin_width (GmeDOMHTMLIFrameElement *self);
gboolean gme_dom_html_iframe_element_set_margin_width (GmeDOMHTMLIFrameElement *self, const gchar *param);
gchar* gme_dom_html_iframe_element_get_name (GmeDOMHTMLIFrameElement *self);
gboolean gme_dom_html_iframe_element_set_name (GmeDOMHTMLIFrameElement *self, const gchar *param);
gchar* gme_dom_html_iframe_element_get_scrolling (GmeDOMHTMLIFrameElement *self);
gboolean gme_dom_html_iframe_element_set_scrolling (GmeDOMHTMLIFrameElement *self, const gchar *param);
gchar* gme_dom_html_iframe_element_get_src (GmeDOMHTMLIFrameElement *self);
gboolean gme_dom_html_iframe_element_set_src (GmeDOMHTMLIFrameElement *self, const gchar *param);
gchar* gme_dom_html_iframe_element_get_width (GmeDOMHTMLIFrameElement *self);
gboolean gme_dom_html_iframe_element_set_width (GmeDOMHTMLIFrameElement *self, const gchar *param);
GmeDOMDocument* gme_dom_html_iframe_element_get_content_document (GmeDOMHTMLIFrameElement *self);


G_END_DECLS

#endif /* __GME_DOM_HTML_IFRAME_ELEMENT_H__ */
