/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __GME_DOM_TEXT_H__
#define __GME_DOM_TEXT_H__

#include <glib.h>
#include <glib-object.h>
#include <glib/gmacros.h>
#include <gtkmozedit/dom/gme-dom-forward.h>

G_BEGIN_DECLS

#define GME_TYPE_DOM_TEXT                  (gme_dom_text_get_gtype ())
#define GME_DOM_TEXT(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), GME_TYPE_DOM_TEXT, GmeDOMText))
#define GME_DOM_TEXT_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST ((klass), GME_TYPE_DOM_TEXT, GmeDOMTextClass))
#define GME_IS_DOM_TEXT(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GME_TYPE_DOM_TEXT))
#define GME_IS_DOM_TEXT_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), GME_TYPE_DOM_TEXT))
#define GME_DOM_TEXT_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS ((obj), GME_TYPE_DOM_TEXT, GmeDOMTextClass))

typedef struct _GmeDOMTextClass GmeDOMTextClass;

GType gme_dom_text_get_gtype (void);

/**
 * The GmeDOMText interface inherits from GmeDOMCharacterData and represents 
 * the textual content (termed character data in XML) of an Element or Attr.
 *
 * For more information on this interface please see 
 * http://www.w3.org/TR/DOM-Level-2-Core/
 *
 * @status FROZEN
 */

GmeDOMText* gme_dom_text_split_text (GmeDOMText *self, guint32 offset);

G_END_DECLS

#endif /* __GME_DOM_TEXT_H__ */
