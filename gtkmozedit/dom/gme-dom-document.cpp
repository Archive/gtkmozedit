/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <gtkmozedit/gtkmozedit-config.h>

#include "nsIDOMDocument.h"

#include "gme-dom-document-private.h"
#include "gme-dom-attr-private.h"
#include "gme-dom-cdata-section-private.h"
#include "gme-dom-comment-private.h"
#include "gme-dom-document-fragment-private.h"
#include "gme-dom-document-type-private.h"
#include "gme-dom-dom-implementation-private.h"
#include "gme-dom-element-private.h"
#include "gme-dom-entity-reference-private.h"
#include "gme-dom-node-list-private.h"
#include "gme-dom-processing-instruction-private.h"
#include "gme-dom-text-private.h"

enum {
	PROP_0,
	PROP_WRAPPED_PTR,
	_NUM_PROPS
};

static GmeSupportsClass *gme_dom_document_parent_class = NULL;

static void
instance_init (GmeDOMDocument *self)
{
	self->doc = NULL;
	self->is_disposed = FALSE;
}

static void
instance_dispose (GObject *instance)
{
	GmeDOMDocument *self = GME_DOM_DOCUMENT (instance);

	if (self->is_disposed)
		return;

	if (self->doc) NS_RELEASE (self->doc);
	self->doc = NULL;
	self->is_disposed = TRUE;

	gme_dom_document_parent_class->dispose (G_OBJECT (self));
}

static void
set_property (GObject      *object,
	      guint         prop_id,
	      GValue const *value,
	      GParamSpec   *pspec)
{
	GmeDOMDocument *self = GME_DOM_DOCUMENT (object);
	nsISupports *ptr = NULL;
	nsresult rv;

	switch (prop_id) {
	case PROP_WRAPPED_PTR:
		ptr = reinterpret_cast<nsISupports*>(g_value_get_pointer (value));
		rv = CallQueryInterface (ptr, &self->doc);
		if (NS_SUCCEEDED (rv) && self->doc) {
			NS_ADDREF (self->doc);
			/* constuction param, init parent */
			gme_supports_private_set_wrapped_ptr (GME_SUPPORTS (self), self->doc);
		}
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
get_property (GObject    *object,
	      guint       prop_id,
	      GValue     *value,
	      GParamSpec *pspec)
{
	GmeDOMDocument *self = GME_DOM_DOCUMENT (object);

	switch (prop_id) {
	case PROP_WRAPPED_PTR:
		g_value_set_pointer (value, (gpointer) self->doc);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
class_init (GmeDOMDocumentClass *klass)
{
	GObjectClass *gobject_class = G_OBJECT_CLASS (klass);

	/* vfuncs */
	klass->dispose = instance_dispose;

	/* hook gobject vfuncs */
	gobject_class->dispose = instance_dispose;

	gme_dom_document_parent_class = (GmeSupportsClass*) g_type_class_peek_parent (klass);

	gobject_class->set_property = set_property;
	gobject_class->get_property = get_property;

	g_object_class_install_property (gobject_class,
		PROP_WRAPPED_PTR,
		g_param_spec_pointer ("wrapped-ptr", _("Wrapped Pointer"),
			_("Pointer to the wrapped c++ object"),
			(GParamFlags)(G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY)));
}

GType
gme_dom_document_get_gtype (void)
{
        static GType type = 0;
        if (!type) {
                static const GTypeInfo info = {
                        sizeof (GmeDOMDocumentClass),
                        NULL,           /* base_init */
                        NULL,           /* base_finalize */
                        (GClassInitFunc) class_init,
                        NULL,           /* class_finalize */
                        NULL,           /* class_data */
                        sizeof (GmeDOMDocument),
                        0,              /* n_preallocs */
                        (GInstanceInitFunc) instance_init,
                };
                type = g_type_register_static (GME_TYPE_SUPPORTS, "GmeDOMDocument", &info, (GTypeFlags)0);
        }
        return type;
}

void 
gme_dom_document_private_set_wrapped_ptr (GmeDOMDocument *self, 
					  nsIDOMDocument *doc)
{
	g_assert (self && doc);
	self->doc = doc;
	NS_ADDREF (self->doc);
	/* constuction param, init parent */
	gme_supports_private_set_wrapped_ptr (GME_SUPPORTS (self), self->doc);
}

GmeDOMDocument*
gme_dom_document_new (nsIDOMDocument *doc)
{
	return GME_DOM_DOCUMENT (g_object_new (GME_TYPE_DOM_DOCUMENT, "wrapped-ptr", doc, NULL));
}

GmeDOMDocumentType* 
gme_dom_document_get_doctype (GmeDOMDocument *self)
{
	nsIDOMDocumentType *dt = NULL;
	GmeDOMDocumentType *doctype = NULL;
	nsresult rv;
	g_assert (self);

	rv = self->doc->GetDoctype (&(dt));
	if (NS_SUCCEEDED (rv)) {
		doctype = gme_dom_document_type_new (dt);
	}
	return doctype;
}

GmeDOMDOMImplementation* 
gme_dom_document_get_implementation (GmeDOMDocument *self)
{
	nsIDOMDOMImplementation *di = NULL;
	GmeDOMDOMImplementation *impl = NULL;
	nsresult rv;
	g_assert (self);

	rv = self->doc->GetImplementation (&(di));
	if (NS_SUCCEEDED (rv)) {
		impl = gme_dom_dom_implementation_new (di);
	}
	return impl;
}

GmeDOMElement* 
gme_dom_document_get_document_element (GmeDOMDocument *self)
{
	nsIDOMElement *e = NULL;
	GmeDOMElement *element = NULL;
	nsresult rv;
	g_assert (self);

	rv = self->doc->GetDocumentElement (&(e));
	if (NS_SUCCEEDED (rv)) {
		element = gme_dom_element_new (e);
	}
	return element;
}

GmeDOMElement* 
gme_dom_document_create_element (GmeDOMDocument *self, 
				 const gchar *tag_name)
{
	nsIDOMElement *e = NULL;
	GmeDOMElement *element = NULL;
	nsEmbedString tn;
	nsresult rv;
	g_assert (self);

	tn = NS_ConvertUTF8toUTF16 (tag_name);
	rv = self->doc->CreateElement (tn, &(e));
	if (NS_SUCCEEDED (rv)) {
		element = gme_dom_element_new (e);
	}
	return element;
}

GmeDOMDocumentFragment* 
gme_dom_document_create_document_fragment (GmeDOMDocument *self)
{
	nsIDOMDocumentFragment *df = NULL;
	GmeDOMDocumentFragment *frag = NULL;
	nsresult rv;
	g_assert (self);

	rv = self->doc->CreateDocumentFragment (&(df));
	if (NS_SUCCEEDED (rv)) {
		frag = gme_dom_document_fragment_new (df);
	}
	return frag;
}

GmeDOMText* 
gme_dom_document_create_text_node (GmeDOMDocument *self, 
				   const gchar *data)
{
	nsIDOMText *t = NULL;
	GmeDOMText *text = NULL;
	nsEmbedString d;
	nsresult rv;
	g_assert (self);

	d = NS_ConvertUTF8toUTF16 (data);
	rv = self->doc->CreateTextNode (d, &(t));
	if (NS_SUCCEEDED (rv)) {
		text = gme_dom_text_new (t);
	}
	return text;
}

GmeDOMComment* 
gme_dom_document_create_comment (GmeDOMDocument *self, 
				 const gchar *data)
{
	nsIDOMComment *c = NULL;
	GmeDOMComment *comment = NULL;
	nsEmbedString d;
	nsresult rv;
	g_assert (self);

	d = NS_ConvertUTF8toUTF16 (data);
	rv = self->doc->CreateComment (d, &(c));
	if (NS_SUCCEEDED (rv)) {
		comment = gme_dom_comment_new (c);
	}
	return comment;
}

GmeDOMCDATASection* 
gme_dom_document_create_cdata_section (GmeDOMDocument *self, 
				       const gchar *data)
{
	nsIDOMCDATASection *c = NULL;
	GmeDOMCDATASection *cdata = NULL;
	nsEmbedString d;
	nsresult rv;
	g_assert (self);

	d = NS_ConvertUTF8toUTF16 (data);
	rv = self->doc->CreateCDATASection (d, &(c));
	if (NS_SUCCEEDED (rv)) {
		cdata = gme_dom_cdata_section_new (c);
	}
	return cdata;
}

GmeDOMProcessingInstruction* 
gme_dom_document_create_processing_instruction (GmeDOMDocument *self, 
						const gchar *target, 
						const gchar *data)
{
	nsIDOMProcessingInstruction *pi = NULL;
	GmeDOMProcessingInstruction *instr = NULL;
	nsEmbedString t, d;
	nsresult rv;
	g_assert (self);

	t = NS_ConvertUTF8toUTF16 (target);
	d = NS_ConvertUTF8toUTF16 (data);
	rv = self->doc->CreateProcessingInstruction (t, d, &(pi));
	if (NS_SUCCEEDED (rv)) {
		instr = gme_dom_processing_instruction_new (pi);
	}
	return instr;
}

GmeDOMAttr* 
gme_dom_document_create_attribute (GmeDOMDocument *self, 
				   const gchar *name)
{
	nsIDOMAttr *a = NULL;
	GmeDOMAttr *attr = NULL;
	nsEmbedString n;
	nsresult rv;
	g_assert (self);

	n = NS_ConvertUTF8toUTF16 (name);
	rv = self->doc->CreateAttribute (n, &(a));
	if (NS_SUCCEEDED (rv)) {
		attr = gme_dom_attr_new (a);
	}
	return attr;
}

GmeDOMEntityReference* 
gme_dom_document_create_entity_reference (GmeDOMDocument *self, 
					  const gchar *name)
{
	nsIDOMEntityReference *er = NULL;
	GmeDOMEntityReference *ref = NULL;
	nsEmbedString n;
	nsresult rv;
	g_assert (self);

	n = NS_ConvertUTF8toUTF16 (name);
	rv = self->doc->CreateEntityReference (n, &(er));
	if (NS_SUCCEEDED (rv)) {
		ref = gme_dom_entity_reference_new (er);
	}
	return ref;
}

GmeDOMNodeList* 
gme_dom_document_get_elements_by_tag_name (GmeDOMDocument *self, 
					   const gchar *tag_name)
{
	nsIDOMNodeList *l = NULL;
	GmeDOMNodeList *list = NULL;
	nsEmbedString tn;
	nsresult rv;
	g_assert (self);

	tn = NS_ConvertUTF8toUTF16 (tag_name);
	rv = self->doc->GetElementsByTagName (tn, &(l));
	if (NS_SUCCEEDED (rv)) {
		list = gme_dom_node_list_new (l);
	}
	return list;
}

GmeDOMNode* 
gme_dom_document_import_node (GmeDOMDocument *self, 
			      GmeDOMNode *imported_node, 
			      gboolean deep)
{
	nsIDOMNode *n = NULL;
	GmeDOMNode *node = NULL;
	nsresult rv;
	g_assert (self);

	rv = self->doc->ImportNode (imported_node->node, deep, &(n));
	if (NS_SUCCEEDED (rv)) {
		node = gme_dom_node_new (n);
	}
	return node;
}

GmeDOMElement* 
gme_dom_document_get_element_by_id (GmeDOMDocument *self, 
				    const gchar *element_id)
{
	nsIDOMElement *e = NULL;
	GmeDOMElement *element = NULL;
	nsEmbedString ei;
	nsresult rv;
	g_assert (self);

	ei = NS_ConvertUTF8toUTF16 (element_id);
	rv = self->doc->GetElementById (ei, &e);
	if (NS_SUCCEEDED (rv)) {
		NS_ADDREF (e);
		element = gme_dom_element_new (e);
		NS_RELEASE (e);
	}
	return element;
}
