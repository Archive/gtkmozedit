/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <gtkmozedit/gtkmozedit-config.h>

#include "nsIDOMNamedNodeMap.h"

#include "gme-dom-document-type-private.h"
#include "gme-dom-named-node-map-private.h"

enum {
	PROP_0,
	PROP_WRAPPED_PTR,
	_NUM_PROPS
};

static GmeSupportsClass *gme_dom_document_type_parent_class = NULL;

static void
instance_init (GmeDOMDocumentType *self)
{
	self->doctype = NULL;
	self->is_disposed = FALSE;
}

static void
instance_dispose (GObject *instance)
{
	GmeDOMDocumentType *self = GME_DOM_DOCUMENT_TYPE (instance);

	if (self->is_disposed)
		return;

	if (self->doctype) NS_RELEASE (self->doctype);
	self->doctype = NULL;
	self->is_disposed = TRUE;

	gme_dom_document_type_parent_class->dispose (G_OBJECT (self));
}

static void
set_property (GObject      *object,
	      guint         prop_id,
	      GValue const *value,
	      GParamSpec   *pspec)
{
	GmeDOMDocumentType *self = GME_DOM_DOCUMENT_TYPE (object);
	nsISupports *ptr = NULL;
	nsresult rv;

	switch (prop_id) {
	case PROP_WRAPPED_PTR:
		ptr = reinterpret_cast<nsISupports*>(g_value_get_pointer (value));
		rv = CallQueryInterface (ptr, &self->doctype);
		if (NS_SUCCEEDED (rv) && self->doctype) {
			NS_ADDREF (self->doctype);
			/* constuction param, init parent */
			gme_supports_private_set_wrapped_ptr (GME_SUPPORTS (self), self->doctype);
		}
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
get_property (GObject    *object,
	      guint       prop_id,
	      GValue     *value,
	      GParamSpec *pspec)
{
	GmeDOMDocumentType *self = GME_DOM_DOCUMENT_TYPE (object);

	switch (prop_id) {
	case PROP_WRAPPED_PTR:
		g_value_set_pointer (value, (gpointer) self->doctype);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
class_init (GmeDOMDocumentTypeClass *klass)
{
	GObjectClass *gobject_class = G_OBJECT_CLASS (klass);

	/* vfuncs */
	klass->dispose = instance_dispose;

	/* hook gobject vfuncs */
	gobject_class->dispose = instance_dispose;

	gme_dom_document_type_parent_class = (GmeSupportsClass*) g_type_class_peek_parent (klass);

	gobject_class->set_property = set_property;
	gobject_class->get_property = get_property;

	g_object_class_install_property (gobject_class,
		PROP_WRAPPED_PTR,
		g_param_spec_pointer ("wrapped-ptr", _("Wrapped Pointer"),
			_("Pointer to the wrapped c++ object"),
			(GParamFlags)(G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY)));
}

GType
gme_dom_document_type_get_gtype (void)
{
        static GType type = 0;
        if (!type) {
                static const GTypeInfo info = {
                        sizeof (GmeDOMDocumentTypeClass),
                        NULL,           /* base_init */
                        NULL,           /* base_finalize */
                        (GClassInitFunc) class_init,
                        NULL,           /* class_finalize */
                        NULL,           /* class_data */
                        sizeof (GmeDOMDocumentType),
                        0,              /* n_preallocs */
                        (GInstanceInitFunc) instance_init,
                };
                type = g_type_register_static (GME_TYPE_SUPPORTS, "GmeDOMDocumentType", &info, (GTypeFlags)0);
        }
        return type;
}

void 
gme_dom_document_type_private_set_wrapped_ptr (GmeDOMDocumentType *self, 
					       nsIDOMDocumentType *doctype)

{
	g_assert (self && doctype);
	self->doctype = doctype;
	NS_ADDREF (self->doctype);
	/* constuction param, init parent */
	gme_supports_private_set_wrapped_ptr (GME_SUPPORTS (self), self->doctype);
}

GmeDOMDocumentType* 
gme_dom_document_type_new (nsIDOMDocumentType *doctype)
{
	return GME_DOM_DOCUMENT_TYPE (g_object_new (GME_TYPE_DOM_DOCUMENT_TYPE, "wrapped-ptr", doctype, NULL));
}

gchar*
gme_dom_document_type_get_name (GmeDOMDocumentType *self)
{
	nsEmbedString name;
	nsresult rv;
	g_assert (self);

	rv = self->doctype->GetName (name);
	if (NS_SUCCEEDED (rv)) {
		return g_strdup (NS_ConvertUTF16toUTF8 (name).get ());
	}
	else {
		return NULL;
	}
}

GmeDOMNamedNodeMap* 
gme_dom_document_type_get_entities (GmeDOMDocumentType *self)
{
	nsIDOMNamedNodeMap *m = NULL;
	GmeDOMNamedNodeMap *map = NULL;
	nsresult rv;
	g_assert (self);

	rv = self->doctype->GetEntities (&(m));
	if (NS_SUCCEEDED (rv)) {
		map = gme_dom_named_node_map_new (m);
	}
	return map;
}

GmeDOMNamedNodeMap* 
gme_dom_document_type_get_notations (GmeDOMDocumentType *self)
{
	nsIDOMNamedNodeMap *m = NULL;
	GmeDOMNamedNodeMap *map = NULL;
	nsresult rv;
	g_assert (self);

	rv = self->doctype->GetNotations (&(m));
	if (NS_SUCCEEDED (rv)) {
		map = gme_dom_named_node_map_new (m);
	}
	return map;
}

gchar*
gme_dom_document_type_get_public_id (GmeDOMDocumentType *self)
{
	nsEmbedString id;
	nsresult rv;
	g_assert (self);

	rv = self->doctype->GetPublicId (id);
	if (NS_SUCCEEDED (rv)) {
		return g_strdup (NS_ConvertUTF16toUTF8 (id).get ());
	}
	else {
		return NULL;
	}
}

gchar*
gme_dom_document_type_get_system_id (GmeDOMDocumentType *self)
{
	nsEmbedString id;
	nsresult rv;
	g_assert (self);

	rv = self->doctype->GetSystemId (id);
	if (NS_SUCCEEDED (rv)) {
		return g_strdup (NS_ConvertUTF16toUTF8 (id).get ());
	}
	else {
		return NULL;
	}
}

gchar*
gme_dom_document_type_get_internal_subset (GmeDOMDocumentType *self)
{
	nsEmbedString id;
	nsresult rv;
	g_assert (self);

	rv = self->doctype->GetInternalSubset (id);
	if (NS_SUCCEEDED (rv)) {
		return g_strdup (NS_ConvertUTF16toUTF8 (id).get ());
	}
	else {
		return NULL;
	}
}
