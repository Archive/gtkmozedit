/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __GME_DOM_HTML_HR_ELEMENT_H__
#define __GME_DOM_HTML_HR_ELEMENT_H__

#include <glib.h>
#include <glib-object.h>
#include <glib/gmacros.h>
#include <gtkmozedit/dom/gme-dom-forward.h>


G_BEGIN_DECLS

#define GME_TYPE_DOM_HTML_HR_ELEMENT                  (gme_dom_html_hr_element_get_gtype ())
#define GME_DOM_HTML_HR_ELEMENT(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), GME_TYPE_DOM_HTML_HR_ELEMENT, GmeDOMHTMLHRElement))
#define GME_DOM_HTML_HR_ELEMENT_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST ((klass), GME_TYPE_DOM_HTML_HR_ELEMENT, GmeDOMHTMLHRElementClass))
#define GME_IS_DOM_HTML_HR_ELEMENT(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GME_TYPE_DOM_HTML_HR_ELEMENT))
#define GME_IS_DOM_HTML_HR_ELEMENT_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), GME_TYPE_DOM_HTML_HR_ELEMENT))
#define GME_DOM_HTML_HR_ELEMENT_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS ((obj), GME_TYPE_DOM_HTML_HR_ELEMENT, GmeDOMHTMLHRElementClass))

typedef struct _GmeDOMHTMLHRElementClass GmeDOMHTMLHRElementClass;

GType gme_dom_html_hr_element_get_gtype (void);

gchar* gme_dom_html_hr_element_get_align (GmeDOMHTMLHRElement *self);
gboolean gme_dom_html_hr_element_set_align (GmeDOMHTMLHRElement *self, const gchar *param);
gboolean gme_dom_html_hr_element_get_no_shade (GmeDOMHTMLHRElement *self);
gboolean gme_dom_html_hr_element_set_no_shade (GmeDOMHTMLHRElement *self, gboolean param);
gchar* gme_dom_html_hr_element_get_size (GmeDOMHTMLHRElement *self);
gboolean gme_dom_html_hr_element_set_size (GmeDOMHTMLHRElement *self, const gchar *param);
gchar* gme_dom_html_hr_element_get_width (GmeDOMHTMLHRElement *self);
gboolean gme_dom_html_hr_element_set_width (GmeDOMHTMLHRElement *self, const gchar *param);


G_END_DECLS

#endif /* __GME_DOM_HTML_HR_ELEMENT_H__ */
