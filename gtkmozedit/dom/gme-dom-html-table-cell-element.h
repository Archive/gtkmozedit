/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __GME_DOM_HTML_TABLE_CELL_ELEMENT_H__
#define __GME_DOM_HTML_TABLE_CELL_ELEMENT_H__

#include <glib.h>
#include <glib-object.h>
#include <glib/gmacros.h>
#include <gtkmozedit/dom/gme-dom-forward.h>


G_BEGIN_DECLS

#define GME_TYPE_DOM_HTML_TABLE_CELL_ELEMENT                  (gme_dom_html_table_cell_element_get_gtype ())
#define GME_DOM_HTML_TABLE_CELL_ELEMENT(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), GME_TYPE_DOM_HTML_TABLE_CELL_ELEMENT, GmeDOMHTMLTableCellElement))
#define GME_DOM_HTML_TABLE_CELL_ELEMENT_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST ((klass), GME_TYPE_DOM_HTML_TABLE_CELL_ELEMENT, GmeDOMHTMLTableCellElementClass))
#define GME_IS_DOM_HTML_TABLE_CELL_ELEMENT(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GME_TYPE_DOM_HTML_TABLE_CELL_ELEMENT))
#define GME_IS_DOM_HTML_TABLE_CELL_ELEMENT_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), GME_TYPE_DOM_HTML_TABLE_CELL_ELEMENT))
#define GME_DOM_HTML_TABLE_CELL_ELEMENT_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS ((obj), GME_TYPE_DOM_HTML_TABLE_CELL_ELEMENT, GmeDOMHTMLTableCellElementClass))

typedef struct _GmeDOMHTMLTableCellElementClass GmeDOMHTMLTableCellElementClass;

GType gme_dom_html_table_cell_element_get_gtype (void);

gint64 gme_dom_html_table_cell_element_get_cell_index (GmeDOMHTMLTableCellElement *self);
gchar* gme_dom_html_table_cell_element_get_abbr (GmeDOMHTMLTableCellElement *self);
gboolean gme_dom_html_table_cell_element_set_abbr (GmeDOMHTMLTableCellElement *self, const gchar *param);
gchar* gme_dom_html_table_cell_element_get_align (GmeDOMHTMLTableCellElement *self);
gboolean gme_dom_html_table_cell_element_set_align (GmeDOMHTMLTableCellElement *self, const gchar *param);
gchar* gme_dom_html_table_cell_element_get_axis (GmeDOMHTMLTableCellElement *self);
gboolean gme_dom_html_table_cell_element_set_axis (GmeDOMHTMLTableCellElement *self, const gchar *param);
gchar* gme_dom_html_table_cell_element_get_bg_color (GmeDOMHTMLTableCellElement *self);
gboolean gme_dom_html_table_cell_element_set_bg_color (GmeDOMHTMLTableCellElement *self, const gchar *param);
gchar* gme_dom_html_table_cell_element_get_ch (GmeDOMHTMLTableCellElement *self);
gboolean gme_dom_html_table_cell_element_set_ch (GmeDOMHTMLTableCellElement *self, const gchar *param);
gchar* gme_dom_html_table_cell_element_get_ch_off (GmeDOMHTMLTableCellElement *self);
gboolean gme_dom_html_table_cell_element_set_ch_off (GmeDOMHTMLTableCellElement *self, const gchar *param);
gint64 gme_dom_html_table_cell_element_get_col_span (GmeDOMHTMLTableCellElement *self);
gboolean gme_dom_html_table_cell_element_set_col_span (GmeDOMHTMLTableCellElement *self, gint32 param);
gchar* gme_dom_html_table_cell_element_get_headers (GmeDOMHTMLTableCellElement *self);
gboolean gme_dom_html_table_cell_element_set_headers (GmeDOMHTMLTableCellElement *self, const gchar *param);
gchar* gme_dom_html_table_cell_element_get_height (GmeDOMHTMLTableCellElement *self);
gboolean gme_dom_html_table_cell_element_set_height (GmeDOMHTMLTableCellElement *self, const gchar *param);
gboolean gme_dom_html_table_cell_element_get_no_wrap (GmeDOMHTMLTableCellElement *self);
gboolean gme_dom_html_table_cell_element_set_no_wrap (GmeDOMHTMLTableCellElement *self, gboolean param);
gint64 gme_dom_html_table_cell_element_get_row_span (GmeDOMHTMLTableCellElement *self);
gboolean gme_dom_html_table_cell_element_set_row_span (GmeDOMHTMLTableCellElement *self, gint32 param);
gchar* gme_dom_html_table_cell_element_get_scope (GmeDOMHTMLTableCellElement *self);
gboolean gme_dom_html_table_cell_element_set_scope (GmeDOMHTMLTableCellElement *self, const gchar *param);
gchar* gme_dom_html_table_cell_element_get_v_align (GmeDOMHTMLTableCellElement *self);
gboolean gme_dom_html_table_cell_element_set_v_align (GmeDOMHTMLTableCellElement *self, const gchar *param);
gchar* gme_dom_html_table_cell_element_get_width (GmeDOMHTMLTableCellElement *self);
gboolean gme_dom_html_table_cell_element_set_width (GmeDOMHTMLTableCellElement *self, const gchar *param);


G_END_DECLS

#endif /* __GME_DOM_HTML_TABLE_CELL_ELEMENT_H__ */
