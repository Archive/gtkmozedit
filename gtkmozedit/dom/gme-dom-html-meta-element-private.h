/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __GME_DOM_HTML_META_ELEMENT_PRIVATE_H__
#define __GME_DOM_HTML_META_ELEMENT_PRIVATE_H__

#include <glib/gmacros.h>
#include <nsIDOMHTMLMetaElement.h>

#include "gme-dom-html-meta-element.h"
#include "gme-dom-html-element-private.h"

G_BEGIN_DECLS

struct _GmeDOMHTMLMetaElement {
	GmeDOMHTMLElement parent;
	nsIDOMHTMLMetaElement *wrapped_ptr;
	gboolean is_disposed;
};

struct _GmeDOMHTMLMetaElementClass {
	GmeDOMHTMLElementClass parent;
	void (* dispose) (GObject *instance);
};

GmeDOMHTMLMetaElement* gme_dom_html_meta_element_new (nsIDOMHTMLMetaElement *wrapped_ptr);
void gme_dom_html_meta_element_private_set_wrapped_ptr (GmeDOMHTMLMetaElement *self, nsIDOMHTMLMetaElement *wrapped_ptr);

G_END_DECLS

#endif /* __GME_DOM_HTML_META_ELEMENT_PRIVATE_H__ */
