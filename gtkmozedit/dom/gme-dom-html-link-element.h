/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __GME_DOM_HTML_LINK_ELEMENT_H__
#define __GME_DOM_HTML_LINK_ELEMENT_H__

#include <glib.h>
#include <glib-object.h>
#include <glib/gmacros.h>
#include <gtkmozedit/dom/gme-dom-forward.h>


G_BEGIN_DECLS

#define GME_TYPE_DOM_HTML_LINK_ELEMENT                  (gme_dom_html_link_element_get_gtype ())
#define GME_DOM_HTML_LINK_ELEMENT(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), GME_TYPE_DOM_HTML_LINK_ELEMENT, GmeDOMHTMLLinkElement))
#define GME_DOM_HTML_LINK_ELEMENT_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST ((klass), GME_TYPE_DOM_HTML_LINK_ELEMENT, GmeDOMHTMLLinkElementClass))
#define GME_IS_DOM_HTML_LINK_ELEMENT(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GME_TYPE_DOM_HTML_LINK_ELEMENT))
#define GME_IS_DOM_HTML_LINK_ELEMENT_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), GME_TYPE_DOM_HTML_LINK_ELEMENT))
#define GME_DOM_HTML_LINK_ELEMENT_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS ((obj), GME_TYPE_DOM_HTML_LINK_ELEMENT, GmeDOMHTMLLinkElementClass))

typedef struct _GmeDOMHTMLLinkElementClass GmeDOMHTMLLinkElementClass;

GType gme_dom_html_link_element_get_gtype (void);

gboolean gme_dom_html_link_element_get_disabled (GmeDOMHTMLLinkElement *self);
gboolean gme_dom_html_link_element_set_disabled (GmeDOMHTMLLinkElement *self, gboolean param);
gchar* gme_dom_html_link_element_get_charset (GmeDOMHTMLLinkElement *self);
gboolean gme_dom_html_link_element_set_charset (GmeDOMHTMLLinkElement *self, const gchar *param);
gchar* gme_dom_html_link_element_get_href (GmeDOMHTMLLinkElement *self);
gboolean gme_dom_html_link_element_set_href (GmeDOMHTMLLinkElement *self, const gchar *param);
gchar* gme_dom_html_link_element_get_hreflang (GmeDOMHTMLLinkElement *self);
gboolean gme_dom_html_link_element_set_hreflang (GmeDOMHTMLLinkElement *self, const gchar *param);
gchar* gme_dom_html_link_element_get_media (GmeDOMHTMLLinkElement *self);
gboolean gme_dom_html_link_element_set_media (GmeDOMHTMLLinkElement *self, const gchar *param);
gchar* gme_dom_html_link_element_get_rel (GmeDOMHTMLLinkElement *self);
gboolean gme_dom_html_link_element_set_rel (GmeDOMHTMLLinkElement *self, const gchar *param);
gchar* gme_dom_html_link_element_get_rev (GmeDOMHTMLLinkElement *self);
gboolean gme_dom_html_link_element_set_rev (GmeDOMHTMLLinkElement *self, const gchar *param);
gchar* gme_dom_html_link_element_get_target (GmeDOMHTMLLinkElement *self);
gboolean gme_dom_html_link_element_set_target (GmeDOMHTMLLinkElement *self, const gchar *param);
gchar* gme_dom_html_link_element_get_type (GmeDOMHTMLLinkElement *self);
gboolean gme_dom_html_link_element_set_type (GmeDOMHTMLLinkElement *self, const gchar *param);


G_END_DECLS

#endif /* __GME_DOM_HTML_LINK_ELEMENT_H__ */
