/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <gtkmozedit/gtkmozedit-config.h>

#include "nsIDOMText.h"

#include "gme-dom-text-private.h"
#include "gme-dom-character-data-private.h"

enum {
	PROP_0,
	PROP_WRAPPED_PTR,
	_NUM_PROPS
};

static GmeDOMCharacterDataClass *gme_dom_text_parent_class = NULL;

static void
instance_init (GmeDOMText *self)
{
	self->text = NULL;
	self->is_disposed = FALSE;
}

static void
instance_dispose (GObject *instance)
{
	GmeDOMText *self = GME_DOM_TEXT (instance);

	if (self->is_disposed)
		return;

	if (self->text) NS_RELEASE (self->text);
	self->text = NULL;
	self->is_disposed = TRUE;

	gme_dom_text_parent_class->dispose (G_OBJECT (self));
}

static void
set_property (GObject      *object,
	      guint         prop_id,
	      GValue const *value,
	      GParamSpec   *pspec)
{
	GmeDOMText *self = GME_DOM_TEXT (object);
	nsISupports *ptr = NULL;
	nsresult rv;

	switch (prop_id) {
	case PROP_WRAPPED_PTR:
		ptr = reinterpret_cast<nsISupports*>(g_value_get_pointer (value));
		rv = CallQueryInterface (ptr, &self->text);
		if (NS_SUCCEEDED (rv) && self->text) {
			NS_ADDREF (self->text);
			/* constuction param, init parent */
			gme_dom_character_data_private_set_wrapped_ptr (GME_DOM_CHARACTER_DATA (self), self->text);
		}
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
get_property (GObject    *object,
	      guint       prop_id,
	      GValue     *value,
	      GParamSpec *pspec)
{
	GmeDOMText *self = GME_DOM_TEXT (object);

	switch (prop_id) {
	case PROP_WRAPPED_PTR:
		g_value_set_pointer (value, (gpointer) self->text);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
class_init (GmeDOMTextClass *klass)
{
	GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
	GmeDOMCharacterDataClass *node_class = GME_DOM_CHARACTER_DATA_CLASS (klass);

	/* vfuncs */
	klass->dispose = instance_dispose;

	/* hook gobject vfuncs */
	gobject_class->dispose = instance_dispose;

	gme_dom_text_parent_class = (GmeDOMCharacterDataClass*) g_type_class_peek_parent (klass);

	gobject_class->set_property = set_property;
	gobject_class->get_property = get_property;

	g_object_class_install_property (gobject_class,
		PROP_WRAPPED_PTR,
		g_param_spec_pointer ("wrapped-ptr", _("Wrapped Pointer"),
			_("Pointer to the wrapped c++ object"),
			(GParamFlags)(G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY)));
}

GType
gme_dom_text_get_gtype (void)
{
        static GType type = 0;
        if (!type) {
                static const GTypeInfo info = {
                        sizeof (GmeDOMTextClass),
                        NULL,           /* base_init */
                        NULL,           /* base_finalize */
                        (GClassInitFunc) class_init,
                        NULL,           /* class_finalize */
                        NULL,           /* class_data */
                        sizeof (GmeDOMText),
                        0,              /* n_preallocs */
                        (GInstanceInitFunc) instance_init,
                };
                type = g_type_register_static (GME_TYPE_DOM_CHARACTER_DATA, "GmeDOMText", &info, (GTypeFlags)0);        }
        return type;
}

void 
gme_dom_text_private_set_wrapped_ptr (GmeDOMText *self, 
				      nsIDOMText *text)
{
	g_assert (self && text);
	self->text = text;
	NS_ADDREF (self->text);
	/* constuction param, init parent */
	gme_dom_character_data_private_set_wrapped_ptr (GME_DOM_CHARACTER_DATA (self), self->text);
}

GmeDOMText* 
gme_dom_text_new (nsIDOMText *text)
{
	return GME_DOM_TEXT (g_object_new (GME_TYPE_DOM_TEXT, "wrapped-ptr", text, NULL));
}

GmeDOMText* 
gme_dom_text_split_text (GmeDOMText *self, 
			 guint32 offset)
{
	nsIDOMText *t = NULL; 
	GmeDOMText *text = NULL;
	nsresult rv;
	g_assert (self);

	rv = self->text->SplitText (offset, &(t));
	if (NS_SUCCEEDED (rv)) {
		text = gme_dom_text_new (t);
	}
	return text;
}
