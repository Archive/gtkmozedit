/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __GME_DOM_DOCUMENT_H__
#define __GME_DOM_DOCUMENT_H__

#include <glib.h>
#include <glib-object.h>
#include <glib/gmacros.h>

#include <gtkmozedit/dom/gme-dom-forward.h>

G_BEGIN_DECLS

#define GME_TYPE_DOM_DOCUMENT                  (gme_dom_document_get_gtype ())
#define GME_DOM_DOCUMENT(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), GME_TYPE_DOM_DOCUMENT, GmeDOMDocument))
#define GME_DOM_DOCUMENT_CLASS(klass)          (G_TYPE_CHECK_CLASS_CAST ((klass), GME_TYPE_DOM_DOCUMENT, GmeDOMDocumentClass))
#define GME_IS_DOM_DOCUMENT(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GME_TYPE_DOM_DOCUMENT))
#define GME_IS_DOM_DOCUMENT_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), GME_TYPE_DOM_DOCUMENT))
#define GME_DOM_DOCUMENT_GET_CLASS(obj)        (G_TYPE_INSTANCE_GET_CLASS ((obj), GME_TYPE_DOM_DOCUMENT, GmeDOMDocumentClass))

typedef struct _GmeDOMDocumentClass GmeDOMDocumentClass;

GType gme_dom_document_get_gtype (void);

/**
 * The GmeDOMDocument interface represents the entire HTML or XML document.
 * Conceptually, it is the root of the document tree, and provides the 
 * primary access to the document's data.
 * Since elements, text nodes, comments, processing instructions, etc. 
 * cannot exist outside the context of a Document, the GmeDOMDocument 
 * interface also contains the factory methods needed to create these 
 * objects.
 *
 * For more information on this interface please see 
 * http://www.w3.org/TR/DOM-Level-2-Core/
 *
 * @status FROZEN
 */

GmeDOMDocumentType* gme_dom_document_get_doctype (GmeDOMDocument *self);
GmeDOMDOMImplementation* gme_dom_document_get_implementation (GmeDOMDocument *self);
GmeDOMElement* gme_dom_document_get_document_element (GmeDOMDocument *self);
GmeDOMElement* gme_dom_document_create_element (GmeDOMDocument *self, const gchar *tag_name);
GmeDOMDocumentFragment* gme_dom_document_create_document_fragment (GmeDOMDocument *self);
GmeDOMText* gme_dom_document_create_text_node (GmeDOMDocument *self, const gchar *data);
GmeDOMComment* gme_dom_document_create_comment (GmeDOMDocument *self, const gchar *data);
GmeDOMCDATASection* gme_dom_document_create_cdata_section (GmeDOMDocument *self, const gchar *data);
GmeDOMProcessingInstruction* gme_dom_document_create_processing_instruction (GmeDOMDocument *self, const gchar *target, 
                                                        const gchar *data);
GmeDOMAttr* gme_dom_document_create_attribute (GmeDOMDocument *self, const gchar *name);
GmeDOMEntityReference* gme_dom_document_create_entity_reference (GmeDOMDocument *self, const gchar *name);
GmeDOMNodeList* gme_dom_document_get_elements_by_tag_name (GmeDOMDocument *self, const gchar *tag_name);
/* Introduced in DOM Level 2 */
GmeDOMNode* gme_dom_document_import_node (GmeDOMDocument *self, GmeDOMNode *imported_node, gboolean deep);

/* Introduced in DOM Level 2:
GmeDOMElement* gme_dom_document_create_element_ns (GmeDOMDocument *self, const gchar *namespace_uri,
                                        const gchar *qualified_name)
*/
/* Introduced in DOM Level 2:
GmeDOMAttr* gme_dom_document_create_attribute_ns (GmeDOMDocument *self, const gchar *namespace_uri,
                                                const gchar *qualified_name)
*/
/* Introduced in DOM Level 2:
GmeDOMNodeList* gme_dom_document_get_elements_by_tag_name_ns (GmeDOMDocument *self, const gchar *namespace_uri,
                                                const gchar *local_name);
*/
/* Introduced in DOM Level 2 */
GmeDOMElement* gme_dom_document_get_element_by_id (GmeDOMDocument *self, const gchar *element_id);

G_END_DECLS

#endif /* __GME_DOM_DOCUMENT_H__ */
