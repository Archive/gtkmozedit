/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <gtkmozedit/gtkmozedit-config.h>

#include "nsIDOMHTMLTableElement.h"

#include "gme-dom-html-table-element-private.h"
#include "gme-dom-html-table-caption-element-private.h"
#include "gme-dom-html-table-section-element-private.h"
#include "gme-dom-html-table-section-element-private.h"
#include "gme-dom-html-collection-private.h"
#include "gme-dom-html-collection-private.h"


enum {
	PROP_0,
	PROP_WRAPPED_PTR,
	_NUM_PROPS
};

static GmeDOMHTMLElementClass *gme_dom_html_table_element_parent_class = NULL;

static void
instance_init (GmeDOMHTMLTableElement *self)
{
	self->wrapped_ptr = NULL;
	self->is_disposed = FALSE;
}

static void
instance_dispose (GObject *instance)
{
	GmeDOMHTMLTableElement *self = GME_DOM_HTML_TABLE_ELEMENT (instance);

	if (self->is_disposed)
		return;

	if (self->wrapped_ptr) NS_RELEASE (self->wrapped_ptr);
	self->wrapped_ptr = NULL;
	self->is_disposed = TRUE;

	gme_dom_html_table_element_parent_class->dispose (G_OBJECT (self));
}

static void
set_property (GObject      *object,
	      guint         prop_id,
	      GValue const *value,
	      GParamSpec   *pspec)
{
	GmeDOMHTMLTableElement *self = GME_DOM_HTML_TABLE_ELEMENT (object);
	nsISupports *ptr = NULL;
	nsresult rv;

	switch (prop_id) {
	case PROP_WRAPPED_PTR:
		ptr = reinterpret_cast<nsISupports*>(g_value_get_pointer (value));
		rv = CallQueryInterface (ptr, &self->wrapped_ptr);
		if (NS_SUCCEEDED (rv) && self->wrapped_ptr) {
			NS_ADDREF (self->wrapped_ptr);
			/* constuction param, init parent */
			gme_dom_html_element_private_set_wrapped_ptr (GME_DOM_HTML_ELEMENT (self), self->wrapped_ptr);
		}
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
get_property (GObject    *object,
	      guint       prop_id,
	      GValue     *value,
	      GParamSpec *pspec)
{
	GmeDOMHTMLTableElement *self = GME_DOM_HTML_TABLE_ELEMENT (object);

	switch (prop_id) {
	case PROP_WRAPPED_PTR:
		g_value_set_pointer (value, (gpointer) self->wrapped_ptr);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
class_init (GmeDOMHTMLTableElementClass *klass)
{
	GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
	GmeDOMHTMLElementClass *node_class = GME_DOM_HTML_ELEMENT_CLASS (klass);

	/* vfuncs */
	klass->dispose = instance_dispose;

	/* hook gobject vfuncs */
	gobject_class->dispose = instance_dispose;

	gme_dom_html_table_element_parent_class = (GmeDOMHTMLElementClass*) g_type_class_peek_parent (klass);

	gobject_class->set_property = set_property;
	gobject_class->get_property = get_property;

	g_object_class_install_property (gobject_class,
		PROP_WRAPPED_PTR,
		g_param_spec_pointer ("wrapped-ptr", _("Wrapped Pointer"),
			_("Pointer to the wrapped c++ object"),
			(GParamFlags)(G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY)));
}

GType
gme_dom_html_table_element_get_gtype (void)
{
        static GType type = 0;
        if (!type) {
                static const GTypeInfo info = {
                        sizeof (GmeDOMHTMLTableElementClass),
                        NULL,           /* base_init */
                        NULL,           /* base_finalize */
                        (GClassInitFunc) class_init,
                        NULL,           /* class_finalize */
                        NULL,           /* class_data */
                        sizeof (GmeDOMHTMLTableElement),
                        0,              /* n_preallocs */
                        (GInstanceInitFunc) instance_init,
                };
                type = g_type_register_static (GME_TYPE_DOM_HTML_ELEMENT, "GmeDOMHTMLTableElement", &info, (GTypeFlags)0);
        }
        return type;
}

void 
gme_dom_html_table_element_private_set_wrapped_ptr (GmeDOMHTMLTableElement *self, 
				      nsIDOMHTMLTableElement *wrapped_ptr)
{
	g_assert (self && wrapped_ptr);
	self->wrapped_ptr = wrapped_ptr;
	NS_ADDREF (self->wrapped_ptr);
	/* constuction param, init parent */
	gme_dom_html_element_private_set_wrapped_ptr (GME_DOM_HTML_ELEMENT (self), self->wrapped_ptr);
}

GmeDOMHTMLTableElement* 
gme_dom_html_table_element_new (nsIDOMHTMLTableElement *wrapped_ptr)
{
	return GME_DOM_HTML_TABLE_ELEMENT (g_object_new (GME_TYPE_DOM_HTML_TABLE_ELEMENT, "wrapped-ptr", wrapped_ptr, NULL));
}

GmeDOMHTMLTableCaptionElement* 
gme_dom_html_table_element_get_caption (GmeDOMHTMLTableElement *self)
{
	nsIDOMHTMLTableCaptionElement *param = NULL;
	GmeDOMHTMLTableCaptionElement *object = NULL;
	nsresult rv;
	g_assert (self);
	
	rv = self->wrapped_ptr->GetCaption (&param);
	if (NS_SUCCEEDED (rv)) {
		object = gme_dom_html_table_caption_element_new (param);
	}
	return object;
}

gboolean 
gme_dom_html_table_element_set_caption (GmeDOMHTMLTableElement *self, GmeDOMHTMLTableCaptionElement *param)
{
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->SetCaption (param->wrapped_ptr);
	return NS_SUCCEEDED (rv);
}

GmeDOMHTMLTableSectionElement* 
gme_dom_html_table_element_get_thead (GmeDOMHTMLTableElement *self)
{
	nsIDOMHTMLTableSectionElement *param = NULL;
	GmeDOMHTMLTableSectionElement *object = NULL;
	nsresult rv;
	g_assert (self);
	
	rv = self->wrapped_ptr->GetTHead (&param);
	if (NS_SUCCEEDED (rv)) {
		object = gme_dom_html_table_section_element_new (param);
	}
	return object;
}

gboolean 
gme_dom_html_table_element_set_thead (GmeDOMHTMLTableElement *self, GmeDOMHTMLTableSectionElement *param)
{
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->SetTHead (param->wrapped_ptr);
	return NS_SUCCEEDED (rv);
}

GmeDOMHTMLTableSectionElement* 
gme_dom_html_table_element_get_tfoot (GmeDOMHTMLTableElement *self)
{
	nsIDOMHTMLTableSectionElement *param = NULL;
	GmeDOMHTMLTableSectionElement *object = NULL;
	nsresult rv;
	g_assert (self);
	
	rv = self->wrapped_ptr->GetTFoot (&param);
	if (NS_SUCCEEDED (rv)) {
		object = gme_dom_html_table_section_element_new (param);
	}
	return object;
}

gboolean 
gme_dom_html_table_element_set_tfoot (GmeDOMHTMLTableElement *self, GmeDOMHTMLTableSectionElement *param)
{
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->SetTFoot (param->wrapped_ptr);
	return NS_SUCCEEDED (rv);
}

GmeDOMHTMLCollection* 
gme_dom_html_table_element_get_rows (GmeDOMHTMLTableElement *self)
{
	nsIDOMHTMLCollection *param = NULL;
	GmeDOMHTMLCollection *object = NULL;
	nsresult rv;
	g_assert (self);
	
	rv = self->wrapped_ptr->GetRows (&param);
	if (NS_SUCCEEDED (rv)) {
		object = gme_dom_html_collection_new (param);
	}
	return object;
}

GmeDOMHTMLCollection* 
gme_dom_html_table_element_get_tbodies (GmeDOMHTMLTableElement *self)
{
	nsIDOMHTMLCollection *param = NULL;
	GmeDOMHTMLCollection *object = NULL;
	nsresult rv;
	g_assert (self);
	
	rv = self->wrapped_ptr->GetTBodies (&param);
	if (NS_SUCCEEDED (rv)) {
		object = gme_dom_html_collection_new (param);
	}
	return object;
}

gchar* 
gme_dom_html_table_element_get_align (GmeDOMHTMLTableElement *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetAlign (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_html_table_element_set_align (GmeDOMHTMLTableElement *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetAlign (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_html_table_element_get_bg_color (GmeDOMHTMLTableElement *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetBgColor (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_html_table_element_set_bg_color (GmeDOMHTMLTableElement *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetBgColor (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_html_table_element_get_border (GmeDOMHTMLTableElement *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetBorder (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_html_table_element_set_border (GmeDOMHTMLTableElement *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetBorder (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_html_table_element_get_cell_padding (GmeDOMHTMLTableElement *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetCellPadding (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_html_table_element_set_cell_padding (GmeDOMHTMLTableElement *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetCellPadding (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_html_table_element_get_cell_spacing (GmeDOMHTMLTableElement *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetCellSpacing (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_html_table_element_set_cell_spacing (GmeDOMHTMLTableElement *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetCellSpacing (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_html_table_element_get_frame (GmeDOMHTMLTableElement *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetFrame (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_html_table_element_set_frame (GmeDOMHTMLTableElement *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetFrame (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_html_table_element_get_rules (GmeDOMHTMLTableElement *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetRules (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_html_table_element_set_rules (GmeDOMHTMLTableElement *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetRules (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_html_table_element_get_summary (GmeDOMHTMLTableElement *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetSummary (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_html_table_element_set_summary (GmeDOMHTMLTableElement *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetSummary (p);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_html_table_element_get_width (GmeDOMHTMLTableElement *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetWidth (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_html_table_element_set_width (GmeDOMHTMLTableElement *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetWidth (p);
	return NS_SUCCEEDED (rv);
}

GmeDOMHTMLElement* 
gme_dom_html_table_element_create_thead (GmeDOMHTMLTableElement *self)
{
	nsIDOMHTMLElement *param = NULL;
	GmeDOMHTMLElement *object = NULL;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->CreateTHead (&param);
	if (NS_SUCCEEDED (rv)) {
		object = gme_dom_html_element_new (param);
	}
	return object;
}

gboolean 
gme_dom_html_table_element_delete_thead (GmeDOMHTMLTableElement *self)
{
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->DeleteTHead ();
	return NS_SUCCEEDED (rv);
}

GmeDOMHTMLElement* 
gme_dom_html_table_element_create_tfoot (GmeDOMHTMLTableElement *self)
{
	nsIDOMHTMLElement *param = NULL;
	GmeDOMHTMLElement *object = NULL;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->CreateTFoot (&param);
	if (NS_SUCCEEDED (rv)) {
		object = gme_dom_html_element_new (param);
	}
	return object;
}

gboolean 
gme_dom_html_table_element_delete_tfoot (GmeDOMHTMLTableElement *self)
{
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->DeleteTFoot ();
	return NS_SUCCEEDED (rv);
}

GmeDOMHTMLElement* 
gme_dom_html_table_element_create_caption (GmeDOMHTMLTableElement *self)
{
	nsIDOMHTMLElement *param = NULL;
	GmeDOMHTMLElement *object = NULL;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->CreateCaption (&param);
	if (NS_SUCCEEDED (rv)) {
		object = gme_dom_html_element_new (param);
	}
	return object;
}

gboolean 
gme_dom_html_table_element_delete_caption (GmeDOMHTMLTableElement *self)
{
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->DeleteCaption ();
	return NS_SUCCEEDED (rv);
}
