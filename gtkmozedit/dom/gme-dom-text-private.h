/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __GME_DOM_TEXT_PRIVATE_H__
#define __GME_DOM_TEXT_PRIVATE_H__

#include <glib/gmacros.h>
#include <nsIDOMText.h>

#include "gme-dom-text.h"
#include "gme-dom-character-data-private.h"

G_BEGIN_DECLS

struct _GmeDOMText {
	GmeDOMCharacterData parent;
	nsIDOMText *text;
	gboolean is_disposed;
};

struct _GmeDOMTextClass {
	GmeDOMCharacterDataClass parent;
	void (* dispose) (GObject *instance);
};

GmeDOMText* gme_dom_text_new (nsIDOMText *text);
void gme_dom_text_private_set_wrapped_ptr (GmeDOMText *self, nsIDOMText *text);

G_END_DECLS

#endif /* __GME_DOM_TEXT_PRIVATE_H__ */
