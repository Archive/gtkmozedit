/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <gtkmozedit/gtkmozedit-config.h>

#include "nsIDOMNode.h"
#include "nsIDOMNodeList.h"

#include "gme-dom-node-private.h"
#include "gme-dom-document-private.h"
#include "gme-dom-node-list-private.h"
#include "gme-dom-named-node-map-private.h"

enum {
	PROP_0,
	PROP_WRAPPED_PTR,
	_NUM_PROPS
};

static GmeSupportsClass *gme_dom_node_parent_class = NULL;

static void
instance_init (GmeDOMNode *self)
{
	self->node = NULL;
	self->is_disposed = FALSE;
}

static void
instance_dispose (GObject *instance)
{
	GmeDOMNode *self = GME_DOM_NODE (instance);

	if (self->is_disposed)
		return;

	if (self->node) NS_RELEASE (self->node);
	self->node = NULL;
	self->is_disposed = TRUE;

	gme_dom_node_parent_class->dispose (G_OBJECT (self));
}

static void
set_property (GObject      *object,
	      guint         prop_id,
	      GValue const *value,
	      GParamSpec   *pspec)
{
	GmeDOMNode *self = GME_DOM_NODE (object);
	nsISupports *ptr = NULL;
	nsresult rv;

	switch (prop_id) {
	case PROP_WRAPPED_PTR:
		ptr = reinterpret_cast<nsISupports*>(g_value_get_pointer (value));
		rv = CallQueryInterface (ptr, &self->node);
		if (NS_SUCCEEDED (rv) && self->node) {
			NS_ADDREF (self->node);
			/* constuction param, init parent */
			gme_supports_private_set_wrapped_ptr (GME_SUPPORTS (self), self->node);
		}
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
get_property (GObject    *object,
	      guint       prop_id,
	      GValue     *value,
	      GParamSpec *pspec)
{
	GmeDOMNode *self = GME_DOM_NODE (object);

	switch (prop_id) {
	case PROP_WRAPPED_PTR:
		g_value_set_pointer (value, (gpointer) self->node);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
class_init (GmeDOMNodeClass *klass)
{
	GObjectClass *gobject_class = G_OBJECT_CLASS (klass);

	/* vfuncs */
	klass->dispose = instance_dispose;

	/* hook gobject vfuncs */
	gobject_class->dispose = instance_dispose;

	gme_dom_node_parent_class = (GmeSupportsClass*) g_type_class_peek_parent (klass);

	gobject_class->set_property = set_property;
	gobject_class->get_property = get_property;

	g_object_class_install_property (gobject_class,
		PROP_WRAPPED_PTR,
		g_param_spec_pointer ("wrapped-ptr", _("Wrapped Pointer"),
			_("Pointer to the wrapped c++ object"),
			(GParamFlags)(G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY)));
}

GType
gme_dom_node_get_gtype (void)
{
        static GType type = 0;
        if (!type) {
                static const GTypeInfo info = {
                        sizeof (GmeDOMNodeClass),
                        NULL,           /* base_init */
                        NULL,           /* base_finalize */
                        (GClassInitFunc) class_init,
                        NULL,           /* class_finalize */
                        NULL,           /* class_data */
                        sizeof (GmeDOMNode),
                        0,              /* n_preallocs */
                        (GInstanceInitFunc) instance_init,
                };
                type = g_type_register_static (GME_TYPE_SUPPORTS, "GmeDOMNode", &info, (GTypeFlags)0);
        }
        return type;
}

void 
gme_dom_node_private_set_wrapped_ptr (GmeDOMNode *self, 
				      nsIDOMNode *node)
{
	g_assert (self && node);
	self->node = node;
	NS_ADDREF (self->node);
	/* constuction param, init parent */
	gme_supports_private_set_wrapped_ptr (GME_SUPPORTS (self), self->node);
}

GmeDOMNode* 
gme_dom_node_new (nsIDOMNode *node)
{
	return GME_DOM_NODE (g_object_new (GME_TYPE_DOM_NODE, "wrapped-ptr", node, NULL));
}

gchar* 
gme_dom_node_get_node_name (GmeDOMNode *self)
{
	nsEmbedString name;
	nsresult rv;
	g_assert (self);

	rv = self->node->GetNodeName (name);
	if (NS_SUCCEEDED (rv)) {
		return g_strdup (NS_ConvertUTF16toUTF8 (name).get ());
	}
	return NULL;
}

gchar* 
gme_dom_node_get_node_value (GmeDOMNode *self)
{
	nsEmbedString value;
	nsresult rv;
	g_assert (self);

	rv = self->node->GetNodeValue (value);
	if (NS_SUCCEEDED (rv)) {
		return g_strdup (NS_ConvertUTF16toUTF8 (value).get ());
	}
	return NULL;
}

gboolean 
gme_dom_node_set_node_value (GmeDOMNode *self, const gchar *value)
{
	nsEmbedString v;
	nsresult rv;
	g_assert (self);

	v = NS_ConvertUTF8toUTF16 (value);
	rv = self->node->SetNodeValue (v);
	return NS_SUCCEEDED (rv);
}

gint32 
gme_dom_node_get_node_type (GmeDOMNode *self)
{
	guint16 type;
	nsresult rv;
	g_assert (self);

	rv = self->node->GetNodeType (&type);
	if (NS_SUCCEEDED (rv)) {
		type;
	}
	return -1;
}

GmeDOMNode* 
gme_dom_node_get_parent_node (GmeDOMNode *self)
{
	nsIDOMNode *n = NULL;
	GmeDOMNode *node = NULL;
	nsresult rv;
	g_assert (self);

	rv = self->node->GetParentNode (&(n));
	if (NS_SUCCEEDED (rv)) {
		node = gme_dom_node_new (n);
	}
	return node;
}

GmeDOMNodeList* 
gme_dom_node_get_child_nodes (GmeDOMNode *self)
{
	nsIDOMNodeList *l = NULL;
	GmeDOMNodeList *list = NULL;
	nsresult rv;
	g_assert (self);

	rv = self->node->GetChildNodes (&(l));
	if (NS_SUCCEEDED (rv)) {
		list = gme_dom_node_list_new (l);
	}
	return list;
}

GmeDOMNode* 
gme_dom_node_get_first_child (GmeDOMNode *self)
{
	nsIDOMNode *n = NULL;
	GmeDOMNode *node = NULL;
	nsresult rv;
	g_assert (self);

	rv = self->node->GetFirstChild (&(n));
	if (NS_SUCCEEDED (rv)) {
		node = gme_dom_node_new (n);
	}
	return node;
}

GmeDOMNode* 
gme_dom_node_get_last_child (GmeDOMNode *self)
{
	nsIDOMNode *n = NULL;
	GmeDOMNode *node = NULL;
	nsresult rv;
	g_assert (self);

	rv = self->node->GetLastChild (&(n));
	if (NS_SUCCEEDED (rv)) {
		node = gme_dom_node_new (n);
	}
	return node;
}

GmeDOMNode* 
gme_dom_node_get_previous_sibling (GmeDOMNode *self)
{
	nsIDOMNode *n = NULL;
	GmeDOMNode *node = NULL;
	nsresult rv;
	g_assert (self);

	rv = self->node->GetPreviousSibling (&(n));
	if (NS_SUCCEEDED (rv)) {
		node = gme_dom_node_new (n);
	}
	return node;
}

GmeDOMNode* 
gme_dom_node_get_next_sibling (GmeDOMNode *self)
{
	nsIDOMNode *n = NULL;
	GmeDOMNode *node = NULL;
	nsresult rv;
	g_assert (self);

	rv = self->node->GetNextSibling (&(n));
	if (NS_SUCCEEDED (rv)) {
		node = gme_dom_node_new (n);
	}
	return node;
}

GmeDOMNamedNodeMap*
gme_dom_node_get_attributes (GmeDOMNode *self)
{
	nsIDOMNamedNodeMap *m = NULL;
	GmeDOMNamedNodeMap *map = NULL;
	nsresult rv;
	g_assert (self);

	rv = self->node->GetAttributes (&(m));
	if (NS_SUCCEEDED (rv)) {
		map = gme_dom_named_node_map_new (m);
	}
	return map;
}

GmeDOMDocument*
gme_dom_node_get_owner_document (GmeDOMNode *self)
{
	nsIDOMDocument *d = NULL;
	GmeDOMDocument *doc = NULL;
	nsresult rv;
	g_assert (self);

	rv = self->node->GetOwnerDocument (&(d));
	if (NS_SUCCEEDED (rv)) {
		doc = gme_dom_document_new (d);
	}
	return doc;
}

GmeDOMNode* 
gme_dom_node_insert_before (GmeDOMNode *self, GmeDOMNode *new_child, GmeDOMNode *ref_child)
{
	nsIDOMNode *n = NULL;
	GmeDOMNode *node = NULL;
	nsresult rv;
	g_assert (self);

	rv = self->node->InsertBefore (new_child->node, ref_child->node, &(n));
	if (NS_SUCCEEDED (rv)) {
		node = gme_dom_node_new (n);
	}
	return node;
}

GmeDOMNode* 
gme_dom_node_replace_child (GmeDOMNode *self, GmeDOMNode *new_child, GmeDOMNode *old_child)
{
	nsIDOMNode *n = NULL;
	GmeDOMNode *node = NULL;
	nsresult rv;
	g_assert (self);

	rv = self->node->ReplaceChild (new_child->node, old_child->node, &(n));
	if (NS_SUCCEEDED (rv)) {
		node = gme_dom_node_new (n);
	}
	return node;
}

GmeDOMNode* 
gme_dom_node_remove_child (GmeDOMNode *self, GmeDOMNode *old_child)
{
	nsIDOMNode *n = NULL;
	GmeDOMNode *node = NULL;
	nsresult rv;
	g_assert (self);

	rv = self->node->RemoveChild (old_child->node, &(n));
	if (NS_SUCCEEDED (rv)) {
		node = gme_dom_node_new (n);
	}
	return node;
}

GmeDOMNode* 
gme_dom_node_append_child (GmeDOMNode *self, GmeDOMNode *new_child)
{
	nsIDOMNode *n = NULL;
	GmeDOMNode *node = NULL;
	nsresult rv;
	g_assert (self);

	rv = self->node->AppendChild (new_child->node, &(n));
	if (NS_SUCCEEDED (rv)) {
		node = gme_dom_node_new (n);
	}
	return node;
}

gboolean 
gme_dom_node_has_child_nodes (GmeDOMNode *self)
{
	gboolean has_child;
	nsresult rv;
	g_assert (self);

	rv = self->node->HasChildNodes (&has_child);
	if (NS_SUCCEEDED (rv)) {
		return has_child;
	}
	return FALSE;
}

GmeDOMNode* 
gme_dom_node_clone_node (GmeDOMNode *self, gboolean deep)
{
	nsIDOMNode *n = NULL;
	GmeDOMNode *node = NULL;
	nsresult rv;
	g_assert (self);

	rv = self->node->CloneNode (deep, &(n));
	if (NS_SUCCEEDED (rv)) {
		node = gme_dom_node_new (n);
	}
	return node;
}

gboolean 
gme_dom_node_normalize (GmeDOMNode *self)
{
	nsresult rv;
	g_assert (self);

	rv = self->node->Normalize ();
	return NS_SUCCEEDED (rv);
}

gboolean 
gme_dom_node_is_supported (GmeDOMNode *self, const gchar *feature, const gchar *version)
{	
	gboolean is_supported;
	nsEmbedString f;
	nsEmbedString v;
	nsresult rv;
	g_assert (self);

	f = NS_ConvertUTF8toUTF16 (feature);
	v = NS_ConvertUTF8toUTF16 (version);
	rv = self->node->IsSupported (f, v, &is_supported);
	if (NS_SUCCEEDED (rv)) {
		return is_supported;
	}
	return FALSE;
}

gchar* 
gme_dom_node_get_namespace_uri (GmeDOMNode *self)
{
	nsEmbedString namespace_uri;
	nsresult rv;
	g_assert (self);

	rv = self->node->GetNamespaceURI (namespace_uri);
	if (NS_SUCCEEDED (rv)) {
		return g_strdup (NS_ConvertUTF16toUTF8 (namespace_uri).get ());
	}
	return NULL;
}

gchar* 
gme_dom_node_get_prefix (GmeDOMNode *self)
{
	nsEmbedString prefix;
	nsresult rv;
	g_assert (self);

	rv = self->node->GetPrefix (prefix);
	if (NS_SUCCEEDED (rv)) {
		return g_strdup (NS_ConvertUTF16toUTF8 (prefix).get ());
	}
	return NULL;
}

gchar* 
gme_dom_node_get_local_name (GmeDOMNode *self)
{
	nsEmbedString local_name;
	nsresult rv;
	g_assert (self);

	rv = self->node->GetLocalName (local_name);
	if (NS_SUCCEEDED (rv)) {
		return g_strdup (NS_ConvertUTF16toUTF8 (local_name).get ());
	}
	return NULL;
}

gboolean 
gme_dom_node_has_attributes (GmeDOMNode *self)
{
	gboolean has_attributes;
	nsresult rv;
	g_assert (self);

	rv = self->node->HasAttributes (&has_attributes);
	if (NS_SUCCEEDED (rv)) {
		return has_attributes;
	}
	return FALSE;
}
