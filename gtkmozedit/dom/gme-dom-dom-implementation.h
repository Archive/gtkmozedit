/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __GME_DOM_DOM_IMPLEMENTATION_H__
#define __GME_DOM_DOM_IMPLEMENTATION_H__

#include <glib.h>
#include <glib-object.h>
#include <glib/gmacros.h>
#include <gtkmozedit/dom/gme-dom-forward.h>
#include <gtkmozedit/dom/gme-dom-document.h>
#include <gtkmozedit/dom/gme-dom-document-type.h>

G_BEGIN_DECLS

#define GME_TYPE_DOM_DOM_IMPLEMENTATION                  (gme_dom_dom_implementation_get_gtype ())
#define GME_DOM_DOM_IMPLEMENTATION(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), GME_TYPE_DOM_DOM_IMPLEMENTATION, GmeDOMDOMImplementation))
#define GME_DOM_DOM_IMPLEMENTATION_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST ((klass), GME_TYPE_DOM_DOM_IMPLEMENTATION, GmeDOMDOMImplementationClass))
#define GME_IS_DOM_DOM_IMPLEMENTATION(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GME_TYPE_DOM_DOM_IMPLEMENTATION))
#define GME_IS_DOM_DOM_IMPLEMENTATION_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), GME_TYPE_DOM_DOM_IMPLEMENTATION))
#define GME_DOM_DOM_IMPLEMENTATION_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS ((obj), GME_TYPE_DOM_DOM_IMPLEMENTATION, GmeDOMDOMImplementationClass))

typedef struct _GmeDOMDOMImplementationClass GmeDOMDOMImplementationClass;

GType gme_dom_dom_implementation_get_gtype (void);

/**
 * The GmeDOMDOMImplementation interface provides a number of methods for 
 * performing operations that are independent of any particular instance 
 * of the document object model.
 *
 * For more information on this interface please see 
 * http://www.w3.org/TR/DOM-Level-2-Core/
 *
 * @status FROZEN
 */

gboolean gme_dom_dom_implementation_has_feature (GmeDOMDOMImplementation *self, 
						 const gchar *feature, 
						 const gchar *version);
GmeDOMDocumentType* gme_dom_dom_implementation_create_document_type (GmeDOMDOMImplementation *self,
								     const gchar *qualified_name, 
								     const gchar *public_id, 
								     const gchar *system_id);
GmeDOMDocument* gme_dom_dom_implementation_create_document (GmeDOMDOMImplementation *self,
							    const gchar *namespace_uri,
                                           		    const gchar *qualified_name, 
							    GmeDOMDocumentType *doctype);

G_END_DECLS

#endif /* __GME_DOM_DOM_IMPLEMENTATION_H__ */
