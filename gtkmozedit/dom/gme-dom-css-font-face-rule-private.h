/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __GME_DOM_CSS_FONT_FACE_RULE_PRIVATE_H__
#define __GME_DOM_CSS_FONT_FACE_RULE_PRIVATE_H__

#include <glib/gmacros.h>
#include <nsIDOMCSSFontFaceRule.h>

#include "gme-dom-css-font-face-rule.h"
#include "gme-dom-css-rule-private.h"

G_BEGIN_DECLS

struct _GmeDOMCSSFontFaceRule {
	GmeDOMCSSRule parent;
	nsIDOMCSSFontFaceRule *wrapped_ptr;
	gboolean is_disposed;
};

struct _GmeDOMCSSFontFaceRuleClass {
	GmeDOMCSSRuleClass parent;
	void (* dispose) (GObject *instance);
};

GmeDOMCSSFontFaceRule* gme_dom_css_font_face_rule_new (nsIDOMCSSFontFaceRule *wrapped_ptr);
void gme_dom_css_font_face_rule_private_set_wrapped_ptr (GmeDOMCSSFontFaceRule *self, nsIDOMCSSFontFaceRule *wrapped_ptr);

G_END_DECLS

#endif /* __GME_DOM_CSS_FONT_FACE_RULE_PRIVATE_H__ */
