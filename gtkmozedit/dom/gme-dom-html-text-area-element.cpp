/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <gtkmozedit/gtkmozedit-config.h>

#include "nsIDOMHTMLTextAreaElement.h"

#include "gme-dom-html-text-area-element-private.h"
#include "gme-dom-html-form-element-private.h"


enum {
	PROP_0,
	PROP_WRAPPED_PTR,
	_NUM_PROPS
};

static GmeDOMHTMLElementClass *gme_dom_html_text_area_element_parent_class = NULL;

static void
instance_init (GmeDOMHTMLTextAreaElement *self)
{
	self->wrapped_ptr = NULL;
	self->is_disposed = FALSE;
}

static void
instance_dispose (GObject *instance)
{
	GmeDOMHTMLTextAreaElement *self = GME_DOM_HTML_TEXT_AREA_ELEMENT (instance);

	if (self->is_disposed)
		return;

	if (self->wrapped_ptr) NS_RELEASE (self->wrapped_ptr);
	self->wrapped_ptr = NULL;
	self->is_disposed = TRUE;

	gme_dom_html_text_area_element_parent_class->dispose (G_OBJECT (self));
}

static void
set_property (GObject      *object,
	      guint         prop_id,
	      GValue const *value,
	      GParamSpec   *pspec)
{
	GmeDOMHTMLTextAreaElement *self = GME_DOM_HTML_TEXT_AREA_ELEMENT (object);
	nsISupports *ptr = NULL;
	nsresult rv;

	switch (prop_id) {
	case PROP_WRAPPED_PTR:
		ptr = reinterpret_cast<nsISupports*>(g_value_get_pointer (value));
		rv = CallQueryInterface (ptr, &self->wrapped_ptr);
		if (NS_SUCCEEDED (rv) && self->wrapped_ptr) {
			NS_ADDREF (self->wrapped_ptr);
			/* constuction param, init parent */
			gme_dom_html_element_private_set_wrapped_ptr (GME_DOM_HTML_ELEMENT (self), self->wrapped_ptr);
		}
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
get_property (GObject    *object,
	      guint       prop_id,
	      GValue     *value,
	      GParamSpec *pspec)
{
	GmeDOMHTMLTextAreaElement *self = GME_DOM_HTML_TEXT_AREA_ELEMENT (object);

	switch (prop_id) {
	case PROP_WRAPPED_PTR:
		g_value_set_pointer (value, (gpointer) self->wrapped_ptr);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
class_init (GmeDOMHTMLTextAreaElementClass *klass)
{
	GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
	GmeDOMHTMLElementClass *node_class = GME_DOM_HTML_ELEMENT_CLASS (klass);

	/* vfuncs */
	klass->dispose = instance_dispose;

	/* hook gobject vfuncs */
	gobject_class->dispose = instance_dispose;

	gme_dom_html_text_area_element_parent_class = (GmeDOMHTMLElementClass*) g_type_class_peek_parent (klass);

	gobject_class->set_property = set_property;
	gobject_class->get_property = get_property;

	g_object_class_install_property (gobject_class,
		PROP_WRAPPED_PTR,
		g_param_spec_pointer ("wrapped-ptr", _("Wrapped Pointer"),
			_("Pointer to the wrapped c++ object"),
			(GParamFlags)(G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY)));
}

GType
gme_dom_html_text_area_element_get_gtype (void)
{
        static GType type = 0;
        if (!type) {
                static const GTypeInfo info = {
                        sizeof (GmeDOMHTMLTextAreaElementClass),
                        NULL,           /* base_init */
                        NULL,           /* base_finalize */
                        (GClassInitFunc) class_init,
                        NULL,           /* class_finalize */
                        NULL,           /* class_data */
                        sizeof (GmeDOMHTMLTextAreaElement),
                        0,              /* n_preallocs */
                        (GInstanceInitFunc) instance_init,
                };
                type = g_type_register_static (GME_TYPE_DOM_HTML_ELEMENT, "GmeDOMHTMLTextAreaElement", &info, (GTypeFlags)0);
        }
        return type;
}

void 
gme_dom_html_text_area_element_private_set_wrapped_ptr (GmeDOMHTMLTextAreaElement *self, 
				      nsIDOMHTMLTextAreaElement *wrapped_ptr)
{
	g_assert (self && wrapped_ptr);
	self->wrapped_ptr = wrapped_ptr;
	NS_ADDREF (self->wrapped_ptr);
	/* constuction param, init parent */
	gme_dom_html_element_private_set_wrapped_ptr (GME_DOM_HTML_ELEMENT (self), self->wrapped_ptr);
}

GmeDOMHTMLTextAreaElement* 
gme_dom_html_text_area_element_new (nsIDOMHTMLTextAreaElement *wrapped_ptr)
{
	return GME_DOM_HTML_TEXT_AREA_ELEMENT (g_object_new (GME_TYPE_DOM_HTML_TEXT_AREA_ELEMENT, "wrapped-ptr", wrapped_ptr, NULL));
}

gchar* 
gme_dom_html_text_area_element_get_default_value (GmeDOMHTMLTextAreaElement *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetDefaultValue (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_html_text_area_element_set_default_value (GmeDOMHTMLTextAreaElement *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetDefaultValue (p);
	return NS_SUCCEEDED (rv);
}

GmeDOMHTMLFormElement* 
gme_dom_html_text_area_element_get_form (GmeDOMHTMLTextAreaElement *self)
{
	nsIDOMHTMLFormElement *param = NULL;
	GmeDOMHTMLFormElement *object = NULL;
	nsresult rv;
	g_assert (self);
	
	rv = self->wrapped_ptr->GetForm (&param);
	if (NS_SUCCEEDED (rv)) {
		object = gme_dom_html_form_element_new (param);
	}
	return object;
}

gchar* 
gme_dom_html_text_area_element_get_access_key (GmeDOMHTMLTextAreaElement *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetAccessKey (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_html_text_area_element_set_access_key (GmeDOMHTMLTextAreaElement *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetAccessKey (p);
	return NS_SUCCEEDED (rv);
}

gint64 
gme_dom_html_text_area_element_get_cols (GmeDOMHTMLTextAreaElement *self)
{
	gint32 param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetCols (&param);
	if (NS_SUCCEEDED (rv)) {
		return param;
	}
	return -1;
}

gboolean 
gme_dom_html_text_area_element_set_cols (GmeDOMHTMLTextAreaElement *self, gint32 param)
{
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->SetCols (param);
	return NS_SUCCEEDED (rv);
}

gboolean 
gme_dom_html_text_area_element_get_disabled (GmeDOMHTMLTextAreaElement *self)
{
	gboolean param;
	nsresult rv;
	g_assert (self);
	
	rv = self->wrapped_ptr->GetDisabled (&param);
	return param;
}

gboolean 
gme_dom_html_text_area_element_set_disabled (GmeDOMHTMLTextAreaElement *self, gboolean param)
{
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->SetDisabled (param);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_html_text_area_element_get_name (GmeDOMHTMLTextAreaElement *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetName (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_html_text_area_element_set_name (GmeDOMHTMLTextAreaElement *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetName (p);
	return NS_SUCCEEDED (rv);
}

gboolean 
gme_dom_html_text_area_element_get_read_only (GmeDOMHTMLTextAreaElement *self)
{
	gboolean param;
	nsresult rv;
	g_assert (self);
	
	rv = self->wrapped_ptr->GetReadOnly (&param);
	return param;
}

gboolean 
gme_dom_html_text_area_element_set_read_only (GmeDOMHTMLTextAreaElement *self, gboolean param)
{
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->SetReadOnly (param);
	return NS_SUCCEEDED (rv);
}

gint64 
gme_dom_html_text_area_element_get_rows (GmeDOMHTMLTextAreaElement *self)
{
	gint32 param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetRows (&param);
	if (NS_SUCCEEDED (rv)) {
		return param;
	}
	return -1;
}

gboolean 
gme_dom_html_text_area_element_set_rows (GmeDOMHTMLTextAreaElement *self, gint32 param)
{
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->SetRows (param);
	return NS_SUCCEEDED (rv);
}

gint64 
gme_dom_html_text_area_element_get_tab_index (GmeDOMHTMLTextAreaElement *self)
{
	gint32 param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetTabIndex (&param);
	if (NS_SUCCEEDED (rv)) {
		return param;
	}
	return -1;
}

gboolean 
gme_dom_html_text_area_element_set_tab_index (GmeDOMHTMLTextAreaElement *self, gint32 param)
{
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->SetTabIndex (param);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_html_text_area_element_get_type (GmeDOMHTMLTextAreaElement *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetType (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gchar* 
gme_dom_html_text_area_element_get_value (GmeDOMHTMLTextAreaElement *self)
{
	nsEmbedString param;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetValue (param);
	if (NS_SUCCEEDED (rv)) {
		 return g_strdup (NS_ConvertUTF16toUTF8 (param).get ());
	}
	return NULL;
}

gboolean 
gme_dom_html_text_area_element_set_value (GmeDOMHTMLTextAreaElement *self, const gchar *param)
{
	nsEmbedString p;
	nsresult rv;
	g_assert (self);

	p = NS_ConvertUTF8toUTF16 (param);
	rv = self->wrapped_ptr->SetValue (p);
	return NS_SUCCEEDED (rv);
}

gboolean 
gme_dom_html_text_area_element_blur (GmeDOMHTMLTextAreaElement *self)
{
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->Blur ();
	return NS_SUCCEEDED (rv);
}

gboolean 
gme_dom_html_text_area_element_focus (GmeDOMHTMLTextAreaElement *self)
{
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->Focus ();
	return NS_SUCCEEDED (rv);
}

gboolean 
gme_dom_html_text_area_element_select (GmeDOMHTMLTextAreaElement *self)
{
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->Select ();
	return NS_SUCCEEDED (rv);
}

