/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __GME_DOM_WINDOW_H__
#define __GME_DOM_WINDOW_H__

#include <glib.h>
#include <glib-object.h>
#include <glib/gmacros.h>
#include <gtkmozedit/dom/gme-dom-forward.h>
#include <gtkmozedit/dom/gme-dom-document.h>

G_BEGIN_DECLS

#define GME_TYPE_DOM_WINDOW                  (gme_dom_window_get_gtype ())
#define GME_DOM_WINDOW(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), GME_TYPE_DOM_WINDOW, GmeDOMWindow))
#define GME_DOM_WINDOW_CLASS(klass)          (G_TYPE_CHECK_CLASS_CAST ((klass), GME_TYPE_DOM_WINDOW, GmeDOMWindowClass))
#define GME_IS_DOM_WINDOW(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GME_TYPE_DOM_WINDOW))
#define GME_IS_DOM_WINDOW_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), GME_TYPE_DOM_WINDOW))
#define GME_DOM_WINDOW_GET_CLASS(obj)        (G_TYPE_INSTANCE_GET_CLASS ((obj), GME_TYPE_DOM_WINDOW, GmeDOMWindowClass))

typedef struct _GmeDOMWindowClass GmeDOMWindowClass;

GType gme_dom_window_get_gtype (void);

/**
 * The nsIDOMWindow interface is the primary interface for a DOM
 * window object. It represents a single window object that may
 * contain child windows if the document in the window contains a
 * HTML frameset document or if the document contains iframe elements.
 *
 * This interface is not officially defined by any standard bodies, it
 * originates from the defacto DOM Level 0 standard.
 *
 * @status FROZEN
 */

GmeDOMDocument* gme_dom_window_get_document (GmeDOMWindow *self);

/*
  readonly attribute nsIDOMWindow                       parent;
  readonly attribute nsIDOMWindow                       top;
  readonly attribute nsIDOMBarProp                      scrollbars;
  [noscript] readonly attribute nsIDOMWindowCollection  frames;
           attribute DOMString                          name;
  [noscript] attribute float                            textZoom;
  readonly attribute long                               scrollX;
  readonly attribute long                               scrollY;
  void                      scrollTo(in long xScroll, in long yScroll);
  void                      scrollBy(in long xScrollDif, in long yScrollDif);
  nsISelection              getSelection();
  void                      scrollByLines(in long numLines);
  void                      scrollByPages(in long numPages);
  void                      sizeToContent();
*/
G_END_DECLS

#endif /* __GME_DOM_WINDOW_H__ */
