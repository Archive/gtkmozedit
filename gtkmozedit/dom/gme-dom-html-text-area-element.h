/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __GME_DOM_HTML_TEXT_AREA_ELEMENT_H__
#define __GME_DOM_HTML_TEXT_AREA_ELEMENT_H__

#include <glib.h>
#include <glib-object.h>
#include <glib/gmacros.h>
#include <gtkmozedit/dom/gme-dom-forward.h>
#include <gtkmozedit/dom/gme-dom-html-form-element.h>


G_BEGIN_DECLS

#define GME_TYPE_DOM_HTML_TEXT_AREA_ELEMENT                  (gme_dom_html_text_area_element_get_gtype ())
#define GME_DOM_HTML_TEXT_AREA_ELEMENT(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), GME_TYPE_DOM_HTML_TEXT_AREA_ELEMENT, GmeDOMHTMLTextAreaElement))
#define GME_DOM_HTML_TEXT_AREA_ELEMENT_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST ((klass), GME_TYPE_DOM_HTML_TEXT_AREA_ELEMENT, GmeDOMHTMLTextAreaElementClass))
#define GME_IS_DOM_HTML_TEXT_AREA_ELEMENT(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GME_TYPE_DOM_HTML_TEXT_AREA_ELEMENT))
#define GME_IS_DOM_HTML_TEXT_AREA_ELEMENT_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), GME_TYPE_DOM_HTML_TEXT_AREA_ELEMENT))
#define GME_DOM_HTML_TEXT_AREA_ELEMENT_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS ((obj), GME_TYPE_DOM_HTML_TEXT_AREA_ELEMENT, GmeDOMHTMLTextAreaElementClass))

typedef struct _GmeDOMHTMLTextAreaElementClass GmeDOMHTMLTextAreaElementClass;

GType gme_dom_html_text_area_element_get_gtype (void);

gchar* gme_dom_html_text_area_element_get_default_value (GmeDOMHTMLTextAreaElement *self);
gboolean gme_dom_html_text_area_element_set_default_value (GmeDOMHTMLTextAreaElement *self, const gchar *param);
GmeDOMHTMLFormElement* gme_dom_html_text_area_element_get_form (GmeDOMHTMLTextAreaElement *self);
gchar* gme_dom_html_text_area_element_get_access_key (GmeDOMHTMLTextAreaElement *self);
gboolean gme_dom_html_text_area_element_set_access_key (GmeDOMHTMLTextAreaElement *self, const gchar *param);
gint64 gme_dom_html_text_area_element_get_cols (GmeDOMHTMLTextAreaElement *self);
gboolean gme_dom_html_text_area_element_set_cols (GmeDOMHTMLTextAreaElement *self, gint32 param);
gboolean gme_dom_html_text_area_element_get_disabled (GmeDOMHTMLTextAreaElement *self);
gboolean gme_dom_html_text_area_element_set_disabled (GmeDOMHTMLTextAreaElement *self, gboolean param);
gchar* gme_dom_html_text_area_element_get_name (GmeDOMHTMLTextAreaElement *self);
gboolean gme_dom_html_text_area_element_set_name (GmeDOMHTMLTextAreaElement *self, const gchar *param);
gboolean gme_dom_html_text_area_element_get_read_only (GmeDOMHTMLTextAreaElement *self);
gboolean gme_dom_html_text_area_element_set_read_only (GmeDOMHTMLTextAreaElement *self, gboolean param);
gint64 gme_dom_html_text_area_element_get_rows (GmeDOMHTMLTextAreaElement *self);
gboolean gme_dom_html_text_area_element_set_rows (GmeDOMHTMLTextAreaElement *self, gint32 param);
gint64 gme_dom_html_text_area_element_get_tab_index (GmeDOMHTMLTextAreaElement *self);
gboolean gme_dom_html_text_area_element_set_tab_index (GmeDOMHTMLTextAreaElement *self, gint32 param);
gchar* gme_dom_html_text_area_element_get_type (GmeDOMHTMLTextAreaElement *self);
gchar* gme_dom_html_text_area_element_get_value (GmeDOMHTMLTextAreaElement *self);
gboolean gme_dom_html_text_area_element_set_value (GmeDOMHTMLTextAreaElement *self, const gchar *param);
gboolean gme_dom_html_text_area_element_blur (GmeDOMHTMLTextAreaElement *self);
gboolean gme_dom_html_text_area_element_focus (GmeDOMHTMLTextAreaElement *self);
gboolean gme_dom_html_text_area_element_select (GmeDOMHTMLTextAreaElement *self);

G_END_DECLS

#endif /* __GME_DOM_HTML_TEXT_AREA_ELEMENT_H__ */
