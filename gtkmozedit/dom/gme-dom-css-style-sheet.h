/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __GME_DOM_CSS_STYLE_SHEET_H__
#define __GME_DOM_CSS_STYLE_SHEET_H__

#include <glib.h>
#include <glib-object.h>
#include <glib/gmacros.h>
#include <gtkmozedit/dom/gme-dom-forward.h>
#include <gtkmozedit/gme-supports.h>

G_BEGIN_DECLS

#define GME_TYPE_DOM_CSS_STYLE_SHEET                  (gme_dom_css_style_sheet_get_gtype ())
#define GME_DOM_CSS_STYLE_SHEET(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), GME_TYPE_DOM_CSS_STYLE_SHEET, GmeDOMCSSStyleSheet))
#define GME_DOM_CSS_STYLE_SHEET_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST ((klass), GME_TYPE_DOM_CSS_STYLE_SHEET, GmeDOMCSSStyleSheetClass))
#define GME_IS_DOM_CSS_STYLE_SHEET(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GME_TYPE_DOM_CSS_STYLE_SHEET))
#define GME_IS_DOM_CSS_STYLE_SHEET_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), GME_TYPE_DOM_CSS_STYLE_SHEET))
#define GME_DOM_CSS_STYLE_SHEET_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS ((obj), GME_TYPE_DOM_CSS_STYLE_SHEET, GmeDOMCSSStyleSheetClass))

typedef struct _GmeDOMCSSStyleSheetClass GmeDOMCSSStyleSheetClass;

GType gme_dom_css_style_sheet_get_gtype (void);

GmeDOMCSSRule* gme_dom_css_style_sheet_get_owner_rule (GmeDOMCSSStyleSheet *self);
GmeDOMCSSRuleList* gme_dom_css_style_sheet_get_css_rules (GmeDOMCSSStyleSheet *self);
gint64 gme_dom_css_style_sheet_insert_rule (GmeDOMCSSStyleSheet *self, const gchar *rule, guint32 index);
gboolean gme_dom_css_style_sheet_delete_rule (GmeDOMCSSStyleSheet *self, guint32 index);

G_END_DECLS

#endif /* __GME_DOM_CSS_STYLE_SHEET_H__ */
