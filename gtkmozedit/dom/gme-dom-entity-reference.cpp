/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <gtkmozedit/gtkmozedit-config.h>

#include "nsIDOMNode.h"
#include "nsIDOMEntityReference.h"

#include "gme-dom-entity-reference-private.h"
#include "gme-dom-node-private.h"

enum {
	PROP_0,
	PROP_WRAPPED_PTR,
	_NUM_PROPS
};

static GmeDOMNodeClass *gme_dom_entity_reference_parent_class = NULL;

static void
instance_init (GmeDOMEntityReference *self)
{
	self->ref = NULL;
	self->is_disposed = FALSE;
}

static void
instance_dispose (GObject *instance)
{
	GmeDOMEntityReference *self = GME_DOM_ENTITY_REFERENCE (instance);

	if (self->is_disposed)
		return;

	if (self->ref) NS_RELEASE (self->ref);
	self->ref = NULL;
	self->is_disposed = TRUE;

	gme_dom_entity_reference_parent_class->dispose (G_OBJECT (self));
}

static void
set_property (GObject      *object,
	      guint         prop_id,
	      GValue const *value,
	      GParamSpec   *pspec)
{
	GmeDOMEntityReference *self = GME_DOM_ENTITY_REFERENCE (object);
	nsISupports *ptr = NULL;
	nsresult rv;

	switch (prop_id) {
	case PROP_WRAPPED_PTR:
		ptr = reinterpret_cast<nsISupports*>(g_value_get_pointer (value));
		rv = CallQueryInterface (ptr, &self->ref);
		if (NS_SUCCEEDED (rv) && self->ref) {
			NS_ADDREF (self->ref);
			/* constuction param, init parent */
			gme_dom_node_private_set_wrapped_ptr (GME_DOM_NODE (self), self->ref);
		}
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
get_property (GObject    *object,
	      guint       prop_id,
	      GValue     *value,
	      GParamSpec *pspec)
{
	GmeDOMEntityReference *self = GME_DOM_ENTITY_REFERENCE (object);

	switch (prop_id) {
	case PROP_WRAPPED_PTR:
		g_value_set_pointer (value, (gpointer) self->ref);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
class_init (GmeDOMEntityReferenceClass *klass)
{
	GObjectClass *gobject_class = G_OBJECT_CLASS (klass);

	/* vfuncs */
	klass->dispose = instance_dispose;

	/* hook gobject vfuncs */
	gobject_class->dispose = instance_dispose;

	gme_dom_entity_reference_parent_class = (GmeDOMNodeClass*) g_type_class_peek_parent (klass);

	gobject_class->set_property = set_property;
	gobject_class->get_property = get_property;

	g_object_class_install_property (gobject_class,
		PROP_WRAPPED_PTR,
		g_param_spec_pointer ("wrapped-ptr", _("Wrapped Pointer"),
			_("Pointer to the wrapped c++ object"),
			(GParamFlags)(G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY)));
}

GType
gme_dom_entity_reference_get_gtype (void)
{
        static GType type = 0;
        if (!type) {
                static const GTypeInfo info = {
                        sizeof (GmeDOMEntityReferenceClass),
                        NULL,           /* base_init */
                        NULL,           /* base_finalize */
                        (GClassInitFunc) class_init,
                        NULL,           /* class_finalize */
                        NULL,           /* class_data */
                        sizeof (GmeDOMEntityReference),
                        0,              /* n_preallocs */
                        (GInstanceInitFunc) instance_init,
                };
                type = g_type_register_static (GME_TYPE_DOM_NODE, "GmeDOMEntityReference", &info, (GTypeFlags)0);
        }
        return type;
}

void 
gme_dom_entity_reference_private_set_wrapped_ptr (GmeDOMEntityReference *self, 
					 nsIDOMEntityReference *ref)
{
	g_assert (self && ref);
	self->ref = ref;
	NS_ADDREF (self->ref);
	/* constuction param, init parent */
	gme_supports_private_set_wrapped_ptr (GME_SUPPORTS (self), self->ref);
}

GmeDOMEntityReference* 
gme_dom_entity_reference_new (nsIDOMEntityReference *ref)
{
	return GME_DOM_ENTITY_REFERENCE (g_object_new (GME_TYPE_DOM_ENTITY_REFERENCE, "wrapped-ptr", ref, NULL));
}
