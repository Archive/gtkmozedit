/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <gtkmozedit/gtkmozedit-config.h>

#include "nsIDOMDOMImplementation.h"

#include "gme-dom-dom-implementation-private.h"
#include "gme-dom-document-private.h"
#include "gme-dom-document-type-private.h"

enum {
	PROP_0,
	PROP_WRAPPED_PTR,
	_NUM_PROPS
};

static GmeSupportsClass *gme_dom_dom_implementation_parent_class = NULL;

static void
instance_init (GmeDOMDOMImplementation *self)
{
	self->impl = NULL;
	self->is_disposed = FALSE;
}

static void
instance_dispose (GObject *instance)
{
	GmeDOMDOMImplementation *self = GME_DOM_DOM_IMPLEMENTATION (instance);

	if (self->is_disposed)
		return;

	if (self->impl) NS_RELEASE (self->impl);
	self->impl = NULL;
	self->is_disposed = TRUE;

	gme_dom_dom_implementation_parent_class->dispose (G_OBJECT (self));
}

static void
set_property (GObject      *object,
	      guint         prop_id,
	      GValue const *value,
	      GParamSpec   *pspec)
{
	GmeDOMDOMImplementation *self = GME_DOM_DOM_IMPLEMENTATION (object);
	nsISupports *ptr = NULL;
	nsresult rv;

	switch (prop_id) {
	case PROP_WRAPPED_PTR:
		ptr = reinterpret_cast<nsISupports*>(g_value_get_pointer (value));
		rv = CallQueryInterface (ptr, &self->impl);
		if (NS_SUCCEEDED (rv) && self->impl) {
			NS_ADDREF (self->impl);
			/* constuction param, init parent */
			gme_supports_private_set_wrapped_ptr (GME_SUPPORTS (self), self->impl);
		}
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
get_property (GObject    *object,
	      guint       prop_id,
	      GValue     *value,
	      GParamSpec *pspec)
{
	GmeDOMDOMImplementation *self = GME_DOM_DOM_IMPLEMENTATION (object);

	switch (prop_id) {
	case PROP_WRAPPED_PTR:
		g_value_set_pointer (value, (gpointer) self->impl);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
class_init (GmeDOMDOMImplementationClass *klass)
{
	GObjectClass *gobject_class = G_OBJECT_CLASS (klass);

	/* vfuncs */
	klass->dispose = instance_dispose;

	/* hook gobject vfuncs */
	gobject_class->dispose = instance_dispose;

	gme_dom_dom_implementation_parent_class = (GmeSupportsClass*) g_type_class_peek_parent (klass);

	gobject_class->set_property = set_property;
	gobject_class->get_property = get_property;

	g_object_class_install_property (gobject_class,
		PROP_WRAPPED_PTR,
		g_param_spec_pointer ("wrapped-ptr", _("Wrapped Pointer"),
			_("Pointer to the wrapped c++ object"),
			(GParamFlags)(G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY)));
}

GType
gme_dom_dom_implementation_get_gtype (void)
{
        static GType type = 0;
        if (!type) {
                static const GTypeInfo info = {
                        sizeof (GmeDOMDOMImplementationClass),
                        NULL,           /* base_init */
                        NULL,           /* base_finalize */
                        (GClassInitFunc) class_init,
                        NULL,           /* class_finalize */
                        NULL,           /* class_data */
                        sizeof (GmeDOMDOMImplementation),
                        0,              /* n_preallocs */
                        (GInstanceInitFunc) instance_init,
                };
                type = g_type_register_static (GME_TYPE_SUPPORTS, "GmeDOMDOMImplementation", &info, (GTypeFlags)0);
        }
        return type;
}

void 
gme_dom_dom_implementation_private_set_wrapped_ptr (GmeDOMDOMImplementation *self, 
						    nsIDOMDOMImplementation *impl)
{
	g_assert (self && impl);
	self->impl = impl;
	NS_ADDREF (self->impl);
	/* constuction param, init parent */
	gme_supports_private_set_wrapped_ptr (GME_SUPPORTS (self), self->impl);
}

GmeDOMDOMImplementation* 
gme_dom_dom_implementation_new (nsIDOMDOMImplementation *impl)
{
	return GME_DOM_DOM_IMPLEMENTATION (g_object_new (GME_TYPE_DOM_DOM_IMPLEMENTATION, "wrapped-ptr", impl, NULL));
}

gboolean 
gme_dom_dom_implementation_has_feature (GmeDOMDOMImplementation *self,
					const gchar *feature, 
					const gchar *version)
{
	nsEmbedString f, v;
	nsresult rv;
	gboolean has_feature;
	g_assert (self);

	f = NS_ConvertUTF8toUTF16 (feature);
	v = NS_ConvertUTF8toUTF16 (version);
	rv = self->impl->HasFeature (f, v, &has_feature);
	return has_feature;
}

GmeDOMDocumentType* 
gme_dom_dom_implementation_create_document_type (GmeDOMDOMImplementation *self,
						 const gchar *qualified_name, 
						 const gchar *public_id, 
						 const gchar *system_id)
{
	nsIDOMDocumentType *dt = NULL;
	GmeDOMDocumentType *doctype = NULL;
	nsEmbedString qn, pi, si;
	nsresult rv;
	g_assert (self);

	qn = NS_ConvertUTF8toUTF16 (qualified_name);
	pi = NS_ConvertUTF8toUTF16 (public_id);
	si = NS_ConvertUTF8toUTF16 (system_id);
	rv = self->impl->CreateDocumentType (qn, pi, si, &(dt));
	if (NS_SUCCEEDED (rv)) {
		doctype = gme_dom_document_type_new (dt);
	}
	return doctype;
}

GmeDOMDocument* 
gme_dom_dom_implementation_create_document (GmeDOMDOMImplementation *self,
					    const gchar *namespace_uri,
					    const gchar *qualified_name, 
					    GmeDOMDocumentType *doctype)
{
	nsIDOMDocument *d = NULL;
	GmeDOMDocument *doc = NULL;
	nsEmbedString ni, qn;
	nsresult rv;
	g_assert (self && doctype);

	rv = self->impl->CreateDocument (ni, qn, doctype->doctype, &(d));
	if (NS_SUCCEEDED (rv)) {
		doc = gme_dom_document_new (d);
	}
	return doc;
}
