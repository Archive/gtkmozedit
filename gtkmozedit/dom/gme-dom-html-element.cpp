/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <gtkmozedit/gtkmozedit-config.h>

#include "nsIDOMHTMLElement.h"

#include "gme-dom-html-element-private.h"
#include "gme-dom-element-private.h"

enum {
	PROP_0,
	PROP_WRAPPED_PTR,
	_NUM_PROPS
};

static GmeDOMElementClass *gme_dom_html_element_parent_class = NULL;

static void
instance_init (GmeDOMHTMLElement *self)
{
	self->element = NULL;
	self->is_disposed = FALSE;
}

static void
instance_dispose (GObject *instance)
{
	GmeDOMHTMLElement *self = GME_DOM_HTML_ELEMENT (instance);

	if (self->is_disposed)
		return;

	if (self->element) NS_RELEASE (self->element);
	self->element = NULL;
	self->is_disposed = TRUE;

	gme_dom_html_element_parent_class->dispose (G_OBJECT (self));
}

static void
set_property (GObject      *object,
	      guint         prop_id,
	      GValue const *value,
	      GParamSpec   *pspec)
{
	GmeDOMHTMLElement *self = GME_DOM_HTML_ELEMENT (object);
	nsISupports *ptr = NULL;
	nsresult rv;

	switch (prop_id) {
	case PROP_WRAPPED_PTR:
		ptr = reinterpret_cast<nsISupports*>(g_value_get_pointer (value));
		rv = CallQueryInterface (ptr, &self->element);
		if (NS_SUCCEEDED (rv) && self->element) {
			NS_ADDREF (self->element);
			/* constuction param, init parent */
			gme_dom_element_private_set_wrapped_ptr (GME_DOM_ELEMENT (self), self->element);
		}
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
get_property (GObject    *object,
	      guint       prop_id,
	      GValue     *value,
	      GParamSpec *pspec)
{
	GmeDOMHTMLElement *self = GME_DOM_HTML_ELEMENT (object);

	switch (prop_id) {
	case PROP_WRAPPED_PTR:
		g_value_set_pointer (value, (gpointer) self->element);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
class_init (GmeDOMHTMLElementClass *klass)
{
	GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
	GmeDOMElementClass *node_class = GME_DOM_ELEMENT_CLASS (klass);

	/* vfuncs */
	klass->dispose = instance_dispose;

	/* hook gobject vfuncs */
	gobject_class->dispose = instance_dispose;

	gme_dom_html_element_parent_class = (GmeDOMElementClass*) g_type_class_peek_parent (klass);

	gobject_class->set_property = set_property;
	gobject_class->get_property = get_property;

	g_object_class_install_property (gobject_class,
		PROP_WRAPPED_PTR,
		g_param_spec_pointer ("wrapped-ptr", _("Wrapped Pointer"),
			_("Pointer to the wrapped c++ object"),
			(GParamFlags)(G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY)));
}

GType
gme_dom_html_element_get_gtype (void)
{
        static GType type = 0;
        if (!type) {
                static const GTypeInfo info = {
                        sizeof (GmeDOMHTMLElementClass),
                        NULL,           /* base_init */
                        NULL,           /* base_finalize */
                        (GClassInitFunc) class_init,
                        NULL,           /* class_finalize */
                        NULL,           /* class_data */
                        sizeof (GmeDOMHTMLElement),
                        0,              /* n_preallocs */
                        (GInstanceInitFunc) instance_init,
                };
                type = g_type_register_static (GME_TYPE_DOM_ELEMENT, "GmeDOMHTMLElement", &info, (GTypeFlags)0);
        }
        return type;
}

void 
gme_dom_html_element_private_set_wrapped_ptr (GmeDOMHTMLElement *self, 
				      nsIDOMHTMLElement *element)
{
	g_assert (self && element);
	self->element = element;
	NS_ADDREF (self->element);
	/* constuction param, init parent */
	gme_dom_element_private_set_wrapped_ptr (GME_DOM_ELEMENT (self), self->element);
}

GmeDOMHTMLElement* 
gme_dom_html_element_new (nsIDOMHTMLElement *element)
{
	return GME_DOM_HTML_ELEMENT (g_object_new (GME_TYPE_DOM_HTML_ELEMENT, "wrapped-ptr", element, NULL));
}

gchar* 
gme_dom_html_element_get_id (GmeDOMHTMLElement *self)
{
	nsEmbedString id;
	nsresult rv;
	g_assert (self);

	rv = self->element->GetId (id);
	if (NS_SUCCEEDED (rv)) {
		return g_strdup (NS_ConvertUTF16toUTF8 (id).get ());
	}
	return NULL;
}

gboolean 
gme_dom_html_element_set_id (GmeDOMHTMLElement *self, const gchar *id)
{
	nsEmbedString i;
	nsresult rv;
	g_assert (self);

	i = NS_ConvertUTF8toUTF16 (id);
	rv = self->element->SetId (i);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_html_element_get_title (GmeDOMHTMLElement *self)
{
	nsEmbedString title;
	nsresult rv;
	g_assert (self);

	rv = self->element->GetTitle (title);
	if (NS_SUCCEEDED (rv)) {
		return g_strdup (NS_ConvertUTF16toUTF8 (title).get ());
	}
	return NULL;
}

gboolean 
gme_dom_html_element_set_title (GmeDOMHTMLElement *self, const gchar *title)
{
	nsEmbedString t;
	nsresult rv;
	g_assert (self);

	t = NS_ConvertUTF8toUTF16 (title);
	rv = self->element->SetTitle (t);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_html_element_get_lang (GmeDOMHTMLElement *self)
{
	nsEmbedString lang;
	nsresult rv;
	g_assert (self);

	rv = self->element->GetLang (lang);
	if (NS_SUCCEEDED (rv)) {
		return g_strdup (NS_ConvertUTF16toUTF8 (lang).get ());
	}
	return NULL;
}

gboolean 
gme_dom_html_element_set_lang (GmeDOMHTMLElement *self, const gchar *lang)
{
	nsEmbedString l;
	nsresult rv;
	g_assert (self);

	l = NS_ConvertUTF8toUTF16 (lang);
	rv = self->element->SetLang (l);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_html_element_get_dir (GmeDOMHTMLElement *self)
{
	nsEmbedString dir;
	nsresult rv;
	g_assert (self);

	rv = self->element->GetDir (dir);
	if (NS_SUCCEEDED (rv)) {
		return g_strdup (NS_ConvertUTF16toUTF8 (dir).get ());
	}
	return NULL;
}

gboolean 
gme_dom_html_element_set_dir (GmeDOMHTMLElement *self, const gchar *dir)
{
	nsEmbedString d;
	nsresult rv;
	g_assert (self);

	d = NS_ConvertUTF8toUTF16 (dir);
	rv = self->element->SetDir (d);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_html_element_get_class_name (GmeDOMHTMLElement *self)
{
	nsEmbedString cn;
	nsresult rv;
	g_assert (self);

	rv = self->element->GetClassName (cn);
	if (NS_SUCCEEDED (rv)) {
		return g_strdup (NS_ConvertUTF16toUTF8 (cn).get ());
	}
	return NULL;
}

gboolean 
gme_dom_html_element_set_class_name (GmeDOMHTMLElement *self, const gchar *name)
{
	nsEmbedString n;
	nsresult rv;
	g_assert (self);

	n = NS_ConvertUTF8toUTF16 (name);
	rv = self->element->SetClassName (n);
	return NS_SUCCEEDED (rv);
}
