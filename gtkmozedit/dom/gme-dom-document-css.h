/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __GME_DOM_DOCUMENT_CSS_H__
#define __GME_DOM_DOCUMENT_CSS_H__

#include <glib.h>
#include <glib-object.h>
#include <glib/gmacros.h>
#include <gtkmozedit/dom/gme-dom-forward.h>
#include <gtkmozedit/gme-supports.h>

G_BEGIN_DECLS

#define GME_TYPE_DOM_DOCUMENT_CSS                  (gme_dom_document_css_get_gtype ())
#define GME_DOM_DOCUMENT_CSS(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), GME_TYPE_DOM_DOCUMENT_CSS, GmeDOMDocumentCSS))
#define GME_DOM_DOCUMENT_CSS_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST ((klass), GME_TYPE_DOM_DOCUMENT_CSS, GmeDOMDocumentCSSClass))
#define GME_IS_DOM_DOCUMENT_CSS(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GME_TYPE_DOM_DOCUMENT_CSS))
#define GME_IS_DOM_DOCUMENT_CSS_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), GME_TYPE_DOM_DOCUMENT_CSS))
#define GME_DOM_DOCUMENT_CSS_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS ((obj), GME_TYPE_DOM_DOCUMENT_CSS, GmeDOMDocumentCSSClass))

typedef struct _GmeDOMDocumentCSSClass GmeDOMDocumentCSSClass;

GType gme_dom_document_css_get_gtype (void);

GmeDOMCSSStyleDeclaration* gme_dom_document_css_get_override_style (GmeDOMDocumentCSS *self, GmeDOMElement *element, const gchar *pseudo_element);

G_END_DECLS

#endif /* __GME_DOM_DOCUMENT_CSS_H__ */
