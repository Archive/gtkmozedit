/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __GME_DOM_NAMED_NODE_MAP_H__
#define __GME_DOM_NAMED_NODE_MAP_H__

#include <glib.h>
#include <glib-object.h>
#include <glib/gmacros.h>
#include <gtkmozedit/dom/gme-dom-node.h>

G_BEGIN_DECLS

#define GME_TYPE_DOM_NAMED_NODE_MAP                  (gme_dom_named_node_map_get_gtype ())
#define GME_DOM_NAMED_NODE_MAP(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), GME_TYPE_DOM_NAMED_NODE_MAP, GmeDOMNamedNodeMap))
#define GME_DOM_NAMED_NODE_MAP_CLASS(klass)          (G_TYPE_CHECK_CLASS_CAST ((klass), GME_TYPE_DOM_NAMED_NODE_MAP, GmeDOMNamedNodeMapClass))
#define GME_IS_DOM_NAMED_NODE_MAP(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GME_TYPE_DOM_NAMED_NODE_MAP))
#define GME_IS_DOM_NAMED_NODE_MAP_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), GME_TYPE_DOM_NAMED_NODE_MAP))
#define GME_DOM_NAMED_NODE_MAP_GET_CLASS(obj)        (G_TYPE_INSTANCE_GET_CLASS ((obj), GME_TYPE_DOM_NAMED_NODE_MAP, GmeDOMNamedNodeMapClass))

typedef struct _GmeDOMNamedNodeMapClass GmeDOMNamedNodeMapClass;

GType gme_dom_named_node_map_get_gtype (void);

/**
 * Objects implementing the GmeDOMNamedNodeMap interface are used to 
 * represent collections of nodes that can be accessed by name.
 *
 * For more information on this interface please see 
 * http://www.w3.org/TR/DOM-Level-2-Core/
 *
 * @status FROZEN
 */

GmeDOMNode* gme_dom_named_node_map_get_named_item (GmeDOMNamedNodeMap *self, const gchar *name);
GmeDOMNode* gme_dom_named_node_map_set_named_item (GmeDOMNamedNodeMap *self, GmeDOMNode *arg);
GmeDOMNode* gme_dom_named_node_map_remove_named_item (GmeDOMNamedNodeMap *self, const gchar *name);
GmeDOMNode* gme_dom_named_node_map_item (GmeDOMNamedNodeMap *self, guint32 index);
gint64 gme_dom_named_node_map_get_length (GmeDOMNamedNodeMap *self);

/* TODO not implemented yet
  // Introduced in DOM Level 2:
GmeDOMNode* gme_dom_named_node_map_getNamedItemNS(in DOMString namespaceURI, 
                                           in DOMString localName);
  // Introduced in DOM Level 2:
GmeDOMNode* gme_dom_named_node_map_setNamedItemNS(in nsIDOMNode arg)
                                  raises(DOMException);
  // Introduced in DOM Level 2:
GmeDOMNode* gme_dom_named_node_map_removeNamedItemNS(in DOMString namespaceURI, 
                                              in DOMString localName)
                                  raises(DOMException);
*/
G_END_DECLS

#endif /* __GME_DOM_NAMED_NODE_MAP_H__ */
