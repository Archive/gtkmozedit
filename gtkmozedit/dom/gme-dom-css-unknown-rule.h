/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __GME_DOM_CSS_UNKNOWN_RULE_H__
#define __GME_DOM_CSS_UNKNOWN_RULE_H__

#include <glib.h>
#include <glib-object.h>
#include <glib/gmacros.h>
#include <gtkmozedit/dom/gme-dom-forward.h>


G_BEGIN_DECLS

#define GME_TYPE_DOM_CSS_UNKNOWN_RULE                  (gme_dom_css_unknown_rule_get_gtype ())
#define GME_DOM_CSS_UNKNOWN_RULE(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), GME_TYPE_DOM_CSS_UNKNOWN_RULE, GmeDOMCSSUnknownRule))
#define GME_DOM_CSS_UNKNOWN_RULE_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST ((klass), GME_TYPE_DOM_CSS_UNKNOWN_RULE, GmeDOMCSSUnknownRuleClass))
#define GME_IS_DOM_CSS_UNKNOWN_RULE(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GME_TYPE_DOM_CSS_UNKNOWN_RULE))
#define GME_IS_DOM_CSS_UNKNOWN_RULE_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), GME_TYPE_DOM_CSS_UNKNOWN_RULE))
#define GME_DOM_CSS_UNKNOWN_RULE_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS ((obj), GME_TYPE_DOM_CSS_UNKNOWN_RULE, GmeDOMCSSUnknownRuleClass))

typedef struct _GmeDOMCSSUnknownRuleClass GmeDOMCSSUnknownRuleClass;

GType gme_dom_css_unknown_rule_get_gtype (void);



G_END_DECLS

#endif /* __GME_DOM_CSS_UNKNOWN_RULE_H__ */
