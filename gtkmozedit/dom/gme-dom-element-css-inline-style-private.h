/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __GME_DOM_ELEMENT_CSS_INLINE_STYLE_PRIVATE_H__
#define __GME_DOM_ELEMENT_CSS_INLINE_STYLE_PRIVATE_H__

#include <glib/gmacros.h>
#include <nsIDOMElementCSSInlineStyle.h>

#include <gtkmozedit/dom/gme-dom-element-css-inline-style.h>
#include <gtkmozedit/gme-supports-private.h>

G_BEGIN_DECLS

struct _GmeDOMElementCSSInlineStyle {
	GmeSupports parent;
	nsIDOMElementCSSInlineStyle *wrapped_ptr;
	gboolean is_disposed;
};

struct _GmeDOMElementCSSInlineStyleClass {
	GmeSupportsClass parent;
	void (* dispose) (GObject *instance);
};

GmeDOMElementCSSInlineStyle* gme_dom_element_css_inline_style_new (nsIDOMElementCSSInlineStyle *wrapped_ptr);
void gme_dom_element_css_inline_style_private_set_wrapped_ptr (GmeDOMElementCSSInlineStyle *self, nsIDOMElementCSSInlineStyle *wrapped_ptr);

G_END_DECLS

#endif /* __GME_DOM_ELEMENT_CSS_INLINE_STYLE_PRIVATE_H__ */
