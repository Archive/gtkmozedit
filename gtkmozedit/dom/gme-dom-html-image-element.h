/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __GME_DOM_HTML_IMAGE_ELEMENT_H__
#define __GME_DOM_HTML_IMAGE_ELEMENT_H__

#include <glib.h>
#include <glib-object.h>
#include <glib/gmacros.h>
#include <gtkmozedit/dom/gme-dom-forward.h>


G_BEGIN_DECLS

#define GME_TYPE_DOM_HTML_IMAGE_ELEMENT                  (gme_dom_html_image_element_get_gtype ())
#define GME_DOM_HTML_IMAGE_ELEMENT(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), GME_TYPE_DOM_HTML_IMAGE_ELEMENT, GmeDOMHTMLImageElement))
#define GME_DOM_HTML_IMAGE_ELEMENT_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST ((klass), GME_TYPE_DOM_HTML_IMAGE_ELEMENT, GmeDOMHTMLImageElementClass))
#define GME_IS_DOM_HTML_IMAGE_ELEMENT(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GME_TYPE_DOM_HTML_IMAGE_ELEMENT))
#define GME_IS_DOM_HTML_IMAGE_ELEMENT_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), GME_TYPE_DOM_HTML_IMAGE_ELEMENT))
#define GME_DOM_HTML_IMAGE_ELEMENT_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS ((obj), GME_TYPE_DOM_HTML_IMAGE_ELEMENT, GmeDOMHTMLImageElementClass))

typedef struct _GmeDOMHTMLImageElementClass GmeDOMHTMLImageElementClass;

GType gme_dom_html_image_element_get_gtype (void);

gchar* gme_dom_html_image_element_get_name (GmeDOMHTMLImageElement *self);
gboolean gme_dom_html_image_element_set_name (GmeDOMHTMLImageElement *self, const gchar *param);
gchar* gme_dom_html_image_element_get_align (GmeDOMHTMLImageElement *self);
gboolean gme_dom_html_image_element_set_align (GmeDOMHTMLImageElement *self, const gchar *param);
gchar* gme_dom_html_image_element_get_alt (GmeDOMHTMLImageElement *self);
gboolean gme_dom_html_image_element_set_alt (GmeDOMHTMLImageElement *self, const gchar *param);
gchar* gme_dom_html_image_element_get_border (GmeDOMHTMLImageElement *self);
gboolean gme_dom_html_image_element_set_border (GmeDOMHTMLImageElement *self, const gchar *param);
gint64 gme_dom_html_image_element_get_height (GmeDOMHTMLImageElement *self);
gboolean gme_dom_html_image_element_set_height (GmeDOMHTMLImageElement *self, gint32 param);
gint64 gme_dom_html_image_element_get_hspace (GmeDOMHTMLImageElement *self);
gboolean gme_dom_html_image_element_set_hspace (GmeDOMHTMLImageElement *self, gint32 param);
gboolean gme_dom_html_image_element_get_is_map (GmeDOMHTMLImageElement *self);
gboolean gme_dom_html_image_element_set_is_map (GmeDOMHTMLImageElement *self, gboolean param);
gchar* gme_dom_html_image_element_get_long_desc (GmeDOMHTMLImageElement *self);
gboolean gme_dom_html_image_element_set_long_desc (GmeDOMHTMLImageElement *self, const gchar *param);
gchar* gme_dom_html_image_element_get_src (GmeDOMHTMLImageElement *self);
gboolean gme_dom_html_image_element_set_src (GmeDOMHTMLImageElement *self, const gchar *param);
gchar* gme_dom_html_image_element_get_use_map (GmeDOMHTMLImageElement *self);
gboolean gme_dom_html_image_element_set_use_map (GmeDOMHTMLImageElement *self, const gchar *param);
gint64 gme_dom_html_image_element_get_vspace (GmeDOMHTMLImageElement *self);
gboolean gme_dom_html_image_element_set_vspace (GmeDOMHTMLImageElement *self, gint32 param);
gint64 gme_dom_html_image_element_get_width (GmeDOMHTMLImageElement *self);
gboolean gme_dom_html_image_element_set_width (GmeDOMHTMLImageElement *self, gint32 param);


G_END_DECLS

#endif /* __GME_DOM_HTML_IMAGE_ELEMENT_H__ */
