/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <gtkmozedit/gtkmozedit-config.h>

#include "nsIDOMNode.h"
#include "nsIDOMNamedNodeMap.h"

#include "gme-dom-named-node-map-private.h"
#include "gme-dom-node-private.h"

enum {
	PROP_0,
	PROP_WRAPPED_PTR,
	_NUM_PROPS
};

static GmeSupportsClass *gme_dom_named_node_map_parent_class = NULL;

static void
instance_init (GmeDOMNamedNodeMap *self)
{
	self->map = NULL;
	self->is_disposed = FALSE;
}

static void
instance_dispose (GObject *instance)
{
	GmeDOMNamedNodeMap *self = GME_DOM_NAMED_NODE_MAP (instance);

	if (self->is_disposed)
		return;

	if (self->map) NS_RELEASE (self->map);
	self->map = NULL;
	self->is_disposed = TRUE;

	gme_dom_named_node_map_parent_class->dispose (G_OBJECT (self));
}

static void
set_property (GObject      *object,
	      guint         prop_id,
	      GValue const *value,
	      GParamSpec   *pspec)
{
	GmeDOMNamedNodeMap *self = GME_DOM_NAMED_NODE_MAP (object);
	nsISupports *ptr = NULL;
	nsresult rv;

	switch (prop_id) {
	case PROP_WRAPPED_PTR:
		ptr = reinterpret_cast<nsISupports*>(g_value_get_pointer (value));
		rv = CallQueryInterface (ptr, &self->map);
		if (NS_SUCCEEDED (rv) && self->map) {
			NS_ADDREF (self->map);
			/* constuction param, init parent */
			gme_supports_private_set_wrapped_ptr (GME_SUPPORTS (self), self->map);
		}
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
get_property (GObject    *object,
	      guint       prop_id,
	      GValue     *value,
	      GParamSpec *pspec)
{
	GmeDOMNamedNodeMap *self = GME_DOM_NAMED_NODE_MAP (object);

	switch (prop_id) {
	case PROP_WRAPPED_PTR:
		g_value_set_pointer (value, (gpointer) self->map);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
class_init (GmeDOMNamedNodeMapClass *klass)
{
	GObjectClass *gobject_class = G_OBJECT_CLASS (klass);

	/* vfuncs */
	klass->dispose = instance_dispose;

	/* hook gobject vfuncs */
	gobject_class->dispose = instance_dispose;

	gme_dom_named_node_map_parent_class = (GmeSupportsClass*) g_type_class_peek_parent (klass);

	gobject_class->set_property = set_property;
	gobject_class->get_property = get_property;

	g_object_class_install_property (gobject_class,
		PROP_WRAPPED_PTR,
		g_param_spec_pointer ("wrapped-ptr", _("Wrapped Pointer"),
			_("Pointer to the wrapped c++ object"),
			(GParamFlags)(G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY)));
}

GType
gme_dom_named_node_map_get_gtype (void)
{
        static GType type = 0;
        if (!type) {
                static const GTypeInfo info = {
                        sizeof (GmeDOMNamedNodeMapClass),
                        NULL,           /* base_init */
                        NULL,           /* base_finalize */
                        (GClassInitFunc) class_init,
                        NULL,           /* class_finalize */
                        NULL,           /* class_data */
                        sizeof (GmeDOMNamedNodeMap),
                        0,              /* n_preallocs */
                        (GInstanceInitFunc) instance_init,
                };
                type = g_type_register_static (GME_TYPE_SUPPORTS, "GmeDOMNamedNodeMap", &info, (GTypeFlags)0);
        }
        return type;
}

void 
gme_dom_named_node_map_private_set_wrapped_ptr (GmeDOMNamedNodeMap *self, 
						nsIDOMNamedNodeMap *map)
{
	g_assert (self && map);
	self->map = map;
	NS_ADDREF (self->map);
	/* constuction param, init parent */
	gme_supports_private_set_wrapped_ptr (GME_SUPPORTS (self), self->map);
}

GmeDOMNamedNodeMap*
gme_dom_named_node_map_new (nsIDOMNamedNodeMap *map)
{
	return GME_DOM_NAMED_NODE_MAP (g_object_new (GME_TYPE_DOM_NAMED_NODE_MAP, "wrapped-ptr", map, NULL));
}

GmeDOMNode* 
gme_dom_named_node_map_get_named_item (GmeDOMNamedNodeMap *self, 
				       const gchar *name)
{
	nsIDOMNode *item = NULL;
	GmeDOMNode *node = NULL;
	nsEmbedString n;
	nsresult rv;
	g_assert (self);

	n = NS_ConvertUTF8toUTF16 (name);
	rv = self->map->GetNamedItem (n, &(item));
	if (NS_SUCCEEDED (rv)) {
		node = gme_dom_node_new (item);
	}
	return node;
}

GmeDOMNode* 
gme_dom_named_node_map_set_named_item (GmeDOMNamedNodeMap *self, 
				       GmeDOMNode *arg)
{
	g_error ("not implemented");
	return NULL;
}

GmeDOMNode* 
gme_dom_named_node_map_remove_named_item (GmeDOMNamedNodeMap *self, 
					  const gchar *name)
{
	g_error ("not implemented");
	return NULL;	
}

GmeDOMNode* 
gme_dom_named_node_map_item (GmeDOMNamedNodeMap *self, 
			     guint32 index)
{
	nsIDOMNode *item = NULL;
	GmeDOMNode *node = NULL;
	nsresult rv;
	g_assert (self);

	rv = self->map->Item (index, &(item));
	if (NS_SUCCEEDED (rv)) {
		node = gme_dom_node_new (item);
	}
	return node;
}

gint64
gme_dom_named_node_map_get_length (GmeDOMNamedNodeMap *self)
{
	guint32 length;
	nsresult rv;
	g_assert (self);

	rv = self->map->GetLength (&length);
	if (NS_SUCCEEDED (rv)) {
		return length;
	}
	return -1;
}
