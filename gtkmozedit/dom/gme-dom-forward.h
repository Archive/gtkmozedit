/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __GME_DOM_FORWARD_H__
#define __GME_DOM_FORWARD_H__

#define _(S) S

/* forward decls for circular referencing types */
typedef struct _GmeDOMAbstractView GmeDOMAbstractView;
typedef struct _GmeDOMAttr GmeDOMAttr;
typedef struct _GmeDOMCDATASection GmeDOMCDATASection;
typedef struct _GmeDOMCharacterData GmeDOMCharacterData;
typedef struct _GmeDOMComment GmeDOMComment;
typedef struct _GmeDOMCSSProperties GmeDOMCSSProperties;
typedef struct _GmeDOMCSSNSProperties GmeDOMCSSNSProperties;
typedef struct _GmeDOMCSSCharsetRule GmeDOMCSSCharsetRule;
typedef struct _GmeDOMElementCSSInlineStyle GmeDOMElementCSSInlineStyle;
typedef struct _GmeDOMCSSFontFaceRule GmeDOMCSSFontFaceRule;
typedef struct _GmeDOMCSSImportRule GmeDOMCSSImportRule;
typedef struct _GmeDOMCSSMediaRule GmeDOMCSSMediaRule;
typedef struct _GmeDOMCSSPageRule GmeDOMCSSPageRule;
typedef struct _GmeDOMCSSRule GmeDOMCSSRule;
typedef struct _GmeDOMCSSStyleRule GmeDOMCSSStyleRule;
typedef struct _GmeDOMCSSUnknownRule GmeDOMCSSUnknownRule;
typedef struct _GmeDOMCSSRuleList GmeDOMCSSRuleList;
typedef struct _GmeDOMCSSStyleDeclaration GmeDOMCSSStyleDeclaration;
typedef struct _GmeDOMCSSStyleSheet GmeDOMCSSStyleSheet;
typedef struct _GmeDOMCSSValue GmeDOMCSSValue;
typedef struct _GmeDOMCSSValueList GmeDOMCSSValueList;
typedef struct _GmeDOMDocument GmeDOMDocument;
typedef struct _GmeDOMDocumentCSS GmeDOMDocumentCSS;
typedef struct _GmeDOMDocumentFragment GmeDOMDocumentFragment;
typedef struct _GmeDOMDocumentStyle GmeDOMDocumentStyle;
typedef struct _GmeDOMDocumentType GmeDOMDocumentType;
typedef struct _GmeDOMDocumentView GmeDOMDocumentView;
typedef struct _GmeDOMDOMImplementation GmeDOMDOMImplementation;
typedef struct _GmeDOMElement GmeDOMElement;
typedef struct _GmeDOMEntityReference GmeDOMEntityReference;
typedef struct _GmeDOMHTMLCollection GmeDOMHTMLCollection;
typedef struct _GmeDOMHTMLDocument GmeDOMHTMLDocument;
typedef struct _GmeDOMHTMLAnchorElement GmeDOMHTMLAnchorElement;
typedef struct _GmeDOMHTMLAppletElement GmeDOMHTMLAppletElement;
typedef struct _GmeDOMHTMLAreaElement GmeDOMHTMLAreaElement;
typedef struct _GmeDOMHTMLBaseElement GmeDOMHTMLBaseElement;
typedef struct _GmeDOMHTMLBaseFontElement GmeDOMHTMLBaseFontElement;
typedef struct _GmeDOMHTMLBodyElement GmeDOMHTMLBodyElement;
typedef struct _GmeDOMHTMLBRElement GmeDOMHTMLBRElement;
typedef struct _GmeDOMHTMLButtonElement GmeDOMHTMLButtonElement;
typedef struct _GmeDOMHTMLDirectoryElement GmeDOMHTMLDirectoryElement;
typedef struct _GmeDOMHTMLDivElement GmeDOMHTMLDivElement;
typedef struct _GmeDOMHTMLDListElement GmeDOMHTMLDListElement;
typedef struct _GmeDOMHTMLElement GmeDOMHTMLElement;
typedef struct _GmeDOMHTMLEmbedElement GmeDOMHTMLEmbedElement;
typedef struct _GmeDOMHTMLFieldSetElement GmeDOMHTMLFieldSetElement;
typedef struct _GmeDOMHTMLFontElement GmeDOMHTMLFontElement;
typedef struct _GmeDOMHTMLFormElement GmeDOMHTMLFormElement;
typedef struct _GmeDOMHTMLFrameElement GmeDOMHTMLFrameElement;
typedef struct _GmeDOMHTMLFrameSetElement GmeDOMHTMLFrameSetElement;
typedef struct _GmeDOMHTMLHeadElement GmeDOMHTMLHeadElement;
typedef struct _GmeDOMHTMLHeadingElement GmeDOMHTMLHeadingElement;
typedef struct _GmeDOMHTMLHRElement GmeDOMHTMLHRElement;
typedef struct _GmeDOMHTMLHtmlElement GmeDOMHTMLHtmlElement;
typedef struct _GmeDOMHTMLIFrameElement GmeDOMHTMLIFrameElement;
typedef struct _GmeDOMHTMLImageElement GmeDOMHTMLImageElement;
typedef struct _GmeDOMHTMLInputElement GmeDOMHTMLInputElement;
typedef struct _GmeDOMHTMLIsIndexElement GmeDOMHTMLIsIndexElement;
typedef struct _GmeDOMHTMLLabelElement GmeDOMHTMLLabelElement;
typedef struct _GmeDOMHTMLLegendElement GmeDOMHTMLLegendElement;
typedef struct _GmeDOMHTMLLIElement GmeDOMHTMLLIElement;
typedef struct _GmeDOMHTMLLinkElement GmeDOMHTMLLinkElement;
typedef struct _GmeDOMHTMLMapElement GmeDOMHTMLMapElement;
typedef struct _GmeDOMHTMLMenuElement GmeDOMHTMLMenuElement;
typedef struct _GmeDOMHTMLMetaElement GmeDOMHTMLMetaElement;
typedef struct _GmeDOMHTMLModElement GmeDOMHTMLModElement;
typedef struct _GmeDOMHTMLObjectElement GmeDOMHTMLObjectElement;
typedef struct _GmeDOMHTMLOListElement GmeDOMHTMLOListElement;
typedef struct _GmeDOMHTMLOptGroupElement GmeDOMHTMLOptGroupElement;
typedef struct _GmeDOMHTMLOptionElement GmeDOMHTMLOptionElement;
typedef struct _GmeDOMHTMLOptionsCollection GmeDOMHTMLOptionsCollection;
typedef struct _GmeDOMHTMLParagraphElement GmeDOMHTMLParagraphElement;
typedef struct _GmeDOMHTMLParamElement GmeDOMHTMLParamElement;
typedef struct _GmeDOMHTMLPreElement GmeDOMHTMLPreElement;
typedef struct _GmeDOMHTMLQuoteElement GmeDOMHTMLQuoteElement;
typedef struct _GmeDOMHTMLScriptElement GmeDOMHTMLScriptElement;
typedef struct _GmeDOMHTMLSelectElement GmeDOMHTMLSelectElement;
typedef struct _GmeDOMHTMLStyleElement GmeDOMHTMLStyleElement;
typedef struct _GmeDOMHTMLTableCaptionElement GmeDOMHTMLTableCaptionElement;
typedef struct _GmeDOMHTMLTableCellElement GmeDOMHTMLTableCellElement;
typedef struct _GmeDOMHTMLTableColElement GmeDOMHTMLTableColElement;
typedef struct _GmeDOMHTMLTableElement GmeDOMHTMLTableElement;
typedef struct _GmeDOMHTMLTableRowElement GmeDOMHTMLTableRowElement;
typedef struct _GmeDOMHTMLTableSectionElement GmeDOMHTMLTableSectionElement;
typedef struct _GmeDOMHTMLTextAreaElement GmeDOMHTMLTextAreaElement;
typedef struct _GmeDOMHTMLTitleElement GmeDOMHTMLTitleElement;
typedef struct _GmeDOMHTMLUListElement GmeDOMHTMLUListElement;
typedef struct _GmeDOMMediaList GmeDOMMediaList;
typedef struct _GmeDOMNamedNodeMap GmeDOMNamedNodeMap;
typedef struct _GmeDOMNode GmeDOMNode;
typedef struct _GmeDOMNodeList GmeDOMNodeList;
typedef struct _GmeDOMProcessingInstruction GmeDOMProcessingInstruction;
typedef struct _GmeDOMStyleSheet GmeDOMStyleSheet;
typedef struct _GmeDOMStyleSheetList GmeDOMStyleSheetList;
typedef struct _GmeDOMText GmeDOMText;
typedef struct _GmeDOMViewCSS GmeDOMViewCSS;
typedef struct _GmeDOMWindow GmeDOMWindow;

#endif /* __GME_DOM_FORWARD_H__ */
