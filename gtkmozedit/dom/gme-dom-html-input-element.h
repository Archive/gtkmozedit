/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __GME_DOM_HTML_INPUT_ELEMENT_H__
#define __GME_DOM_HTML_INPUT_ELEMENT_H__

#include <glib.h>
#include <glib-object.h>
#include <glib/gmacros.h>
#include <gtkmozedit/dom/gme-dom-forward.h>
#include <gtkmozedit/dom/gme-dom-html-form-element.h>


G_BEGIN_DECLS

#define GME_TYPE_DOM_HTML_INPUT_ELEMENT                  (gme_dom_html_input_element_get_gtype ())
#define GME_DOM_HTML_INPUT_ELEMENT(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), GME_TYPE_DOM_HTML_INPUT_ELEMENT, GmeDOMHTMLInputElement))
#define GME_DOM_HTML_INPUT_ELEMENT_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST ((klass), GME_TYPE_DOM_HTML_INPUT_ELEMENT, GmeDOMHTMLInputElementClass))
#define GME_IS_DOM_HTML_INPUT_ELEMENT(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GME_TYPE_DOM_HTML_INPUT_ELEMENT))
#define GME_IS_DOM_HTML_INPUT_ELEMENT_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), GME_TYPE_DOM_HTML_INPUT_ELEMENT))
#define GME_DOM_HTML_INPUT_ELEMENT_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS ((obj), GME_TYPE_DOM_HTML_INPUT_ELEMENT, GmeDOMHTMLInputElementClass))

typedef struct _GmeDOMHTMLInputElementClass GmeDOMHTMLInputElementClass;

GType gme_dom_html_input_element_get_gtype (void);

gchar* gme_dom_html_input_element_get_default_value (GmeDOMHTMLInputElement *self);
gboolean gme_dom_html_input_element_set_default_value (GmeDOMHTMLInputElement *self, const gchar *param);
gboolean gme_dom_html_input_element_get_default_checked (GmeDOMHTMLInputElement *self);
gboolean gme_dom_html_input_element_set_default_checked (GmeDOMHTMLInputElement *self, gboolean param);
GmeDOMHTMLFormElement* gme_dom_html_input_element_get_form (GmeDOMHTMLInputElement *self);
gchar* gme_dom_html_input_element_get_accept (GmeDOMHTMLInputElement *self);
gboolean gme_dom_html_input_element_set_accept (GmeDOMHTMLInputElement *self, const gchar *param);
gchar* gme_dom_html_input_element_get_access_key (GmeDOMHTMLInputElement *self);
gboolean gme_dom_html_input_element_set_access_key (GmeDOMHTMLInputElement *self, const gchar *param);
gchar* gme_dom_html_input_element_get_align (GmeDOMHTMLInputElement *self);
gboolean gme_dom_html_input_element_set_align (GmeDOMHTMLInputElement *self, const gchar *param);
gchar* gme_dom_html_input_element_get_alt (GmeDOMHTMLInputElement *self);
gboolean gme_dom_html_input_element_set_alt (GmeDOMHTMLInputElement *self, const gchar *param);
gboolean gme_dom_html_input_element_get_checked (GmeDOMHTMLInputElement *self);
gboolean gme_dom_html_input_element_set_checked (GmeDOMHTMLInputElement *self, gboolean param);
gboolean gme_dom_html_input_element_get_disabled (GmeDOMHTMLInputElement *self);
gboolean gme_dom_html_input_element_set_disabled (GmeDOMHTMLInputElement *self, gboolean param);
gint64 gme_dom_html_input_element_get_max_length (GmeDOMHTMLInputElement *self);
gboolean gme_dom_html_input_element_set_max_length (GmeDOMHTMLInputElement *self, gint32 param);
gchar* gme_dom_html_input_element_get_name (GmeDOMHTMLInputElement *self);
gboolean gme_dom_html_input_element_set_name (GmeDOMHTMLInputElement *self, const gchar *param);
gboolean gme_dom_html_input_element_get_read_only (GmeDOMHTMLInputElement *self);
gboolean gme_dom_html_input_element_set_read_only (GmeDOMHTMLInputElement *self, gboolean param);
gint64 gme_dom_html_input_element_get_size (GmeDOMHTMLInputElement *self);
gboolean gme_dom_html_input_element_set_size (GmeDOMHTMLInputElement *self, gint32 param);
gchar* gme_dom_html_input_element_get_src (GmeDOMHTMLInputElement *self);
gboolean gme_dom_html_input_element_set_src (GmeDOMHTMLInputElement *self, const gchar *param);
gint64 gme_dom_html_input_element_get_tab_index (GmeDOMHTMLInputElement *self);
gboolean gme_dom_html_input_element_set_tab_index (GmeDOMHTMLInputElement *self, gint32 param);
gchar* gme_dom_html_input_element_get_type (GmeDOMHTMLInputElement *self);
gboolean gme_dom_html_input_element_set_type (GmeDOMHTMLInputElement *self, const gchar *param);
gchar* gme_dom_html_input_element_get_use_map (GmeDOMHTMLInputElement *self);
gboolean gme_dom_html_input_element_set_use_map (GmeDOMHTMLInputElement *self, const gchar *param);
gchar* gme_dom_html_input_element_get_value (GmeDOMHTMLInputElement *self);
gboolean gme_dom_html_input_element_set_value (GmeDOMHTMLInputElement *self, const gchar *param);
gboolean gme_dom_html_input_element_blur (GmeDOMHTMLInputElement *self);
gboolean gme_dom_html_input_element_focus (GmeDOMHTMLInputElement *self);
gboolean gme_dom_html_input_element_select (GmeDOMHTMLInputElement *self);
gboolean gme_dom_html_input_element_click (GmeDOMHTMLInputElement *self);


G_END_DECLS

#endif /* __GME_DOM_HTML_INPUT_ELEMENT_H__ */
