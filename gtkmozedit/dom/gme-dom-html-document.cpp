/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <gtkmozedit/gtkmozedit-config.h>

#include "nsIDOMHTMLDocument.h"

#include "gme-dom-html-document-private.h"
#include "gme-dom-html-collection-private.h"
#include "gme-dom-html-element-private.h"
#include "gme-dom-node-list-private.h"

enum {
	PROP_0,
	PROP_WRAPPED_PTR,
	_NUM_PROPS
};

static GmeDOMDocumentClass *gme_dom_html_document_parent_class = NULL;

static void
instance_init (GmeDOMHTMLDocument *self)
{
	self->doc = NULL;
	self->is_disposed = FALSE;
}

static void
instance_dispose (GObject *instance)
{
	GmeDOMHTMLDocument *self = GME_DOM_HTML_DOCUMENT (instance);

	if (self->is_disposed)
		return;

	if (self->doc) NS_RELEASE (self->doc);
	self->doc = NULL;
	self->is_disposed = TRUE;

	gme_dom_html_document_parent_class->dispose (G_OBJECT (self));
}

static void
set_property (GObject      *object,
	      guint         prop_id,
	      GValue const *value,
	      GParamSpec   *pspec)
{
	GmeDOMHTMLDocument *self = GME_DOM_HTML_DOCUMENT (object);
	nsISupports *ptr = NULL;
	nsresult rv;

	switch (prop_id) {
	case PROP_WRAPPED_PTR:
		ptr = reinterpret_cast<nsISupports*>(g_value_get_pointer (value));
		rv = CallQueryInterface (ptr, &self->doc);
		if (NS_SUCCEEDED (rv) && self->doc) {
			NS_ADDREF (self->doc);
			/* constuction param, init parent */
			gme_dom_document_private_set_wrapped_ptr (GME_DOM_DOCUMENT (self), self->doc);
		}
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
get_property (GObject    *object,
	      guint       prop_id,
	      GValue     *value,
	      GParamSpec *pspec)
{
	GmeDOMHTMLDocument *self = GME_DOM_HTML_DOCUMENT (object);

	switch (prop_id) {
	case PROP_WRAPPED_PTR:
		g_value_set_pointer (value, (gpointer) self->doc);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
class_init (GmeDOMHTMLDocumentClass *klass)
{
	GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
	GmeDOMDocumentClass *node_class = GME_DOM_DOCUMENT_CLASS (klass);

	/* vfuncs */
	klass->dispose = instance_dispose;

	/* hook gobject vfuncs */
	gobject_class->dispose = instance_dispose;

	gme_dom_html_document_parent_class = (GmeDOMDocumentClass*) g_type_class_peek_parent (klass);

	gobject_class->set_property = set_property;
	gobject_class->get_property = get_property;

	g_object_class_install_property (gobject_class,
		PROP_WRAPPED_PTR,
		g_param_spec_pointer ("wrapped-ptr", _("Wrapped Pointer"),
			_("Pointer to the wrapped c++ object"),
			(GParamFlags)(G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY)));
}

GType
gme_dom_html_document_get_gtype (void)
{
        static GType type = 0;
        if (!type) {
                static const GTypeInfo info = {
                        sizeof (GmeDOMHTMLDocumentClass),
                        NULL,           /* base_init */
                        NULL,           /* base_finalize */
                        (GClassInitFunc) class_init,
                        NULL,           /* class_finalize */
                        NULL,           /* class_data */
                        sizeof (GmeDOMHTMLDocument),
                        0,              /* n_preallocs */
                        (GInstanceInitFunc) instance_init,
                };
                type = g_type_register_static (GME_TYPE_DOM_DOCUMENT, "GmeDOMHTMLDocument", &info, (GTypeFlags)0);
        }
        return type;
}

void 
gme_dom_html_document_private_set_wrapped_ptr (GmeDOMHTMLDocument *self, 
				      nsIDOMHTMLDocument *doc)
{
	g_assert (self && doc);
	self->doc = doc;
	NS_ADDREF (self->doc);
	/* constuction param, init parent */
	gme_dom_document_private_set_wrapped_ptr (GME_DOM_DOCUMENT (self), self->doc);
}

GmeDOMHTMLDocument* 
gme_dom_html_document_new (nsIDOMHTMLDocument *doc)
{
	return GME_DOM_HTML_DOCUMENT (g_object_new (GME_TYPE_DOM_HTML_DOCUMENT, "wrapped-ptr", doc, NULL));
}

gchar* 
gme_dom_html_document_get_title (GmeDOMHTMLDocument *self)
{
	nsEmbedString title;
	nsresult rv;
	g_assert (self);

	rv = self->doc->GetTitle (title);
	if (NS_SUCCEEDED (rv)) {
		return g_strdup (NS_ConvertUTF16toUTF8 (title).get ());
	}
	return NULL;
}

gboolean 
gme_dom_html_document_set_title (GmeDOMHTMLDocument *self, const gchar *title)
{
	nsEmbedString t;
	nsresult rv;
	g_assert (self);

	t = NS_ConvertUTF8toUTF16 (title);
	rv = self->doc->SetTitle (t);
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_dom_html_document_get_referrer (GmeDOMHTMLDocument *self)
{
	nsEmbedString referrer;
	nsresult rv;
	g_assert (self);

	rv = self->doc->GetReferrer (referrer);
	if (NS_SUCCEEDED (rv)) {
		return g_strdup (NS_ConvertUTF16toUTF8 (referrer).get ());
	}
	return NULL;
}

gchar* 
gme_dom_html_document_get_domain (GmeDOMHTMLDocument *self)
{
	nsEmbedString domain;
	nsresult rv;
	g_assert (self);

	rv = self->doc->GetDomain (domain);
	if (NS_SUCCEEDED (rv)) {
		return g_strdup (NS_ConvertUTF16toUTF8 (domain).get ());
	}
	return NULL;
}

gchar* 
gme_dom_html_document_get_url (GmeDOMHTMLDocument *self)
{
	nsEmbedString url;
	nsresult rv;
	g_assert (self);

	rv = self->doc->GetURL (url);
	if (NS_SUCCEEDED (rv)) {
		return g_strdup (NS_ConvertUTF16toUTF8 (url).get ());
	}
	return NULL;
}

GmeDOMHTMLElement* 
gme_dom_html_document_get_body (GmeDOMHTMLDocument *self)
{
	nsIDOMHTMLElement *e = NULL;
	GmeDOMHTMLElement *element = NULL;
	nsresult rv;
	g_assert (self);

	rv = self->doc->GetBody (&(e));
	if (NS_SUCCEEDED (rv)) {
		element = gme_dom_html_element_new (e);
	}
	return element;
}

gboolean 
gme_dom_html_document_set_body (GmeDOMHTMLDocument *self, GmeDOMHTMLElement *body)
{
	nsresult rv;
	g_assert (self && body);

	rv = self->doc->SetBody (body->element);
	return NS_SUCCEEDED (rv);
}

GmeDOMHTMLCollection* 
gme_dom_html_document_get_images (GmeDOMHTMLDocument *self)
{
	nsIDOMHTMLCollection *c = NULL;
	GmeDOMHTMLCollection *collection = NULL;
	nsresult rv;
	g_assert (self);

	rv = self->doc->GetImages (&(c));
	if (NS_SUCCEEDED (rv)) {
		collection = gme_dom_html_collection_new (c);
	}
	return collection;
}

GmeDOMHTMLCollection* 
gme_dom_html_document_get_applets (GmeDOMHTMLDocument *self)
{
	nsIDOMHTMLCollection *c = NULL;
	GmeDOMHTMLCollection *collection = NULL;
	nsresult rv;
	g_assert (self);

	rv = self->doc->GetApplets (&(c));
	if (NS_SUCCEEDED (rv)) {
		collection = gme_dom_html_collection_new (c);
	}
	return collection;
}

GmeDOMHTMLCollection* 
gme_dom_html_document_get_links (GmeDOMHTMLDocument *self)
{
	nsIDOMHTMLCollection *c = NULL;
	GmeDOMHTMLCollection *collection = NULL;
	nsresult rv;
	g_assert (self);

	rv = self->doc->GetLinks (&(c));
	if (NS_SUCCEEDED (rv)) {
		collection = gme_dom_html_collection_new (c);
	}
	return collection;
}

GmeDOMHTMLCollection* 
gme_dom_html_document_get_forms (GmeDOMHTMLDocument *self)
{
	nsIDOMHTMLCollection *c = NULL;
	GmeDOMHTMLCollection *collection = NULL;
	nsresult rv;
	g_assert (self);

	rv = self->doc->GetForms (&(c));
	if (NS_SUCCEEDED (rv)) {
		collection = gme_dom_html_collection_new (c);
	}
	return collection;
}

GmeDOMHTMLCollection* 
gme_dom_html_document_get_anchors (GmeDOMHTMLDocument *self)
{
	nsIDOMHTMLCollection *c = NULL;
	GmeDOMHTMLCollection *collection = NULL;
	nsresult rv;
	g_assert (self);

	rv = self->doc->GetAnchors (&(c));
	if (NS_SUCCEEDED (rv)) {
		collection = gme_dom_html_collection_new (c);
	}
	return collection;
}

gchar* 
gme_dom_html_document_get_cookie (GmeDOMHTMLDocument *self)
{
	nsEmbedString cookie;
	nsresult rv;
	g_assert (self);

	rv = self->doc->GetCookie (cookie);
	if (NS_SUCCEEDED (rv)) {
		return g_strdup (NS_ConvertUTF16toUTF8 (cookie).get ());
	}
	return NULL;
}

gboolean 
gme_dom_html_document_set_cookie (GmeDOMHTMLDocument *self, const gchar *cookie)
{
	nsEmbedString c;
	nsresult rv;
	g_assert (self);

	c = NS_ConvertUTF8toUTF16 (cookie);
	rv = self->doc->SetCookie (c);
	return NS_SUCCEEDED (rv);
}

GmeDOMNodeList* 
gme_dom_html_document_get_elements_by_name (GmeDOMHTMLDocument *self, const gchar *name)
{
	nsIDOMNodeList *l = NULL;
	GmeDOMNodeList *list = NULL;
	nsEmbedString n;
	nsresult rv;
	g_assert (self);

	n = NS_ConvertUTF8toUTF16 (name);
	rv = self->doc->GetElementsByName (n, &(l));
	if (NS_SUCCEEDED (rv)) {
		list = gme_dom_node_list_new (l);
	}
	return list;
}
