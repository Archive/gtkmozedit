/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __GME_COMMAND_PARAMS_H__
#define __GME_COMMAND_PARAMS_H__

#include <glib.h>
#include <glib-object.h>
#include <glib/gmacros.h>
#include <gtkmozedit/gme-forward.h>
#include <gtkmozedit/gme-supports.h>

G_BEGIN_DECLS

#define GME_TYPE_COMMAND_PARAMS                  (gme_command_params_get_gtype ())
#define GME_COMMAND_PARAMS(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), GME_TYPE_COMMAND_PARAMS, GmeCommandParams))
#define GME_COMMAND_PARAMS_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST ((klass), GME_TYPE_COMMAND_PARAMS, GmeCommandParamsClass))
#define GME_IS_COMMAND_PARAMS(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GME_TYPE_COMMAND_PARAMS))
#define GME_IS_COMMAND_PARAMS_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), GME_TYPE_COMMAND_PARAMS))
#define GME_COMMAND_PARAMS_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS ((obj), GME_TYPE_COMMAND_PARAMS, GmeCommandParamsClass))

typedef struct _GmeCommandParamsClass GmeCommandParamsClass;

GType gme_command_params_get_gtype (void);

#define GME_COMMAND_PARAMS_NO_TYPE 0
#define GME_COMMAND_PARAMS_BOOLEAN_TYPE 1
#define GME_COMMAND_PARAMS_LONG_TYPE 2
#define GME_COMMAND_PARAMS_DOUBLE_TYPE 3
#define GME_COMMAND_PARAMS_WSTRING_TYPE 4
#define GME_COMMAND_PARAMS_ISUPPORTS_TYPE 5
#define GME_COMMAND_PARAMS_STRING_TYPE 6

GmeCommandParams* gme_command_params_new (void);

gulong gme_command_params_get_value_type (GmeCommandParams *self, const gchar *name);
gboolean gme_command_params_get_boolean_value (GmeCommandParams *self, const gchar *name);
gint32 gme_command_params_get_long_value (GmeCommandParams *self, const gchar *name);
gdouble gme_command_params_get_double_value (GmeCommandParams *self, const gchar *name);
gchar* gme_command_params_get_string_value (GmeCommandParams *self, const gchar *name);
GmeSupports* gme_command_params_get_gme_supports_value (GmeCommandParams *self, const gchar *name);

gboolean gme_command_params_set_boolean_value (GmeCommandParams *self, const gchar *name, gboolean value);
gboolean gme_command_params_set_long_value (GmeCommandParams *self, const gchar *name, gint32 value);
gboolean gme_command_params_set_double_value (GmeCommandParams *self, const gchar *name, gdouble value);
gboolean gme_command_params_set_string_value (GmeCommandParams *self, const gchar *name, const gchar *value);
gboolean gme_command_params_set_gme_supports_value (GmeCommandParams *self, const gchar *name, GmeSupports *value);

gboolean gme_command_params_remove_value (GmeCommandParams *self, const gchar *name);
gboolean gme_command_params_has_more_elements (GmeCommandParams *self);
gboolean gme_command_params_first (GmeCommandParams *self);
gchar* gme_command_params_get_next (GmeCommandParams *self);

G_END_DECLS

#endif /* __GME_COMMAND_PARAMS_H__ */
