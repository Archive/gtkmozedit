/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __GME_OBSERVER_H__
#define __GME_OBSERVER_H__

#include <glib.h>
#include <glib/gmacros.h>
#include <gtkmozedit/gme-forward.h>

G_BEGIN_DECLS

#define GME_TYPE_OBSERVER                  (gme_observer_get_gtype ())
#define GME_OBSERVER(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), GME_TYPE_OBSERVER, GmeObserver))
#define GME_OBSERVER_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST ((klass), GME_TYPE_OBSERVER, GmeObserverClass))
#define GME_IS_OBSERVER(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GME_TYPE_OBSERVER))
#define GME_IS_OBSERVER_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), GME_TYPE_OBSERVER))
#define GME_OBSERVER_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS ((obj), GME_TYPE_OBSERVER, GmeObserverClass))

typedef struct _GmeObserverClass GmeObserverClass;

GType gme_observer_get_gtype (void);

G_END_DECLS

#endif /* __GME_OBSERVER_H__ */
