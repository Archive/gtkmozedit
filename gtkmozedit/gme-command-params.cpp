/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <gtkmozedit/gtkmozedit-config.h>

#include "nsCOMPtr.h"
#include "nsICommandParams.h"
#include "nsComponentManagerUtils.h"

#include "gme-command-params-private.h"
#include "gme-supports-private.h"

enum {
	PROP_0,
	PROP_WRAPPED_PTR,
	_NUM_PROPS
};

static GmeSupportsClass *gme_command_params_parent_class = NULL;

static void
instance_init (GmeCommandParams *self)
{
	nsCOMPtr<nsICommandParams> params;
	nsresult rv;

	params = do_CreateInstance (NS_COMMAND_PARAMS_CONTRACTID, &rv);
	g_assert (NS_SUCCEEDED (rv));
	self->wrapped_ptr = params;
	NS_ADDREF (self->wrapped_ptr);
	self->is_disposed = FALSE;
}

static void
instance_dispose (GObject *instance)
{
	GmeCommandParams *self = GME_COMMAND_PARAMS (instance);

	if (self->is_disposed)
		return;

	if (self->wrapped_ptr) NS_RELEASE (self->wrapped_ptr);
	self->wrapped_ptr = NULL;
	self->is_disposed = TRUE;

	gme_command_params_parent_class->dispose (G_OBJECT (self));
}

static void
set_property (GObject      *object,
	      guint         prop_id,
	      GValue const *value,
	      GParamSpec   *pspec)
{
	GmeCommandParams *self = GME_COMMAND_PARAMS (object);
	nsISupports *ptr = NULL;
	nsresult rv;

	/* this will not happen unless we install a writeable property */

	switch (prop_id) {
	case PROP_WRAPPED_PTR:
		if (self->wrapped_ptr) {
			NS_RELEASE (self->wrapped_ptr);
			self->wrapped_ptr = NULL;
		}

		ptr = reinterpret_cast<nsISupports*>(g_value_get_pointer (value));
		rv = CallQueryInterface (ptr, &self->wrapped_ptr);
		if (NS_SUCCEEDED (rv) && self->wrapped_ptr) {
			NS_ADDREF (self->wrapped_ptr);
			/* constuction param, init parent */
			gme_supports_private_set_wrapped_ptr (GME_SUPPORTS (self), self->wrapped_ptr);
		}
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
get_property (GObject    *object,
	      guint       prop_id,
	      GValue     *value,
	      GParamSpec *pspec)
{
	GmeCommandParams *self = GME_COMMAND_PARAMS (object);

	switch (prop_id) {
	case PROP_WRAPPED_PTR:
		g_value_set_pointer (value, (gpointer) self->wrapped_ptr);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
class_init (GmeCommandParamsClass *klass)
{
	GObjectClass *gobject_class = G_OBJECT_CLASS (klass);

	/* vfuncs */
	klass->dispose = instance_dispose;

	/* hook gobject vfuncs */
	gobject_class->dispose = instance_dispose;

	gme_command_params_parent_class = (GmeSupportsClass*) g_type_class_peek_parent (klass);

	gobject_class->set_property = set_property;
	gobject_class->get_property = get_property;

	g_object_class_install_property (gobject_class,
		PROP_WRAPPED_PTR,
		g_param_spec_pointer ("wrapped-ptr", _("Wrapped Pointer"),
			_("Pointer to the wrapped c++ object"),
			(GParamFlags)(G_PARAM_READABLE)));
}

GType
gme_command_params_get_gtype (void)
{
        static GType type = 0;
        if (!type) {
                static const GTypeInfo info = {
                        sizeof (GmeCommandParamsClass),
                        NULL,           /* base_init */
                        NULL,           /* base_finalize */
                        (GClassInitFunc) class_init,
                        NULL,           /* class_finalize */
                        NULL,           /* class_data */
                        sizeof (GmeCommandParams),
                        0,              /* n_preallocs */
                        (GInstanceInitFunc) instance_init,
                };
                type = g_type_register_static (GME_TYPE_SUPPORTS, "GmeCommandParams", &info, (GTypeFlags)0);
        }
        return type;
}

void 
gme_command_params_private_set_wrapped_ptr (GmeCommandParams *self, 
				      nsICommandParams *wrapped_ptr)
{
	g_assert (self && wrapped_ptr);
	self->wrapped_ptr = wrapped_ptr;
	NS_ADDREF (self->wrapped_ptr);
	/* constuction param, init parent */
	gme_supports_private_set_wrapped_ptr (GME_SUPPORTS (self), self->wrapped_ptr);
}

GmeCommandParams* 
gme_command_params_new (void)
{
	return GME_COMMAND_PARAMS (g_object_new (GME_TYPE_COMMAND_PARAMS, NULL));
}

gulong 
gme_command_params_get_value_type (GmeCommandParams *self, const gchar *name)
{
	gint16 value_type;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetValueType (name, &value_type);
	if (NS_SUCCEEDED (rv)) {
		return value_type;
	}
	return GME_COMMAND_PARAMS_NO_TYPE;
}

gboolean 
gme_command_params_get_boolean_value (GmeCommandParams *self, const gchar *name)
{
	gboolean value;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetBooleanValue (name, &value);
	if (NS_SUCCEEDED (rv)) {
		return value;
	}
	return FALSE;
}

gint32 
gme_command_params_get_long_value (GmeCommandParams *self, const gchar *name)
{
	gint32 value;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetLongValue (name, &value);
	if (NS_SUCCEEDED (rv)) {
		return value;
	}
	return 0;
}

gdouble 
gme_command_params_get_double_value (GmeCommandParams *self, const gchar *name)
{
	gdouble value;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetDoubleValue (name, &value);
	if (NS_SUCCEEDED (rv)) {
		return value;
	}
	return 0;
}

gchar* 
gme_command_params_get_string_value (GmeCommandParams *self, const gchar *name)
{
	nsEmbedString value;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetStringValue (name, value);
	if (NS_SUCCEEDED (rv)) {
		return g_strdup (NS_ConvertUTF16toUTF8 (value).get ());
	}
	return NULL;
}

GmeSupports* 
gme_command_params_get_gme_supports_value (GmeCommandParams *self, const gchar *name)
{
	nsISupports *v = NULL;
	GmeSupports *value = NULL;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetISupportsValue (name, &v);
	if (NS_SUCCEEDED (rv)) {
		return GME_SUPPORTS (g_object_new (GME_TYPE_SUPPORTS, "wrapped-ptr", v, NULL));
	}
	return NULL;
}

gboolean 
gme_command_params_set_boolean_value (GmeCommandParams *self, const gchar *name, gboolean value)
{
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->SetBooleanValue (name, value);
	return NS_SUCCEEDED (rv);
}

gboolean 
gme_command_params_set_long_value (GmeCommandParams *self, const gchar *name, gint32 value)
{
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->SetLongValue (name, value);
	return NS_SUCCEEDED (rv);
}

gboolean 
gme_command_params_set_double_value (GmeCommandParams *self, const gchar *name, gdouble value)
{
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->SetDoubleValue (name, value);
	return NS_SUCCEEDED (rv);
}

gboolean 
gme_command_params_set_string_value (GmeCommandParams *self, const gchar *name, const gchar *value)
{
	/* nsEmbedString v; */
	nsresult rv;
	g_assert (self);

	/* 
	v.AssignWithConversion (value); 
	rv = self->wrapped_ptr->SetStringValue (name, v);
	*/
	rv = self->wrapped_ptr->SetCStringValue (name, value);
	return NS_SUCCEEDED (rv);
}

/*
gboolean 
gme_command_params_set_cstring_value (GmeCommandParams *self, const gchar *name, const gchar *value)
{
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->SetCStringValue (name, value);
	return NS_SUCCEEDED (rv);
}
*/

gboolean 
gme_command_params_set_gme_supports_value (GmeCommandParams *self, const gchar *name, GmeSupports *value)
{
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->SetISupportsValue (name, value->wrapped_ptr);
	return NS_SUCCEEDED (rv);
}

gboolean 
gme_command_params_remove_value (GmeCommandParams *self, const gchar *name)
{
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->RemoveValue (name);
	return NS_SUCCEEDED (rv);
}

gboolean 
gme_command_params_has_more_elements (GmeCommandParams *self)
{
	gboolean has_more;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->HasMoreElements (&has_more);
	return NS_SUCCEEDED (rv);
}

gboolean 
gme_command_params_first (GmeCommandParams *self)
{
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->First ();
	return NS_SUCCEEDED (rv);
}

gchar* 
gme_command_params_get_next (GmeCommandParams *self)
{
	gchar *next = NULL;
	nsresult rv;
	g_assert (self);

	rv = self->wrapped_ptr->GetNext (&next);
	if (NS_SUCCEEDED (rv)) {
		return next;
	}
	return NULL;
}
