/*
 *  Copyright (C) 2005 Robert Staudinger
 *
 *  This software is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __GME_FORWARD_H__
#define __GME_FORWARD_H__

#define _(S) S

/* forward decls for circular referencing types */
typedef struct _GmeCommandManager GmeCommandManager;
typedef struct _GmeCommandParams GmeCommandParams;
typedef struct _GmeEditor GmeEditor;
typedef struct _GmeEditingSession GmeEditingSession;
typedef struct _GmeHTMLEditor GmeHTMLEditor;
typedef struct _GmeObserver GmeObserver;
typedef struct _GmeSupports GmeSupports;
typedef struct _GmeWebBrowser GmeWebBrowser;
typedef struct _GmeHTMLView GmeHTMLView;

#endif /* __GME_FORWARD_H__ */
